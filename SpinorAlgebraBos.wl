(* ::Package:: *)

(* ::Title:: *)
(*Spinor Algebra Bosonic*)


(* ::Section::Closed:: *)
(*Beginning of package and function descriptions*)


(* ::Subsection::Closed:: *)
(*Usage messages*)


BeginPackage["SpinorAlgebra`"];

(*Usage of the functions*)
u::usage="u[\!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Operator to apply on functions to make them act on and return in compact notation: \!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)[\[LeftAngleBracket]user notation\[RightAngleBracket]] -> u[\!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\[LeftAngleBracket]compact notation\[RightAngleBracket]].";
uu::usage="uu[\!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Operator to apply on functions to make them act on and return in compact notation. Similar to \!\(\*StyleBox[\"u\",\nFontFamily->\"DejaVu Sans Mono\"]\) but applies the transformation to the first two arguments.";
SplitMonomials::usage="SplitMonomials[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Transforms from compact notation to user notation: e.g. \[Eta]1x12\[Theta]b2 \[Rule] d[\[Eta]1, x12, \[Theta]b2].";
WriteMonomials::usage="WriteMonomials[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Transforms from user notation to compact notation: e.g. d[\[Eta]1, x12, \[Theta]b2] \[Rule] \[Eta]1x12\[Theta]b2";
Compare::usage="Compare[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\" \",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"[\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\",\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"options\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] equates \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) to zero and returns the constraints on the arbitrary coefficients contained in \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).\nThe options are:\nexpReplacement ({}): Random replacements for common terms in the exponents of e.g. x12sq^\[CapitalDelta].\nvariables (ConstantsList): Usual set of symbols treated as unknowns.\nverbose (values 0,1,2): Prints more information about the steps of the computation.\nminEqns (-1) the number of equation generated is the max of minEqns and the number of variables.";
ConstQ::usage="ConstQ[\!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Returns True if \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) is considered a constant and False otherwise.";
ComplexConj::usage="ComplexConj[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the complex conjugate of the expression \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\). Constants are left unchanged (i.e. considered as Real).";
SetToZero::usage="SetToZero[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"vars\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Sets \!\(\*StyleBox[\"vars\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\" \",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)to zero in the expression \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\). If \!\(\*StyleBox[\"vars\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\" \",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)is a list sets all the variables in the list to zero.";
Reduction::usage="Reduction[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Reduces the expression \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) by applying rules such as \!\(\*SuperscriptBox[\(\[Theta]\), \(\[Alpha]\)]\)...\!\(\*SuperscriptBox[\(\[Theta]\), \(\[Beta]\)]\) = -1/2\!\(\*SuperscriptBox[\(\[CurlyEpsilon]\), \(\[Alpha]\[Beta]\)]\)\!\(\*SuperscriptBox[\(\[Theta]\), \(2\)]\)... . Puts to zero terms with more than two identical \[Theta]s.";
FPower::usage="FPower[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"p\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] for positive integer \!\(\*StyleBox[\"p\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) it computes the power \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"^\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"p\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) by reducing terms with more than one \[Theta] using the function Reduction.\nFPower[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"around\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"p\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"n\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] expands the power \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"^\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"p\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) around \!\(\*StyleBox[\"around\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) up to order \!\(\*StyleBox[\"n\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) reducing terms with more than one \[Theta] using the function Reduction.";
FTaylor::usage="FTaylor[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"n\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] Taylor expands \!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] around 0 up to order \!\(\*StyleBox[\"n\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
Nice::usage="Nice[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Displays an expression in a nicer form. Only for reading.";
UnNice::usage="UnNice[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)], Reverts the Nice function.";
d::usage="d[\!\(\*StyleBox[\"a\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"...\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Represents the fully contracted monomial \!\(\*StyleBox[\"ab...\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), where, for example,  \!\(\*SubscriptBox[\(\[Eta]\), \(1\)]\)\!\(\*SubscriptBox[\(\[Eta]\), \(2\)]\) = \!\(\*SubsuperscriptBox[\(\[Eta]\), \(1\), \(\[Alpha]\)]\)\!\(\*SubscriptBox[\(\[Eta]\), \(2  \[Alpha]\)]\),  \!\(\*SubscriptBox[\(\[Eta]\), \(1\)]\)\!\(\*SubscriptBox[\(x\), \(12\)]\)\!\(\*SubscriptBox[OverscriptBox[\(\[Eta]\), \(_\)], \(2\)]\) = \!\(\*SubsuperscriptBox[\(\[Eta]\), \(1\), \(\[Alpha]\)]\)\!\(\*SubscriptBox[\(x\), \(12  \[Alpha] \*OverscriptBox[\(\[Alpha]\), \(.\)]\)]\)\!\(\*SuperscriptBox[OverscriptBox[\(\[Eta]\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\), etc ...";
glue::usage="glue[\!\(\*StyleBox[\"a\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"...\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Returns a Symbol obtained concatenating \!\(\*StyleBox[\"ab\[CenterDot]\[CenterDot]\[CenterDot]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) .";
\[Eta]d\[Eta]::usage="\[Eta]d\[Eta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(\[Eta]\), \(1\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), SubscriptBox[\(\[Eta]\), \(2\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
\[Eta]bd\[Eta]b::usage="\[Eta]bd\[Eta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[OverscriptBox[\(\[Eta]\), \(_\)], \(1\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), SubscriptBox[OverscriptBox[\(\[Eta]\), \(_\)], \(2\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
xdx::usage="xdx[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(x\), \(1\)]\)\[CenterDot]\!\(\*SubscriptBox[\(\[PartialD]\), SubscriptBox[\(x\), \(2\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
d\[Eta]d\[Eta]::usage="d\[Eta]d\[Eta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), SubscriptBox[\(\[Eta]\), \(1\)]]\)\!\(\*SubscriptBox[\(\[PartialD]\), SubscriptBox[\(\[Eta]\), \(2\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
d\[Eta]bd\[Eta]b::usage="d\[Eta]bd\[Eta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), SubscriptBox[OverscriptBox[\(\[Eta]\), \(_\)], \(1\)]]\) \!\(\*SubscriptBox[\(\[PartialD]\), SubscriptBox[OverscriptBox[\(\[Eta]\), \(_\)], \(2\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
\[Eta]dx\[Eta]b::usage="\[Eta]dx\[Eta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \[Eta] \!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\) \!\(\*OverscriptBox[\(\[Eta]\), \(_\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
d\[Eta]dxd\[Eta]b::usage="d\[Eta]dxd\[Eta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta]\)]\) \!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\) \!\(\*SubscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Eta]\), \(_\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
d\[Eta]xd\[Eta]b::usage="d\[Eta]xd\[Eta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta]\)]\) x \!\(\*SubscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Eta]\), \(_\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
dxsq::usage="dxsq[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SuperscriptBox[SubscriptBox[\(\[PartialD]\), \(x\)], \(2\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
d\[Eta]dx\[Eta]b::usage="d\[Eta]dx\[Eta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta]\)]\) \!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\) \!\(\*OverscriptBox[\(\[Eta]\), \(_\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
\[Eta]dxd\[Eta]b::usage="\[Eta]dxd\[Eta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \[Eta] \!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\) \!\(\*SubscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Eta]\), \(_\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
SchoutenContract::usage="SchoutenContract[\!\(\*StyleBox[\"a1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"a2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"a3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"a4\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)], where \!\(\*StyleBox[\"a1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) is a monomial containing \[Eta]01, \!\(\*StyleBox[\"a2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) is a monomial containing \[Eta]02 and so on, returns a Schouten identity for the terms contained in \!\(\*StyleBox[\"a1,a2,a3,a4\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) (without the \[Eta]0i).\nFor example ShoutenContract[\[Eta]01\[Eta]1,\[Eta]02\[Eta]2,\[Eta]03\[Eta]3,\[Eta]04\[Eta]4] = \[Eta]1\[Eta]4 \[Eta]2\[Eta]3-\[Eta]1\[Eta]3 \[Eta]2\[Eta]4+\[Eta]1\[Eta]2 \[Eta]3\[Eta]4 = 0.";
SchoutenContractb::usage="SchoutenContractb[\!\(\*StyleBox[\"a1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"a2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"a3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"a4\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)], where \!\(\*StyleBox[\"a1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) is a monomial containing \[Eta]b01, \!\(\*StyleBox[\"a2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) is a monomial containing \[Eta]b02 and so on, returns a Schouten identity for the terms contained in \!\(\*StyleBox[\"a1,a2,a3,a4\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) (without the \[Eta]b0i).\nFor an example see ?ShoutenContract";
GenerateSchouten::usage="GenerateSchouten[{\!\(\*StyleBox[\"terms\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"termsb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}] generates a list of replacement rules implementing the Schouten identities obtained by contracting the terms in \!\(\*StyleBox[\"terms\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with the antisymmetrized undotted \[Epsilon] \[Epsilon] and the terms in \!\(\*StyleBox[\"termsb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with the antisymmetrized dotted \[Epsilon] \[Epsilon].\nThe terms in \!\(\*StyleBox[\"terms\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) must be objects with one undotted index contracted with the placeholder \[Eta]00 and those in \!\(\*StyleBox[\"termsb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) must be objects with one dotted index contracted with \[Eta]b00.";
Replace\[Eta]ToAny::usage="ReplaceToAny[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"any\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] replaces all \*StyleBox[\(\!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)s\)] in the expression \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with the quantity \!\(\*StyleBox[\"any\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), which must have a free index. The parameter \!\(\*StyleBox[\"any\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) is given as follows: it must be a List of pairs. Each pair is a term of the expression to be substituted and its entries consist in one-argument functions. The first entry implements the substitution for \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with the lower index and the second for \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with the upper index, the index being the argument passed to the function. Each function must return a List or a Product of terms in explicit index notation.";
Contraction::usage="Contraction[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] contracts all explicit indices of the expression \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), which must be given in internal notation.";
up::usage="up[\!\(\*StyleBox[\"obj\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"a\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] represents an object \!\(\*StyleBox[\"obj\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with one upper index \!\(\*StyleBox[\"a\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) in the internal notation.";
low::usage="low[\!\(\*StyleBox[\"obj\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"a\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] represents an object \!\(\*StyleBox[\"obj\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with one lower index \!\(\*StyleBox[\"a\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) in the internal notation.";
v::usage="v[\!\(\*StyleBox[\"obj\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"m\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] represents an object \!\(\*StyleBox[\"obj\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with one Lorentz index \!\(\*StyleBox[\"m\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) in the internal notation.";
sq::usage="sq[\!\(\*StyleBox[\"sym\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] returns a Symbol \!\(\*StyleBox[\"sym\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)sq representing the square of the given Symbol.";
fastApply::usage="fastApply[\!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] collects the various monomials in \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), applies \!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) on them and adds the result to the definition of \!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) (so that the next application within the same kernel session will be faster).";
fastReduction::usage="fastReduction[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] collects the various monomials in \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), applies Reduction on them and adds the result to the definition of Reduction (so that the next application within the same kernel session will be faster).";
Unprotect[NonCommutativeMultiply];
NonCommutativeMultiply::usage="\!\(\*StyleBox[\"a\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)**\!\(\*StyleBox[\"b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)**\!\(\*StyleBox[\"c\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) ...  is a non commutative product to be used for terms that have odd fermion number. This product automatically takes out bosonic factors.";
Protect[NonCommutativeMultiply];
FromCFTs4D::usage="FromCFTs4D[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] takes an expression \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) in embedding space from the package CFTs4D and translates it into this package's user notation.";
NonSUSY3pf::usage="NonSUSY3pf[{\!\(\*StyleBox[\"\[CapitalDelta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[CapitalDelta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[CapitalDelta]3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {{\!\(\*StyleBox[\"j1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"j2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"j3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}} \!\(\*StyleBox[\"[, points]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] returns a non-supersymmetric three-point function of the operators with conformal dimensions \!\(\*StyleBox[\"\[CapitalDelta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[CapitalDelta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) and \!\(\*StyleBox[\"\[CapitalDelta]3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) and spins (\!\(\*StyleBox[\"j1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)), (\!\(\*StyleBox[\"j2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)) and (\!\(\*StyleBox[\"j3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)).\nThis is obtained by translating to user notation the output of CFTs4D`n3CorrelationFunction.\nWith the option \!\(\*StyleBox[\"points\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) the user can specify which \!\(\*SubscriptBox[\(x\), \(ij\)]\) must be expressed in terms of the others. E.g. \!\(\*StyleBox[\"points \[Rule] \",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) x12 (default) expresses the result as a function of x23 and x13.";
NonSUSY2pf::usage="NonSUSY2pf[\!\(\*StyleBox[\"\[CapitalDelta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), {\!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}] returns the non-supersymmetric two-point function of an operator with conformal dimension \!\(\*StyleBox[\"\[CapitalDelta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) and spin (\!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)) in user notation.";
SUSY2pf::usage="NonSUSY2pf[{\!\(\*StyleBox[\"q\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}] returns the supersymmetric two-point function of a superprimary with conformal dimension \!\(\*StyleBox[\"q\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)+\!\(\*StyleBox[\"qb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), R-charge 2/3(\!\(\*StyleBox[\"q\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)-\!\(\*StyleBox[\"qb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)), and spin (\!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)) in user notation (precomputed).";
SUSY3pf::usage="SUSY3pf[{{\!\(\*StyleBox[\"q1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"q2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"q3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}}, {{\!\(\*StyleBox[\"j1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"j2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"j3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}} \!\(\*StyleBox[\"[, list]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] returns a supersymmetric three-point function of the superprimaries with charges (\!\(\*StyleBox[\"q1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)), (\!\(\*StyleBox[\"q2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)) and (\!\(\*StyleBox[\"q3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)) and spins (\!\(\*StyleBox[\"j1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)), (\!\(\*StyleBox[\"j2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)) and (\!\(\*StyleBox[\"j3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)).\nIn order to obtain a non-zero result the sum of the R-charges must be 0,\[PlusMinus]1 or \[PlusMinus]2.\nIf the option \"list\" is True the output is list of lists, each containing the structures for a given order in \[CapitalTheta]3, \[CapitalTheta]b3. If False (default) the output is an expressions with arbitrary coefficients \[ScriptCapitalC][n].";


(*Memoized version of some functions*)
fdxsq::usage="fdxsq[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] memoized version of dxsq";
fd\[Eta]d\[Eta]::usage="fd\[Eta]d\[Eta][\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]1\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]2\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of d\[Eta]d\[Eta]";
fd\[Eta]bd\[Eta]b::usage="fd\[Eta]bd\[Eta]b[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b1\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b2\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of d\[Eta]bd\[Eta]b";
(**)
fd\[Eta]dxd\[Eta]b::usage="fd\[Eta]dxd\[Eta]b[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of d\[Eta]dxd\[Eta]b";
f\[Eta]dxd\[Eta]b::usage="f\[Eta]dxd\[Eta]b[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of \[Eta]dxd\[Eta]b";
fd\[Eta]dx\[Eta]b::usage="fd\[Eta]dx\[Eta]b[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of d\[Eta]dx\[Eta]b";
f\[Eta]dx\[Eta]b::usage="f\[Eta]dx\[Eta]b[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of \[Eta]dx\[Eta]b";
fd\[Eta]xd\[Eta]b::usage="fd\[Eta]xd\[Eta]b[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of d\[Eta]xd\[Eta]b";


(* ::Subsection::Closed:: *)
(*Some preparatory definitions*)


(*List of symbols that should be treated as constants*)
ConstantsList={\[ScriptCapitalA],\[ScriptCapitalB],\[ScriptCapitalC],\[ScriptCapitalD],\[ScriptCapitalE],\[ScriptCapitalF],\[ScriptCapitalG],\[ScriptCapitalH],\[ScriptCapitalI],\[ScriptCapitalJ],\[ScriptCapitalK],\[ScriptCapitalL],\[ScriptCapitalM],\[ScriptCapitalN],\[ScriptCapitalO],\[ScriptCapitalP],\[ScriptCapitalQ],\[ScriptCapitalR],\[ScriptCapitalS],\[ScriptCapitalT],\[ScriptCapitalU],\[ScriptCapitalV],\[ScriptCapitalW],\[ScriptCapitalX],\[ScriptCapitalY],\[ScriptCapitalZ]};
Format[\[ScriptCapitalC][subscriptn_]]:=Subscript[\[ScriptCapitalC], subscriptn];
(*Protected symbols used in the internal notation*)
Protect[\[Sigma],\[Sigma]b,\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],\[Kappa],\[Lambda],\[Mu],\[Nu],\[Rho]];
(*I put these symbols here so that they are not generated with the context SpinorAlgebra`Private` in front*)


(*Can be specified so that it doesn't have to check which xs appear in an expression, if it's always the same ones.*)
workingxs = {};


(* ::Subsection::Closed:: *)
(*Begin `Private`*)


Begin["`Private`"];


(* ::Section::Closed:: *)
(*Initial definitions*)


(* ::Subsection::Closed:: *)
(*Basic definitions*)


toString[x_]:=ToString[x,InputForm];
toStringNoContext[x_]:=StringReplace[ToString[x,InputForm],"SpinorAlgebra`"->""];


(*Definition of the canonical ordering*)
chosenOrdering=OrderedQ[{#1,#2}]&;              (*you can also choose a customized ordering*)
FSort[a_List]:=Sort[a,chosenOrdering];


ConstQ[x_?NumericQ]:=True;
ConstQ[x_Plus]:=And@@(ConstQ[#]&/@List@@x);
ConstQ[x_Times]:=And@@(ConstQ[#]&/@List@@x);
ConstQ[x_^a_]:=ConstQ[x]&&ConstQ[a];
ToExpression[
    StringJoin@@Table[
        toString[con]<>"/: ConstQ["<>toString[con]<>"] := True; ",
    {con,ConstantsList}]];
ConstQ[CFTs4D`\[Lambda]M[x_]]:= True;
ConstQ[CFTs4D`\[Lambda]P[x_]]:= True;
ConstQ[CFTs4D`\[Lambda][x_]] := True;

(*Use ConstQ to know whether a symbol is considered a constant or not*)


(* ::Section::Closed:: *)
(*From user form to compact form*)


(* ::Subsection::Closed:: *)
(*Useful auxiliary functions*)


(*Recognizes the type of variables in the monomials*)
\[Eta]Q[x_]:=StringMatchQ[toString[x],"\[Eta]"~~(DigitCharacter..)];
\[Chi]Q[x_]:=StringMatchQ[toString[x],"\[Chi]"~~(DigitCharacter..)];
\[Eta]bQ[x_]:=StringMatchQ[toString[x],"\[Eta]b"~~(DigitCharacter..)];
\[Chi]bQ[x_]:=StringMatchQ[toString[x],"\[Chi]b"~~(DigitCharacter..)];
\[Eta]\[Eta]bQ[x_]:=\[Eta]Q[x]||\[Eta]bQ[x];
\[Chi]\[Chi]bQ[x_]:=\[Chi]Q[x]||\[Chi]bQ[x];
\[Eta]\[Theta]\[Chi]Q[x_]:=\[Eta]Q[x]||\[Chi]Q[x];
\[Eta]b\[Theta]b\[Chi]bQ[x_]:=\[Eta]bQ[x]||\[Chi]bQ[x];
xQ[x_]:=StringMatchQ[toString[x],"x"~~(DigitCharacter..)]&&(!StringMatchQ[toString[x],"x"~~(DigitCharacter..)~~"v"]);
xvQ[x_]:=StringMatchQ[toString[x],"x"~~(DigitCharacter..)~~"v"~~__];
sQ[x_]:=StringMatchQ[toString[x],"s"~~(DigitCharacter..)]&&(!StringMatchQ[toString[x],"s"~~(DigitCharacter..)~~"v"]);
svQ[x_]:=StringMatchQ[toString[x],"s"~~(DigitCharacter..)~~"v"~~__];
bQ[x_]:=StringMatchQ[toString[x],"\[Eta]b"|"\[Chi]b"~~(DigitCharacter..)];
ubQ[x_]:=(!StringMatchQ[toString[x],__~~"b"~~(DigitCharacter..)])&&StringMatchQ[toString[x],"\[Eta]"|"\[Chi]"~~(DigitCharacter..)];
sqQ[x_]:=StringMatchQ[toString[x],__~~"sq"];
xxQ[x_]:=StringMatchQ[toString[x],"x"~~(DigitCharacter..)~~"x"~~(DigitCharacter..)];
xsQ[x_]:=StringMatchQ[toString[x],("s"~~(DigitCharacter..)~~"x"~~(DigitCharacter..))|("x"~~(DigitCharacter..)~~"s"~~(DigitCharacter..))];
ssQ[x_]:=StringMatchQ[toString[x],"s"~~(DigitCharacter..)~~"s"~~(DigitCharacter..)];
xsqQ[x_]:=StringMatchQ[toString[x],"x"~~DigitCharacter..~~"sq"];


(*Some useful auxiliary functions*)
sq[x_]:=ToExpression[toString[x]<>"sq"];
glue[x___]:= ToExpression[StringTrim[StringJoin@@(ToString/@{x})," "]];
lorentzd[x_?xvQ,y_?xvQ]/;(x[[1]]===y[[1]]):=If[x===y,ToExpression[StringDrop[toString[Head[x]],-1]<>"sq"],
                                               ToExpression[StringDrop[toString[Head[x]],-1]<>StringDrop[toString[Head[y]],-1]]];
lorentzd[x_?xQ,y_?xQ]:=If[x===y,sq[x],glue[x,y]];
bar[x_?ubQ]:=ToExpression[StringReplace[toString[x],a_~~(b:DigitCharacter..):>a<>"b"<>b]];
unbar[x_?bQ]:=ToExpression[StringReplace[toString[x],a_~~"b"~~(b:DigitCharacter..):>a<>b]];


(*Sets some variables to zero*)
SetToZero[expr_,var_?\[Eta]\[Eta]bQ]:=expr/.var->0/.d[a___,0,b___]:>0;
SetToZero[expr_,var_?xQ]:=expr/.{var->0,sq[var]->0,
                          ToExpression[toString[var]<>"v"][\[Mu]_]-> 0,
                          x_?(StringMatchQ[toString[#],toString[var]~~"x"~~(DigitCharacter..)]&):> 0,
                          x_?(StringMatchQ[toString[#],"x"~~(DigitCharacter..)~~toString[var]]&):> 0}/.d[a___,0,b___]:>0;
SetToZero[expr_,var_List]:=Fold[SetToZero,expr,var];


(* ::Subsection::Closed:: *)
(*Conversion of the notations and operator d*)


(*Splits monomials into contractions made by the operator d. Henceforth referred to as a d-expression*)
SplitMonomials[m_Times]:=Times@@(SplitMonomials/@ List@@m);
SplitMonomials[s_Plus]:=Plus@@(SplitMonomials/@ List@@s);
SplitMonomials[x_?ConstQ]:=x;
SplitMonomials[Power[x_,p_]]:=Power[SplitMonomials[x],p];

SplitMonomials[s_Symbol]:=Module[{str=SymbolName[s],int},
    int=StringReplace[str,
        {"x"~~(y:DigitCharacter..)~~(z:"\[Mu]"|"\[Nu]"|"\[Rho]"|"\[Lambda]"|"\[Kappa]"):>  "x"<>y<>"v["<>z<>"]|",
        "x"~~(y:DigitCharacter..)~~"sq":>"x"<>y<>"sq|",
        "x"~~(y:DigitCharacter..):> "x"<>y<>"|",
        "\[Eta]"~~(y:DigitCharacter..):> "\[Eta]"<>y<>"|",
        "\[Eta]b"~~(y:DigitCharacter..):> "\[Eta]b"<>y<>"|",
        "\[Sigma]b"~~(z:"\[Mu]\[Nu]"|"\[Nu]\[Mu]"|"\[Mu]\[Rho]"|"\[Mu]\[Lambda]"|"\[Mu]\[Kappa]"|"\[Nu]\[Rho]"|"\[Nu]\[Lambda]"|"\[Nu]\[Kappa]"|"\[Rho]\[Lambda]"|"\[Rho]\[Kappa]"|"\[Kappa]\[Lambda]"):> "\[Sigma]b["<>StringInsert[z,",",2]<>"]|",
        "\[Sigma]"~~(z:"\[Mu]\[Nu]"|"\[Nu]\[Mu]"|"\[Mu]\[Rho]"|"\[Mu]\[Lambda]"|"\[Mu]\[Kappa]"|"\[Nu]\[Rho]"|"\[Nu]\[Lambda]"|"\[Nu]\[Kappa]"|"\[Rho]\[Lambda]"|"\[Rho]\[Kappa]"|"\[Kappa]\[Lambda]"):> "\[Sigma]["<>StringInsert[z,",",2]<>"]|",
        "\[Sigma]"~~(z:"\[Mu]"|"\[Nu]"|"\[Rho]"|"\[Lambda]"|"\[Kappa]"):> "\[Sigma]["<>z<>"]|",
        "\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda]":> "\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda]|",
        "\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu]":> "\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu]|"
    }];
    Return[(d@@ToExpression/@StringSplit[int,"|"])]
];


(*Writes monomials back to the user form*)
WriteMonomials[m_Times]:=Times@@(WriteMonomials/@ List@@m);
WriteMonomials[s_Plus]:=Plus@@(WriteMonomials/@ List@@s);
WriteMonomials[x_?ConstQ]:=x;
WriteMonomials[Power[x_,p_]]:=Power[WriteMonomials[x],p];
WriteMonomials[x_Symbol]:=x;
WriteMonomials[x_List]:=x;

(*WriteMonomials[x_d]:=Module[{rule},
    rule={\[Sigma][m_,n_] :> ToExpression["\[Sigma]"<>toString[m]<>toString[n]],
    \[Sigma]b[m_,n_] :> ToExpression["\[Sigma]b"<>toString[m]<>toString[n]],
    \[Sigma][m_]:>ToExpression["\[Sigma]"<>toString[m]],
    \[Sigma]b[m_]:>ToExpression["\[Sigma]b"<>toString[m]],
    a_?xvQ:>ToExpression[StringReplace[toString[a],"v["~~y_~~"]":>y]]};

    glue@@(x/.rule)];*)
(*The commented out part is the new notation: x1\[Mu]x2\[Nu]\[Eta]\[Sigma]\[Mu]\[Nu]\[Eta], this is the old one x1\[Mu]\[Nu]\[Eta]\[Sigma]\[Mu]\[Nu]\[Eta]x2\[Nu]. Moreover this one always outputs a \[Sigma]\[Mu]\[Nu] with indices \[Mu] and \[Nu]*)
WriteMonomials[x_d]:=Module[{rule,tempglue,ind},
    ind=x/.{d[a_[m_],b_[n_],\[Eta]1_,\[Sigma][m_,n_],\[Eta]2_]:>{Rule[m,\[Mu]],Rule[n,\[Nu]]},d[a_[m_],b_[n_],\[Eta]1_,\[Sigma]b[m_,n_],\[Eta]2_]:>{Rule[m,\[Mu]],Rule[n,\[Nu]]},_->{}};

    rule={\[Sigma][m_,n_] :> "\[Sigma]"<>toString[m/.ind]<>toString[n/.ind],
    \[Sigma]b[m_,n_] :> "\[Sigma]b"<>toString[m/.ind]<>toString[n/.ind],
    \[Sigma][m_]:>ToExpression["\[Sigma]"<>toString[m]],
    \[Sigma]b[m_]:>ToExpression["\[Sigma]b"<>toString[m]],
    a_?xvQ:>StringTake[toString[a],{1,StringPosition[toString[a],"v"][[1,1]]-1}]<>toStringNoContext[First@a/.ind]};

    (tempglue@@(x/.rule))/.{tempglue[a_,b_,c_,"\[Sigma]\[Mu]\[Nu]",d_]:>glue[a,c,"\[Sigma]\[Mu]\[Nu]",d,b],tempglue[a_,b_,c_,"\[Sigma]b\[Mu]\[Nu]",d_]:>glue[a,c,"\[Sigma]b\[Mu]\[Nu]",d,b]}/.tempglue->glue
    
];


(*Linearity over constants*)
d[a___,n_*b_,c___]/;ConstQ[n]:=n*d[a,b,c];
d[a___,b1_+b2_,c___]:=d[a,b1,c]+d[a,b2,c];

(*Enforces the ordering of the operator d*)
\[Sigma]Q[x_]:=StringMatchQ[toString[Head[x]],"\[Sigma]"];
\[Sigma]bQ[x_]:=StringMatchQ[toString[Head[x]],"\[Sigma]b"];
(*Sorts \[Eta]i\[Eta]j. Imposes \[Eta]\[Eta] \[Rule] 0*)
d[x_?\[Eta]\[Eta]bQ,x_?\[Eta]\[Eta]bQ]:=0;
d[x_?\[Eta]\[Eta]bQ,y_?\[Eta]\[Eta]bQ]/;(!chosenOrdering[x,y]):=-d[y,x];

(*Sorts monomials with three terms of the form \[Eta]x\[Eta]b*)
d[x_?\[Eta]bQ,y_?xQ,z_?\[Eta]Q]:=d[z,y,x];
d[x_?\[Eta]bQ,y_?\[Sigma]bQ,z_?\[Eta]Q]:=d[z,\[Sigma]@@y,x];

(*Sorts monomials with \[Sigma]\[Mu]\[Nu] or \[Sigma]b\[Mu]\[Nu] to (example) x12\[Mu]x23\[Nu]\[Eta]2\[Sigma]\[Mu]\[Nu]\[Theta]3. Puts to zero x12\[Mu]x12\[Nu]..\[Sigma]\[Mu]\[Nu]..*)
d[x___,y_?\[Eta]\[Eta]bQ|y_?xQ|y_?\[Sigma]Q|y_?\[Sigma]bQ,z_?xvQ,w___]/;FreeQ[{x,z,w},\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda]|\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu]]:=d[x,z,y,w];
d[x__?xvQ,y___]/;(DuplicateFreeQ[Head/@{x}]&&(!OrderedQ[Head/@{x}])):=d[Sequence@@Sort[{x}],y];
d[x_[\[Mu]_],y_[\[Nu]_],\[Eta]1_,\[Sigma][\[Nu]_,\[Mu]_],\[Eta]2_]:=-d[x[\[Mu]],y[\[Nu]],\[Eta]1,\[Sigma][\[Mu],\[Nu]],\[Eta]2];
d[x_[\[Mu]_],y_[\[Nu]_],\[Eta]b1_,\[Sigma]b[\[Nu]_,\[Mu]_],\[Eta]b2_]:=-d[x[\[Mu]],y[\[Nu]],\[Eta]b1,\[Sigma]b[\[Mu],\[Nu]],\[Eta]b2];
d[a_?xvQ,b_?xvQ,x__,y_?\[Sigma]Q|y_?\[Sigma]bQ,z__]/;(Head[a]===Head[b]):=0;
d[x__?xvQ,y_,s_?\[Sigma]Q|s_?\[Sigma]bQ,z_]/;(!chosenOrdering[y,z]):=d[x,z,s,y];

(*Puts \[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda] at the beginning. Puts to zero \[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda] contracted with equal xs. Orders canonically the xs contracted with \[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda].*)
d[x__,\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],y___]:=d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],x,y];
d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],y___]/;(!DuplicateFreeQ[Head/@Select[{y},xvQ[#]&]]):=0;
d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],x__?xvQ]/;(DuplicateFreeQ[Head/@{x}]&&(!OrderedQ[Head/@{x}])):=Module[{sign,hlist},
    hlist=SortBy[{x},Head];
    sign=-Signature[First/@hlist];
    sign*d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],Sequence@@(hlist/.Thread[Rule[First/@hlist,{\[Mu],\[Nu],\[Rho],\[Lambda]}]])]
];
d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],x__?xvQ]/;(DuplicateFreeQ[Head/@{x}]&&(OrderedQ[Head/@{x}])&&(First/@{x}=!={\[Mu],\[Nu],\[Rho],\[Lambda]})):=Module[{sign},
    sign=-Signature[First/@{x}];
    sign*d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],Sequence@@({x}/.Thread[Rule[First/@{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]])]
];
d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],a___,x_?\[Eta]\[Eta]bQ|x_?\[Sigma]Q|x_?\[Sigma]bQ,y_?xvQ,b___]:=d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],a,y,x,b]; (*Check this*)
d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],a__?xvQ,x_?\[Eta]bQ,y_?\[Sigma]bQ,z_?\[Eta]Q]:=d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],a,z,\[Sigma]@@y,x];
d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],x__?xvQ,\[Eta]_,s_?\[Sigma]Q,\[Eta]b_]/;(DuplicateFreeQ[Head/@{x}]&&(First/@Append[{x},s]=!={\[Mu],\[Nu],\[Rho],\[Lambda]})):=Module[{sign},
    sign=-Signature[First/@Append[{x},s]];
    sign*d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],Sequence@@({x}/.Thread[Rule[First/@{x},{\[Mu],\[Nu],\[Rho]}]]),\[Eta],\[Sigma][\[Lambda]],\[Eta]b]
];
d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],x__?xvQ,\[Eta]_,s_?\[Sigma]Q,\[Eta]b_]/;(DuplicateFreeQ[Head/@{x}]&&(First/@Append[{x},s]==={\[Mu],\[Nu],\[Rho],\[Lambda]})&&!OrderedQ[Head/@{x}]):=Module[{sign},
    sign=Signature[Head/@{x}];
    sign*d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],Sequence@@({x}/.Thread[Rule[Head/@{x},Sort[Head/@{x}]]]),\[Eta],\[Sigma][\[Lambda]],\[Eta]b]
];


(*Puts \[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu] at the beginning. Puts to zero \[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu] contracted with equal xs. Orders canonically the xs contracted with \[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu].*)
d[x__,\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],y___]:=d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x,y];
d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],y___]/;(!DuplicateFreeQ[Head/@Select[{y},xvQ[#]&]]):=0;
d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x__?xvQ]/;(DuplicateFreeQ[Head/@{x}]&&(!OrderedQ[Head/@{x}])):=Module[{sign,hlist},
    hlist=SortBy[{x},Head];
    sign=Signature[First/@hlist];
    sign*d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],Sequence@@(hlist/.Thread[Rule[First/@hlist,{\[Kappa],\[Lambda],\[Mu],\[Nu]}]])]
];
d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x__?xvQ]/;(DuplicateFreeQ[Head/@{x}]&&(OrderedQ[Head/@{x}])&&(First/@{x}=!={\[Kappa],\[Lambda],\[Mu],\[Nu]})):=Module[{sign},
    sign=Signature[First/@{x}];
    sign*d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],Sequence@@({x}/.Thread[Rule[First/@{x},{\[Kappa],\[Lambda],\[Mu],\[Nu]}]])]
];
d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],a___,x_?\[Eta]\[Eta]bQ|x_?\[Sigma]Q|x_?\[Sigma]bQ,y_?xvQ,b___]:=d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],a,y,x,b];   (*Check this*)
d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],a__?xvQ,x_?\[Eta]bQ,y_?\[Sigma]bQ,z_?\[Eta]Q]:=d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],a,z,\[Sigma]@@y,x];
d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x__?xvQ,\[Eta]_,s_?\[Sigma]Q,\[Eta]b_]/;(DuplicateFreeQ[Head/@{x}]&&(First/@Append[{x},s]=!={\[Kappa],\[Lambda],\[Mu],\[Nu]})):=Module[{sign},
    sign=Signature[First/@Append[{x},s]];
    sign*d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],Sequence@@({x}/.Thread[Rule[First/@{x},{\[Kappa],\[Lambda],\[Mu]}]]),\[Eta],\[Sigma][\[Nu]],\[Eta]b]
];
d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x__?xvQ,\[Eta]_,s_?\[Sigma]Q,\[Eta]b_]/;(DuplicateFreeQ[Head/@{x}]&&(First/@Append[{x},s]==={\[Kappa],\[Lambda],\[Mu],\[Nu]})&&!OrderedQ[Head/@{x}]):=Module[{sign},
    sign=Signature[Head/@{x}];
    sign*d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],Sequence@@({x}/.Thread[Rule[Head/@{x},Sort[Head/@{x}]]]),\[Eta],\[Sigma][\[Nu]],\[Eta]b]
];

(*d[X.Y]=XY*)
d[x_?sqQ]:=x;
d[x_?xQ,x_?xQ]:=sq[x];
d[x_?xQ,y_?xQ]:=glue[x,y];


(*Operator on functions that transforms the input in internal notation, applies the function, and transfrorms back in user form.*)
u[f_]:=WriteMonomials[f[SplitMonomials[#],##2]]&;
uu[f_]:=WriteMonomials[f[SplitMonomials[#1],SplitMonomials[#2],##3]]&;


(* ::Subsection::Closed:: *)
(*Other useful functions that use the definitions of d*)


ComplexConj[expr_]:=Module[{tempd},expr/.{Complex[a_,b_]:>Complex[a,-b]
                       }/.{\[Sigma][\[Mu]_,\[Nu]_]:>-\[Sigma]b[\[Mu],\[Nu]],\[Sigma]b[\[Mu]_,\[Nu]_]:>-\[Sigma][\[Mu],\[Nu]]
                       }/.{d[x__]:>Reverse[tempd[x]]
                       }/.{a_?bQ:>unbar[a],b_?ubQ:>bar[b]
                       }/.{tempd->d}];


(* ::Subsection::Closed:: *)
(*Display notation in a nice form*)


Nice[expr_]:=expr/.{d->dNice,a_Symbol?xxQ:>dNice[a],a_Symbol?sqQ:>dNice[a]}/.NonCommutativeMultiply->NiceNCM;
UnNice[expr_]:=expr/.dNice->d/.NiceNCM->NonCommutativeMultiply


Format[dNice[a_?sqQ]]:=StringCases[toString[a],{(x:"x")~~(y:DigitCharacter..)~~"sq":> Subscript[x, y]}][[1]]^2;
Format[dNice[a_?xxQ]]:=StringCases[toString[a],{"x"~~(y:DigitCharacter..)~~"x"~~(z:DigitCharacter..):> Row[{Subscript["x", y],"\[NegativeThinSpace]\[CenterDot]\[NegativeThinSpace]",Subscript["x", z]}]}][[1]];
Format[dNice[a__]]:=Row[{"\[ThinSpace]",a,"\[ThinSpace]"}/.{\[Sigma][\[Mu],\[Nu]]->"\!\(\*SubscriptBox[\(\[Sigma]\), \(\[Mu]\[Nu]\)]\)",\[Sigma]b[\[Mu],\[Nu]]->"\!\(\*SubscriptBox[OverscriptBox[\(\[Sigma]\), \(_\)], \(\[Mu]\[Nu]\)]\)",\[Sigma][\[Nu]]->"\!\(\*SubscriptBox[\(\[Sigma]\), \(\[Nu]\)]\)",
                              \[Sigma][\[Nu],\[Mu]]->"\!\(\*SubscriptBox[\(\[Sigma]\), \(\[Nu]\[Mu]\)]\)",\[Sigma]b[\[Nu],\[Mu]]->"\!\(\*SubscriptBox[OverscriptBox[\(\[Sigma]\), \(_\)], \(\[Nu]\[Mu]\)]\)",\[Sigma][\[Lambda]]->"\!\(\*SubscriptBox[\(\[Sigma]\), \(\[Lambda]\)]\)",
                              x_?xvQ:>StringCases[toString[x],"x"~~(y:DigitCharacter..)~~"v["~~z_~~"]":>  Subscript["x",y]^z][[1]],
                              x_?\[Eta]\[Eta]bQ:>StringCases[toString[x],{"\[Eta]"~~(z:DigitCharacter..):>Subscript["\[Eta]", z],"\[Eta]b"~~(z:DigitCharacter..):>Subscript["\!\(\*OverscriptBox[\(\[Eta]\), \(_\)]\)", z]}][[1]],
                              x_?xQ:>StringCases[toString[x],{"x"~~(y:DigitCharacter..):>Subscript["x", y]}][[1]],
                              \[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu] -> "\!\(\*SubscriptBox[\(\[CurlyEpsilon]\), \(\[Kappa]\[Lambda]\[Mu]\[Nu]\)]\)", \[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda] -> "\!\(\*SubscriptBox[\(\[CurlyEpsilon]\), \(\[Mu]\[Nu]\[Rho]\[Lambda]\)]\)" 
                              }];
Format[NiceNCM[a__]]:=Infix[NiceNCM[a],""];


(* ::Section::Closed:: *)
(*Reduction of expressions*)


(* ::Subsection::Closed:: *)
(*Algebra of Pauli \[Sigma] matrices*)


SetAttributes[\[ScriptG],Orderless];
SetAttributes[\[Delta],Orderless];
\[ScriptG]/:ConstQ[\[ScriptG]]:=True; (*\[ScriptG] is the 4 dimensional metric tensor*)
Unprotect[\[CurlyEpsilon]];\[CurlyEpsilon]/:ConstQ[\[CurlyEpsilon]]:=True;Protect[\[CurlyEpsilon]];(*\[CurlyEpsilon] is the 2 dimensional \[Epsilon]-tensor*)
\[Epsilon]4/:ConstQ[\[Epsilon]4]:=True; (*\[Epsilon]4 is the 4 dimensional \[Epsilon]-tensor*)
\[Delta]/:ConstQ[\[Delta]]:=True; (*\[Delta] is the 2 dimensional Kronecker \[Delta]*)
(*Rules to carry out the Pauli algebra*)
PauliRulesgen={
            \[Delta][\[Alpha]_,\[Beta]_]ANY_[i___,\[Beta]_,f___]:>ANY[i,\[Alpha],f],
			\[ScriptG][\[Mu]_,\[Nu]_]ANY_[i___,\[Nu]_,f___]:>ANY[i,\[Mu],f],
			\[ScriptG][\[Mu]_,\[Nu]_]\[Sigma][i___,\[Nu]_,f___][any__]:>\[Sigma][i,\[Mu],f][any],
			\[ScriptG][\[Mu]_,\[Nu]_]\[Sigma]b[i___,\[Nu]_,f___][any__]:>\[Sigma]b[i,\[Mu],f][any],
            \[ScriptG][\[Mu]_,\[Mu]_]->4,\[ScriptG][\[Mu]_,\[Nu]_]^2->4,
            \[Delta][\[Alpha]_,\[Alpha]_]->2,\[Delta][\[Alpha]_,\[Beta]_]^2->2,
            \[ScriptG][\[Mu]_,\[Nu]_]\[ScriptG][\[Mu]_,\[Rho]_]:>\[ScriptG][\[Nu],\[Rho]],
            \[Delta][\[Mu]_,\[Nu]_]\[Delta][\[Mu]_,\[Rho]_]:>\[Delta][\[Nu],\[Rho]],
            \[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Alpha]_] :> 0, \[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]_,\[Alpha]_] :> 0,
            \[CurlyEpsilon][x:OrderlessPatternSequence[\[Alpha]_,\[Beta]_]]\[CurlyEpsilon][y:OrderlessPatternSequence[\[Beta]_,\[Gamma]_]] :> If[{x}[[2]]===\[Beta],1,-1]*If[{y}[[1]]===\[Beta],1,-1]*\[Delta][\[Alpha],\[Gamma]],
            \[CurlyEpsilon][\[Alpha]_,\[Beta]_]^2:>-2,
            \[Epsilon]4[a__]/;!DuplicateFreeQ[{a}]:>0,
		    \[CurlyEpsilon][\[Alpha]_,\[Beta]_]up[X_][\[Beta]_]:>low[X][\[Alpha]],
			\[CurlyEpsilon][\[Alpha]_,\[Beta]_]low[X_][\[Beta]_]:>up[X][\[Alpha]],
			\[CurlyEpsilon][\[Beta]_,\[Alpha]_]up[X_][\[Beta]_]:>-low[X][\[Alpha]],
			\[CurlyEpsilon][\[Beta]_,\[Alpha]_]low[X_][\[Beta]_]:>-up[X][\[Alpha]],
            \[ScriptG][\[Mu]_,\[Nu]_]v[x_][\[Nu]_] :> v[x][\[Mu]]
            };

PauliRed  :={
            \[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[Sigma]b[\[Mu]_][\[Beta]d_,\[Beta]_]:>-2\[Delta][\[Alpha],\[Beta]]\[Delta][\[Alpha]d,\[Beta]d],
            \[Sigma]b[\[Mu]_][\[Alpha]d_,\[Alpha]_]\[Sigma]b[\[Mu]_][\[Beta]d_,\[Beta]_]:>-2\[CurlyEpsilon][\[Alpha],\[Beta]]\[CurlyEpsilon][\[Alpha]d,\[Beta]d],
            \[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[Sigma][\[Mu]_][\[Beta]_,\[Beta]d_]:>-2\[CurlyEpsilon][\[Alpha],\[Beta]]\[CurlyEpsilon][\[Alpha]d,\[Beta]d],
            \[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[CurlyEpsilon][x:OrderlessPatternSequence[\[Alpha]_,\[Beta]_]]\[CurlyEpsilon][y:OrderlessPatternSequence[\[Alpha]d_,\[Beta]d_]]:>\[Sigma]b[\[Mu]][\[Beta]d,\[Beta]]If[{x}[[1]]===\[Alpha],1,-1]If[{y}[[1]]===\[Alpha]d,1,-1],
            \[Sigma]b[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_]][\[Beta]d_,\[Gamma]d_]\[Sigma][\[Nu]_][\[Alpha]_,\[Alpha]d_]:>-1/2 (((\[Sigma][\[Mu]][\[Alpha],#]\[CurlyEpsilon][#,\[Beta]d]\[CurlyEpsilon][\[Gamma]d,\[Alpha]d])&@Unique[\[Zeta]d])-\[Sigma][\[Mu]][\[Alpha],\[Gamma]d]\[Delta][\[Alpha]d,\[Beta]d])If[{x}[[1]]===\[Mu],1,-1],
            \[Sigma][x:OrderlessPatternSequence[\[Mu]_,\[Nu]_]][\[Beta]_,\[Gamma]_]\[Sigma][\[Nu]_][\[Alpha]_,\[Alpha]d_]:>-1/2 (\[Delta][\[Gamma],\[Alpha]]\[Sigma][\[Mu]][\[Beta],\[Alpha]d]-((\[CurlyEpsilon][\[Alpha],\[Beta]]\[CurlyEpsilon][\[Gamma],#]\[Sigma][\[Mu]][#,\[Alpha]d])&@Unique[\[Zeta]]))If[{x}[[1]]===\[Mu],1,-1],
            \[Sigma]b[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_]][\[Beta]d_,\[Gamma]d_]\[Sigma]b[\[Nu]_][\[Alpha]d_,\[Alpha]_]:>-1/2 ((-(\[Sigma][\[Mu]][#,\[Gamma]d]\[CurlyEpsilon][\[Alpha],#]\[CurlyEpsilon][\[Alpha]d,\[Beta]d])&@Unique[\[Zeta]])+\[Sigma]b[\[Mu]][\[Beta]d,\[Alpha]]\[Delta][\[Alpha]d,\[Gamma]d])If[{x}[[1]]===\[Mu],1,-1],
            \[Sigma][x:OrderlessPatternSequence[\[Mu]_,\[Nu]_]][\[Beta]_,\[Gamma]_]\[Sigma]b[\[Nu]_][\[Alpha]d_,\[Alpha]_]:>-1/2 (-\[Delta][\[Alpha],\[Beta]]\[Sigma]b[\[Mu]][\[Alpha]d,\[Gamma]]+((\[CurlyEpsilon][\[Gamma],\[Alpha]]\[CurlyEpsilon][#,\[Alpha]d]\[Sigma][\[Mu]][\[Beta],#])&@Unique[\[Zeta]d]))If[{x}[[1]]===\[Mu],1,-1],
            \[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[CurlyEpsilon][\[Alpha]_,\[Beta]_]\[Sigma][\[Nu]_][\[Beta]_,\[Beta]d_]:>(\[ScriptG][\[Mu],\[Nu]]\[CurlyEpsilon][\[Alpha]d,\[Beta]d]-2(\[CurlyEpsilon][\[Alpha]d,#]\[Sigma]b[\[Mu],\[Nu]][#,\[Beta]d])&@Unique[\[Gamma]d]),
            \[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[CurlyEpsilon][\[Beta]_,\[Alpha]_]\[Sigma][\[Nu]_][\[Beta]_,\[Beta]d_]:>(-\[ScriptG][\[Mu],\[Nu]]\[CurlyEpsilon][\[Alpha]d,\[Beta]d]+2(\[CurlyEpsilon][\[Alpha]d,#]\[Sigma]b[\[Mu],\[Nu]][#,\[Beta]d])&@Unique[\[Gamma]d]),
            \[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[CurlyEpsilon][\[Alpha]d_,\[Beta]d_]\[Sigma][\[Nu]_][\[Beta]_,\[Beta]d_]:>(\[ScriptG][\[Mu],\[Nu]]\[CurlyEpsilon][\[Alpha],\[Beta]]-2(\[Sigma][\[Mu],\[Nu]][\[Alpha],#]\[CurlyEpsilon][#,\[Beta]])&@Unique[\[Gamma]d]),
            \[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[CurlyEpsilon][\[Beta]d_,\[Alpha]d_]\[Sigma][\[Nu]_][\[Beta]_,\[Beta]d_]:>(-\[ScriptG][\[Mu],\[Nu]]\[CurlyEpsilon][\[Alpha],\[Beta]]+2(\[Sigma][\[Mu],\[Nu]][\[Alpha],#]\[CurlyEpsilon][#,\[Beta]])&@Unique[\[Gamma]d]),
            \[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[Sigma]b[\[Nu]_][\[Alpha]d_,\[Beta]_]:>-\[ScriptG][\[Mu],\[Nu]]\[Delta][\[Alpha],\[Beta]]+2 \[Sigma][\[Mu],\[Nu]][\[Alpha],\[Beta]],
            \[Sigma]b[\[Mu]_][\[Alpha]d_,\[Alpha]_]\[Sigma][\[Nu]_][\[Alpha]_,\[Beta]d_]:>-\[ScriptG][\[Mu],\[Nu]]\[Delta][\[Alpha]d,\[Beta]d]+2 \[Sigma]b[\[Mu],\[Nu]][\[Alpha]d,\[Beta]d],
            \[CurlyEpsilon][x:OrderlessPatternSequence[\[Beta]_,\[Alpha]_]]\[Sigma]b[\[Mu]_][\[Beta]d_,\[Beta]_]:>If[{x}[[1]]===\[Beta],1,-1]\[Sigma][\[Mu]][\[Alpha],\[Beta]]\[CurlyEpsilon][\[Beta],\[Beta]d],
            \[CurlyEpsilon][x:OrderlessPatternSequence[\[Alpha]d_,\[Beta]d_]]\[Sigma]b[\[Mu]_][\[Beta]d_,\[Beta]_]:>If[{x}[[1]]===\[Alpha]d,1,-1]\[Sigma][\[Mu]][\[Beta]d,\[Alpha]d]\[CurlyEpsilon][\[Beta],\[Beta]d],
            \[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Gamma]_]\[CurlyEpsilon][\[Alpha]_,\[Delta]_]\[Sigma][\[Rho]_,\[Lambda]_][\[Delta]_,e_]:>(1/4 (\[ScriptG][\[Mu],\[Lambda]]\[ScriptG][\[Rho],\[Nu]]-\[ScriptG][\[Rho],\[Mu]]\[ScriptG][\[Lambda],\[Nu]]+I \[Epsilon]4[\[Lambda],\[Rho],\[Mu],\[Nu]])\[CurlyEpsilon][e,\[Gamma]]+1/2 (\[CurlyEpsilon][e,#](\[Sigma][\[Mu],\[Lambda]][#,\[Gamma]]\[ScriptG][\[Nu],\[Rho]]+\[Sigma][\[Nu],\[Rho]][#,\[Gamma]]\[ScriptG][\[Mu],\[Lambda]]-\[Sigma][\[Nu],\[Lambda]][#,\[Gamma]]\[ScriptG][\[Mu],\[Rho]]-\[Sigma][\[Mu],\[Rho]][#,\[Gamma]]\[ScriptG][\[Nu],\[Lambda]]))&@Unique[\[Zeta]]),
            \[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Gamma]_]\[CurlyEpsilon][\[Delta]_,\[Alpha]_]\[Sigma][\[Rho]_,\[Lambda]_][\[Delta]_,e_]:>(-(1/4)(\[ScriptG][\[Mu],\[Lambda]]\[ScriptG][\[Rho],\[Nu]]-\[ScriptG][\[Rho],\[Mu]]\[ScriptG][\[Lambda],\[Nu]]+I \[Epsilon]4[\[Lambda],\[Rho],\[Mu],\[Nu]])\[CurlyEpsilon][e,\[Gamma]]-1/2 (\[CurlyEpsilon][e,#](\[Sigma][\[Mu],\[Lambda]][#,\[Gamma]]\[ScriptG][\[Nu],\[Rho]]+\[Sigma][\[Nu],\[Rho]][#,\[Gamma]]\[ScriptG][\[Mu],\[Lambda]]-\[Sigma][\[Nu],\[Lambda]][#,\[Gamma]]\[ScriptG][\[Mu],\[Rho]]-\[Sigma][\[Mu],\[Rho]][#,\[Gamma]]\[ScriptG][\[Nu],\[Lambda]]))&@Unique[\[Zeta]]),
            \[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[Sigma][\[Rho]_,\[Lambda]_][\[Delta]_,\[Alpha]_]:>((\[Sigma][\[Mu],\[Nu]][\[Alpha],\[Beta]]\[CurlyEpsilon][\[Alpha],#1]\[Sigma][\[Rho],\[Lambda]][#1,#2]\[CurlyEpsilon][\[Delta],#2])&@@{Unique[\[Zeta]],Unique[\[Gamma]]}),
            \[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[Sigma][\[Rho]_,\[Lambda]_][\[Beta]_,\[Delta]_]:>((\[Sigma][\[Mu],\[Nu]][#1,\[Beta]]\[CurlyEpsilon][#1,#2]\[Sigma][\[Rho],\[Lambda]][#2,\[Delta]]\[CurlyEpsilon][\[Beta],\[Alpha]])&@@{Unique[\[Zeta]],Unique[\[Gamma]]}),
            \[CurlyEpsilon][\[Beta]_,\[Delta]_]\[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[Sigma][\[Rho]_,\[Lambda]_][\[Gamma]_,\[Delta]_]:>\[CurlyEpsilon][\[Beta],\[Alpha]]\[Sigma][\[Mu],\[Nu]][\[Delta],\[Beta]]\[Sigma][\[Rho],\[Lambda]][\[Gamma],\[Delta]],
            \[CurlyEpsilon][\[Delta]_,\[Beta]_]\[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[Sigma][\[Rho]_,\[Lambda]_][\[Gamma]_,\[Delta]_]:>\[CurlyEpsilon][\[Alpha],\[Beta]]\[Sigma][\[Mu],\[Nu]][\[Delta],\[Beta]]\[Sigma][\[Rho],\[Lambda]][\[Gamma],\[Delta]],
            \[Sigma][\[Mu]_][\[Beta]_,\[Beta]d_]\[Sigma]b[\[Nu]_][\[Beta]d_,\[Delta]_]\[Sigma][\[Rho]_][\[Delta]_,\[Delta]d_]:> (I \[Epsilon]4[\[Mu],\[Nu],\[Rho],#]\[Sigma][#][\[Beta],\[Delta]d]&@Unique[\[Lambda]])+\[ScriptG][\[Mu],\[Rho]]\[Sigma][\[Nu]][\[Beta],\[Delta]d]-\[ScriptG][\[Mu],\[Nu]]\[Sigma][\[Rho]][\[Beta],\[Delta]d]-\[ScriptG][\[Nu],\[Rho]]\[Sigma][\[Mu]][\[Beta],\[Delta]d],
            \[Sigma]b[\[Mu]_][\[Delta]d_,\[Delta]_]\[Sigma][\[Nu]_][\[Delta]_,\[Gamma]d_]\[Sigma]b[\[Rho]_][\[Gamma]d_,\[Beta]_]:> (-I \[Epsilon]4[\[Mu],\[Nu],\[Rho],#]\[Sigma]b[#][\[Delta]d,\[Beta]]&@Unique[\[Lambda]])+\[ScriptG][\[Mu],\[Rho]]\[Sigma]b[\[Nu]][\[Delta]d,\[Beta]]-\[ScriptG][\[Mu],\[Nu]]\[Sigma]b[\[Rho]][\[Delta]d,\[Beta]]-\[ScriptG][\[Nu],\[Rho]]\[Sigma]b[\[Mu]][\[Delta]d,\[Beta]],
            \[Sigma][\[Mu]_,\[Nu]_][\[Beta]_,\[Delta]_]\[Sigma][\[Rho]_][\[Delta]_,\[Delta]d_]:> 1/2 ((I \[Epsilon]4[\[Mu],\[Nu],\[Rho],#]\[Sigma][#][\[Beta],\[Delta]d]&@Unique[\[Lambda]])+\[ScriptG][\[Mu],\[Rho]]\[Sigma][\[Nu]][\[Beta],\[Delta]d]-\[ScriptG][\[Nu],\[Rho]]\[Sigma][\[Mu]][\[Beta],\[Delta]d]),
            \[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]d_,\[Beta]d_]\[Sigma]b[\[Rho]_][\[Beta]d_,\[Beta]_]:> 1/2 ((-I \[Epsilon]4[\[Mu],\[Nu],\[Rho],#]\[Sigma]b[#][\[Alpha]d,\[Beta]]&@Unique[\[Lambda]])+\[ScriptG][\[Mu],\[Rho]]\[Sigma]b[\[Nu]][\[Alpha]d,\[Beta]]-\[ScriptG][\[Nu],\[Rho]]\[Sigma]b[\[Mu]][\[Alpha]d,\[Beta]]),
            \[Sigma][\[Mu]_,\[Nu]_][\[Beta]_,\[Delta]_]\[Sigma]b[\[Rho]_][\[Delta]d_,\[Beta]_]:> -1/2 ((I \[Epsilon]4[\[Mu],\[Nu],\[Rho],#]\[Sigma]b[#][\[Delta]d,\[Delta]]&@Unique[\[Lambda]])+\[ScriptG][\[Mu],\[Rho]]\[Sigma]b[\[Nu]][\[Delta]d,\[Delta]]-\[ScriptG][\[Nu],\[Rho]]\[Sigma]b[\[Mu]][\[Delta]d,\[Delta]]),
            \[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]d_,\[Beta]d_]\[Sigma][\[Rho]_][\[Beta]_,\[Alpha]d_]:> -1/2 ((-I \[Epsilon]4[\[Mu],\[Nu],\[Rho],#]\[Sigma][#][\[Beta],\[Beta]d]&@Unique[\[Lambda]])+\[ScriptG][\[Mu],\[Rho]]\[Sigma][\[Nu]][\[Beta],\[Beta]d]-\[ScriptG][\[Nu],\[Rho]]\[Sigma][\[Mu]][\[Beta],\[Beta]d]),
            \[Sigma][\[Mu]_,\[Nu]_][\[Beta]_,\[Delta]_]\[Sigma]b[\[Rho]_][\[Delta]d_,\[Gamma]_]\[CurlyEpsilon][x:OrderlessPatternSequence[\[Delta]_,\[Gamma]_]]:> \[Sigma][\[Mu],\[Nu]][\[Gamma],\[Delta]]\[Sigma]b[\[Rho]][\[Delta]d,\[Gamma]]\[CurlyEpsilon][\[Delta],\[Beta]]If[{x}[[1]]===\[Delta],1,-1],
            \[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]d_,\[Beta]d_]\[Sigma][\[Rho]_][\[Beta]_,\[Gamma]d_]\[CurlyEpsilon][x:OrderlessPatternSequence[\[Beta]d_,\[Gamma]d_]]:> \[Sigma]b[\[Mu],\[Nu]][\[Gamma]d,\[Beta]d]\[Sigma][\[Rho]][\[Beta],\[Gamma]d]\[CurlyEpsilon][\[Beta]d,\[Alpha]d]If[{x}[[1]]===\[Beta]d,1,-1],
            \[Sigma][\[Mu]_,\[Nu]_][\[Beta]_,\[Delta]_]\[Sigma][\[Rho]_][\[Gamma]_,\[Alpha]d_]\[CurlyEpsilon][x:OrderlessPatternSequence[\[Beta]_,\[Gamma]_]]:> \[Sigma][\[Mu],\[Nu]][\[Beta],\[Gamma]]\[Sigma][\[Rho]][\[Gamma],\[Alpha]d]\[CurlyEpsilon][\[Beta],\[Delta]]If[{x}[[1]]===\[Beta],1,-1],
            \[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]d_,\[Beta]d_]\[Sigma]b[\[Rho]_][\[Gamma]d_,\[Alpha]_]\[CurlyEpsilon][x:OrderlessPatternSequence[\[Alpha]d_,\[Gamma]d_]]:> \[Sigma]b[\[Mu],\[Nu]][\[Alpha]d,\[Gamma]d]\[Sigma]b[\[Rho]][\[Gamma]d,\[Alpha]]\[CurlyEpsilon][\[Alpha]d,\[Beta]d]If[{x}[[1]]===\[Alpha]d,1,-1],
            (*REDUNDANT\[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[Sigma]b[\[Rho]_,\[Nu]_][\[Alpha]d_,\[Beta]d_]:>-(1/8)(\[Sigma][\[Mu]][\[Alpha],\[Beta]d]\[Sigma]b[\[Rho]][\[Alpha]d,\[Beta]]-\[Sigma][\[Mu]][\[Alpha],#2]\[CurlyEpsilon][#2,\[Alpha]d]\[Sigma][\[Rho]][#1,\[Beta]d]\[CurlyEpsilon][\[Beta],#1]-\[Sigma]b[\[Mu]][#2,\[Beta]]\[CurlyEpsilon][\[Beta]d,#2]\[Sigma]b[\[Rho]][\[Alpha]d,#1]\[CurlyEpsilon][#1,\[Alpha]]+\[Sigma]b[\[Mu]][\[Alpha]d,\[Beta]]\[Sigma][\[Rho]][\[Alpha],\[Beta]d])&@@{Unique[\[Zeta]],Unique[\[Zeta]d]},*)
            \[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[Sigma]b[\[Nu]_][\[Alpha]d_,\[Beta]_]:>2\[Sigma][\[Mu],\[Nu]][\[Alpha],\[Beta]]-\[ScriptG][\[Mu],\[Nu]]\[Delta][\[Alpha],\[Beta]],
            \[Sigma]b[\[Mu]_][\[Beta]d_,\[Alpha]_]\[Sigma][\[Nu]_][\[Alpha]_,\[Alpha]d_]:>2\[Sigma]b[\[Mu],\[Nu]][\[Beta]d,\[Alpha]d]-\[ScriptG][\[Mu],\[Nu]]\[Delta][\[Alpha]d,\[Beta]d],
            \[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[CurlyEpsilon][\[Alpha]_,\[Gamma]_]\[Sigma][\[Rho]_][\[Gamma]_,\[Gamma]d_]:>\[Sigma][\[Mu],\[Nu]][\[Alpha],\[Gamma]]\[Sigma][\[Rho]][\[Gamma],\[Gamma]d] \[CurlyEpsilon][\[Alpha],\[Beta]],
            \[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[CurlyEpsilon][\[Gamma]_,\[Alpha]_]\[Sigma][\[Rho]_][\[Gamma]_,\[Gamma]d_]:>\[Sigma][\[Mu],\[Nu]][\[Alpha],\[Gamma]]\[Sigma][\[Rho]][\[Gamma],\[Gamma]d] \[CurlyEpsilon][\[Beta],\[Alpha]],
            \[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[ScriptG][\[Mu]_,\[Nu]_]:>0,\[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[ScriptG][\[Mu]_,\[Nu]_]:>0,
            \[Sigma][\[Mu]_,\[Mu]_][\[Alpha]_,\[Beta]_]:>0,\[Sigma]b[\[Mu]_,\[Mu]_][\[Alpha]_,\[Beta]_]:>0,
            \[Sigma]b[\[Mu]_,\[Nu]_][\[Gamma]d_,\[Alpha]d_]\[CurlyEpsilon][\[Alpha]d_,\[Delta]d_]\[Sigma]b[\[Rho]_,\[Lambda]_][ed_,\[Delta]d_]:>(1/4 (\[ScriptG][\[Mu],\[Lambda]]\[ScriptG][\[Rho],\[Nu]]-\[ScriptG][\[Rho],\[Mu]]\[ScriptG][\[Lambda],\[Nu]]-I \[Epsilon]4[\[Lambda],\[Rho],\[Mu],\[Nu]])\[CurlyEpsilon][ed,\[Gamma]d]+1/2 (\[CurlyEpsilon][#,\[Gamma]d](\[Sigma]b[\[Mu],\[Lambda]][ed,#]\[ScriptG][\[Nu],\[Rho]]+\[Sigma]b[\[Nu],\[Rho]][ed,#]\[ScriptG][\[Mu],\[Lambda]]-\[Sigma]b[\[Nu],\[Lambda]][ed,#]\[ScriptG][\[Mu],\[Rho]]-\[Sigma]b[\[Mu],\[Rho]][ed,#]\[ScriptG][\[Nu],\[Lambda]]))&@Unique[\[Zeta]]),
            \[Sigma]b[\[Mu]_,\[Nu]_][\[Gamma]d_,\[Alpha]d_]\[CurlyEpsilon][\[Delta]d_,\[Alpha]d_]\[Sigma]b[\[Rho]_,\[Lambda]_][ed_,\[Delta]d_]:>(-1/4 (\[ScriptG][\[Mu],\[Lambda]]\[ScriptG][\[Rho],\[Nu]]-\[ScriptG][\[Rho],\[Mu]]\[ScriptG][\[Lambda],\[Nu]]-I \[Epsilon]4[\[Lambda],\[Rho],\[Mu],\[Nu]])\[CurlyEpsilon][ed,\[Gamma]d]-1/2 (\[CurlyEpsilon][#,\[Gamma]d](\[Sigma]b[\[Mu],\[Lambda]][ed,#]\[ScriptG][\[Nu],\[Rho]]+\[Sigma]b[\[Nu],\[Rho]][ed,#]\[ScriptG][\[Mu],\[Lambda]]-\[Sigma]b[\[Nu],\[Lambda]][ed,#]\[ScriptG][\[Mu],\[Rho]]-\[Sigma]b[\[Mu],\[Rho]][ed,#]\[ScriptG][\[Nu],\[Lambda]]))&@Unique[\[Zeta]]),
            \[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[Sigma]b[\[Rho]_,\[Lambda]_][\[Delta]_,\[Alpha]_]:>((\[Sigma]b[\[Mu],\[Nu]][\[Alpha],#1]\[CurlyEpsilon][#1,#2]\[Sigma]b[\[Rho],\[Lambda]][\[Delta],#2]\[CurlyEpsilon][\[Alpha],\[Beta]])&@@{Unique[\[Zeta]],Unique[\[Gamma]]}),
            \[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[Sigma]b[\[Rho]_,\[Lambda]_][\[Beta]_,\[Delta]_]:>((\[Sigma]b[\[Mu],\[Nu]][\[Alpha],\[Beta]]\[CurlyEpsilon][\[Beta],#1]\[Sigma]b[\[Rho],\[Lambda]][#2,#1]\[CurlyEpsilon][\[Delta],#2])&@@{Unique[\[Zeta]],Unique[\[Gamma]]}),
            \[CurlyEpsilon][\[Alpha]_,\[Gamma]_]\[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[Sigma]b[\[Rho]_,\[Lambda]_][\[Gamma]_,\[Delta]_]:>\[CurlyEpsilon][\[Alpha],\[Beta]]\[Sigma]b[\[Mu],\[Nu]][\[Alpha],\[Gamma]]\[Sigma]b[\[Rho],\[Lambda]][\[Gamma],\[Delta]],
            \[CurlyEpsilon][\[Gamma]_,\[Alpha]_]\[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[Sigma]b[\[Rho]_,\[Lambda]_][\[Gamma]_,\[Delta]_]:>\[CurlyEpsilon][\[Beta],\[Alpha]]\[Sigma]b[\[Mu],\[Nu]][\[Alpha],\[Gamma]]\[Sigma]b[\[Rho],\[Lambda]][\[Gamma],\[Delta]],
            \[Sigma][x:OrderlessPatternSequence[\[Mu]_,\[Nu]_]][\[Alpha]_,\[Beta]_] \[Sigma][y:OrderlessPatternSequence[\[Mu]_,\[Rho]_]][\[Gamma]_,\[Delta]_]:>(\[Sigma][x][\[Alpha],\[Beta]]/.Open\[Sigma]\[Mu]\[Nu])(\[Sigma][y][\[Gamma],\[Delta]]/.Open\[Sigma]\[Mu]\[Nu]),
            \[Sigma]b[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_]][\[Alpha]_,\[Beta]_] \[Sigma][y:OrderlessPatternSequence[\[Mu]_,\[Rho]_]][\[Gamma]_,\[Delta]_]:>(\[Sigma]b[x][\[Alpha],\[Beta]]/.Open\[Sigma]\[Mu]\[Nu])(\[Sigma][y][\[Gamma],\[Delta]]/.Open\[Sigma]\[Mu]\[Nu]),
            \[Sigma]b[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_]][\[Alpha]_,\[Beta]_] \[Sigma]b[y:OrderlessPatternSequence[\[Mu]_,\[Rho]_]][\[Gamma]_,\[Delta]_]:>(\[Sigma]b[x][\[Alpha],\[Beta]]/.Open\[Sigma]\[Mu]\[Nu])(\[Sigma]b[y][\[Gamma],\[Delta]]/.Open\[Sigma]\[Mu]\[Nu])
};
(*\[Epsilon]4*)       
PauliRules\[Epsilon]4:={\[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Epsilon]4[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_] :> -24*Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]],
            \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Epsilon]4[y:OrderlessPatternSequence[\[Kappa]_,\[Nu]_,\[Rho]_,\[Lambda]_]] :> -6*\[ScriptG][\[Mu],\[Kappa]]*Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]]*Signature[PermutationList[FindPermutation[{y},{\[Kappa],\[Nu],\[Rho],\[Lambda]}]]],
            \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Epsilon]4[y:OrderlessPatternSequence[\[Kappa]_,\[Tau]_,\[Rho]_,\[Lambda]_]] :> -2*Det[Table[\[ScriptG][ii,jj],{ii,{\[Mu],\[Nu]}},{jj,{\[Kappa],\[Tau]}}]]*Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]]*Signature[PermutationList[FindPermutation[{y},{\[Kappa],\[Tau],\[Rho],\[Lambda]}]]],
            \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Epsilon]4[y:OrderlessPatternSequence[\[Kappa]_,\[Tau]_,\[Omega]_,\[Lambda]_]] :> -Det[Table[\[ScriptG][ii,jj],{ii,{\[Mu],\[Nu],\[Rho]}},{jj,{\[Kappa],\[Tau],\[Omega]}}]]*Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]]*Signature[PermutationList[FindPermutation[{y},{\[Kappa],\[Tau],\[Omega],\[Lambda]}]]],
            \[Epsilon]4[x:PatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Epsilon]4[y:PatternSequence[\[Kappa]_,\[Tau]_,\[Omega]_,\[Upsilon]_]] :> -Det[Table[\[ScriptG][ii,jj],{ii,{x}},{jj,{y}}]],
            \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Sigma][y:OrderlessPatternSequence[\[Rho]_,\[Lambda]_]][\[Alpha]_,\[Beta]_] :> -2 I \[Sigma][\[Mu],\[Nu]][\[Alpha],\[Beta]] If[{y}[[1]]===\[Rho],1,-1]*Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]],
            \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Sigma]b[y:OrderlessPatternSequence[\[Rho]_,\[Lambda]_]][\[Alpha]d_,\[Beta]d_] :> 2 I \[Sigma]b[\[Mu],\[Nu]][\[Alpha]d,\[Beta]d] If[{y}[[1]]===\[Rho],1,-1]*Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]],
(*To check*)\[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Sigma][y:OrderlessPatternSequence[\[Lambda]_,\[Kappa]_]][\[Alpha]_,\[Beta]_] :> (I/2 \[Epsilon]4[x]\[Sigma][#1,#2][\[Alpha],\[Beta]]\[Epsilon]4[\[Lambda],\[Kappa],#1,#2]&@@{Unique[\[Tau]],Unique[\[Omega]]}),
(*To check*)\[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Sigma]b[y:OrderlessPatternSequence[\[Lambda]_,\[Kappa]_]][\[Alpha]d_,\[Beta]d_] :> (-I/2 \[Epsilon]4[x]\[Sigma]b[#1,#2][\[Alpha]d,\[Beta]d]\[Epsilon]4[\[Lambda],\[Kappa],#1,#2]&@@{Unique[\[Tau]],Unique[\[Omega]]}),
(*These last 3 rules must be checked*)
            \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[Sigma][\[Nu]_][\[Beta]_,\[Beta]d_]:>(Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]]\[Epsilon]4[\[Mu],\[Nu],\[Rho],\[Lambda]](\[Sigma][\[Mu],\[Nu]][\[Alpha],#1]\[CurlyEpsilon][#1,\[Beta]]\[CurlyEpsilon][\[Alpha]d,\[Beta]d]+\[Sigma]b[\[Mu],\[Nu]][#2,\[Beta]d]\[CurlyEpsilon][\[Alpha]d,#2]\[CurlyEpsilon][\[Alpha],\[Beta]])&@@{Unique[\[Zeta]],Unique[\[Zeta]d]}),
            \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Sigma]b[\[Mu]_][\[Alpha]d_,\[Alpha]_]\[Sigma]b[\[Nu]_][\[Beta]d_,\[Beta]_]:>(Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]]\[Epsilon]4[\[Mu],\[Nu],\[Rho],\[Lambda]](\[Sigma][\[Mu],\[Nu]][#1,\[Beta]]\[CurlyEpsilon][\[Alpha],#1]\[CurlyEpsilon][\[Alpha]d,\[Beta]d]+\[Sigma]b[\[Mu],\[Nu]][\[Alpha]d,#2]\[CurlyEpsilon][#2,\[Beta]d]\[CurlyEpsilon][\[Alpha],\[Beta]])&@@{Unique[\[Zeta]],Unique[\[Zeta]d]}),
            \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[Sigma]b[\[Nu]_][\[Beta]d_,\[Beta]_]:>Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]]\[Epsilon]4[\[Mu],\[Nu],\[Rho],\[Lambda]](\[Sigma][\[Mu],\[Nu]][\[Alpha],\[Beta]]\[Delta][\[Alpha]d,\[Beta]d]-\[Sigma]b[\[Mu],\[Nu]][\[Beta]d,\[Alpha]d]\[Delta][\[Alpha],\[Beta]])
};
(*Split open \[Sigma]\[Mu]\[Nu] and \[Sigma]b\[Mu]\[Nu]*)
Open\[Sigma]\[Mu]\[Nu] :=  {\[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_] :> (1/4 (\[Sigma][\[Mu]][\[Alpha],#]\[Sigma]b[\[Nu]][#,\[Beta]]-\[Sigma][\[Nu]][\[Alpha],#]\[Sigma]b[\[Mu]][#,\[Beta]])&@Unique[\[Alpha]d]),
            \[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]d_,\[Beta]d_] :> (1/4 (\[Sigma]b[\[Mu]][\[Alpha]d,#]\[Sigma][\[Nu]][#,\[Beta]d]-\[Sigma]b[\[Nu]][\[Alpha]d,#]\[Sigma][\[Mu]][#,\[Beta]d])&@Unique[\[Alpha]])};



(*Rules to contract objects with \[CurlyEpsilon] or \[ScriptG]*)
(*The function hold applied on d makes it disregard any signs due to anticommutations of \[Theta] or \[Theta]b*)
ContrRule = {v[x_][\[Mu]_]^2 :> sq[x],
			 v[x_][\[Mu]_]v[y_][\[Mu]_] :> glue[x,y],
			 up[h_?ubQ][\[Alpha]_]low[k_?ubQ][\[Alpha]_] :> d[h,k],
			 low[h_?bQ][\[Alpha]_]up[k_?bQ][\[Alpha]_] :> d[h,k],		 
			 up[X_?ubQ][\[Alpha]_]\[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]up[Y_?bQ][\[Alpha]d_]v[V_][\[Mu]_]:>d[X,V,Y],
			 low[X_?bQ][\[Alpha]d_]\[Sigma]b[\[Mu]_][\[Alpha]d_,\[Alpha]_]low[Y_?ubQ][\[Alpha]_]v[V_][\[Mu]_]:>d[Y,V,X],
			 v[V_][m_]up[X_][\[Alpha]_]\[Sigma][m_,n_][\[Alpha]_,\[Beta]_]low[Y_][\[Beta]_]v[W_][n_]:>If[V===W,0,1]d[glue[V,"v[\[Mu]]"],glue[W,"v[\[Nu]]"],X,\[Sigma][\[Mu],\[Nu]],Y],
			 v[V_][m_]low[X_][\[Alpha]d_]\[Sigma]b[m_,n_][\[Alpha]d_,\[Beta]d_]up[Y_][\[Beta]d_]v[W_][n_]:>If[V===W,0,1]d[glue[V,"v[\[Mu]]"],glue[W,"v[\[Nu]]"],X,\[Sigma]b[\[Mu],\[Nu]],Y],
			 v[W_][\[Mu]_]^2:>glue[W,"sq"],
			 \[Epsilon]4[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]v[X_][\[Mu]_]v[Y_][\[Nu]_]v[Z_][\[Rho]_]v[T_][\[Lambda]_]:>d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],glue[X,"v[\[Kappa]]"],glue[Y,"v[\[Lambda]]"],glue[Z,"v[\[Mu]]"],glue[T,"v[\[Nu]]"]],
			 \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]v[X_][\[Mu]_]v[Y_][\[Nu]_]v[Z_][\[Rho]_]up[H_][\[Alpha]_]up[Hb_][\[Alpha]d_]\[Sigma][\[Lambda]_][\[Alpha]_,\[Alpha]d_]:>Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]]d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],glue[X,"v[\[Kappa]]"],glue[Y,"v[\[Lambda]]"],glue[Z,"v[\[Mu]]"],H,\[Sigma][Symbol["\[Nu]"]],Hb],
			 \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]v[X_][\[Mu]_]v[Y_][\[Nu]_]v[Z_][\[Rho]_]low[H_][\[Alpha]_]low[Hb_][\[Alpha]d_]\[Sigma]b[\[Lambda]_][\[Alpha]d_,\[Alpha]_]:>Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]]d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],glue[X,"v[\[Kappa]]"],glue[Y,"v[\[Lambda]]"],glue[Z,"v[\[Mu]]"],H,\[Sigma][Symbol["\[Nu]"]],Hb]
};


(*Apply a replacement rule repeatedly expanding the expression every time*)
MAXITERATIONS=100;
Rex[expr_,rule_]:=FixedPoint[Expand[#]/.rule&,expr,MAXITERATIONS];
Rex[rule_]:=Rex[#,rule]&;


(*Writes any d-expression containing \[Chi] ,s and/or \[Theta](b) by putting explicitly the indices.*)
PutIndices[X__Plus]:=Plus@@(PutIndices/@(List@@X));
PutIndices[X__Times]:=Times@@(PutIndices/@(List@@X));
PutIndices[X_^p_]/;IntegerQ[p]&&p>0:=Module[{tempNCM},Flatten[tempNCM@@(Table[PutIndices[X],{\[ScriptT],1,p}])/.{Times->tempNCM},Infinity,tempNCM]/.tempNCM->Times];
PutIndices[X_^p_]:=X^p;
PutIndices[CC_?ConstQ]:=CC;


PutIndices[x_?xsqQ]:=x;
PutIndices[xdx_?xxQ]:=xdx;(*StringCases[toString[xdx],(a:{"x"~~DigitCharacter..})~~(b:{"x"~~DigitCharacter..}):>v[ToExpression[a]][#] v[ToExpression[b]][#]][[1]]&@Unique[\[Mu]];*)
PutIndices[d[something__]]/;Select[{something},StringMatchQ[toString[#],("\[Chi]"|"s"|"\[Theta]")~~__]&]==={}:=d[something];
PutIndices[d[a_?\[Eta]\[Theta]\[Chi]Q,b_?\[Eta]\[Theta]\[Chi]Q]]:=(up[a][#]low[b][#])&@Unique[\[Alpha]];
PutIndices[d[a_?\[Eta]b\[Theta]b\[Chi]bQ,b_?\[Eta]b\[Theta]b\[Chi]bQ]]:=(low[a][#]up[b][#])&@Unique[\[Alpha]d];
PutIndices[d[a_?\[Eta]\[Theta]\[Chi]Q,x_?xQ|x_?sQ,b_?\[Eta]b\[Theta]b\[Chi]bQ]]:=(v[x][#3]\[Sigma][#3][#1,#2]up[a][#1]up[b][#2])&@@{Unique[\[Alpha]],Unique[\[Alpha]d],Unique[\[Mu]]};
PutIndices[d[(x_?xvQ|x_?svQ),(y_?xvQ|y_?svQ),a_?\[Eta]\[Theta]\[Chi]Q,\[Sigma][\[Mu]_,\[Nu]_],b_?\[Eta]\[Theta]\[Chi]Q]]:=((v[ToExpression[StringDrop[toString[Head[x]],-1]]]@@x)(v[ToExpression[StringDrop[toString[Head[y]],-1]]]@@y)\[Sigma][\[Mu],\[Nu]][#1,#2]up[a][#1]low[b][#2]/.{\[Mu]->#3,\[Nu]->#4})&@@{Unique[\[Alpha]],Unique[\[Beta]],Unique[\[Mu]],Unique[\[Nu]]};
PutIndices[d[(x_?xvQ|x_?svQ),(y_?xvQ|y_?svQ),a_?\[Eta]b\[Theta]b\[Chi]bQ,\[Sigma]b[\[Mu]_,\[Nu]_],b_?\[Eta]b\[Theta]b\[Chi]bQ]]:=((v[ToExpression[StringDrop[toString[Head[x]],-1]]]@@x)(v[ToExpression[StringDrop[toString[Head[y]],-1]]]@@y)\[Sigma]b[\[Mu],\[Nu]][#1,#2]low[a][#1]up[b][#2]/.{\[Mu]->#3,\[Nu]->#4})&@@{Unique[\[Alpha]d],Unique[\[Beta]d],Unique[\[Mu]],Unique[\[Nu]]};
PutIndices[th_?\[Theta]sqQ]:=up[ToExpression[StringDrop[toString[th],-2]]][#]low[ToExpression[StringDrop[toString[th],-2]]][#]&@Unique[\[Alpha]];
PutIndices[th_?\[Theta]bsqQ]:=low[ToExpression[StringDrop[toString[th],-2]]][#]up[ToExpression[StringDrop[toString[th],-2]]][#]&@Unique[\[Alpha]d];
PutIndices[xdx_?xsQ]:=StringCases[toString[xdx],(a:{"x"~~DigitCharacter..})~~(b:{"s"~~DigitCharacter..}):>v[ToExpression[a]][#] v[ToExpression[b]][#]][[1]]&@Unique[\[Mu]];
PutIndices[dxdx_?ssQ]:=StringCases[toString[dxdx],(a:{"s"~~DigitCharacter..})~~(b:{"s"~~DigitCharacter..}):>v[ToExpression[a]][#] v[ToExpression[b]][#]][[1]]&@Unique[\[Mu]];
PutIndices[d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],(x_?xvQ|x_?svQ),(y_?xvQ|y_?svQ),(z_?xvQ|z_?svQ),(t_?xvQ|t_?svQ)]]:=(\[Epsilon]4[#1,#2,#3,#4]((v[ToExpression[StringDrop[toString[Head[x]],-1]]]@@x)(v[ToExpression[StringDrop[toString[Head[y]],-1]]]@@y)(v[ToExpression[StringDrop[toString[Head[z]],-1]]]@@z)(v[ToExpression[StringDrop[toString[Head[t]],-1]]]@@t)/.{\[Mu]->#1,\[Nu]->#2,\[Rho]->#3,\[Lambda]->#4}))&@@{Unique[\[Mu]],Unique[\[Nu]],Unique[\[Rho]],Unique[\[Lambda]]};
PutIndices[d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],(x_?xvQ|x_?svQ),(y_?xvQ|y_?svQ),(z_?xvQ|z_?svQ),(t_?xvQ|t_?svQ)]]:=(\[Epsilon]4[#1,#2,#3,#4]((v[ToExpression[StringDrop[toString[Head[x]],-1]]]@@x)(v[ToExpression[StringDrop[toString[Head[y]],-1]]]@@y)(v[ToExpression[StringDrop[toString[Head[z]],-1]]]@@z)(v[ToExpression[StringDrop[toString[Head[t]],-1]]]@@t)/.{\[Kappa]->#1,\[Lambda]->#2,\[Mu]->#3,\[Nu]->#4}))&@@{Unique[\[Kappa]],Unique[\[Lambda]],Unique[\[Mu]],Unique[\[Nu]]};
PutIndices[d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],(x_?xvQ|x_?svQ),(y_?xvQ|y_?svQ),(z_?xvQ|z_?svQ),a_?\[Eta]\[Theta]\[Chi]Q,si_?\[Sigma]Q,b_?\[Eta]b\[Theta]b\[Chi]bQ]]:=(\[Epsilon]4[#1,#2,#3,#4]((v[ToExpression[StringDrop[toString[Head[x]],-1]]]@@x)(v[ToExpression[StringDrop[toString[Head[y]],-1]]]@@y)(v[ToExpression[StringDrop[toString[Head[z]],-1]]]@@z)(si[#5,#6])/.{\[Mu]->#1,\[Nu]->#2,\[Rho]->#3,\[Lambda]->#4})up[a][#5]up[b][#6])&@@{Unique[\[Mu]],Unique[\[Nu]],Unique[\[Rho]],Unique[\[Lambda]],Unique[\[Alpha]],Unique[\[Alpha]d]};
PutIndices[d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],(x_?xvQ|x_?svQ),(y_?xvQ|y_?svQ),(z_?xvQ|z_?svQ),a_?\[Eta]\[Theta]\[Chi]Q,si_?\[Sigma]Q,b_?\[Eta]b\[Theta]b\[Chi]bQ]]:=(\[Epsilon]4[#1,#2,#3,#4]((v[ToExpression[StringDrop[toString[Head[x]],-1]]]@@x)(v[ToExpression[StringDrop[toString[Head[y]],-1]]]@@y)(v[ToExpression[StringDrop[toString[Head[z]],-1]]]@@z)(si[#5,#6])/.{\[Kappa]->#1,\[Lambda]->#2,\[Mu]->#3,\[Nu]->#4})up[a][#5]up[b][#6])&@@{Unique[\[Kappa]],Unique[\[Lambda]],Unique[\[Mu]],Unique[\[Nu]],Unique[\[Alpha]],Unique[\[Alpha]d]};


(* ::Subsection::Closed:: *)
(*Removal of placeholders for derivatives \[Chi] and s*)


(*Takes the derivative of the auxiliary variables \[Chi] and s*)
D\[Chi]up[\[Chi]_,\[ScriptA]_] := {low[\[Chi]][\[Beta]_]:>\[CurlyEpsilon][\[Beta],\[ScriptA]], up[\[Chi]][\[Beta]_]:>\[Delta][\[ScriptA],\[Beta]]};
D\[Chi]low[\[Chi]_,\[ScriptA]_] := {up[\[Chi]][\[Beta]_]:>\[CurlyEpsilon][\[Beta],\[ScriptA]], low[\[Chi]][\[Beta]_]:>\[Delta][\[ScriptA],\[Beta]]};
\[Sigma]Dx[s_,\[ScriptA]_,\[ScriptA]d_] := {v[s][\[Mu]_]:>\[Sigma][\[Mu]][\[ScriptA],\[ScriptA]d]};
\[Sigma]bDx[s_,\[ScriptA]d_,\[ScriptA]_] := {v[s][\[Mu]_]:>\[Sigma]b[\[Mu]][\[ScriptA]d,\[ScriptA]]};


\[Theta]OrderedList[mon_Times|mon_NonCommutativeMultiply|mon_d]:=Module[{tempNCM},
    Join[Flatten[tempNCM[mon]/.b_^p_/;IntegerQ[p]&&p>0:>tempNCM@@Table[b,{\[ScriptT],1,p}]/.{Times->tempNCM,NonCommutativeMultiply->tempNCM,d->tempNCM},Infinity,tempNCM]/.{low[a_][b_]:>a,up[a_][b_]:>a}/.{tempNCM[seq__] :> Cases[{seq},_?\[Theta]\[Theta]bQ]},
         Cases[mon,t_?\[Theta]\[Theta]bsqQ:>Sequence[ToExpression[StringDrop[toString[t],-2]],ToExpression[StringDrop[toString[t],-2]]],Infinity]]
];
\[Theta]OrderedList[b_^p_?IntegerQ]/;p>0:=Flatten[Table[\[Theta]OrderedList[b],{\[ScriptT],1,p}]];
\[Theta]OrderedList[b_^p_]:=\[Theta]OrderedList[b] (*This is ok as long as \[Theta]s don't appear inside real powers. They have to be expandede first*)
\[Theta]OrderedList[mon_]/;(Head[Head[mon]]===up||Head[Head[mon]]===low):=Cases[{mon[[0,1]]},t_?\[Theta]\[Theta]bQ];
\[Theta]OrderedList[mon_]/;(Head[Head[mon]]===v):={};
\[Theta]OrderedList[mon_?\[Theta]\[Theta]bsqQ]:={ToExpression[StringDrop[toString[mon],-2]],ToExpression[StringDrop[toString[mon],-2]]};
\[Theta]OrderedList[mon_?xxQ|mon_?xsqQ]:={};
\[Theta]OrderedList[sym_?ConstQ]:={};
\[Theta]OrderedList[sym_]/;Head[sym]===\[Delta]:={};
\[Theta]OrderedList[sym_]/;Head[sym]===\[CurlyEpsilon]:={};


(*This function takes an expression with \[Chi]'s and s's that need to be contracted*)
(*The parameters are:
  derivative : the replacement used to contract \[Chi] and s (it's a pure function, not a replacement table)
  redRules : (Optional) specify the reduction rules
  contRules : (Optional) specify the reduction rules
*)
(*Note: the order PauliRed,PauliRulesgen matters: if you do PauliRulesgen before it'll contract stuff in a stupid way and require more identities*)
Rulesremove\[Chi]s=<|redRules:>Join[PauliRed,PauliRulesgen,PauliRules\[Epsilon]4],contRules:>Join[PauliRulesgen,ContrRule]|>;
remove\[Chi]s::contractionFail="I couldn't contract the indices in the following expression:\n `1`";
(*This prevents Auxremeve\[Chi]s to evaluate the rules red and cont, so that they evaluate everytime they are called*)
SetAttributes[Auxremove\[Chi]s,HoldRest];
remove\[Chi]s[expr_,derivative_]:=Module[{expanded},Plus@@(Auxremove\[Chi]s[#,derivative,Rulesremove\[Chi]s[redRules],Rulesremove\[Chi]s[contRules]]&/@(If[(Head[expanded=Expand[expr]])===Plus,List@@expanded,{expanded}]))];
Auxremove\[Chi]s[a_?ConstQ*b_,derivative_,red_,cont_]:=a Auxremove\[Chi]s[b,derivative,red,cont];
Auxremove\[Chi]s[a_?ConstQ,derivative_,red_,cont_]:=a;
Auxremove\[Chi]s[expr_Times|expr_d|expr_Symbol,derivative_,red_,cont_]:=Module[{uptosign,uptosignl},
   (*applies the derivative*)
     uptosign=(derivative[PutIndices[expr]]);
   (*removes the ** product and applies all reduction rules*)
     uptosign=(uptosign//Rex[red]//Rex[cont]);
   (*lists the terms*)
     uptosignl=If[Head[uptosign]===Plus,List@@(Expand[uptosign]),{uptosign}];
   (*Checking for this error message could waste some time. For now let's have it commented out*)
     (*Do[If[Cases[el,up[x_][y_]|low[z_][t_]|v[o_][p_]|\[Epsilon]4[w__]|\[Delta][v__]|\[CurlyEpsilon][u__]|\[Sigma][m__][n__],Infinity]=!={},Message[remove\[Chi]s::contractionFail,Cases[el,up[x_][y_]|low[z_][t_]|v[o_][p_]|\[Epsilon]4[w__]|\[Delta][v__]|\[CurlyEpsilon][u__]|\[Sigma][m__][n__],Infinity]]],{el,uptosignl}];*)
   (*Returns the sum*)
     Plus@@(uptosignl)
]


(* ::Subsection::Closed:: *)
(*Reduction*)


RulesReduction=<|redRules:>Join[PauliRed,PauliRulesgen,PauliRules\[Epsilon]4],contRules:>Join[PauliRulesgen,ContrRule]|>;
Reduction::contractionFail="I couldn't contract the indices in the following expression:\n `1`";
(*This prevents AuxReduction to evaluate the rules red and cont, so that they evaluate everytime they are called*)
SetAttributes[AuxReduction,HoldRest];
Reduction[expr_]:=Module[{expanded},Plus@@(AuxReduction[#,RulesReduction[redRules],RulesReduction[contRules]]&/@(If[(Head[expanded=Expand[expr]])===Plus,List@@expanded,{expanded}]))];
AuxReduction[a_?ConstQ*b_,red_,cont_]:=a AuxReduction[b,red,cont];
AuxReduction[a_?ConstQ,red_,cont_]:=a;
AuxReduction[expr_,red_,cont_]:=Module[{exprNCM,tempNCM,uptosign,uptosignl},
     uptosign=(Flatten[tempNCM[PutIndices[expr]/.{Times->tempNCM}],Infinity,tempNCM]/.tempNCM->Times);
   (*removes the ** product and applies all reduction rules*)
     uptosign=(uptosign//Rex[red]//Rex[cont]);
   (*lists the terms*)
     uptosignl=If[Head[uptosign]===Plus,List@@(Expand[uptosign]),{uptosign}];
   (*Returns the sum*)
     Plus@@(uptosignl)
]


(* ::Subsection::Closed:: *)
(*Memoize and apply functions*)


memoizeAndEvaluate[f_][expr_]:=Module[{set},
    set[Activate[Inactivate[f[expr]],Function],
        f[expr]
        ]/.set[a__]:>Hold[set[a]]/.Inactive[x_]:>x/.set->Set//ReleaseHold]


fastApply[f_][expr_]:=Module[{all,xsqLis,\[ScriptI]=1,ruledelayed,expression},
    If[workingxs === {},
    all = Join[Cases[expr,x_d:>Sequence@@x,Infinity(*,-1*)],Cases[expr,x_?sqQ,{-1}(*,-1*)],Cases[expr,x_?xxQ,{-1}(*,-1*)]];
    all = DeleteDuplicates[all//.{
          x_?\[Sigma]Q->Nothing,x_?\[Sigma]bQ -> Nothing,x_Power->Nothing}];
    xsqLis = Select[all,xsqQ[#]||xxQ[#]&];,
    xsqLis = workingxs;];
    THETA/:ConstQ[THETA[i_]]:=True;
    expression=(Collect[Rex[
        Join[
            Table[ruledelayed[xsqr^pPattern_. \[ScriptD][yPattern___], \[ScriptD][yPattern,xsqr^pPattern]]/.ruledelayed->RuleDelayed,{xsqr,xsqLis}],
           {d[x__]^j_. \[ScriptD][y___]:>\[ScriptD][y,d[x]^j], A_^pow_ \[ScriptD][y___] :> \[ScriptD][y,A^pow]}
        ]][
       Expand[expr \[ScriptD][]]],
    \[ScriptD][___],THETA[Factor[#]]&]/.\[ScriptD][x___]:>If[Head[f]===Function,memoizeAndEvaluate[f][Times[x]],(f[Times[x]]=f[Times[x]])]);
    Collect[expression, THETA[___]]/.THETA[x_]:>x
];


fastReduction[expr_]:=Module[{all,\[Theta]Lis,\[ScriptI]=1,ruledelayed,expression},
    THETA/:ConstQ[THETA[i_]]:=True;
    expression=(Collect[Rex[
           {d[x__]^j_. \[ScriptD][y___]:>\[ScriptD][y,d[x]^j]}
        ][
       (Expand[expr]) \[ScriptD][]],
    \[ScriptD][___],THETA[Factor[#]]&]/.\[ScriptD]->Times);
    Collect[expression, THETA[___], (Reduction[#]=Reduction[#])&]/.THETA[x_]:>x
];


(*Memoized version of some functions*)
fdxsq[a_,b_]:=fastApply[dxsq[#,b]&][a];
fd\[Eta]d\[Eta][a_,b_,c_]:=fastApply[d\[Eta]d\[Eta][#,b,c]&][a];
fd\[Eta]bd\[Eta]b[a_,b_,c_]:=fastApply[d\[Eta]bd\[Eta]b[#,b,c]&][a];
(**)
fd\[Eta]dxd\[Eta]b[a_,b_,c_,d_]:=fastApply[d\[Eta]dxd\[Eta]b[#,b,c,d]&][a];
f\[Eta]dxd\[Eta]b[a_,b_,c_,d_]:=fastApply[\[Eta]dxd\[Eta]b[#,b,c,d]&][a];
fd\[Eta]dx\[Eta]b[a_,b_,c_,d_]:=fastApply[d\[Eta]dx\[Eta]b[#,b,c,d]&][a];
f\[Eta]dx\[Eta]b[a_,b_,c_,d_]:=fastApply[\[Eta]dx\[Eta]b[#,b,c,d]&][a];
fd\[Eta]xd\[Eta]b[a_,b_,c_,d_]:=fastApply[d\[Eta]xd\[Eta]b[#,b,c,d]&][a];


(* ::Subsection::Closed:: *)
(*From an input with explicit indices*)


RulesContraction=<|redRules:>Join[PauliRed,PauliRulesgen,PauliRules\[Epsilon]4],contRules:>Join[PauliRulesgen,ContrRule]|>;
(*This prevents Contraction to evaluate the rules red and cont, so that they evaluate everytime they are called*)
SetAttributes[AuxContraction,HoldRest];
Contraction[expr_]:=Module[{expanded},Plus@@(AuxContraction[#,RulesContraction[redRules],RulesContraction[contRules]]&/@(If[(Head[expanded=Expand[expr]])===Plus,List@@expanded,{expanded}]))];
AuxContraction[a_?NumericQ*b_,red_,cont_]:=a AuxContraction[b,red,cont];
AuxContraction[a_?NumericQ,red_,cont_]:=a;
AuxContraction[expr_,red_,cont_]:=Module[{exprNCM,tempNCM,uptosign,uptosignl,iniorder},
   (*applies the derivative and the rule for same \[Theta]s*)
     uptosign=(Flatten[tempNCM[expr/.d[X__]^p_:>PutIndices[d[X]^p]/.{d[A__]:>PutIndices[d[A]]
                                     }/.{Times->tempNCM}],Infinity,tempNCM]/.tempNCM->Times);    (*Here you need to make sure that you apply all possible Same\[Theta]rule, because afterwards you take away the ** and you may miss some signs*)
   (*memorizes the initial order of \[Theta]s*)
     iniorder = uptosign;
   (*removes the ** product and applies all reduction rules*)
     uptosign=(uptosign//Rex[red]//Rex[cont]);
   (*lists the terms*)
     uptosignl=If[Head[uptosign]===Plus,List@@(Expand[uptosign]),{uptosign}];
   (*Returns the sum where terms not agreeing with the initial ordering are multiplied by the signature of the permutation*)
     Plus@@(uptosignl)
]


(* ::Subsection::Closed:: *)
(*Taylor expansion*)


(*Raises an expression to a positive integer power*)
FPower[expr_,p_?IntegerQ]/;p>0:=Reduction[Times@@Table[expr,{i,1,p}]];
FPower[expr_,0]:=1;
(*Raises an expression to a real power*)
FPower[expr_,around_,p_,n_]/;\[Theta]FreeQ[around]:=around^p Sum[Binomial[p,k]FPower[(expr-around)/around,k],{k,0,n}];
(*Computes the Taylor expansion of f[expr] around expr = 0.*)
FTaylor[expr_,f_,n_]:=Sum[ SeriesCoefficient[f[x],{x,0,k}] FPower[expr,k],{k,0,n}]


(* ::Section::Closed:: *)
(*Derivatives*)


(* ::Subsection::Closed:: *)
(*Definitions*)


(*Leibniz and graded Leibniz rules*)
Leibniz[f_]:=Module[{factors=List@@#},
    Plus@@Table[(MapAt[Function[{x},f[x,##2]],factors,iter]/.List->Head[#]),{iter,1,Length@factors}]
]&;
dPower[f_,\[ScriptL]_]:=\[ScriptL] #^(\[ScriptL]-1) f[#,##2]&;


(* ::Subsection::Closed:: *)
(*\[Chi]\[PartialD]\[Eta] and s\[PartialD]x*)


(*These functions' only purpose is to replace \[Eta],\[Theta] with \[Chi] and x with s and apply the Leibniz rule in doing so*)


(*\[Chi]\[PartialD]\[Eta] = \[Chi]^\[Alpha]\[PartialD]/\[PartialD]\[Eta]^\[Alpha] or = Overscript[\[Chi], _]^Overscript[\[Alpha], .]\[PartialD]/\[PartialD]Overscript[\[Eta], _]^Overscript[\[Alpha], .]*)
(*Define the behaviour on +, * and ** *)
\[Chi]d\[Eta][expr_Plus,\[Eta]_,\[Chi]_]:=Plus@@(\[Chi]d\[Eta][#,\[Eta],\[Chi]]&/@List@@(expr));
\[Chi]d\[Eta][expr_Times,\[Eta]_,\[Chi]_]:=Leibniz[\[Chi]d\[Eta]][expr,\[Eta],\[Chi]];
\[Chi]d\[Eta][expr_^\[ScriptL]_,\[Eta]_,\[Chi]_]:=dPower[\[Chi]d\[Eta],\[ScriptL]][expr,\[Eta],\[Chi]];
\[Chi]d\[Eta][expr_?ConstQ,\[Eta]_,\[Chi]_]:=0;
(*Actual definition*)
\[Chi]d\[Eta][expr_d,\[Eta]_,\[Chi]_]/;Length[Cases[expr,\[Eta]]]==1:=expr/.\[Eta]->\[Chi];
\[Chi]d\[Eta][expr_d,\[Eta]_,\[Chi]_]/;FreeQ[expr,\[Eta]]:=0;
\[Chi]d\[Eta][d[x_?xvQ,y_?xvQ,\[Eta]_,si_?\[Sigma]Q|si_?\[Sigma]bQ,\[Eta]_],\[Eta]_,\[Chi]_]:=2d[x,y,\[Chi],si,\[Eta]];
\[Chi]d\[Eta][a_?xxQ,\[Eta]_,\[Chi]_]:=0;
\[Chi]d\[Eta][a_?xsQ,\[Eta]_,\[Chi]_]:=0;
\[Chi]d\[Eta][a_?ssQ,\[Eta]_,\[Chi]_]:=0;
\[Chi]d\[Eta][a_?sqQ,\[Eta]_,\[Chi]_]:=0;


(*s\[PartialD]x = s^\[Mu]\[PartialD]/\[PartialD]x^\[Mu]*)
(*Define the behaviour on +, * and ** *)
sdx[expr_Plus,x_,s_]:=Plus@@(sdx[#,x,s]&/@List@@(expr));
sdx[expr_Times,x_,s_]:=Leibniz[sdx][expr,x,s];
sdx[expr_^\[ScriptL]_,\[Eta]_,\[Chi]_]:=dPower[sdx,\[ScriptL]][expr,\[Eta],\[Chi]];
sdx[expr_?ConstQ,x_,s_]:=0;
(*These two functions return 1,-1 or 0: e.g xMatchQ[x12,x1]=1,xMatchQ[x12,x2]=-1,xMatchQ[x12,x3]=0*)
xMatchQ[xx_,x_]:=If[StringMatchQ[ToString[xx],ToString[x]~~DigitCharacter..],1,If[StringMatchQ[ToString[xx],"x"~~DigitCharacter~~StringTake[ToString[x],-1]],-1,If[StringMatchQ[ToString[xx],ToString[x]],1,0]]];
xvMatchQ[xx_,x_]:=If[StringMatchQ[ToString[Head[xx]],ToString[x]~~DigitCharacter..~~"v"],1,If[StringMatchQ[ToString[Head[xx]],"x"~~DigitCharacter~~StringTake[ToString[x],-1]~~"v"],-1,If[StringMatchQ[ToString[Head[xx]],ToString[x]~~"v"],1,0]]];
(*Actual definition*)
sdx[d[H_,xx_,Hb_],x_,s_]:=xMatchQ[xx,x]d[H,s,Hb];
sdx[d[x1_?xvQ,x2_?xvQ,\[Eta]1_,si_?\[Sigma]Q|si_?\[Sigma]bQ,\[Eta]2_],x_,s_]:=xvMatchQ[x1,x]d[Symbol[ToString[s]<>"v"]@@x1,x2,\[Eta]1,si,\[Eta]2]+xvMatchQ[x2,x]d[x1,Symbol[ToString[s]<>"v"]@@x2,\[Eta]1,si,\[Eta]2];
sdx[d[s1_?svQ,x2_?xvQ,\[Eta]1_,si_?\[Sigma]Q|si_?\[Sigma]bQ,\[Eta]2_],x_,s_]:=xvMatchQ[x2,x]d[s1,Symbol[ToString[s]<>"v"]@@x2,\[Eta]1,si,\[Eta]2];
sdx[d[x1_?xvQ,s2_?svQ,\[Eta]1_,si_?\[Sigma]Q|si_?\[Sigma]bQ,\[Eta]2_],x_,s_]:=xvMatchQ[x1,x]d[Symbol[ToString[s]<>"v"]@@x1,s2,\[Eta]1,si,\[Eta]2];
sdx[d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x1_?xvQ|x1_?svQ,x2_?xvQ|x2_?svQ,x3_?xvQ|x3_?svQ,\[Eta]1_,si_?\[Sigma]Q|si_?\[Sigma]bQ,\[Eta]b2_],x_,s_]:=xvMatchQ[x1,x]d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],Symbol[ToString[s]<>"v"]@@x1,x2,x3,\[Eta]1,si,\[Eta]b2]+xvMatchQ[x2,x]d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x1,Symbol[ToString[s]<>"v"]@@x2,x3,\[Eta]1,si,\[Eta]b2]+xvMatchQ[x3,x]d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x1,x2,Symbol[ToString[s]<>"v"]@@x3,\[Eta]1,si,\[Eta]b2];
sdx[a_?sqQ,x_,s_]:=xMatchQ[StringDrop[ToString[a],-2],x]*2*Symbol[StringDrop[ToString[a],-2]<>ToString[s]];
sdx[a_?xxQ,x_,s_]:=Module[{aa},aa=StringCases[ToString[a],{(e:("x"~~DigitCharacter..))~~(f:("x"~~DigitCharacter..)):>{e,f}}][[1]];xMatchQ[aa[[1]],x]Symbol[aa[[2]]<>ToString[s]]+xMatchQ[aa[[2]],x]Symbol[aa[[1]]<>ToString[s]]];
sdx[a_?xsQ,x_,s_]:=Module[{aa},aa=StringCases[ToString[a],{(e:("x"~~DigitCharacter..))~~(f:("s"~~DigitCharacter..)):>{e,f},(e:("s"~~DigitCharacter..))~~(f:("x"~~DigitCharacter..)):>{f,e}}][[1]];xMatchQ[aa[[1]],x]Symbol[aa[[2]]<>ToString[s]]];
sdx[d[a_,b_],x_,s_]:=0;


(*This is an alias, but careful: the arguments are swapped!*)
\[Eta]d\[Eta][expr_,\[Chi]_,\[Eta]_]:=\[Chi]d\[Eta][expr,\[Eta],\[Chi]];
\[Eta]bd\[Eta]b[expr_,\[Chi]b_,\[Eta]b_]:=\[Chi]d\[Eta][expr,\[Eta]b,\[Chi]b];
xdx[expr_,s_,x_]:=sdx[expr,x,s];


(* ::Subsection::Closed:: *)
(*\[PartialD]\[Eta]\[PartialD]\[Eta]*)


(*\[PartialD]\[Eta]\[PartialD]\[Eta]*)
(*Define the differentiation rules*)
d\[Eta]d\[Eta]Rule[expr_,\[Chi]1_,\[Chi]2_]:=(expr/.D\[Chi]up[\[Chi]1,#1]/.D\[Chi]low[\[Chi]2,#1])&@Unique[\[Alpha]]; 

(*Definition*)
d\[Eta]d\[Eta][expr_Plus,\[Eta]1_,\[Eta]2_]:=Plus@@(d\[Eta]d\[Eta][#,\[Eta]1,\[Eta]2]&/@List@@(expr));
(***The auxiliary spinor for \[PartialD]\[Theta] must be recognized as bosonic.*)
d\[Eta]d\[Eta][expr_Times|expr_d|expr_Power|expr_Symbol,\[Eta]1_,\[Eta]2_]:=Block[{\[Chi]1,\[Chi]2},\[Chi]1=Unique["\[Chi]"];\[Chi]2=Unique["\[Chi]"];remove\[Chi]s[\[Chi]d\[Eta][\[Chi]d\[Eta][expr,\[Eta]2,\[Chi]1],\[Eta]1,\[Chi]2],d\[Eta]d\[Eta]Rule[#,\[Chi]1,\[Chi]2]&]];
d\[Eta]d\[Eta][expr_?ConstQ,\[Eta]1_,\[Eta]2_]:=0;


(* ::Subsection::Closed:: *)
(*\[PartialD]\[Eta]b\[PartialD]\[Eta]b*)


(*\[PartialD]\[Eta]b\[PartialD]\[Eta]b*)
(*Define the differentiation rules*)
d\[Eta]bd\[Eta]bRule[expr_,\[Chi]b1_,\[Chi]b2_]:=(expr/.D\[Chi]low[\[Chi]b1,#1]/.D\[Chi]up[\[Chi]b2,#])&@Unique[\[Alpha]d]; 

(*Definition*)
d\[Eta]bd\[Eta]b[expr_Plus,\[Eta]b1_,\[Eta]b2_]:=Plus@@(d\[Eta]bd\[Eta]b[#,\[Eta]b1,\[Eta]b2]&/@List@@(expr));
(***The auxiliary spinor for \[PartialD]\[Theta] must be recognized as bosonic.*)
d\[Eta]bd\[Eta]b[expr_Times|expr_d|expr_Power|expr_Symbol,\[Eta]b1_,\[Eta]b2_]:=Block[{\[Chi]b1,\[Chi]b2},\[Chi]b1=Unique["\[Chi]b"];\[Chi]b2=Unique["\[Chi]b"];remove\[Chi]s[\[Chi]d\[Eta][\[Chi]d\[Eta][expr,\[Eta]b2,\[Chi]b1],\[Eta]b1,\[Chi]b2],d\[Eta]bd\[Eta]bRule[#,\[Chi]b1,\[Chi]b2]&]];
d\[Eta]bd\[Eta]b[expr_?ConstQ,\[Eta]b1_,\[Eta]b2_]:=0;


(* ::Subsection::Closed:: *)
(*\[Eta]\[PartialD]x\[Eta]b*)


(*\[Eta]\[PartialD]x\[Eta]b*)
(*Define the differentiation rules*)
\[Eta]dx\[Eta]bRule[expr_,\[Chi]_,s_,\[Chi]b_]:=(expr*up[\[Chi]][#1]*up[\[Chi]b][#2]/.\[Sigma]Dx[s,#1,#2])&@@{Unique[\[Alpha]],Unique[\[Alpha]d]};

(*Definition*)
\[Eta]dx\[Eta]b[expr_Plus,\[Eta]_,x_,\[Eta]b_]:=Plus@@(\[Eta]dx\[Eta]b[#,\[Eta],x,\[Eta]b]&/@List@@(expr));
\[Eta]dx\[Eta]b[expr_Times|expr_d|expr_Symbol|expr_Power|expr_Symbol,\[Eta]_,x_,\[Eta]b_]:=Block[{s1},s1=Unique["s"];remove\[Chi]s[sdx[expr,x,s1],\[Eta]dx\[Eta]bRule[#,\[Eta],s1,\[Eta]b]&]];
\[Eta]dx\[Eta]b[expr_?ConstQ,\[Eta]_,x_,\[Eta]b_]:=0;


(* ::Subsection::Closed:: *)
(*\[PartialD]\[Eta]\[PartialD]x\[PartialD]\[Eta]b*)


(*\[PartialD]\[Eta]\[PartialD]x\[PartialD]\[Eta]b*)
(*Define the differentiation rules*)
d\[Eta]dxd\[Eta]bRule[expr_,\[Chi]_,s_,\[Chi]b_]:=(expr/.D\[Chi]low[\[Chi],#1]/.D\[Chi]low[\[Chi]b,#2]/.\[Sigma]Dx[s,#1,#2])&@@{Unique[\[Alpha]],Unique[\[Alpha]d]};

(*Definition*)
d\[Eta]dxd\[Eta]b[expr_Plus,\[Eta]_,x_,\[Eta]b_]:=Plus@@(d\[Eta]dxd\[Eta]b[#,\[Eta],x,\[Eta]b]&/@List@@(expr));
d\[Eta]dxd\[Eta]b[expr_Times|expr_d|expr_Power|expr_Symbol,\[Eta]_,x_,\[Eta]b_]:=Block[{s1,\[Chi]1,\[Chi]b1},\[Chi]1=Unique["\[Chi]"];\[Chi]b1=Unique["\[Chi]b"];s1=Unique["s"];remove\[Chi]s[\[Chi]d\[Eta][\[Chi]d\[Eta][sdx[expr,x,s1],\[Eta],\[Chi]1],\[Eta]b,\[Chi]b1],d\[Eta]dxd\[Eta]bRule[#,\[Chi]1,s1,\[Chi]b1]&]];
d\[Eta]dxd\[Eta]b[expr_?ConstQ,\[Eta]_,x_,\[Eta]b_]:=0;


(* ::Subsection::Closed:: *)
(*\[PartialD]\[Eta]x\[PartialD]\[Eta]b*)


(*\[PartialD]\[Eta]\[PartialD]x\[PartialD]\[Eta]b*)
(*Define the differentiation rules*)
d\[Eta]xd\[Eta]bRule[expr_,\[Chi]_,x_,\[Chi]b_]:=(v[x][#3] \[Sigma][#3][#1,#2] expr/.D\[Chi]low[\[Chi],#1]/.D\[Chi]low[\[Chi]b,#2])&@@{Unique[\[Alpha]],Unique[\[Alpha]d],Unique[\[Mu]]};

(*Definition*)
d\[Eta]xd\[Eta]b[expr_Plus,\[Eta]_,x_,\[Eta]b_]:=Plus@@(d\[Eta]xd\[Eta]b[#,\[Eta],x,\[Eta]b]&/@List@@(expr));
d\[Eta]xd\[Eta]b[expr_Times|expr_d|expr_Power|expr_Symbol,\[Eta]_,x_,\[Eta]b_]:=Block[{\[Chi]1,\[Chi]b1},\[Chi]1=Unique["\[Chi]"];\[Chi]b1=Unique["\[Chi]b"];remove\[Chi]s[\[Chi]d\[Eta][\[Chi]d\[Eta][expr,\[Eta],\[Chi]1],\[Eta]b,\[Chi]b1],d\[Eta]xd\[Eta]bRule[#,\[Chi]1,x,\[Chi]b1]&]];
d\[Eta]xd\[Eta]b[expr_?ConstQ,\[Eta]_,x_,\[Eta]b_]:=0;


(* ::Subsection::Closed:: *)
(*\[PartialD]xsq*)


(*\[PartialD]xsq*)
(*Define the differentiation rules*)
dxsqRule[expr_,s1_,s2_]:=(expr*\[ScriptG][#1,#2]/.v[s1][m_]:>\[ScriptG][m,#1]/.v[s2][n_]:>\[ScriptG][n,#2])&@@{Unique[\[Mu]],Unique[\[Nu]]};

(*Definition*)
dxsq[expr_Plus,x_]:=Plus@@(dxsq[#,x]&/@List@@(expr));
dxsq[expr_Times|expr_d|expr_Symbol|expr_Power,x_]:=Block[{s1,s2},s1=Unique["s"];s2=Unique["s"];remove\[Chi]s[sdx[sdx[expr,x,s1],x,s2],dxsqRule[#,s1,s2]&]];
dxsq[expr_?ConstQ,x_]:=0;


(* ::Subsection::Closed:: *)
(*\[PartialD]\[Eta]\[PartialD]x\[Eta]b and \[Eta]\[PartialD]x\[PartialD]\[Eta]b*)


(*\[PartialD]\[Eta]\[PartialD]x\[Eta]b*)
(*Define the differentiation rules*)
d\[Eta]dx\[Eta]bRule[expr_,\[Chi]_,s_,\[Chi]b_]:=(up[\[Chi]b][#2]*expr/.D\[Chi]low[\[Chi],#1]/.\[Sigma]Dx[s,#1,#2])&@@{Unique[\[Alpha]],Unique[\[Alpha]d]};

(*Definition*)
d\[Eta]dx\[Eta]b[expr_Plus,\[Eta]_,x_,\[Eta]b_]:=Plus@@(d\[Eta]dx\[Eta]b[#,\[Eta],x,\[Eta]b]&/@List@@(expr));
d\[Eta]dx\[Eta]b[expr_Times|expr_d|expr_Power|expr_Symbol,\[Eta]_,x_,\[Eta]b_]:=Block[{s1,\[Chi]1},\[Chi]1=Unique["\[Chi]"];s1=Unique["s"];remove\[Chi]s[\[Chi]d\[Eta][sdx[expr,x,s1],\[Eta],\[Chi]1],d\[Eta]dx\[Eta]bRule[#,\[Chi]1,s1,\[Eta]b]&]];
d\[Eta]dx\[Eta]b[expr_?ConstQ,\[Eta]_,x_,\[Eta]b_]:=0;


(*\[Eta]\[PartialD]x\[PartialD]\[Eta]b*)
(*Define the fermion numbers of this operator*)
\[Eta]dxd\[Eta]b/:FN[\[Eta]dxd\[Eta]b[expr_,\[Eta]_,x_,\[Eta]b_]]:=FN[expr];

(*Define the differentiation rules*)
\[Eta]dxd\[Eta]bRule[expr_,\[Chi]_,s_,\[Chi]b_]:=(up[\[Chi]][#1]*expr/.D\[Chi]low[\[Chi]b,#2]/.\[Sigma]Dx[s,#1,#2])&@@{Unique[\[Alpha]],Unique[\[Alpha]d]};

(*Definition*)
\[Eta]dxd\[Eta]b[expr_Plus,\[Eta]_,x_,\[Eta]b_]:=Plus@@(\[Eta]dxd\[Eta]b[#,\[Eta],x,\[Eta]b]&/@List@@(expr));
\[Eta]dxd\[Eta]b[expr_Times|expr_d|expr_Power|expr_Symbol,\[Eta]_,x_,\[Eta]b_]:=Block[{s1,\[Chi]b1},\[Chi]b1=Unique["\[Chi]b"];s1=Unique["s"];remove\[Chi]s[\[Chi]d\[Eta][sdx[expr,x,s1],\[Eta]b,\[Chi]b1],\[Eta]dxd\[Eta]bRule[#,\[Eta],s1,\[Chi]b1]&]];
\[Eta]dxd\[Eta]b[expr_?ConstQ,\[Eta]_,x_,\[Eta]b_]:=0;


(* ::Section::Closed:: *)
(*Replacements*)


(* ::Subsection::Closed:: *)
(*Replace \[Eta] with any expression with one \[Alpha] index*)


Replace\[Eta]ToAny[expr_Plus,\[Eta]_,any_]:=Plus@@(Replace\[Eta]ToAny[#,\[Eta],any]&/@List@@(Expand[expr]));
Replace\[Eta]ToAny[expr_?ConstQ,\[Eta]_,any_]:=expr;
Replace\[Eta]ToAny[expr_Symbol,\[Eta]_,any_]:=expr;
Replace\[Eta]ToAny[expr_^y_,\[Eta]_,any_]:=Power[Replace\[Eta]ToAny[expr,\[Eta],any],y];
Replace\[Eta]ToAny[expr1_ expr2_^y_,\[Eta]_,any_]:=Replace\[Eta]ToAny[expr1,\[Eta],any] Power[Replace\[Eta]ToAny[expr2,\[Eta],any],y];

Replace\[Eta]ToAny[expr_Times|expr_d,\[Eta]_,any_]/;FreeQ[expr,Power]:=Module[
{n\[Eta],\[Chi]s,i=1,exprw\[Chi],anyRule,Sumremove\[Chi]s,tempNCM,expanded,uplow},

n\[Eta] = Count[List@@expr/.{Times->Sequence,d->Sequence},\[Eta]];
\[Chi]s=If[\[Eta]Q[\[Eta]],Table[Unique["\[Chi]1"],{n,1,n\[Eta]}],If[\[Eta]bQ[\[Eta]],Table[Unique["\[Chi]b1"],{n,1,n\[Eta]}]]];
exprw\[Chi] = expr/.\[Eta]:>(\[Chi]s[[i++]]);
anyRule[\[Xi]1_]:=
  Table[uplow={low[\[Xi]1][a_]->Sequence@@(case[[1]][a]),up[\[Xi]1][a_]->Sequence@@(case[[2]][a])};
        Function[exp,Flatten[tempNCM[exp/.{Times->tempNCM}],Infinity,tempNCM]/.Ruplow/.tempNCM->Times]/.Ruplow->uplow
   ,{case,any}];
Sumremove\[Chi]s[exp_,{rules__}]:=Sum[remove\[Chi]s[exp,r],{r,{rules}}];

Fold[Sumremove\[Chi]s,exprw\[Chi],anyRule/@\[Chi]s]
];



(* ::Section::Closed:: *)
(*Comparing expressions*)


(* ::Subsection::Closed:: *)
(*Definition of the numerical values*)


(*If I insert the ordering rule for ** the numerical values comparison fails. Only for one coefficient though... Find out what goes wrong.*)


\[Sigma]1={{0,1},{1,0}};\[Sigma]2={{0,-I},{I,0}};\[Sigma]3={{1,0},{0,-1}};
\[Epsilon]={{0,1},{-1,0}};
unit={{-1,0},{0,-1}};
\[CapitalSigma]\[Mu]\[Nu]={{{{0,0},{0,0}},{{0,1/2},{1/2,0}},{{0,-(I/2)},{I/2,0}},{{1/2,0},{0,-(1/2)}}},{{{0,-(1/2)},{-(1/2),0}},{{0,0},{0,0}},{{-(I/2),0},{0,I/2}},{{0,1/2},{-(1/2),0}}},{{{0,I/2},{-(I/2),0}},{{I/2,0},{0,-(I/2)}},{{0,0},{0,0}},{{0,-(I/2)},{-(I/2),0}}},{{{-(1/2),0},{0,1/2}},{{0,-(1/2)},{1/2,0}},{{0,I/2},{I/2,0}},{{0,0},{0,0}}}};
\[CapitalSigma]b\[Mu]\[Nu]={{{{0,0},{0,0}},{{0,-(1/2)},{-(1/2),0}},{{0,I/2},{-(I/2),0}},{{-(1/2),0},{0,1/2}}},{{{0,1/2},{1/2,0}},{{0,0},{0,0}},{{-(I/2),0},{0,I/2}},{{0,1/2},{-(1/2),0}}},{{{0,-(I/2)},{I/2,0}},{{I/2,0},{0,-(I/2)}},{{0,0},{0,0}},{{0,-(I/2)},{-(I/2),0}}},{{{1/2,0},{0,-(1/2)}},{{0,-(1/2)},{1/2,0}},{{0,I/2},{I/2,0}},{{0,0},{0,0}}}};
g[m_,n_]:=If[m==n,Switch[m,1,-1,2,1,3,1,4,1],0];


(*The numerical contraction d \[Rule] nd*)
nd[x_n,y_n]:=x.\[Epsilon].y/.n->List;
nd[x_b,y_b]:=y.\[Epsilon].x/.b->List;

nd[a_b,x__,c_n]:=c.Dot[x].a/.{n->List,b->List};
nd[a_n,x__,c_b]:=a.Dot[x].c/.{n->List,b->List};

nd[v[x__,\[Alpha]_],v[y__,\[Beta]_],m1__,\[Sigma][\[Alpha]_,\[Beta]_],m2__]:=Sum[({x}[[\[Mu]]])*(m1.\[CapitalSigma]\[Mu]\[Nu][[\[Mu],\[Nu]]].\[Epsilon].m2)*({y}[[\[Nu]]]),{\[Mu],1,4},{\[Nu],1,4}]/.{n->List,b->List};
nd[v[x__,\[Alpha]_],v[y__,\[Beta]_],m1__,\[Sigma]b[\[Alpha]_,\[Beta]_],m2__]:= Sum[({x}[[\[Mu]]])*(-m1.\[Epsilon].\[CapitalSigma]b\[Mu]\[Nu][[\[Mu],\[Nu]]].m2)*({y}[[\[Nu]]]),{\[Mu],1,4},{\[Nu],1,4}]/.{n->List,b->List};
nd[v[x__,\[Beta]_],v[y__,\[Alpha]_],m1__,\[Sigma][\[Alpha]_,\[Beta]_],m2__]:=-Sum[({x}[[\[Mu]]])*(m1.\[CapitalSigma]\[Mu]\[Nu][[\[Mu],\[Nu]]].\[Epsilon].m2)*({y}[[\[Nu]]]),{\[Mu],1,4},{\[Nu],1,4}]/.{n->List,b->List};
nd[v[x__,\[Beta]_],v[y__,\[Alpha]_],m1__,\[Sigma]b[\[Alpha]_,\[Beta]_],m2__]:= -Sum[({x}[[\[Mu]]])*(-m1.\[Epsilon].\[CapitalSigma]b\[Mu]\[Nu][[\[Mu],\[Nu]]].m2)*({y}[[\[Nu]]]),{\[Mu],1,4},{\[Nu],1,4}]/.{n->List,b->List};

nd[v[x__],v[y__]]:=Sum[{x}[[m]]*{y}[[m]]*g[m,m],{m,1,4}];

(*Sometimes when expressions with \[Epsilon] have the x's ordered but the indices not are left as they are. This may result in a non matching of the rules below. Maybe fixed!*)
nd[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],v[v1__,\[Kappa]],v[v2__,\[Lambda]],v[v3__,\[Mu]],v[v4__,\[Nu]]]:=Sum[LeviCivitaTensor[4][[k,l,m,n]]{v1}[[k]]{v2}[[l]]{v3}[[m]]{v4}[[n]],{k,1,4},{l,1,4},{m,1,4},{n,1,4}];
nd[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],v[v1__,\[Mu]],v[v2__,\[Nu]],v[v3__,\[Rho]],v[v4__,\[Lambda]]]:=Sum[LeviCivitaTensor[4][[k,l,m,n]]{v1}[[k]]{v2}[[l]]{v3}[[m]]{v4}[[n]],{k,1,4},{l,1,4},{m,1,4},{n,1,4}];
nd[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],v[v1__,\[Kappa]],v[v2__,\[Lambda]],v[v3__,\[Mu]],e_n,\[Sigma][\[Nu]],f_b]:=Sum[LeviCivitaTensor[4][[k,l,m,nn]]{v1}[[k]]{v2}[[l]]{v3}[[m]](e.{unit,\[Sigma]1,\[Sigma]2,\[Sigma]3}[[nn]].f/.{n->List,b->List})g[nn,nn],{k,1,4},{l,1,4},{m,1,4},{nn,1,4}];
nd[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],v[v1__,\[Mu]],v[v2__,\[Nu]],v[v3__,\[Rho]],e_n,\[Sigma][\[Lambda]],f_b]:=Sum[LeviCivitaTensor[4][[k,l,m,nn]]{v1}[[k]]{v2}[[l]]{v3}[[m]](e.{unit,\[Sigma]1,\[Sigma]2,\[Sigma]3}[[nn]].f/.{n->List,b->List})g[nn,nn],{k,1,4},{l,1,4},{m,1,4},{nn,1,4}];


nd[x_?NumericQ]:=x;
nd[x_?ConstQ]:=x;


(*Replacement rule for rational values*)
rule[\[Eta]s_,xs_][\[Eta]1_List,\[Eta]2_List,\[Eta]1i_List,\[Eta]2i_List,xa_List,xb_List,xc_List,xd_List]:=Module[{\[Eta]List,xList,xxList},
\[Eta]List = Join[Table[\[Eta]s[[e]] -> n[\[Eta]1[[e]]+I \[Eta]1i[[e]],\[Eta]2[[e]]+I \[Eta]2i[[e]]],{e,1,Length[\[Eta]s]}],
    Table[bar[\[Eta]s[[e]]] -> b[\[Eta]1[[e]]-I \[Eta]1i[[e]],\[Eta]2[[e]]-I \[Eta]2i[[e]]],{e,1,Length[\[Eta]s]}]];

xList = Join[Table[xs[[e]] -> xa[[e]] \[Sigma]1 + xb[[e]] \[Sigma]2 + xc[[e]] \[Sigma]3 + xd[[e]] unit,{e,1,Length[xs]}],
    Table[sq[xs[[e]]] -> -Det[xa[[e]] \[Sigma]1 + xb[[e]] \[Sigma]2 + xc[[e]] \[Sigma]3 + xd[[e]] unit],{e,1,Length[xs]}],
    Table[glue[xs[[e]],"v"][m_] -> v[xd[[e]] ,xa[[e]], xb[[e]], xc[[e]],m],{e,1,Length[xs]}]];

xxList = Flatten[Table[glue[xs[[e]],xs[[f]]] -> nd[v[xd[[e]] ,xa[[e]], xb[[e]], xc[[e]]],v[xd[[f]] ,xa[[f]], xb[[f]], xc[[f]]]],{e,1,Length[xs]},{f,1,Length[xs]}]];

Return[Join[\[Eta]List,xList,xxList]]
];


(*Replacement rule for random rational values*)
denom = 19;

ran[\[Eta]s_,xs_]:=rule[\[Eta]s,xs][
    RandomInteger[{-denom,denom},Length@\[Eta]s]/denom,
    RandomInteger[{-denom,denom},Length@\[Eta]s]/denom,
    RandomInteger[{-denom,denom},Length@\[Eta]s]/denom,
    RandomInteger[{-denom,denom},Length@\[Eta]s]/denom,
    RandomInteger[{-denom,denom},Length@xs]/denom,
    RandomInteger[{-denom,denom},Length@xs]/denom,
    RandomInteger[{-denom,denom},Length@xs]/denom,
    RandomInteger[{-denom,denom},Length@xs]/denom
]


(* ::Subsection::Closed:: *)
(*Compare an expression to zero*)


Options[Compare]={expReplacement -> {(*q->2,qb\[Rule]2,\[ScriptL]->2,l->2,k->1,\[CapitalDelta]\[Rule]4,j->0,\[CapitalDelta]\[Phi]->1*)},(*Replacements for the exponents of e.g. x12sq^\[CapitalDelta]*)
                  variables -> ConstantsList,(*Usual set of symbols treated as constants, which are the unknowns of this system*)
                  verbose -> 0,  (*Displays more info on the system that is being solved*)
                  minEqns -> -1};  (*Writes at least mineqns equations, if -1 the number of equations equals the number of variables matched*)

(*Solves expr \[Congruent] \[ScriptCapitalA] t1 + \[ScriptCapitalB] t2 + ... = 0 for \[ScriptCapitalA],\[ScriptCapitalB],..*)
Compare[expr_,OptionsPattern[]]:=Module[{vars,powerRule,zeroExpr,all,\[Eta]Lis,xLis,eqnlist={},vPrint,vvPrint,ruledelayed,\[ScriptF]},
    (*Prints if verbose \[Rule] >0*)
    vPrint[str__]:=If[(OptionValue[verbose]/.{True->1,False->0})>0,Print[str],Null];
    vvPrint[{str__},{str2__}]:=If[(OptionValue[verbose]/.{True->1,False->0})>0,If[OptionValue[verbose]>1,Print[str2],Print[str]],Null];
    (*Sets a rule to treat the list usualVariables as constants for the theta Algebra*)
    ToExpression[
        StringJoin@@Table[
            toString[con]<>"/: ConstQ["<>toString[con]<>"] := True; ",
        {con,OptionValue[variables]}]];

    vars = Cases[Variables[expr],Alternatives@@OptionValue[variables]];
    vPrint["Solving for ",vars,"."];
    
    powerRule = {Power[b_,e_]:>Power[b,e/.OptionValue[expReplacement]]};
    all = Join[Cases[expr,x_d:>Sequence@@x,Infinity(*,-1*)],Cases[expr,x_?sqQ,{-1}(*,-1*)],Cases[expr,x_?xxQ,{-1}(*,-1*)]];
    all = DeleteDuplicates[all//.{x_?bQ :> unbar[x],
          x_?\[Sigma]Q->Nothing,x_?\[Sigma]bQ -> Nothing,x_Power->Nothing,
          x_?xvQ :> ToExpression[StringDrop[toString[Head[x]],-1]],
          x_?sqQ :> ToExpression[StringDrop[toString[x],-2]],
          x_?xxQ :> Sequence@@(ToExpression/@StringCases[toString[x],"x"~~(DigitCharacter..)])}];
    (*List of variables that will get assigned numerical values*)
    \[Eta]Lis = Select[all,\[Eta]Q];
    xLis = Select[all,xQ];
    vPrint["Replacing numerical values to ",\[Eta]Lis," ",xLis,"."];
    
    (*Now it collects similarly as fastApply. For some reason this is slower in some cases. I'll comment it out for now and replace it with this line.*)
    zeroExpr = expr /. powerRule /. d->nd;
    (*zeroExpr = Collect[
        Rex[
            Join[
                Table[ruledelayed[xsqr^pPattern_. \[ScriptD][yPattern___], \[ScriptD][yPattern,xsqr^pPattern]]/.ruledelayed->RuleDelayed,{xsqr,sq/@xLis}],
               {d[x__]^j_. \[ScriptD][y___]:>\[ScriptD][y,d[x]^j]}
            ]][
           (Expand[expr]) \[ScriptD][]]
    ,\[ScriptD][___],\[ScriptF][Factor[#]]&];
    zeroExpr = zeroExpr /. {
               \[ScriptD][a___] :> \[ScriptD][Times[a]/.powerRule/.d->nd]
               } /. {\[ScriptD][x_] :> x, \[ScriptD][] -> 1};*)

    (*Solves the linear system for vars - ForceCoeffs*)

    Do[
    AppendTo[eqnlist,Expand[zeroExpr/.ran[\[Eta]Lis,xLis]]/.\[ScriptF][x_]:>x];
    vvPrint[{Variables[Last[eqnlist]]},{Last[eqnlist]}];
    ,{i,1,Max[Length@vars,OptionValue[minEqns]]}];
    
    Solve[
        #==0&/@DeleteCases[Collect[#,vars,Collect[#,OptionValue[expReplacement][[All,1]]]&]&/@Flatten[eqnlist],0]
        ,vars
    ]/.Rule[symb_,something_]:> Rule[symb,Collect[something,vars,Factor]]
];



(* ::Subsection::Closed:: *)
(*Schouten identities*)


SchoutenContract[t1_,t2_,t3_,t4_]:=Module[{T1,T2,T3,T4,\[Chi]1=Symbol["\[Chi]1"],\[Chi]2=Symbol["\[Chi]2"],\[Chi]3=Symbol["\[Chi]3"],\[Chi]4=Symbol["\[Chi]4"]},

{T1,T2,T3,T4}=({t1,t2,t3,t4})/.{Global`\[Eta]01->\[Chi]1,Global`\[Eta]02->\[Chi]2,Global`\[Eta]03->\[Chi]3,Global`\[Eta]04->\[Chi]4};
remove\[Chi]s[NonCommutativeMultiply@@{T1,T2,T3,T4},d\[Theta]d\[Eta]Rule[d\[Theta]d\[Eta]Rule[#,\[Chi]1,\[Chi]2],\[Chi]3,\[Chi]4]&]+
remove\[Chi]s[NonCommutativeMultiply@@{T1,T2,T3,T4},d\[Theta]d\[Eta]Rule[d\[Theta]d\[Eta]Rule[#,\[Chi]3,\[Chi]1],\[Chi]2,\[Chi]4]&]+
remove\[Chi]s[NonCommutativeMultiply@@{T1,T2,T3,T4},d\[Theta]d\[Eta]Rule[d\[Theta]d\[Eta]Rule[#,\[Chi]1,\[Chi]4],\[Chi]2,\[Chi]3]&]
];

SchoutenContractb[t1_,t2_,t3_,t4_]:=Module[{T1,T2,T3,T4,\[Chi]b1=Symbol["\[Chi]b1"],\[Chi]b2=Symbol["\[Chi]b2"],\[Chi]b3=Symbol["\[Chi]b3"],\[Chi]b4=Symbol["\[Chi]b4"]},

{T1,T2,T3,T4}=({t1,t2,t3,t4})/.{Global`\[Eta]b01->\[Chi]b1,Global`\[Eta]b02->\[Chi]b2,Global`\[Eta]b03->\[Chi]b3,Global`\[Eta]b04->\[Chi]b4};
remove\[Chi]s[NonCommutativeMultiply@@{T1,T2,T3,T4},d\[Theta]bd\[Eta]bRule[d\[Theta]bd\[Eta]bRule[#,\[Chi]b1,\[Chi]b2],\[Chi]b3,\[Chi]b4]&]+
remove\[Chi]s[NonCommutativeMultiply@@{T1,T2,T3,T4},d\[Theta]bd\[Eta]bRule[d\[Theta]bd\[Eta]bRule[#,\[Chi]b3,\[Chi]b1],\[Chi]b2,\[Chi]b4]&]+
remove\[Chi]s[NonCommutativeMultiply@@{T1,T2,T3,T4},d\[Theta]bd\[Eta]bRule[d\[Theta]bd\[Eta]bRule[#,\[Chi]b1,\[Chi]b4],\[Chi]b2,\[Chi]b3]&]
];


GenerateSchouten[{terms__},{termsb__}]:=Module[{list,schout,listb,schoutb,ctimes,schoutrule},

list  = {#1/.Global`\[Eta]00  -> Global`\[Eta]01, #2/.Global`\[Eta]00  -> Global`\[Eta]02, #3/.Global`\[Eta]00  -> Global`\[Eta]03, #4/.Global`\[Eta]00  -> Global`\[Eta]04 }&@@@Subsets[{terms},{4}];
listb = {#1/.Global`\[Eta]b00 -> Global`\[Eta]b01,#2/.Global`\[Eta]b00 -> Global`\[Eta]b02,#3/.Global`\[Eta]b00 -> Global`\[Eta]b03,#4/.Global`\[Eta]b00 -> Global`\[Eta]b04}&@@@Subsets[{termsb},{4}];
schout  = SchoutenContract@@@list;
schoutb = SchoutenContractb@@@listb;
schout  = If[Head[#]===Times,#/.x_Symbol?xsqQ->1,#]&/@Factor/@schout;
schoutb = If[Head[#]===Times,#/.x_Symbol?xsqQ->1,#]&/@Factor/@schoutb;

SetAttributes[ctimes,Orderless];
ctimes[-1,a__]:=-ctimes[a];

Join[Solve[0==#&/@((ReorderNCM/@schout) /.Times->ctimes)][[1]]/.ctimes->Times,
     Solve[0==#&/@((ReorderNCM/@schoutb)/.Times->ctimes)][[1]]/.ctimes->Times]
];


(* ::Section::Closed:: *)
(*Two and three-point functions*)


(* ::Subsection::Closed:: *)
(*From CFTs4D package*)


oglue["x",i_,j_]:=If[j>i,glue["x",i,j], - glue["x",j,i]];
oglue["x",i_,j_,"sq"]:=If[j>i,glue["x",i,j,"sq"], glue["x",j,i,"sq"]];
oglue["x",i_,j_,"v",\[Mu]_]:=If[j>i,glue["x",i,j,"v"][\[Mu]], - glue["x",j,i,"v"][\[Mu]]];


removex13[x12_,x23_,{x13_,x13v_,x13sq_}]:={x13 -> x12 + x23, x13sq -> sq[x12]+2 glue[x12,x23]+sq[x23], x13v[m_] :> glue[x12,"v"][m] + glue[x23,"v"][m]};
removex12[x13_,x23_,{x12_,x12v_,x12sq_}]:={x12 -> x13 - x23, x12sq -> sq[x13]-2 glue[x13,x23]+sq[x23], x12v[m_] :> glue[x13,"v"][m] - glue[x23,"v"][m]};
removex23[x12_,x13_,{x23_,x23v_,x23sq_}]:={x23 -> x13 - x12, x23sq -> sq[x13]-2 glue[x12,x13]+sq[x12], x23v[m_] :> glue[x13,"v"][m] - glue[x12,"v"][m]};


(*Xij*)
myspM[i_,j_]:=oglue["x",i,j,"sq"];
(*I^ij*)
myinvSbSnorm[{i_,j_}, {}]:=d[glue["\[Eta]",j],oglue["x",i,j],glue["\[Eta]b",i]];
(*J^k_ij*)
myinvSbSnorm[{k_,k_}, {i_, j_}]:=(oglue["x",i,k,"sq"] oglue["x",j,k,"sq"])/oglue["x",i,j,"sq"] (d[glue["\[Eta]",k],oglue["x",i,k],glue["\[Eta]b",k]]/oglue["x",i,k,"sq"]-d[glue["\[Eta]",k],oglue["x",j,k],glue["\[Eta]b",k]]/oglue["x",j,k,"sq"]);
(*I^ij_kl*)
myinvSbSnorm[{i_,j_}, {k_, l_}]/;i=!=j:=1/(2 oglue["x",k,l,"sq"]) (
                                        (oglue["x",i,k,"sq"]d[glue["\[Eta]",j],oglue["x",j,l],glue["\[Eta]b",i]]-oglue["x",i,l,"sq"]d[glue["\[Eta]",j],oglue["x",j,k],glue["\[Eta]b",i]])+
                                        (oglue["x",j,k,"sq"]d[glue["\[Eta]",j],oglue["x",i,l],glue["\[Eta]b",i]]-oglue["x",j,l,"sq"]d[glue["\[Eta]",j],oglue["x",i,k],glue["\[Eta]b",i]])-
                                         oglue["x",i,j,"sq"]d[glue["\[Eta]",j],oglue["x",k,l],glue["\[Eta]b",i]]-oglue["x",k,l,"sq"]d[glue["\[Eta]",j],oglue["x",i,j],glue["\[Eta]b",i]]+
                                         2I d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],oglue["x",i,k,"v",\[Kappa]],oglue["x",l,j,"v",SpinorAlgebra`\[Lambda]],oglue["x",l,k,"v",\[Mu]],glue["\[Eta]",j],\[Sigma][\[Nu]],glue["\[Eta]b",i]]);
(*K^ij_k*)
myinvSSnorm[{i_,j_},{k_}]:=1/2 Sqrt[oglue["x",i,j,"sq"]/(oglue["x",i,k,"sq"] oglue["x",j,k,"sq"])] (-4 d[oglue["x",i,k,"v",\[Mu]],oglue["x",j,k,"v",\[Nu]],glue["\[Eta]",i],\[Sigma][\[Mu],\[Nu]],glue["\[Eta]",j]]+d[glue["\[Eta]",i],glue["\[Eta]",j]] (-oglue["x",i,j,"sq"]+oglue["x",i,k,"sq"]+oglue["x",j,k,"sq"]));
(*Kb^ij_k*)
myinvSbSbnorm[{i_,j_},{k_}]:=1/2 Sqrt[oglue["x",i,j,"sq"]/(oglue["x",i,k,"sq"] oglue["x",j,k,"sq"])] (-4 d[oglue["x",i,k,"v",\[Mu]],oglue["x",j,k,"v",\[Nu]],glue["\[Eta]b",i],\[Sigma]b[\[Mu],\[Nu]],glue["\[Eta]b",j]]+d[glue["\[Eta]b",i],glue["\[Eta]b",j]] (-oglue["x",i,j,"sq"]+oglue["x",i,k,"sq"]+oglue["x",j,k,"sq"]));
(*L^i_jkl*)
myinvSSnorm[{i_,i_},{j_,k_,l_}]:=2/Sqrt[oglue["x",j,k,"sq"]oglue["x",k,l,"sq"]oglue["x",l,j,"sq"]] (
                                 oglue["x",i,j,"sq"]d[oglue["x",k,l,"v",\[Mu]],oglue["x",i,l,"v",\[Nu]],glue["\[Eta]",i],\[Sigma][\[Mu],\[Nu]],glue["\[Eta]",i]]+
                                 oglue["x",i,k,"sq"]d[oglue["x",l,j,"v",\[Mu]],oglue["x",i,j,"v",\[Nu]],glue["\[Eta]",i],\[Sigma][\[Mu],\[Nu]],glue["\[Eta]",i]]+
                                 oglue["x",i,l,"sq"]d[oglue["x",j,k,"v",\[Mu]],oglue["x",i,k,"v",\[Nu]],glue["\[Eta]",i],\[Sigma][\[Mu],\[Nu]],glue["\[Eta]",i]]);
(*Lb^i_jkl*)
myinvSbSbnorm[{i_,i_},{j_,k_,l_}]:=2/Sqrt[oglue["x",j,k,"sq"]oglue["x",k,l,"sq"]oglue["x",l,j,"sq"]] (
                                 oglue["x",i,j,"sq"]d[oglue["x",k,l,"v",\[Mu]],oglue["x",i,l,"v",\[Nu]],glue["\[Eta]b",i],\[Sigma]b[\[Mu],\[Nu]],glue["\[Eta]b",i]]+
                                 oglue["x",i,k,"sq"]d[oglue["x",l,j,"v",\[Mu]],oglue["x",i,j,"v",\[Nu]],glue["\[Eta]b",i],\[Sigma]b[\[Mu],\[Nu]],glue["\[Eta]b",i]]+
                                 oglue["x",i,l,"sq"]d[oglue["x",j,k,"v",\[Mu]],oglue["x",i,k,"v",\[Nu]],glue["\[Eta]b",i],\[Sigma]b[\[Mu],\[Nu]],glue["\[Eta]b",i]]);


FromCFTs4D[expr_]:=expr/.{CFTs4D`spM        -> myspM,
                          CFTs4D`invSbSnorm -> myinvSbSnorm,
                          CFTs4D`invSSnorm  -> myinvSSnorm,
                          CFTs4D`invSbSbnorm-> myinvSbSbnorm};


(* ::Subsection::Closed:: *)
(*Non supersymmetric two and three-point functions*)


Options[NonSUSY3pf]={points -> Global`x12};
NonSUSY3pf::point="The allowed values for the option points are x12, x23 or x13. Defaulting to x12.";
NonSUSY3pf[{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_},{{l1_,lb1_},{l2_,lb2_},{l3_,lb3_}},OptionsPattern[]]:=
    FromCFTs4D[CFTs4D`n3CorrelationFunction[{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3},{{l1,lb1},{l2,lb2},{l3,lb3}}]]/.Switch[
    OptionValue[points]
    ,Global`x12, removex12[Global`x13,Global`x23,{Global`x12,Global`x12v,Global`x12sq}]
    ,Global`x13, removex13[Global`x12,Global`x23,{Global`x13,Global`x13v,Global`x13sq}]
    ,Global`x23, removex23[Global`x12,Global`x13,{Global`x23,Global`x23v,Global`x23sq}]
    ,_,   Message[NonSUSY3pf::point];
          removex12[Global`x13,Global`x23,{Global`x12,Global`x12v,Global`x12sq}]];


NonSUSY2pf[\[CapitalDelta]_,{j_,jb_}]:=Module[{m\[Eta]1,m\[Eta]2,m\[Eta]b1,m\[Eta]b2,mx12},
    m\[Eta]1  = Global`\[Eta]1;
    m\[Eta]2  = Global`\[Eta]2;
    m\[Eta]b1 = Global`\[Eta]b1;
    m\[Eta]b2 = Global`\[Eta]b2;
    mx12 = Global`x12;
    
    (I)^(j+jb) (d[m\[Eta]1,mx12,m\[Eta]b2]^j d[m\[Eta]2,mx12,m\[Eta]b1]^jb)/sq[mx12]^(\[CapitalDelta]+(j+jb)/2)
];


(* ::Section::Closed:: *)
(*End of package*)


End[];

EndPackage[];
