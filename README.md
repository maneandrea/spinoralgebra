Spinor Algebra
====

Mathematica tool to perform computations in four dimensional superspace using the index-free formalism.
It includes rules for the algebra of Pauli matrices and the anti-commutativity of Grassmann variables θ, a function to compare expressions and several differential operators.
It can generate superconformal two- and three-point functions in N = 1 superspace.

This program is deprecated: use Superspace4d as a replacement (some features have not been ported yet).

Getting started
---

###  Requisites

This package has been developed and tested on *Mathematica 11.3*.

### Installing

The package can be loaded in a Mathematica session by runninng
~~~
<<SpinorAlgebra` 
~~~

after putting the file *SpinorAlgebra.wl* into a folder contained in `$Path`. Otherwise it is also possible to put SpinorAlgebra.wl in any folder and load it with
~~~
Get["<path-to-folder>/SpinorAlgebra.wl"];
~~~

Documentation
---

The file *SpinorAlgebra_Documentation.nb* is a Mathematica notebook that explains all the features present in the package along with some simple examples.
Furthermore, for every function `f` one can generate a help message by calling `?f`.

Notes
---
This package was used in the following papers

[1] *Implications of ANEC for SCFTs in four dimensions*, Andrea Manenti, Andreas Stergiou and Alessandro Vichi [1905.09293](https://arxiv.org/abs/1905.09293),

[2] *R-current three-point functions in 4d N=1 superconformal theories*, Andrea Manenti, Andreas Stergiou and Alessandro Vichi [1804.09717](https://arxiv.org/abs/1804.09717),

and introduced in

[3] *Differential operators for superconformal correlation functions*, Andrea Manenti [1910.12869](https://arxiv.org/abs/1910.12869).

Some of the features of this package were tested with another (unpublished) package developed by Andreas Stergiou, where the various reduction rules were implemented in a different way.

The function for generating three-point tensor structures relies on the package [CFTs4D](https://gitlab.com/bootstrapcollaboration/CFTs4D).

[4] *General Bootstrap Equations in 4D CFTs*, Gabriel Francisco Cuomo, Denis Karateev, Petr Kravchuk [1705.05401](https://arxiv.org/abs/1705.05401).
