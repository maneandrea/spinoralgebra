(* ::Package:: *)

(* ::Title:: *)
(*Spinor Algebra*)


(* ::Section:: *)
(*Beginning of package and function descriptions*)


(* ::Subsection:: *)
(*Usage messages*)


(*To give them the Global` context*)
{\[Theta]1,\[Theta]2,\[Theta]3,\[Theta]4,\[Theta]5,\[Theta]6,\[Theta]b1,\[Theta]b2,\[Theta]b3,\[Theta]b4,\[Theta]b5,\[Theta]b6,\[Theta]1sq,\[Theta]b1sq,\[Theta]2sq,\[Theta]b2sq,\[Theta]3sq,\[Theta]b3sq,\[Theta]4sq,\[Theta]b4sq,\[Theta]5sq,\[Theta]b5sq,\[Theta]6sq,\[Theta]b6sq,
 \[Eta]1,\[Eta]2,\[Eta]3,\[Eta]4,\[Eta]5,\[Eta]5,\[Eta]b1,\[Eta]b2,\[Eta]b3,\[Eta]b4,\[Eta]b5,\[Eta]b6,\[Eta]999,\[Eta]899,\[Eta]b999,\[Eta]b899,\[Eta]11,\[Eta]22,\[Eta]b11,\[Eta]b22,
 x12,x13,x23,x12sq,x13sq,x23sq,
 x12x23,x12x13,x23x13,
 x23x12,x13x12,x13x23,
 x14,x24,x34,x14sq,x24sq,x34sq};


BeginPackage["SpinorAlgebra`"];

(*Usage of the functions*)
FN::usage="FN[\!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] returns the fermion number (0 if boson, 1 if fermion) of \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\). Only terms that have an odd number of \[Theta] or \[Theta]b are fermionic.";
u::usage="u[\!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Operator to apply on functions to make them act on and return in compact notation: \!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)[\[LeftAngleBracket]user notation\[RightAngleBracket]] -> u[\!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\[LeftAngleBracket]compact notation\[RightAngleBracket]].";
uu::usage="uu[\!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Operator to apply on functions to make them act on and return in compact notation. Similar to \!\(\*StyleBox[\"u\",\nFontFamily->\"DejaVu Sans Mono\"]\) but applies the transformation to the first two arguments.";
SplitMonomials::usage="SplitMonomials[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Transforms from compact notation to user notation: e.g. \[Eta]1x12\[Theta]b2 \[Rule] d[\[Eta]1, x12, \[Theta]b2].";
WriteMonomials::usage="WriteMonomials[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Transforms from user notation to compact notation: e.g. d[\[Eta]1, x12, \[Theta]b2] \[Rule] \[Eta]1x12\[Theta]b2";
Compare::usage="Compare[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\" \",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"[\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\", \",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"options\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] equates \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) to zero and returns the constraints on the arbitrary coefficients contained in \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).\nThe options are:\nexpReplacement ({}): Random replacements for common terms in the exponents of e.g. x12sq^\[CapitalDelta].\nvariables (ConstantsList): Usual set of symbols treated as unknowns.\nverbose (0): Prints more information about the steps of the computation (values 0,1,2).\nminEqns (-1) the number of equation generated is the max of minEqns and the number of variables.";
ReorderNCM::usage="ReorderNCM[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Orders canonically the factors of NonCommutativeMultiply (**).";
ConstQ::usage="ConstQ[\!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Returns True if \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) is considered a constant and False otherwise.";
makeConst::usage="makeConst[\!\(\*StyleBox[\"symbols\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Makes all Symbols or Expressions in \!\(\*StyleBox[\"symbols\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) return True when given to ConstQ.";
ComplexConj::usage="ComplexConj[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the complex conjugate of the expression \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\). Constants are left unchanged (i.e. considered as Real).";
SetToZero::usage="SetToZero[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"vars\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Sets \!\(\*StyleBox[\"vars\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\" \",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)to zero in the expression \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\). If \!\(\*StyleBox[\"vars\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\" \",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)is a list sets all the variables in the list to zero.";
Reduction::usage="Reduction[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Reduces the expression \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) by applying rules such as \!\(\*SuperscriptBox[\(\[Theta]\), \(\[Alpha]\)]\)...\!\(\*SuperscriptBox[\(\[Theta]\), \(\[Beta]\)]\) = -1/2\!\(\*SuperscriptBox[\(\[CurlyEpsilon]\), \(\[Alpha]\[Beta]\)]\)\!\(\*SuperscriptBox[\(\[Theta]\), \(2\)]\)... . Puts to zero terms with more than two identical \[Theta]s.";
FPower::usage="FPower[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"p\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] for positive integer \!\(\*StyleBox[\"p\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) it computes the power \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"^\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"p\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) by reducing terms with more than one \[Theta] using the function Reduction.\nFPower[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"around\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"p\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"n\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] expands the power \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"^\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"p\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) around \!\(\*StyleBox[\"around\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) up to order \!\(\*StyleBox[\"n\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) reducing terms with more than one \[Theta] using the function Reduction.";
FTaylor::usage="FTaylor[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"n\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] Taylor expands \!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] around 0 up to order \!\(\*StyleBox[\"n\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
ChiralDm::usage="ChiralDm[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] = (- \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Theta]\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta]\)]\) + \[ImaginaryI] \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta]\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\)\[Theta]b) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\). It is the chiral derivative operator \!\(\*SubscriptBox[\(D\), \(\[Alpha]\)]\) with index contracted (yields zero on antichiral fields).";
ChiralDbm::usage="ChiralDbm[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] = (- \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Theta]b\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta]b\)]\) - \[ImaginaryI] \[Theta]\!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta]b\)]\)) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\). It is the antichiral derivative operator \!\(\*SubscriptBox[OverscriptBox[\(D\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\) with index contracted (yields zero on chiral fields).";
ChiralDp::usage="ChiralDp[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] = ( \[Eta]\!\(\*SubscriptBox[\(\[PartialD]\), \(\[Theta]\)]\) + \[ImaginaryI] \[Eta]\!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\)\[Theta]b) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\). It is the chiral derivative operator \!\(\*SubscriptBox[\(D\), \(\[Alpha]\)]\) with index symmetrized (yields zero on antichiral fields).";
ChiralDbp::usage="ChiralDbp[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] = (- \[Eta]b\!\(\*SubscriptBox[\(\[PartialD]\), \(\[Theta]b\)]\) - \[ImaginaryI] \[Theta]\!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\)\[Eta]b) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\). It is the antichiral derivative operator \!\(\*SubscriptBox[OverscriptBox[\(D\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\) with index symmetrized (yields zero on chiral fields).";
ChiralQm::usage="ChiralQm[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] = (- \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Theta]\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta]\)]\) - \[ImaginaryI] \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta]\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\)\[Theta]b) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\). It is the chiral derivative operator \!\(\*SubscriptBox[\(Q\), \(\[Alpha]\)]\) with index contracted ([Q,D} = 0).";
ChiralQbm::usage="ChiralQbm[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] = (- \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Theta]b\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta]b\)]\) + \[ImaginaryI] \[Theta]\!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta]b\)]\)) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\). It is the antichiral derivative operator \!\(\*SubscriptBox[OverscriptBox[\(Q\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\) with index contracted ([\!\(\*OverscriptBox[\(Q\), \(_\)]\),\!\(\*OverscriptBox[\(D\), \(_\)]\)} = 0).";
ChiralQp::usage="ChiralQp[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] = ( \[Eta]\!\(\*SubscriptBox[\(\[PartialD]\), \(\[Theta]\)]\) - \[ImaginaryI] \[Eta]\!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\)\[Theta]b) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\). It is the chiral derivative operator \!\(\*SubscriptBox[\(Q\), \(\[Alpha]\)]\) with index symmetrized ([Q,D} = 0).";
ChiralQbp::usage="ChiralQbp[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] = (- \[Eta]b\!\(\*SubscriptBox[\(\[PartialD]\), \(\[Theta]b\)]\) + \[ImaginaryI] \[Theta]\!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\)\[Eta]b) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\). It is the antichiral derivative operator \!\(\*SubscriptBox[OverscriptBox[\(Q\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\) with index symmetrized ([\!\(\*OverscriptBox[\(Q\), \(_\)]\),\!\(\*OverscriptBox[\(D\), \(_\)]\)} = 0).";
ChiralDsq::usage="ChiralDsq[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes \!\(\*SuperscriptBox[\(D\), \(\[Alpha]\)]\)\!\(\*SubscriptBox[\(D\), \(\[Alpha]\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\" \",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)where \!\(\*SubscriptBox[\(D\), \(\[Alpha]\)]\) is the chiral derivative.";
ChiralDbsq::usage="ChiralDbsq[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\),\!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes \!\(\*SubscriptBox[OverscriptBox[\(D\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\)\!\(\*SuperscriptBox[OverscriptBox[\(D\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\" \",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)where \!\(\*SubscriptBox[OverscriptBox[\(D\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\) is the antichiral derivative.";
ChiralQsq::usage="ChiralQsq[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes \!\(\*SuperscriptBox[\(Q\), \(\[Alpha]\)]\)\!\(\*SubscriptBox[\(Q\), \(\[Alpha]\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\" \",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)where \!\(\*SubscriptBox[\(Q\), \(\[Alpha]\)]\) is the Q derivative.";
ChiralQbsq::usage="ChiralQbsq[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\),\!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes \!\(\*SubscriptBox[OverscriptBox[\(Q\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\)\!\(\*SuperscriptBox[OverscriptBox[\(Q\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\" \",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)where \!\(\*SubscriptBox[OverscriptBox[\(Q\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\) is the \!\(\*OverscriptBox[\(Q\), \(_\)]\) derivative.";
Chiral\[ScriptCapitalD]m::usage="Chiral\[ScriptCapitalD]\[NegativeThinSpace]m[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] alias of ChiralQm.";
Chiral\[ScriptCapitalD]bm::usage="Chiral\[ScriptCapitalD]\[NegativeThinSpace]bm[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] alias of ChiralQbm.";
Chiral\[ScriptCapitalD]p::usage="Chiral\[ScriptCapitalD]\[NegativeThinSpace]p[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] alias of ChiralQp.";
Chiral\[ScriptCapitalD]bp::usage="Chiral\[ScriptCapitalD]\[NegativeThinSpace]bp[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] alias of ChiralQbp.";
Chiral\[ScriptCapitalQ]m::usage="Chiral\[ScriptCapitalQ]\[NegativeThinSpace]m[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] alias of ChiralDm.";
Chiral\[ScriptCapitalQ]bm::usage="Chiral\[ScriptCapitalQ]\[NegativeThinSpace]bm[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] alias of ChiralDbm.";
Chiral\[ScriptCapitalQ]p::usage="Chiral\[ScriptCapitalQ]\[NegativeThinSpace]p[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] alias of ChiralDp.";
Chiral\[ScriptCapitalQ]bp::usage="Chiral\[ScriptCapitalQ]\[NegativeThinSpace]bp[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] alias of ChiralDbp.";
Nice::usage="Nice[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Displays an expression in a nicer form. Only for reading.";
UnNice::usage="UnNice[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)], Reverts the Nice function.";
d::usage="d[\!\(\*StyleBox[\"a\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"...\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Represents the fully contracted monomial \!\(\*StyleBox[\"ab...\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), where, for example,  \!\(\*SubscriptBox[\(\[Eta]\), \(1\)]\)\!\(\*SubscriptBox[\(\[Eta]\), \(2\)]\) = \!\(\*SubsuperscriptBox[\(\[Eta]\), \(1\), \(\[Alpha]\)]\)\!\(\*SubscriptBox[\(\[Eta]\), \(2  \[Alpha]\)]\),  \!\(\*SubscriptBox[\(\[Eta]\), \(1\)]\)\!\(\*SubscriptBox[\(x\), \(12\)]\)\!\(\*SubscriptBox[OverscriptBox[\(\[Eta]\), \(_\)], \(2\)]\) = \!\(\*SubsuperscriptBox[\(\[Eta]\), \(1\), \(\[Alpha]\)]\)\!\(\*SubscriptBox[\(x\), \(12  \[Alpha] \*OverscriptBox[\(\[Alpha]\), \(.\)]\)]\)\!\(\*SuperscriptBox[OverscriptBox[\(\[Eta]\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\), etc ...";
glue::usage="glue[\!\(\*StyleBox[\"a\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"...\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Returns a Symbol obtained concatenating \!\(\*StyleBox[\"ab\[CenterDot]\[CenterDot]\[CenterDot]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) .";
\[Eta]d\[Theta]::usage="\[Eta]d\[Theta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \[Eta]\!\(\*SubscriptBox[\(\[PartialD]\), \(\[Theta]\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
\[Eta]d\[Eta]::usage="\[Eta]d\[Eta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(\[Eta]\), \(1\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), SubscriptBox[\(\[Eta]\), \(2\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
\[Eta]bd\[Theta]b::usage="\[Eta]bd\[Theta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*OverscriptBox[\(\[Eta]\), \(_\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Theta]\), \(_\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
\[Eta]bd\[Eta]b::usage="\[Eta]bd\[Eta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[OverscriptBox[\(\[Eta]\), \(_\)], \(1\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), SubscriptBox[OverscriptBox[\(\[Eta]\), \(_\)], \(2\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
xdx::usage="xdx[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(x\), \(1\)]\)\[CenterDot]\!\(\*SubscriptBox[\(\[PartialD]\), SubscriptBox[\(x\), \(2\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
d\[Eta]dx\[Theta]b::usage="d\[Eta]dx\[Theta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta]\)]\) x \!\(\*OverscriptBox[\(\[Theta]\), \(_\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
d\[Theta]dx\[Eta]b::usage="d\[Theta]dx\[Eta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Theta]\)]\) x \!\(\*OverscriptBox[\(\[Eta]\), \(_\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
\[Eta]dxd\[Theta]b::usage="\[Eta]dxd\[Theta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \[Eta] x \!\(\*SubscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Theta]\), \(_\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
\[Theta]dxd\[Eta]b::usage="\[Theta]dxd\[Eta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \[Theta] x \!\(\*SubscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Eta]\), \(_\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
\[Theta]bd\[Eta]b::usage="\[Theta]bd\[Eta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*OverscriptBox[\(\[Theta]\), \(_\)]\) \!\(\*SubscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Eta]\), \(_\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
\[Theta]d\[Eta]::usage="\[Theta]d\[Eta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \[Theta] \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta]\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
d\[Theta]d\[Eta]::usage="d\[Theta]d\[Eta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Theta]\)]\) \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta]\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
d\[Eta]d\[Eta]::usage="d\[Eta]d\[Eta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), SubscriptBox[\(\[Eta]\), \(1\)]]\)\!\(\*SubscriptBox[\(\[PartialD]\), SubscriptBox[\(\[Eta]\), \(2\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
d\[Theta]bd\[Eta]b::usage="d\[Theta]bd\[Eta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Theta]\), \(_\)]]\) \!\(\*SubscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Eta]\), \(_\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
d\[Eta]bd\[Eta]b::usage="d\[Eta]bd\[Eta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), SubscriptBox[OverscriptBox[\(\[Eta]\), \(_\)], \(1\)]]\) \!\(\*SubscriptBox[\(\[PartialD]\), SubscriptBox[OverscriptBox[\(\[Eta]\), \(_\)], \(2\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
\[Eta]dx\[Eta]b::usage="\[Eta]dx\[Eta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \[Eta] \!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\) \!\(\*OverscriptBox[\(\[Eta]\), \(_\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
d\[Eta]dxd\[Eta]b::usage="d\[Eta]dxd\[Eta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta]\)]\) \!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\) \!\(\*SubscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Eta]\), \(_\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
d\[Theta]dxd\[Eta]b::usage="d\[Theta]dxd\[Eta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Theta]\)]\) \!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\) \!\(\*SubscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Eta]\), \(_\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
d\[Eta]dxd\[Theta]b::usage="d\[Eta]dxd\[Theta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta]\)]\) \!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\) \!\(\*SubscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Theta]\), \(_\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
d\[Theta]dxd\[Theta]b::usage="d\[Theta]dxd\[Theta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Theta]\)]\) \!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\) \!\(\*SubscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Theta]\), \(_\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
d\[Eta]xd\[Eta]b::usage="d\[Eta]xd\[Eta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta]\)]\) x \!\(\*SubscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Eta]\), \(_\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
dxsq::usage="dxsq[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SuperscriptBox[SubscriptBox[\(\[PartialD]\), \(x\)], \(2\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
d\[Theta]dx\[Theta]b::usage="d\[Theta]dx\[Theta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Theta]\)]\) \!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\) \!\(\*OverscriptBox[\(\[Theta]\), \(_\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
\[Theta]dxd\[Theta]b::usage="\[Theta]dxd\[Theta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \[Theta] \!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\) \!\(\*SubscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Theta]\), \(_\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
d\[Eta]dx\[Eta]b::usage="d\[Eta]dx\[Eta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta]\)]\) \!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\) \!\(\*OverscriptBox[\(\[Eta]\), \(_\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
\[Eta]dxd\[Eta]b::usage="\[Eta]dxd\[Eta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the derivative \[Eta] \!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\) \!\(\*SubscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Eta]\), \(_\)]]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
SchoutenContract::usage="SchoutenContract[\!\(\*StyleBox[\"a1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"a2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"a3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"a4\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)], where \!\(\*StyleBox[\"a1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) is a monomial containing \[Eta]01, \!\(\*StyleBox[\"a2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) is a monomial containing \[Eta]02 and so on, returns a Schouten identity for the terms contained in \!\(\*StyleBox[\"a1,a2,a3,a4\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) (without the \[Eta]0i).\nFor example ShoutenContract[\[Eta]01\[Eta]1,\[Eta]02\[Eta]2,\[Eta]03\[Eta]3,\[Eta]04\[Eta]4] = \[Eta]1\[Eta]4 \[Eta]2\[Eta]3-\[Eta]1\[Eta]3 \[Eta]2\[Eta]4+\[Eta]1\[Eta]2 \[Eta]3\[Eta]4 = 0.";
SchoutenContractb::usage="SchoutenContractb[\!\(\*StyleBox[\"a1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"a2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"a3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"a4\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)], where \!\(\*StyleBox[\"a1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) is a monomial containing \[Eta]b01, \!\(\*StyleBox[\"a2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) is a monomial containing \[Eta]b02 and so on, returns a Schouten identity for the terms contained in \!\(\*StyleBox[\"a1,a2,a3,a4\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) (without the \[Eta]b0i).\nFor an example see ?ShoutenContract";
GenerateSchouten::usage="GenerateSchouten[{\!\(\*StyleBox[\"terms\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"termsb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}] generates a list of replacement rules implementing the Schouten identities obtained by contracting the terms in \!\(\*StyleBox[\"terms\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with the antisymmetrized undotted \[Epsilon] \[Epsilon] and the terms in \!\(\*StyleBox[\"termsb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with the antisymmetrized dotted \[Epsilon] \[Epsilon].\nThe terms in \!\(\*StyleBox[\"terms\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) must be objects with one undotted index contracted with the placeholder \[Eta]00 and those in \!\(\*StyleBox[\"termsb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) must be objects with one dotted index contracted with \[Eta]b00.";
Replace\[Eta]ToAny::usage="ReplaceToAny[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"any\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] replaces all \*StyleBox[\(\!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)s\)] in the expression \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with the quantity \!\(\*StyleBox[\"any\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), which must have a free index. The parameter \!\(\*StyleBox[\"any\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) is given as follows: it must be a List of pairs. Each pair is a term of the expression to be substituted and its entries consist in one-argument functions. The first entry implements the substitution for \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with the lower index and the second for \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with the upper index, the index being the argument passed to the function. Each function must return a List or a Product of terms in explicit index notation.";
Contraction::usage="Contraction[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] contracts all explicit indices of the expression \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), which must be given in internal notation.";
up::usage="up[\!\(\*StyleBox[\"obj\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"a\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] represents an object \!\(\*StyleBox[\"obj\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with one upper index \!\(\*StyleBox[\"a\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) in the internal notation.";
low::usage="low[\!\(\*StyleBox[\"obj\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"a\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] represents an object \!\(\*StyleBox[\"obj\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with one lower index \!\(\*StyleBox[\"a\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) in the internal notation.";
v::usage="v[\!\(\*StyleBox[\"obj\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"m\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] represents an object \!\(\*StyleBox[\"obj\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with one Lorentz index \!\(\*StyleBox[\"m\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) in the internal notation.";
sq::usage="sq[\!\(\*StyleBox[\"sym\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] returns a Symbol \!\(\*StyleBox[\"sym\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)sq representing the square of the given Symbol.";
fastApply::usage="fastApply[\!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] collects the various monomials in \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), applies \!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) on them and adds the result to the definition of \!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) (so that the next application within the same kernel session will be faster).";
fastReduction::usage="fastReduction[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] collects the various monomials in \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), applies Reduction on them and adds the result to the definition of Reduction (so that the next application within the same kernel session will be faster).";
Unprotect[NonCommutativeMultiply];
NonCommutativeMultiply::usage="\!\(\*StyleBox[\"a\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)**\!\(\*StyleBox[\"b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)**\!\(\*StyleBox[\"c\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) ...  is a non commutative product to be used for terms that have odd fermion number. This product automatically takes out bosonic factors.";
Protect[NonCommutativeMultiply];
FromCFTs4D::usage="FromCFTs4D[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] takes an expression \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) in embedding space from the package CFTs4D and translates it into this package's user notation.";
NonSUSY3pf::usage="NonSUSY3pf[{\!\(\*StyleBox[\"\[CapitalDelta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[CapitalDelta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[CapitalDelta]3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {{\!\(\*StyleBox[\"j1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"j2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"j3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}} \!\(\*StyleBox[\"[, points]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] returns a non-supersymmetric three-point function of the operators with conformal dimensions \!\(\*StyleBox[\"\[CapitalDelta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[CapitalDelta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) and \!\(\*StyleBox[\"\[CapitalDelta]3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) and spins (\!\(\*StyleBox[\"j1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)), (\!\(\*StyleBox[\"j2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)) and (\!\(\*StyleBox[\"j3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)).\nThis is obtained by translating to user notation the output of CFTs4D`n3CorrelationFunction.\nWith the option \!\(\*StyleBox[\"points\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) the user can specify which \!\(\*SubscriptBox[\(x\), \(ij\)]\) must be expressed in terms of the others. E.g. \!\(\*StyleBox[\"points \[Rule] \",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) x12 (default) expresses the result as a function of x23 and x13.";
NonSUSY2pf::usage="NonSUSY2pf[\!\(\*StyleBox[\"\[CapitalDelta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), {\!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}] returns the non-supersymmetric two-point function of an operator with conformal dimension \!\(\*StyleBox[\"\[CapitalDelta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) and spin (\!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)) in user notation.";
SUSY2pf::usage="SUSY2pf[{\!\(\*StyleBox[\"q\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}] returns the supersymmetric two-point function of a superprimary with conformal dimension \!\(\*StyleBox[\"q\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)+\!\(\*StyleBox[\"qb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), R-charge 2/3(\!\(\*StyleBox[\"q\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)-\!\(\*StyleBox[\"qb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)), and spin (\!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)) in user notation (precomputed).";
SUSY3pf::usage="SUSY3pf[{{\!\(\*StyleBox[\"q1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"q2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"q3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}}, {{\!\(\*StyleBox[\"j1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"j2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"j3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}} \!\(\*StyleBox[\"[, list]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] returns a supersymmetric three-point function of the superprimaries with charges (\!\(\*StyleBox[\"q1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)), (\!\(\*StyleBox[\"q2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)) and (\!\(\*StyleBox[\"q3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)) and spins (\!\(\*StyleBox[\"j1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)), (\!\(\*StyleBox[\"j2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)) and (\!\(\*StyleBox[\"j3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)).\nIn order to obtain a non-zero result the sum of the R-charges must be 0,\[PlusMinus]1 or \[PlusMinus]2.\nIf the option \"list\" is True the output is list of lists, each containing the structures for a given order in \[CapitalTheta]3, \[CapitalTheta]b3. If False (default) the output is an expressions with arbitrary coefficients \[ScriptCapitalC][n].";
operatorNorm::usage="operatorNorm[\!\(\*StyleBox[\"string\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), {\!\(\*StyleBox[\"q\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}] returns the operator norm of the operator specified in \!\(\*StyleBox[\"string\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with charges and spin given by \!\(\*StyleBox[\"q\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) and \!\(\*StyleBox[\"jb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
coefficientD::usage="coefficientD[\!\(\*StyleBox[\"string\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), {\!\(\*StyleBox[\"q\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}] returns the coefficient of the differential operator specified in \!\(\*StyleBox[\"string\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with charges and spin given by \!\(\*StyleBox[\"q\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) and \!\(\*StyleBox[\"jb\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
separateOrders::usage="separateOrders[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] returns an Association with all orders of \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) in the Grassmann variables specified in working\[Theta]s.";
permute12::usage="permute12[\!\(\*StyleBox[\"threepf\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), {{\!\(\*StyleBox[\"q1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"q2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"q3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}}, {{\!\(\*StyleBox[\"j1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"j2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"j3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}} \!\(\*StyleBox[\"[\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\", \",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"options\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] permutes points \!\(\*SubscriptBox[\(z\), \(1\)]\) and \!\(\*SubscriptBox[\(z\), \(2\)]\) in the supersymmetric three-point function \!\(\*StyleBox[\"threepf\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) and expands it in the basis of three-point functions having charges (\!\(\*StyleBox[\"q2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)), (\!\(\*StyleBox[\"q1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)) and (\!\(\*StyleBox[\"q3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)) and spins (\!\(\*StyleBox[\"j2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)), (\!\(\*StyleBox[\"j1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)) and (\!\(\*StyleBox[\"j3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)), in this order. The output is a list of rules {\[ScriptCapitalC][1] \[Rule] ...} which can be used as a replacement on the output of SUSY3pf. The \!\(\*StyleBox[\"options\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) are those of Compare except variables.";
permuteCyclic::usage="permuteCyclic[\!\(\*StyleBox[\"threepf\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), {{\!\(\*StyleBox[\"q1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"q2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"q3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}}, {{\!\(\*StyleBox[\"j1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"j2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"j3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}} \!\(\*StyleBox[\"[\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\", \",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"options\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] cyclically permutes points \!\(\*SubscriptBox[\(z\), \(1\)]\), \!\(\*SubscriptBox[\(z\), \(2\)]\) and \!\(\*SubscriptBox[\(z\), \(3\)]\) to \!\(\*SubscriptBox[\(z\), \(2\)]\), \!\(\*SubscriptBox[\(z\), \(3\)]\) and \!\(\*SubscriptBox[\(z\), \(1\)]\) in the supersymmetric three-point function \!\(\*StyleBox[\"threepf\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) and expands it in the basis of three-point functions having charges (\!\(\*StyleBox[\"q2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)), (\!\(\*StyleBox[\"q3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)) and (\!\(\*StyleBox[\"q1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)) and spins (\!\(\*StyleBox[\"j2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)), (\!\(\*StyleBox[\"j3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)) and (\!\(\*StyleBox[\"j1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)), in this order. The output is a list of rules {\[ScriptCapitalC][1] \[Rule] ...} which can be used as a replacement on the output of SUSY3pf. The \!\(\*StyleBox[\"options\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) are those of Compare except variables.";
permuteCyclicBasis::usage="permuteCyclicBasis[\!\(\*StyleBox[\"threepf\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"Basis\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), {{\!\(\*StyleBox[\"q1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"q2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"q3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}}, {{\!\(\*StyleBox[\"j1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"j2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"j3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb3\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}} \!\(\*StyleBox[\"[\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\", \",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"options\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] like permuteCyclic but instead of letting it compute the basis of the new three-point function, it is given by the user. It has the extra option finalOutput -> True";
permute12Basis::usage="permute12[\!\(\*StyleBox[\"threepf\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"Basis\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) \!\(\*StyleBox[\"[\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\", \",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"options\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] like permute12 but instead of letting it compute the basis of the new three-point function, it is given by the user.";
fastCompare::usage="fastCompare[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\" \",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"[\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\", \",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"options\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] like Compare but does an extra step of optimization before solving the internal system of equations.";
defineD::usage="defineD[\!\(\*StyleBox[\"symbol\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"operator\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"derivativeArgs\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"multiplicativeArgs\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Assigns to the Symbol \!\(\*StyleBox[\"symbol\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) a differential operator \!\(\*StyleBox[\"symbol\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)[expr_, args__] that takes the derivative of the expression given in expr corresponding to the operator specified in \!\(\*StyleBox[\"operator\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\). The objects \[Eta], \[Theta], x that appear in \!\(\*StyleBox[\"derivativeArgs\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) are derivatives \[PartialD]/\[PartialD]\[Eta], \[PartialD]/\[PartialD]\[Theta], \[PartialD]/\[PartialD]x, while the objects \[Eta], \[Theta], x that appear in \!\(\*StyleBox[\"multiplicativeArgs\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) are multiplied as given. The parameters args__ of the differential operator are given in the order \!\(\*StyleBox[\"derivativeArgs\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"multiplicativeArgs\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
tokeepSet::usage="tokeepSet[\!\(\*StyleBox[\"toKeep\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] declares that the Grassmann variables to be kept are those in the list \!\(\*StyleBox[\"toKeep\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), sets variable tokeep accordingly";
orderexp::usage="orderexp holds the maximum order at which to expand the building blocks of the expanded correlator";
tokeep::usage="tokeep holds the Grassmann variables to be kept in the expansion. Never change its value, call tokeepSet to access this variable";
expandThreePointFunction::usage="expandThreePointFunction[{{\!\(\*StyleBox[\"q1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\),\!\(\*StyleBox[\"qb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)},{\!\(\*StyleBox[\"q2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\),\!\(\*StyleBox[\"qb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}},{{\!\(\*StyleBox[\"j1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\),\!\(\*StyleBox[\"jb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)},{\!\(\*StyleBox[\"j2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\),\!\(\*StyleBox[\"jb2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}},\!\(\*StyleBox[\"t\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)], given the reduced three-point function \!\(\*StyleBox[\"t\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with the first two operators having quantum numbers given by \!\(\*StyleBox[\"q1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"qb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), etc... it returns the full three-point function expanded in superspace to the order given by orderexp and keeping the Grassmann variables in tokeep";
removex::usage="removex[\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\),\!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] removes the vector x\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) in the three-point function \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) by expressing it in terms of the other ones. E.g. x12 becomes x13 - x23";
removeDotProd::usage="removeDotProd[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] removes the dot products of x's in the three-point function \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) in favor of squares, e.g. x12x23 becomes 1/2(x13sq - x12sq - x23sq).";
saveBuildingBlocks::usage="saveBuildingBlocks[\!\(\*StyleBox[\"path\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] saves the building blocks necessary for expanding three-point functions so that one can compute them once and for all (for a given order in the Grassmann variables).";
loadBuildingBlocks::usage="loadBuildingBlocks[\!\(\*StyleBox[\"path\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] loads the building blocks necessary for expanding three-point functions previously saved with saveBuildingBlocks";


(*Memoized version of some functions*)
fdxsq::usage="fdxsq[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] memoized version of dxsq";
f\[Theta]d\[Eta]::usage="f\[Theta]d\[Eta][\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of \[Theta]d\[Eta]";
f\[Theta]bd\[Eta]b::usage="f\[Theta]bd\[Eta]b[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]b\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of \[Theta]bd\[Eta]b";
fd\[Theta]d\[Eta]::usage="fd\[Theta]d\[Eta][\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of d\[Theta]d\[Eta]";
fd\[Theta]bd\[Eta]b::usage="fd\[Theta]bd\[Eta]b[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]b\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of d\[Theta]bd\[Eta]b";
fd\[Eta]d\[Eta]::usage="fd\[Eta]d\[Eta][\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]1\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]2\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of d\[Eta]d\[Eta]";
fd\[Eta]bd\[Eta]b::usage="fd\[Eta]bd\[Eta]b[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b1\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b2\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of d\[Eta]bd\[Eta]b";
(**)
fd\[Eta]dxd\[Eta]b::usage="fd\[Eta]dxd\[Eta]b[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of d\[Eta]dxd\[Eta]b";
f\[Eta]dxd\[Eta]b::usage="f\[Eta]dxd\[Eta]b[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of \[Eta]dxd\[Eta]b";
fd\[Eta]dx\[Eta]b::usage="fd\[Eta]dx\[Eta]b[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of d\[Eta]dx\[Eta]b";
f\[Eta]dx\[Eta]b::usage="f\[Eta]dx\[Eta]b[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of \[Eta]dx\[Eta]b";
f\[Theta]dxd\[Eta]b::usage="f\[Theta]dxd\[Eta]b[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of \[Theta]dxd\[Eta]b";
fd\[Eta]dx\[Theta]b::usage="fd\[Eta]dx\[Theta]b[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]b\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of d\[Eta]dx\[Theta]b";
fd\[Eta]xd\[Eta]b::usage="fd\[Eta]xd\[Eta]b[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of d\[Eta]xd\[Eta]b";
fd\[Theta]dx\[Theta]b::usage="fd\[Theta]dx\[Theta]b[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]b\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of d\[Theta]dx\[Theta]b";
f\[Theta]dxd\[Theta]b::usage="f\[Theta]dxd\[Theta]b[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]b\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of \[Theta]dxd\[Theta]b";
(**)
fChiralDm::usage="fChiralDm[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of ChiralDm";
fChiralDbm::usage="fChiralDbm[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of ChiralDbm";
fChiralQm::usage="fChiralQm[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of ChiralQm";
fChiralQbm::usage="fChiralQbm[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]b\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)] memoized version of ChiralQbm";


(* ::Subsection::Closed:: *)
(*Some preparatory definitions*)


(*List of symbols that should be treated as constants*)
ConstantsList={\[ScriptCapitalA],\[ScriptCapitalB],\[ScriptCapitalC],\[ScriptCapitalD],\[ScriptCapitalE],\[ScriptCapitalF],\[ScriptCapitalG],\[ScriptCapitalH],\[ScriptCapitalI],\[ScriptCapitalJ],\[ScriptCapitalK],\[ScriptCapitalL],\[ScriptCapitalM],\[ScriptCapitalN],\[ScriptCapitalO],\[ScriptCapitalP],\[ScriptCapitalQ],\[ScriptCapitalR],\[ScriptCapitalS],\[ScriptCapitalT],\[ScriptCapitalU],\[ScriptCapitalV],\[ScriptCapitalW],\[ScriptCapitalX],\[ScriptCapitalY],\[ScriptCapitalZ]};
Format[\[ScriptCapitalC][subscriptn_]]:=Subscript[\[ScriptCapitalC], subscriptn];
(*Protected symbols used in the internal notation*)
Protect[\[Sigma],\[Sigma]b,\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],\[Kappa],\[Lambda],\[Mu],\[Nu],\[Rho],\[CurlyEpsilon]];
(*I put these symbols here so that they are not generated with the context SpinorAlgebra`Private` in front*)


(*Can be specified so that it doesn't have to check which \[Theta]s or xs appear in an expression, if it's always the same ones.*)
working\[Theta]s = {};
workingxs = {};


(* ::Subsection::Closed:: *)
(*Begin `Private`*)


Begin["`Private`"];


(* ::Section::Closed:: *)
(*Initial definitions*)


(* ::Subsection::Closed:: *)
(*Basic definitions*)


toString[x_]:=ToString[x,InputForm];
toStringNoContext[x_]:=StringReplace[ToString[x,InputForm],"SpinorAlgebra`"->""];


(*Definition of fermionic number*)
(*Fermionic variables are \[Theta]n, where n is a non negative integer*)
FN[x_NonCommutativeMultiply]:=Mod[Plus@@(FN/@List@@x),2];
FN[x_d]:=Mod[Plus@@(FN/@List@@x),2];
FN[x_Symbol]:=Mod[Plus@@StringCases[SymbolName[x],{"\[Theta]"~~(DigitCharacter..)->1,"\[Theta]b"~~(DigitCharacter..)->1,"\[Theta]"~~(DigitCharacter..)~~"sq"->1,"\[Theta]b"~~(DigitCharacter..)~~"sq"->1},Overlaps->True],2];
FN[x_Real|x_Complex|x_Rational|x_Integer]:=0;
FN[a_+b_]:=If[FN[a]==FN[b],FN[a],Message[FN::FNfail,a+b];Inactive[FN][a+b],Message[FN::FNfail,a+b];Inactive[FN][a+b]];
FN[]:=0;
FN[a_,b__]:=Mod[FN[a]+FN[b],2];
FN[a_^b_]:=0;
FN[x_Times]:=Mod[Plus@@(FN/@List@@x),2];
FN[up[H_][a_]]:=FN[H];
FN[low[H_][a_]]:=FN[H];
FN[v[H_][a_]]:=0;
FN[\[Sigma][m__][a__]]:=0;
FN[\[Sigma]b[m__][a__]]:=0;
FN[\[Sigma][m__]]:=0;
FN[\[Sigma]b[m__]]:=0;
FN[\[CurlyEpsilon][m__]]:=0;
FN[\[Delta][m__]]:=0;
FN[\[Epsilon]4[m__]]:=0;
FN[x_?xvQ]:=0;


FN::FNfail = "The expression `1` has undefined fermion number."


(*Definition of the canonical ordering*)
chosenOrdering=OrderedQ[{#1,#2}]&;              (*you can also choose a customized ordering*)
FSort[a_List]:=Sort[a,chosenOrdering];
FSignature[a_List]:=Signature[PermutationList[FindPermutation[
    Select[a,FN[#]==1&],
    Select[FSort[a],FN[#]==1&]]]];


ConstQ[x_?NumericQ]:=True;
ConstQ[x_Plus]:=And@@(ConstQ[#]&/@List@@x);
ConstQ[x_Times]:=And@@(ConstQ[#]&/@List@@x);
ConstQ[x_^a_]:=ConstQ[x]&&ConstQ[a];
ToExpression[
    StringJoin@@Table[
        toString[con]<>"/: ConstQ["<>toString[con]<>"] := True; ",
    {con,ConstantsList}]];
ConstQ[CFTs4D`\[Lambda]M[x_]]:= True;
ConstQ[CFTs4D`\[Lambda]P[x_]]:= True;
ConstQ[CFTs4D`\[Lambda][x_]] := True;

(*Use ConstQ to know whether a symbol is considered a constant or not*)


makeConst[s_,symbols__]:=makeConst/@{s,symbols};
makeConst[symbol_Symbol]:=(symbol/:ConstQ[symbol]:=True);
makeConst[symbol_]:=(ConstQ[symbol]:=True)


(* ::Subsection::Closed:: *)
(*NonCommutativeMultiply*)


(*Properties and ordering of NonCommutativeMultiply*)
Unprotect[NonCommutativeMultiply];
ClearAttributes[NonCommutativeMultiply,Flat];

NonCommutativeMultiply[a___,c_?ConstQ b_,e___]:=c*NonCommutativeMultiply[a,b,e];
NonCommutativeMultiply[a___,c_Plus,b___]:=Plus@@(NonCommutativeMultiply[a,#,b]&/@(List@@c));
NonCommutativeMultiply[a___,c_?ConstQ,b___]:=c*NonCommutativeMultiply[a,b];
NonCommutativeMultiply[a___,c_Power,b___]:=c*NonCommutativeMultiply[a,b];

NonCommutativeMultiply[a___,b_,c___]/;(FN[b]==0):=b*a**c;
NonCommutativeMultiply[a___]/;(Length[{a}]===1):=a;
NonCommutativeMultiply[a___]/;(Length[{a}]===0):=1;

NonCommutativeMultiply[a___,b_*c_,e___]:=NonCommutativeMultiply[a,b,c,e];

(*If I insert this rule the numerical values comparison fails... Find out what goes wrong.*)
(*NonCommutativeMultiply[a___]/;(!OrderedQ[{a},chosenOrdering]):=FSignature[{a}]*NonCommutativeMultiply@@FSort[{a}];*)

NonCommutativeMultiply[a___,b_?ConstQ,c___]:=b a**c;

SetAttributes[NonCommutativeMultiply,Flat];
Protect[NonCommutativeMultiply];


(*Reorders the factor of a ** product*)
ReorderNCM[expr_]:=expr/.NonCommutativeMultiply->tempNCM/.tempNCM[a___]:>FSignature[{a}]*NonCommutativeMultiply@@FSort[{a}]/.tempNCM->NonCommutativeMultiply


(* ::Section::Closed:: *)
(*From user form to compact form*)


(* ::Subsection::Closed:: *)
(*Useful auxiliary functions*)


(*Recognizes the type of variables in the monomials*)
\[Theta]Q[x_]:=StringMatchQ[toString[x],"\[Theta]"~~(DigitCharacter..)];
\[Theta]bQ[x_]:=StringMatchQ[toString[x],"\[Theta]b"~~(DigitCharacter..)];
\[Theta]\[Theta]bQ[x_]:=\[Theta]Q[x]||\[Theta]bQ[x];
\[Eta]Q[x_]:=StringMatchQ[toString[x],"\[Eta]"~~(DigitCharacter..)];
\[Chi]Q[x_]:=StringMatchQ[toString[x],"\[Chi]"~~(DigitCharacter..)];
\[Eta]bQ[x_]:=StringMatchQ[toString[x],"\[Eta]b"~~(DigitCharacter..)];
\[Chi]bQ[x_]:=StringMatchQ[toString[x],"\[Chi]b"~~(DigitCharacter..)];
\[Eta]\[Eta]bQ[x_]:=\[Eta]Q[x]||\[Eta]bQ[x];
\[Chi]\[Chi]bQ[x_]:=\[Chi]Q[x]||\[Chi]bQ[x];
\[Eta]\[Theta]\[Chi]Q[x_]:=\[Eta]Q[x]||\[Theta]Q[x]||\[Chi]Q[x];
\[Eta]b\[Theta]b\[Chi]bQ[x_]:=\[Eta]bQ[x]||\[Theta]bQ[x]||\[Chi]bQ[x];
xQ[x_]:=StringMatchQ[toString[x],"x"~~(DigitCharacter..)];
xvQ[x_]:=StringMatchQ[toString[x],"x"~~(DigitCharacter..)~~"v"~~__];
sQ[x_]:=StringMatchQ[toString[x],"s"~~(DigitCharacter..)];
svQ[x_]:=StringMatchQ[toString[x],"s"~~(DigitCharacter..)~~"v"~~__];
bQ[x_]:=StringMatchQ[toString[x],"\[Theta]b"|"\[Eta]b"|"\[Chi]b"~~(DigitCharacter..)];
ubQ[x_]:=(!StringMatchQ[toString[x],__~~"b"~~(DigitCharacter..)])&&StringMatchQ[toString[x],"\[Theta]"|"\[Eta]"|"\[Chi]"~~(DigitCharacter..)];
sqQ[x_]:=StringMatchQ[toString[x],__~~"sq"];
\[Theta]sqQ[x_]:=StringMatchQ[toString[x],"\[Theta]"~~(DigitCharacter..)~~"sq"];
\[Theta]bsqQ[x_]:=StringMatchQ[toString[x],"\[Theta]b"~~(DigitCharacter..)~~"sq"];
\[Theta]\[Theta]bsqQ[x_]:=StringMatchQ[toString[x],"\[Theta]"|"\[Theta]b"~~(DigitCharacter..)~~"sq"];
xxQ[x_]:=StringMatchQ[toString[x],"x"~~(DigitCharacter..)~~"x"~~(DigitCharacter..)];
xsQ[x_]:=StringMatchQ[toString[x],("s"~~(DigitCharacter..)~~"x"~~(DigitCharacter..))|("x"~~(DigitCharacter..)~~"s"~~(DigitCharacter..))];
ssQ[x_]:=StringMatchQ[toString[x],"s"~~(DigitCharacter..)~~"s"~~(DigitCharacter..)];
xsqQ[x_]:=StringMatchQ[toString[x],"x"~~DigitCharacter..~~"sq"];


(*Some useful auxiliary functions*)
sq[x_]:=ToExpression[toString[x]<>"sq"];
glue[x___]:= ToExpression[StringTrim[StringJoin@@(ToString/@{x})," "]];
lorentzd[x_?xvQ,y_?xvQ]/;(x[[1]]===y[[1]]):=If[x===y,ToExpression[StringDrop[toString[Head[x]],-1]<>"sq"],
                                               ToExpression[StringDrop[toString[Head[x]],-1]<>StringDrop[toString[Head[y]],-1]]];
lorentzd[x_?xQ,y_?xQ]:=If[x===y,sq[x],glue[x,y]];
bar[x_?ubQ]:=ToExpression[StringReplace[toString[x],a_~~(b:DigitCharacter..):>a<>"b"<>b]];
unbar[x_?bQ]:=ToExpression[StringReplace[toString[x],a_~~"b"~~(b:DigitCharacter..):>a<>b]];


(*Sets some variables to zero*)
SetToZero[expr_,var_?\[Eta]\[Eta]bQ]:=expr/.var->0/.d[a___,0,b___]:>0;
SetToZero[expr_,var_?\[Theta]\[Theta]bQ]:=expr/.{var->0,sq[var]->0}/.d[a___,0,b___]:>0;
SetToZero[expr_,var_?xQ]:=expr/.{var->0,sq[var]->0,
                          ToExpression[toString[var]<>"v"][\[Mu]_]-> 0,
                          x_?(StringMatchQ[toString[#],toString[var]~~"x"~~(DigitCharacter..)]&):> 0,
                          x_?(StringMatchQ[toString[#],"x"~~(DigitCharacter..)~~toString[var]]&):> 0}/.d[a___,0,b___]:>0;
SetToZero[expr_,var_List]:=Fold[SetToZero,expr,var];


(*SetToZero\[Theta]sq[expr_,{var__?\[Theta]\[Theta]bQ}]:=Module[{expanded,lis},lis=If[Head[expanded=Expand[expr]]===Plus,List@@expanded,{expanded}];
                                         Sum[If[Or@@(#>=2&/@(Exponent[\[Theta]\[Theta]bcount[el]/.x_?\[Theta]\[Theta]bQ/;!MemberQ[{var},x]:>1,#]&/@{var})),0,el],{el,lis}]
                                 ];
SetToZero\[Theta]sq[expr_,var_?\[Theta]\[Theta]bQ]:=SetToZero\[Theta]sq[expr,{var}];*)


(* ::Subsection::Closed:: *)
(*Conversion of the notations and operator d*)


(*Splits monomials into contractions made by the operator d. Henceforth referred to as a d-expression*)
SplitMonomials[m_NonCommutativeMultiply]:=NonCommutativeMultiply@@(SplitMonomials/@ List@@m);
SplitMonomials[m_Times]:=Times@@(SplitMonomials/@ List@@m);
SplitMonomials[s_Plus]:=Plus@@(SplitMonomials/@ List@@s);
SplitMonomials[x_?ConstQ]:=x;
SplitMonomials[Power[x_,p_]]:=Power[SplitMonomials[x],p];

SplitMonomials[s_Symbol]:=Module[{str=SymbolName[s],int},
    int=StringReplace[str,
        {"x"~~(y:DigitCharacter..)~~(z:"\[Mu]"|"\[Nu]"|"\[Rho]"|"\[Lambda]"|"\[Kappa]"):>  "x"<>y<>"v["<>z<>"]|",
        "x"~~(y:DigitCharacter..)~~"sq":>"x"<>y<>"sq|",
        "x"~~(y:DigitCharacter..):> "x"<>y<>"|",
        "\[Eta]"~~(y:DigitCharacter..):> "\[Eta]"<>y<>"|",
        "\[Eta]b"~~(y:DigitCharacter..):> "\[Eta]b"<>y<>"|",
        "\[Theta]"~~(y:DigitCharacter..)~~"sq":>"\[Theta]"<>y<>"sq|",
        "\[Theta]b"~~(y:DigitCharacter..)~~"sq":>"\[Theta]b"<>y<>"sq|",
        "\[Theta]"~~(y:DigitCharacter..):> "\[Theta]"<>y<>"|",
        "\[Theta]b"~~(y:DigitCharacter..):> "\[Theta]b"<>y<>"|",
        "\[Sigma]b"~~(z:"\[Mu]\[Nu]"|"\[Nu]\[Mu]"|"\[Mu]\[Rho]"|"\[Mu]\[Lambda]"|"\[Mu]\[Kappa]"|"\[Nu]\[Rho]"|"\[Nu]\[Lambda]"|"\[Nu]\[Kappa]"|"\[Rho]\[Lambda]"|"\[Rho]\[Kappa]"|"\[Kappa]\[Lambda]"):> "\[Sigma]b["<>StringInsert[z,",",2]<>"]|",
        "\[Sigma]"~~(z:"\[Mu]\[Nu]"|"\[Nu]\[Mu]"|"\[Mu]\[Rho]"|"\[Mu]\[Lambda]"|"\[Mu]\[Kappa]"|"\[Nu]\[Rho]"|"\[Nu]\[Lambda]"|"\[Nu]\[Kappa]"|"\[Rho]\[Lambda]"|"\[Rho]\[Kappa]"|"\[Kappa]\[Lambda]"):> "\[Sigma]["<>StringInsert[z,",",2]<>"]|",
        "\[Sigma]"~~(z:"\[Mu]"|"\[Nu]"|"\[Rho]"|"\[Lambda]"|"\[Kappa]"):> "\[Sigma]["<>z<>"]|",
        "\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda]":> "\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda]|",
        "\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu]":> "\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu]|"
    }];
    Return[(d@@ToExpression/@StringSplit[int,"|"])]
];


(*Writes monomials back to the user form*)
WriteMonomials[m_NonCommutativeMultiply]:=NonCommutativeMultiply@@(WriteMonomials/@ List@@m);
WriteMonomials[m_Times]:=Times@@(WriteMonomials/@ List@@m);
WriteMonomials[s_Plus]:=Plus@@(WriteMonomials/@ List@@s);
WriteMonomials[x_?ConstQ]:=x;
WriteMonomials[Power[x_,p_]]:=Power[WriteMonomials[x],p];
WriteMonomials[x_Symbol]:=x;
WriteMonomials[x_List]:=x;

(*WriteMonomials[x_d]:=Module[{rule},
    rule={\[Sigma][m_,n_] :> ToExpression["\[Sigma]"<>toString[m]<>toString[n]],
    \[Sigma]b[m_,n_] :> ToExpression["\[Sigma]b"<>toString[m]<>toString[n]],
    \[Sigma][m_]:>ToExpression["\[Sigma]"<>toString[m]],
    \[Sigma]b[m_]:>ToExpression["\[Sigma]b"<>toString[m]],
    a_?xvQ:>ToExpression[StringReplace[toString[a],"v["~~y_~~"]":>y]]};

    glue@@(x/.rule)];*)
(*The commented out part is the new notation: x1\[Mu]x2\[Nu]\[Eta]\[Sigma]\[Mu]\[Nu]\[Eta], this is the old one x1\[Mu]\[Nu]\[Eta]\[Sigma]\[Mu]\[Nu]\[Eta]x2\[Nu]. Moreover this one always outputs a \[Sigma]\[Mu]\[Nu] with indices \[Mu] and \[Nu]*)
WriteMonomials[x_d]:=Module[{rule,tempglue,ind},
    ind=x/.{d[a_[m_],b_[n_],\[Eta]1_,\[Sigma][m_,n_],\[Eta]2_]:>{Rule[m,\[Mu]],Rule[n,\[Nu]]},d[a_[m_],b_[n_],\[Eta]1_,\[Sigma]b[m_,n_],\[Eta]2_]:>{Rule[m,\[Mu]],Rule[n,\[Nu]]},_->{}};

    rule={\[Sigma][m_,n_] :> "\[Sigma]"<>toString[m/.ind]<>toString[n/.ind],
    \[Sigma]b[m_,n_] :> "\[Sigma]b"<>toString[m/.ind]<>toString[n/.ind],
    \[Sigma][m_]:>ToExpression["\[Sigma]"<>toString[m]],
    \[Sigma]b[m_]:>ToExpression["\[Sigma]b"<>toString[m]],
    a_?xvQ:>StringTake[toString[a],{1,StringPosition[toString[a],"v"][[1,1]]-1}]<>toStringNoContext[First@a/.ind]};

    (tempglue@@(x/.rule))/.{tempglue[a_,b_,c_,"\[Sigma]\[Mu]\[Nu]",d_]:>glue[a,c,"\[Sigma]\[Mu]\[Nu]",d,b],tempglue[a_,b_,c_,"\[Sigma]b\[Mu]\[Nu]",d_]:>glue[a,c,"\[Sigma]b\[Mu]\[Nu]",d,b]}/.tempglue->glue
    
];


(*Linearity over constants*)
d[a___,n_*b_,c___]/;ConstQ[n]:=n*d[a,b,c];
d[a___,b1_+b2_,c___]:=d[a,b1,c]+d[a,b2,c];

(*Enforces the ordering of the operator d*)
\[Sigma]Q[x_]:=StringMatchQ[toString[Head[x]],"\[Sigma]"];
\[Sigma]bQ[x_]:=StringMatchQ[toString[Head[x]],"\[Sigma]b"];
(*Sorts \[Eta]i\[Theta]j, \[Eta]i\[Eta]j and \[Theta]i\[Theta]j. Imposes \[Theta]\[Theta] \[Rule] \[Theta]sq, \[Eta]\[Eta] \[Rule] 0*)
d[x_?\[Theta]\[Theta]bQ,x_?\[Theta]\[Theta]bQ]:=sq[x];
d[x_?\[Theta]\[Theta]bQ,y_?\[Theta]\[Theta]bQ]/;(!chosenOrdering[x,y]):=d[y,x];
d[x_?\[Eta]\[Eta]bQ,x_?\[Eta]\[Eta]bQ]:=0;
d[x_?\[Eta]\[Eta]bQ,y_?\[Eta]\[Eta]bQ]/;(!chosenOrdering[x,y]):=-d[y,x];
d[x_?\[Eta]\[Eta]bQ,y_?\[Theta]\[Theta]bQ]/;(!chosenOrdering[x,y]):=-d[y,x];
d[x_?\[Theta]\[Theta]bQ,y_?\[Eta]\[Eta]bQ]/;(!chosenOrdering[x,y]):=-d[y,x];

(*Sorts monomials with three terms of the form \[Eta]x\[Eta]b, \[Theta]x\[Theta]b, \[Eta]x\[Theta]b, \[Theta]x\[Eta]b*)
d[x_?\[Theta]bQ,y_?xQ,z_?\[Theta]Q]:=-d[z,y,x];
d[x_?\[Theta]bQ,y_?xQ,z_?\[Eta]Q]:=d[z,y,x];
d[x_?\[Eta]bQ,y_?xQ,z_?\[Eta]Q]:=d[z,y,x];
d[x_?\[Eta]bQ,y_?xQ,z_?\[Theta]Q]:=d[z,y,x];

(*Sorts monomials with \[Sigma]\[Mu]\[Nu] or \[Sigma]b\[Mu]\[Nu] to (example) x12\[Mu]x23\[Nu]\[Eta]2\[Sigma]\[Mu]\[Nu]\[Theta]3. Puts to zero x12\[Mu]x12\[Nu]..\[Sigma]\[Mu]\[Nu].. and \[Theta]i\[Sigma]\[Mu]\[Nu]\[Theta]i, \[Theta]bi\[Sigma]b\[Mu]\[Nu]\[Theta]bi*)
d[x___,y_?\[Theta]\[Theta]bQ|y_?\[Eta]\[Eta]bQ|y_?xQ|y_?\[Sigma]Q|y_?\[Sigma]bQ,z_?xvQ,w___]/;FreeQ[{x,z,w},\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda]|\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu]]:=d[x,z,y,w];
d[x__?xvQ,y___]/;(DuplicateFreeQ[Head/@{x}]&&(!OrderedQ[Head/@{x}])):=d[Sequence@@Sort[{x}],y];
d[x_[\[Mu]_],y_[\[Nu]_],\[Eta]1_,\[Sigma][\[Nu]_,\[Mu]_],\[Eta]2_]:=-d[x[\[Mu]],y[\[Nu]],\[Eta]1,\[Sigma][\[Mu],\[Nu]],\[Eta]2];
d[x_[\[Mu]_],y_[\[Nu]_],\[Eta]b1_,\[Sigma]b[\[Nu]_,\[Mu]_],\[Eta]b2_]:=-d[x[\[Mu]],y[\[Nu]],\[Eta]b1,\[Sigma]b[\[Mu],\[Nu]],\[Eta]b2];
d[a_?xvQ,b_?xvQ,x__,y_?\[Sigma]Q|y_?\[Sigma]bQ,z__]/;(Head[a]===Head[b]):=0;
d[x_?xvQ,y_?\[Theta]\[Theta]bQ,s_?\[Sigma]Q|s_?\[Sigma]bQ,y_?\[Theta]\[Theta]bQ,t_?xvQ]:=0;
d[x__?xvQ,y_,s_?\[Sigma]Q|s_?\[Sigma]bQ,z_]/;(!chosenOrdering[y,z]):=FSignature[{y,z}]*d[x,z,s,y];

(*Puts \[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda] at the beginning. Puts to zero \[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda] contracted with equal xs. Orders canonically the xs contracted with \[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda].*)
d[x__,\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],y___]:=d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],x,y];
d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],y___]/;(!DuplicateFreeQ[Head/@Select[{y},xvQ[#]&]]):=0;
d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],x__?xvQ]/;(DuplicateFreeQ[Head/@{x}]&&(!OrderedQ[Head/@{x}])):=Module[{sign,hlist},
    hlist=SortBy[{x},Head];
    sign=-Signature[First/@hlist];
    sign*d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],Sequence@@(hlist/.Thread[Rule[First/@hlist,{\[Mu],\[Nu],\[Rho],\[Lambda]}]])]
];
d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],x__?xvQ]/;(DuplicateFreeQ[Head/@{x}]&&(OrderedQ[Head/@{x}])&&(First/@{x}=!={\[Mu],\[Nu],\[Rho],\[Lambda]})):=Module[{sign},
    sign=-Signature[First/@{x}];
    sign*d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],Sequence@@({x}/.Thread[Rule[First/@{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]])]
];
d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],a___,x_?\[Theta]\[Theta]bQ|x_?\[Eta]\[Eta]bQ|x_?\[Sigma]Q|x_?\[Sigma]bQ,y_?xvQ,b___]:=d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],a,y,x,b]; (*Check this*)
d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],a__?xvQ,x_?\[Theta]bQ,y_?\[Sigma]bQ,z_?\[Theta]Q]:=-d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],a,z,\[Sigma]@@y,x];
d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],a__?xvQ,x_?\[Theta]bQ,y_?\[Sigma]bQ,z_?\[Eta]Q]:=d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],a,z,\[Sigma]@@y,x];
d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],a__?xvQ,x_?\[Eta]bQ,y_?\[Sigma]bQ,z_?\[Eta]Q]:=d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],a,z,\[Sigma]@@y,x];
d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],a__?xvQ,x_?\[Eta]bQ,y_?\[Sigma]bQ,z_?\[Theta]Q]:=d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],a,z,\[Sigma]@@y,x];
d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],x__?xvQ,\[Eta]_,s_?\[Sigma]Q,\[Eta]b_]/;(DuplicateFreeQ[Head/@{x}]&&(First/@Append[{x},s]=!={\[Mu],\[Nu],\[Rho],\[Lambda]})):=Module[{sign},
    sign=-Signature[First/@Append[{x},s]];
    sign*d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],Sequence@@({x}/.Thread[Rule[First/@{x},{\[Mu],\[Nu],\[Rho]}]]),\[Eta],\[Sigma][\[Lambda]],\[Eta]b]
];
d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],x__?xvQ,\[Eta]_,s_?\[Sigma]Q,\[Eta]b_]/;(DuplicateFreeQ[Head/@{x}]&&(First/@Append[{x},s]==={\[Mu],\[Nu],\[Rho],\[Lambda]})&&!OrderedQ[Head/@{x}]):=Module[{sign},
    sign=Signature[Head/@{x}];
    sign*d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],Sequence@@({x}/.Thread[Rule[Head/@{x},Sort[Head/@{x}]]]),\[Eta],\[Sigma][\[Lambda]],\[Eta]b]
];


(*Puts \[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu] at the beginning. Puts to zero \[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu] contracted with equal xs. Orders canonically the xs contracted with \[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu].*)
d[x__,\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],y___]:=d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x,y];
d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],y___]/;(!DuplicateFreeQ[Head/@Select[{y},xvQ[#]&]]):=0;
d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x__?xvQ]/;(DuplicateFreeQ[Head/@{x}]&&(!OrderedQ[Head/@{x}])):=Module[{sign,hlist},
    hlist=SortBy[{x},Head];
    sign=Signature[First/@hlist];
    sign*d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],Sequence@@(hlist/.Thread[Rule[First/@hlist,{\[Kappa],\[Lambda],\[Mu],\[Nu]}]])]
];
d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x__?xvQ]/;(DuplicateFreeQ[Head/@{x}]&&(OrderedQ[Head/@{x}])&&(First/@{x}=!={\[Kappa],\[Lambda],\[Mu],\[Nu]})):=Module[{sign},
    sign=Signature[First/@{x}];
    sign*d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],Sequence@@({x}/.Thread[Rule[First/@{x},{\[Kappa],\[Lambda],\[Mu],\[Nu]}]])]
];
d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],a___,x_?\[Theta]\[Theta]bQ|x_?\[Eta]\[Eta]bQ|x_?\[Sigma]Q|x_?\[Sigma]bQ,y_?xvQ,b___]:=d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],a,y,x,b];   (*Check this*)
d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],a__?xvQ,x_?\[Theta]bQ,y_?\[Sigma]bQ,z_?\[Theta]Q]:=-d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],a,z,\[Sigma]@@y,x];
d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],a__?xvQ,x_?\[Theta]bQ,y_?\[Sigma]bQ,z_?\[Eta]Q]:=d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],a,z,\[Sigma]@@y,x];
d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],a__?xvQ,x_?\[Eta]bQ,y_?\[Sigma]bQ,z_?\[Eta]Q]:=d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],a,z,\[Sigma]@@y,x];
d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],a__?xvQ,x_?\[Eta]bQ,y_?\[Sigma]bQ,z_?\[Theta]Q]:=d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],a,z,\[Sigma]@@y,x];
d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x__?xvQ,\[Eta]_,s_?\[Sigma]Q,\[Eta]b_]/;(DuplicateFreeQ[Head/@{x}]&&(First/@Append[{x},s]=!={\[Kappa],\[Lambda],\[Mu],\[Nu]})):=Module[{sign},
    sign=Signature[First/@Append[{x},s]];
    sign*d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],Sequence@@({x}/.Thread[Rule[First/@{x},{\[Kappa],\[Lambda],\[Mu]}]]),\[Eta],\[Sigma][\[Nu]],\[Eta]b]
];
d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x__?xvQ,\[Eta]_,s_?\[Sigma]Q,\[Eta]b_]/;(DuplicateFreeQ[Head/@{x}]&&(First/@Append[{x},s]==={\[Kappa],\[Lambda],\[Mu],\[Nu]})&&!OrderedQ[Head/@{x}]):=Module[{sign},
    sign=Signature[Head/@{x}];
    sign*d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],Sequence@@({x}/.Thread[Rule[Head/@{x},Sort[Head/@{x}]]]),\[Eta],\[Sigma][\[Nu]],\[Eta]b]
];

(*d[X.Y]=XY*)
d[x_?sqQ]:=x;
d[x_?xQ,x_?xQ]:=sq[x];
d[x_?xQ,y_?xQ]:=glue[x,y];


(*Momentarily makes d consider \[Theta] and \[Theta]b as commuting*)
(*This is needed for the computation with explicit indices where all signs are put in the end*)
hold[d][x_?\[Theta]\[Theta]bQ,y_?\[Theta]\[Theta]bQ]/;(!chosenOrdering[x,y]):=-d[y,x];
hold[d][x_?\[Theta]bQ,y_?xQ,z_?\[Theta]Q]:=d[z,y,x];
hold[d][x_?\[Theta]bQ,y_?\[Sigma]bQ,z_?\[Theta]Q]:=d[z,\[Sigma]@@y,x];
hold[d][x___,y_?\[Theta]\[Theta]bQ|y_?\[Eta]\[Eta]bQ|y_?xQ|y_?\[Sigma]Q|y_?\[Sigma]bQ,z_?xvQ,w___]/;FreeQ[{x,z,w},\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda]|\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu]]:=hold[d][x,z,y,w];
hold[d][x__?xvQ,y_,s_?\[Sigma]Q|s_?\[Sigma]bQ,z_]/;(!chosenOrdering[y,z]):=d[x,z,s,y];
hold[d][\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],a__?xvQ,x_?\[Theta]bQ,y_?\[Sigma]bQ,z_?\[Theta]Q]:=d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],a,z,\[Sigma]@@y,x];
hold[d][\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],a__?xvQ,x_?\[Theta]bQ,y_?\[Sigma]bQ,z_?\[Theta]Q]:=d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],a,z,\[Sigma]@@y,x];
hold[d][x__]:=d[x]


(*Operator on functions that transforms the input in internal notation, applies the function, and transfrorms back in user form.*)
u[f_]:=WriteMonomials[f[SplitMonomials[#],##2]]&;
uu[f_]:=WriteMonomials[f[SplitMonomials[#1],SplitMonomials[#2],##3]]&;


(* ::Subsection::Closed:: *)
(*Other useful functions that use the definitions of d*)


(*Counts \[Theta]s and \[Theta]bs in a term. Acts on d-expressions*)
\[Theta]count[expr_]:=Module[{tempNCM},
               Flatten[tempNCM[expr]/.(a_**b_)^p_:>(a^p)**(b^p)/.d[a__]^p_:>d@@(#^p &/@{a})/.x_?\[Theta]sqQ:>ToExpression[StringDrop[toString[x],-2]]^2/.{Times->tempNCM,NonCommutativeMultiply->tempNCM,d->tempNCM}
               ,Infinity,tempNCM]/.{tempNCM[seq__] :> Cases[PowerExpand/@{seq},_?\[Theta]Q|(_?\[Theta]Q)^p_]}]/.List->Times;
\[Theta]bcount[expr_]:=Module[{tempNCM},
                Flatten[tempNCM[expr]/.(a_**b_)^p_:>(a^p)**(b^p)/.d[a__]^p_:>d@@(#^p &/@{a})/.x_?\[Theta]bsqQ:>ToExpression[StringDrop[toString[x],-2]]^2/.{Times->tempNCM,NonCommutativeMultiply->tempNCM,d->tempNCM}
                ,Infinity,tempNCM]/.{tempNCM[seq__] :> Cases[PowerExpand/@{seq},_?\[Theta]bQ|(_?\[Theta]bQ)^p_]}]/.List->Times;
\[Theta]\[Theta]bcount[expr_]:=Module[{tempNCM},
                Flatten[tempNCM[expr]/.(a_**b_)^p_:>(a^p)**(b^p)/.d[a__]^p_:>d@@(#^p &/@{a})/.x_?\[Theta]\[Theta]bsqQ:>ToExpression[StringDrop[toString[x],-2]]^2/.{Times->tempNCM,NonCommutativeMultiply->tempNCM,d->tempNCM}
                ,Infinity,tempNCM]/.{tempNCM[seq__] :> Cases[PowerExpand/@{seq},_?\[Theta]\[Theta]bQ|(_?\[Theta]\[Theta]bQ)^p_]}]/.List->Times;
(*Doesn't distinguish between different \[Theta]s and gives directly the number*)
All\[Theta]count[expr_]:=Module[{tempNCM,\[Theta]},
               Flatten[tempNCM[expr]/.(a_**b_)^p_:>(a^p)**(b^p)/.d[a__]^p_:>d@@(#^p &/@{a})/.x_?\[Theta]sqQ:>\[Theta]0^2/.{Times->tempNCM,NonCommutativeMultiply->tempNCM,d->tempNCM}
               ,Infinity,tempNCM]/.{tempNCM[seq__] :> Cases[PowerExpand/@{seq},_?\[Theta]Q|(_?\[Theta]Q)^p_]}/.{x_?\[Theta]Q->\[Theta]}/.List->Times//Exponent[#,\[Theta]]&];
All\[Theta]bcount[expr_]:=Module[{tempNCM,\[Theta]b},
                Flatten[tempNCM[expr]/.(a_**b_)^p_:>(a^p)**(b^p)/.d[a__]^p_:>d@@(#^p &/@{a})/.x_?\[Theta]bsqQ:>\[Theta]b0^2/.{Times->tempNCM,NonCommutativeMultiply->tempNCM,d->tempNCM}
                ,Infinity,tempNCM]/.{tempNCM[seq__] :> Cases[PowerExpand/@{seq},_?\[Theta]bQ|(_?\[Theta]bQ)^p_]}/.{x_?\[Theta]bQ->\[Theta]b}/.List->Times//Exponent[#,\[Theta]b]&];
(*Returns True if there are more than 2 equal \[Theta]'s or \[Theta]b's*)
tooMany\[Theta]Q[expr_]:=With[{\[Theta]countsaved = \[Theta]count[expr]},!And@@(#<=2 & /@ Exponent[\[Theta]countsaved,Variables[\[Theta]countsaved]])];
tooMany\[Theta]bQ[expr_]:=With[{\[Theta]bcountsaved = \[Theta]bcount[expr]},!And@@(#<=2 & /@ Exponent[\[Theta]bcountsaved,Variables[\[Theta]bcountsaved]])];
(*Tells whether a term is completely bosonic or not (i.e. if it contains \[Theta]s and \[Theta]bs at all*)
\[Theta]FreeQ[x_]:=All\[Theta]count[x]+All\[Theta]bcount[x]==0;


ComplexConj[expr_]:=Module[{tempNCM,tempd},expr/.{Complex[a_,b_]:>Complex[a,-b]
                       }/.{NonCommutativeMultiply:>tempNCM
                       }/.{tempNCM[a__]:>Reverse[tempNCM[a]]
                       }/.{\[Sigma][\[Mu]_,\[Nu]_]:>-\[Sigma]b[\[Mu],\[Nu]],\[Sigma]b[\[Mu]_,\[Nu]_]:>-\[Sigma][\[Mu],\[Nu]]
                       }/.{d[x__]:>Reverse[tempd[x]]
                       }/.{a_?bQ:>unbar[a],b_?ubQ:>bar[b],
                           c_?\[Theta]sqQ:>ToExpression[StringReplace[toString[c],"\[Theta]"->"\[Theta]b"]],
                           c_?\[Theta]bsqQ:>ToExpression[StringReplace[toString[c],"\[Theta]b"->"\[Theta]"]]
                       }/.{tempNCM->NonCommutativeMultiply,tempd->d}];


(* ::Subsection::Closed:: *)
(*Display notation in a nice form*)


Nice[expr_]:=expr/.{d->dNice,a_Symbol?xxQ:>dNice[a],a_Symbol?sqQ:>dNice[a]}/.NonCommutativeMultiply->NiceNCM;
UnNice[expr_]:=expr/.dNice->d/.NiceNCM->NonCommutativeMultiply


Format[dNice[a_?sqQ]]:=StringCases[toString[a],{(x:"\[Theta]"|"x")~~(y:DigitCharacter..)~~"sq":> Subscript[x, y],"\[Theta]b"~~(y:DigitCharacter..)~~"sq":> Subscript["\!\(\*OverscriptBox[\(\[Theta]\), \(_\)]\)", y]}][[1]]^2;
Format[dNice[a_?xxQ]]:=StringCases[toString[a],{"x"~~(y:DigitCharacter..)~~"x"~~(z:DigitCharacter..):> Row[{Subscript["x", y],"\[NegativeThinSpace]\[CenterDot]\[NegativeThinSpace]",Subscript["x", z]}]}][[1]];
Format[dNice[a__]]:=Row[{"\[ThinSpace]",a,"\[ThinSpace]"}/.{\[Sigma][\[Mu],\[Nu]]->"\!\(\*SubscriptBox[\(\[Sigma]\), \(\[Mu]\[Nu]\)]\)",\[Sigma]b[\[Mu],\[Nu]]->"\!\(\*SubscriptBox[OverscriptBox[\(\[Sigma]\), \(_\)], \(\[Mu]\[Nu]\)]\)",\[Sigma][\[Nu]]->"\!\(\*SubscriptBox[\(\[Sigma]\), \(\[Nu]\)]\)",
                              \[Sigma][\[Nu],\[Mu]]->"\!\(\*SubscriptBox[\(\[Sigma]\), \(\[Nu]\[Mu]\)]\)",\[Sigma]b[\[Nu],\[Mu]]->"\!\(\*SubscriptBox[OverscriptBox[\(\[Sigma]\), \(_\)], \(\[Nu]\[Mu]\)]\)",\[Sigma][\[Lambda]]->"\!\(\*SubscriptBox[\(\[Sigma]\), \(\[Lambda]\)]\)",
                              x_?xvQ:>StringCases[toString[x],"x"~~(y:DigitCharacter..)~~"v["~~z_~~"]":>  Subscript["x",y]^z][[1]],
                              x_?\[Eta]\[Eta]bQ:>StringCases[toString[x],{"\[Eta]"~~(z:DigitCharacter..):>Subscript["\[Eta]", z],"\[Eta]b"~~(z:DigitCharacter..):>Subscript["\!\(\*OverscriptBox[\(\[Eta]\), \(_\)]\)", z]}][[1]],
                              x_?\[Theta]\[Theta]bQ:>StringCases[toString[x],{"\[Theta]"~~(z:DigitCharacter..):>Subscript["\[Theta]", z],"\[Theta]b"~~(z:DigitCharacter..):>Subscript["\!\(\*OverscriptBox[\(\[Theta]\), \(_\)]\)", z]}][[1]],
                              x_?xQ:>StringCases[toString[x],{"x"~~(y:DigitCharacter..):>Subscript["x", y]}][[1]],
                              \[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu] -> "\!\(\*SubscriptBox[\(\[CurlyEpsilon]\), \(\[Kappa]\[Lambda]\[Mu]\[Nu]\)]\)", \[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda] -> "\!\(\*SubscriptBox[\(\[CurlyEpsilon]\), \(\[Mu]\[Nu]\[Rho]\[Lambda]\)]\)" 
                              }];
Format[NiceNCM[a__]]:=Infix[NiceNCM[a],""];


(* ::Section::Closed:: *)
(*Reduction of expressions*)


(* ::Subsection::Closed:: *)
(*Algebra of Pauli \[Sigma] matrices*)


SetAttributes[\[ScriptG],Orderless];
SetAttributes[\[Delta],Orderless];
\[ScriptG]/:ConstQ[\[ScriptG]]:=True; (*\[ScriptG] is the 4 dimensional metric tensor*)
Unprotect[\[CurlyEpsilon]];\[CurlyEpsilon]/:ConstQ[\[CurlyEpsilon]]:=True;Protect[\[CurlyEpsilon]];(*\[CurlyEpsilon] is the 2 dimensional \[Epsilon]-tensor*)
\[Epsilon]4/:ConstQ[\[Epsilon]4]:=True; (*\[Epsilon]4 is the 4 dimensional \[Epsilon]-tensor*)
\[Delta]/:ConstQ[\[Delta]]:=True; (*\[Delta] is the 2 dimensional Kronecker \[Delta]*)
(*Rules to carry out the Pauli algebra*)
PauliRulesgen={
            \[Delta][\[Alpha]_,\[Beta]_]ANY_[i___,\[Beta]_,f___]:>ANY[i,\[Alpha],f],
			\[ScriptG][\[Mu]_,\[Nu]_]ANY_[i___,\[Nu]_,f___]:>ANY[i,\[Mu],f],
			\[ScriptG][\[Mu]_,\[Nu]_]\[Sigma][i___,\[Nu]_,f___][any__]:>\[Sigma][i,\[Mu],f][any],
			\[ScriptG][\[Mu]_,\[Nu]_]\[Sigma]b[i___,\[Nu]_,f___][any__]:>\[Sigma]b[i,\[Mu],f][any],
            \[ScriptG][\[Mu]_,\[Mu]_]->4,\[ScriptG][\[Mu]_,\[Nu]_]^2->4,
            \[Delta][\[Alpha]_,\[Alpha]_]->2,\[Delta][\[Alpha]_,\[Beta]_]^2->2,
            \[ScriptG][\[Mu]_,\[Nu]_]\[ScriptG][\[Mu]_,\[Rho]_]:>\[ScriptG][\[Nu],\[Rho]],
            \[Delta][\[Mu]_,\[Nu]_]\[Delta][\[Mu]_,\[Rho]_]:>\[Delta][\[Nu],\[Rho]],
            \[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Alpha]_] :> 0, \[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]_,\[Alpha]_] :> 0,
            \[CurlyEpsilon][x:OrderlessPatternSequence[\[Alpha]_,\[Beta]_]]\[CurlyEpsilon][y:OrderlessPatternSequence[\[Beta]_,\[Gamma]_]] :> If[{x}[[2]]===\[Beta],1,-1]*If[{y}[[1]]===\[Beta],1,-1]*\[Delta][\[Alpha],\[Gamma]],
            \[CurlyEpsilon][\[Alpha]_,\[Beta]_]^2:>-2,
            \[Epsilon]4[a__]/;!DuplicateFreeQ[{a}]:>0,
		    \[CurlyEpsilon][\[Alpha]_,\[Beta]_]up[X_][\[Beta]_]:>low[X][\[Alpha]],
			\[CurlyEpsilon][\[Alpha]_,\[Beta]_]low[X_][\[Beta]_]:>up[X][\[Alpha]],
			\[CurlyEpsilon][\[Beta]_,\[Alpha]_]up[X_][\[Beta]_]:>-low[X][\[Alpha]],
			\[CurlyEpsilon][\[Beta]_,\[Alpha]_]low[X_][\[Beta]_]:>-up[X][\[Alpha]],
            \[ScriptG][\[Mu]_,\[Nu]_]v[x_][\[Nu]_] :> v[x][\[Mu]]
            };

PauliRed  :={
            \[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[Sigma]b[\[Mu]_][\[Beta]d_,\[Beta]_]:>-2\[Delta][\[Alpha],\[Beta]]\[Delta][\[Alpha]d,\[Beta]d],
            \[Sigma]b[\[Mu]_][\[Alpha]d_,\[Alpha]_]\[Sigma]b[\[Mu]_][\[Beta]d_,\[Beta]_]:>-2\[CurlyEpsilon][\[Alpha],\[Beta]]\[CurlyEpsilon][\[Alpha]d,\[Beta]d],
            \[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[Sigma][\[Mu]_][\[Beta]_,\[Beta]d_]:>-2\[CurlyEpsilon][\[Alpha],\[Beta]]\[CurlyEpsilon][\[Alpha]d,\[Beta]d],
            \[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[CurlyEpsilon][x:OrderlessPatternSequence[\[Alpha]_,\[Beta]_]]\[CurlyEpsilon][y:OrderlessPatternSequence[\[Alpha]d_,\[Beta]d_]]:>\[Sigma]b[\[Mu]][\[Beta]d,\[Beta]]If[{x}[[1]]===\[Alpha],1,-1]If[{y}[[1]]===\[Alpha]d,1,-1],
            \[Sigma]b[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_]][\[Beta]d_,\[Gamma]d_]\[Sigma][\[Nu]_][\[Alpha]_,\[Alpha]d_]:>-1/2 (((\[Sigma][\[Mu]][\[Alpha],#]\[CurlyEpsilon][#,\[Beta]d]\[CurlyEpsilon][\[Gamma]d,\[Alpha]d])&@Unique[\[Zeta]d])-\[Sigma][\[Mu]][\[Alpha],\[Gamma]d]\[Delta][\[Alpha]d,\[Beta]d])If[{x}[[1]]===\[Mu],1,-1],
            \[Sigma][x:OrderlessPatternSequence[\[Mu]_,\[Nu]_]][\[Beta]_,\[Gamma]_]\[Sigma][\[Nu]_][\[Alpha]_,\[Alpha]d_]:>-1/2 (\[Delta][\[Gamma],\[Alpha]]\[Sigma][\[Mu]][\[Beta],\[Alpha]d]-((\[CurlyEpsilon][\[Alpha],\[Beta]]\[CurlyEpsilon][\[Gamma],#]\[Sigma][\[Mu]][#,\[Alpha]d])&@Unique[\[Zeta]]))If[{x}[[1]]===\[Mu],1,-1],
            \[Sigma]b[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_]][\[Beta]d_,\[Gamma]d_]\[Sigma]b[\[Nu]_][\[Alpha]d_,\[Alpha]_]:>-1/2 ((-(\[Sigma][\[Mu]][#,\[Gamma]d]\[CurlyEpsilon][\[Alpha],#]\[CurlyEpsilon][\[Alpha]d,\[Beta]d])&@Unique[\[Zeta]])+\[Sigma]b[\[Mu]][\[Beta]d,\[Alpha]]\[Delta][\[Alpha]d,\[Gamma]d])If[{x}[[1]]===\[Mu],1,-1],
            \[Sigma][x:OrderlessPatternSequence[\[Mu]_,\[Nu]_]][\[Beta]_,\[Gamma]_]\[Sigma]b[\[Nu]_][\[Alpha]d_,\[Alpha]_]:>-1/2 (-\[Delta][\[Alpha],\[Beta]]\[Sigma]b[\[Mu]][\[Alpha]d,\[Gamma]]+((\[CurlyEpsilon][\[Gamma],\[Alpha]]\[CurlyEpsilon][#,\[Alpha]d]\[Sigma][\[Mu]][\[Beta],#])&@Unique[\[Zeta]d]))If[{x}[[1]]===\[Mu],1,-1],
            \[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[CurlyEpsilon][\[Alpha]_,\[Beta]_]\[Sigma][\[Nu]_][\[Beta]_,\[Beta]d_]:>(\[ScriptG][\[Mu],\[Nu]]\[CurlyEpsilon][\[Alpha]d,\[Beta]d]-2(\[CurlyEpsilon][\[Alpha]d,#]\[Sigma]b[\[Mu],\[Nu]][#,\[Beta]d])&@Unique[\[Gamma]d]),
            \[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[CurlyEpsilon][\[Beta]_,\[Alpha]_]\[Sigma][\[Nu]_][\[Beta]_,\[Beta]d_]:>(-\[ScriptG][\[Mu],\[Nu]]\[CurlyEpsilon][\[Alpha]d,\[Beta]d]+2(\[CurlyEpsilon][\[Alpha]d,#]\[Sigma]b[\[Mu],\[Nu]][#,\[Beta]d])&@Unique[\[Gamma]d]),
            \[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[CurlyEpsilon][\[Alpha]d_,\[Beta]d_]\[Sigma][\[Nu]_][\[Beta]_,\[Beta]d_]:>(\[ScriptG][\[Mu],\[Nu]]\[CurlyEpsilon][\[Alpha],\[Beta]]-2(\[Sigma][\[Mu],\[Nu]][\[Alpha],#]\[CurlyEpsilon][#,\[Beta]])&@Unique[\[Gamma]d]),
            \[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[CurlyEpsilon][\[Beta]d_,\[Alpha]d_]\[Sigma][\[Nu]_][\[Beta]_,\[Beta]d_]:>(-\[ScriptG][\[Mu],\[Nu]]\[CurlyEpsilon][\[Alpha],\[Beta]]+2(\[Sigma][\[Mu],\[Nu]][\[Alpha],#]\[CurlyEpsilon][#,\[Beta]])&@Unique[\[Gamma]d]),
            \[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[Sigma]b[\[Nu]_][\[Alpha]d_,\[Beta]_]:>-\[ScriptG][\[Mu],\[Nu]]\[Delta][\[Alpha],\[Beta]]+2 \[Sigma][\[Mu],\[Nu]][\[Alpha],\[Beta]],
            \[Sigma]b[\[Mu]_][\[Alpha]d_,\[Alpha]_]\[Sigma][\[Nu]_][\[Alpha]_,\[Beta]d_]:>-\[ScriptG][\[Mu],\[Nu]]\[Delta][\[Alpha]d,\[Beta]d]+2 \[Sigma]b[\[Mu],\[Nu]][\[Alpha]d,\[Beta]d],
            \[CurlyEpsilon][x:OrderlessPatternSequence[\[Beta]_,\[Alpha]_]]\[Sigma]b[\[Mu]_][\[Beta]d_,\[Beta]_]:>If[{x}[[1]]===\[Beta],1,-1]\[Sigma][\[Mu]][\[Alpha],\[Beta]]\[CurlyEpsilon][\[Beta],\[Beta]d],
            \[CurlyEpsilon][x:OrderlessPatternSequence[\[Alpha]d_,\[Beta]d_]]\[Sigma]b[\[Mu]_][\[Beta]d_,\[Beta]_]:>If[{x}[[1]]===\[Alpha]d,1,-1]\[Sigma][\[Mu]][\[Beta]d,\[Alpha]d]\[CurlyEpsilon][\[Beta],\[Beta]d],
            \[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Gamma]_]\[CurlyEpsilon][\[Alpha]_,\[Delta]_]\[Sigma][\[Rho]_,\[Lambda]_][\[Delta]_,e_]:>(1/4 (\[ScriptG][\[Mu],\[Lambda]]\[ScriptG][\[Rho],\[Nu]]-\[ScriptG][\[Rho],\[Mu]]\[ScriptG][\[Lambda],\[Nu]]+I \[Epsilon]4[\[Lambda],\[Rho],\[Mu],\[Nu]])\[CurlyEpsilon][e,\[Gamma]]+1/2 (\[CurlyEpsilon][e,#](\[Sigma][\[Mu],\[Lambda]][#,\[Gamma]]\[ScriptG][\[Nu],\[Rho]]+\[Sigma][\[Nu],\[Rho]][#,\[Gamma]]\[ScriptG][\[Mu],\[Lambda]]-\[Sigma][\[Nu],\[Lambda]][#,\[Gamma]]\[ScriptG][\[Mu],\[Rho]]-\[Sigma][\[Mu],\[Rho]][#,\[Gamma]]\[ScriptG][\[Nu],\[Lambda]]))&@Unique[\[Zeta]]),
            \[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Gamma]_]\[CurlyEpsilon][\[Delta]_,\[Alpha]_]\[Sigma][\[Rho]_,\[Lambda]_][\[Delta]_,e_]:>(-(1/4)(\[ScriptG][\[Mu],\[Lambda]]\[ScriptG][\[Rho],\[Nu]]-\[ScriptG][\[Rho],\[Mu]]\[ScriptG][\[Lambda],\[Nu]]+I \[Epsilon]4[\[Lambda],\[Rho],\[Mu],\[Nu]])\[CurlyEpsilon][e,\[Gamma]]-1/2 (\[CurlyEpsilon][e,#](\[Sigma][\[Mu],\[Lambda]][#,\[Gamma]]\[ScriptG][\[Nu],\[Rho]]+\[Sigma][\[Nu],\[Rho]][#,\[Gamma]]\[ScriptG][\[Mu],\[Lambda]]-\[Sigma][\[Nu],\[Lambda]][#,\[Gamma]]\[ScriptG][\[Mu],\[Rho]]-\[Sigma][\[Mu],\[Rho]][#,\[Gamma]]\[ScriptG][\[Nu],\[Lambda]]))&@Unique[\[Zeta]]),
            \[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[Sigma][\[Rho]_,\[Lambda]_][\[Delta]_,\[Alpha]_]:>((\[Sigma][\[Mu],\[Nu]][\[Alpha],\[Beta]]\[CurlyEpsilon][\[Alpha],#1]\[Sigma][\[Rho],\[Lambda]][#1,#2]\[CurlyEpsilon][\[Delta],#2])&@@{Unique[\[Zeta]],Unique[\[Gamma]]}),
            \[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[Sigma][\[Rho]_,\[Lambda]_][\[Beta]_,\[Delta]_]:>((\[Sigma][\[Mu],\[Nu]][#1,\[Beta]]\[CurlyEpsilon][#1,#2]\[Sigma][\[Rho],\[Lambda]][#2,\[Delta]]\[CurlyEpsilon][\[Beta],\[Alpha]])&@@{Unique[\[Zeta]],Unique[\[Gamma]]}),
            \[CurlyEpsilon][\[Beta]_,\[Delta]_]\[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[Sigma][\[Rho]_,\[Lambda]_][\[Gamma]_,\[Delta]_]:>\[CurlyEpsilon][\[Beta],\[Alpha]]\[Sigma][\[Mu],\[Nu]][\[Delta],\[Beta]]\[Sigma][\[Rho],\[Lambda]][\[Gamma],\[Delta]],
            \[CurlyEpsilon][\[Delta]_,\[Beta]_]\[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[Sigma][\[Rho]_,\[Lambda]_][\[Gamma]_,\[Delta]_]:>\[CurlyEpsilon][\[Alpha],\[Beta]]\[Sigma][\[Mu],\[Nu]][\[Delta],\[Beta]]\[Sigma][\[Rho],\[Lambda]][\[Gamma],\[Delta]],
            \[Sigma][\[Mu]_][\[Beta]_,\[Beta]d_]\[Sigma]b[\[Nu]_][\[Beta]d_,\[Delta]_]\[Sigma][\[Rho]_][\[Delta]_,\[Delta]d_]:> (I \[Epsilon]4[\[Mu],\[Nu],\[Rho],#]\[Sigma][#][\[Beta],\[Delta]d]&@Unique[\[Lambda]])+\[ScriptG][\[Mu],\[Rho]]\[Sigma][\[Nu]][\[Beta],\[Delta]d]-\[ScriptG][\[Mu],\[Nu]]\[Sigma][\[Rho]][\[Beta],\[Delta]d]-\[ScriptG][\[Nu],\[Rho]]\[Sigma][\[Mu]][\[Beta],\[Delta]d],
            \[Sigma]b[\[Mu]_][\[Delta]d_,\[Delta]_]\[Sigma][\[Nu]_][\[Delta]_,\[Gamma]d_]\[Sigma]b[\[Rho]_][\[Gamma]d_,\[Beta]_]:> (-I \[Epsilon]4[\[Mu],\[Nu],\[Rho],#]\[Sigma]b[#][\[Delta]d,\[Beta]]&@Unique[\[Lambda]])+\[ScriptG][\[Mu],\[Rho]]\[Sigma]b[\[Nu]][\[Delta]d,\[Beta]]-\[ScriptG][\[Mu],\[Nu]]\[Sigma]b[\[Rho]][\[Delta]d,\[Beta]]-\[ScriptG][\[Nu],\[Rho]]\[Sigma]b[\[Mu]][\[Delta]d,\[Beta]],
            \[Sigma][\[Mu]_,\[Nu]_][\[Beta]_,\[Delta]_]\[Sigma][\[Rho]_][\[Delta]_,\[Delta]d_]:> 1/2 ((I \[Epsilon]4[\[Mu],\[Nu],\[Rho],#]\[Sigma][#][\[Beta],\[Delta]d]&@Unique[\[Lambda]])+\[ScriptG][\[Mu],\[Rho]]\[Sigma][\[Nu]][\[Beta],\[Delta]d]-\[ScriptG][\[Nu],\[Rho]]\[Sigma][\[Mu]][\[Beta],\[Delta]d]),
            \[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]d_,\[Beta]d_]\[Sigma]b[\[Rho]_][\[Beta]d_,\[Beta]_]:> 1/2 ((-I \[Epsilon]4[\[Mu],\[Nu],\[Rho],#]\[Sigma]b[#][\[Alpha]d,\[Beta]]&@Unique[\[Lambda]])+\[ScriptG][\[Mu],\[Rho]]\[Sigma]b[\[Nu]][\[Alpha]d,\[Beta]]-\[ScriptG][\[Nu],\[Rho]]\[Sigma]b[\[Mu]][\[Alpha]d,\[Beta]]),
            \[Sigma][\[Mu]_,\[Nu]_][\[Beta]_,\[Delta]_]\[Sigma]b[\[Rho]_][\[Delta]d_,\[Beta]_]:> -1/2 ((I \[Epsilon]4[\[Mu],\[Nu],\[Rho],#]\[Sigma]b[#][\[Delta]d,\[Delta]]&@Unique[\[Lambda]])+\[ScriptG][\[Mu],\[Rho]]\[Sigma]b[\[Nu]][\[Delta]d,\[Delta]]-\[ScriptG][\[Nu],\[Rho]]\[Sigma]b[\[Mu]][\[Delta]d,\[Delta]]),
            \[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]d_,\[Beta]d_]\[Sigma][\[Rho]_][\[Beta]_,\[Alpha]d_]:> -1/2 ((-I \[Epsilon]4[\[Mu],\[Nu],\[Rho],#]\[Sigma][#][\[Beta],\[Beta]d]&@Unique[\[Lambda]])+\[ScriptG][\[Mu],\[Rho]]\[Sigma][\[Nu]][\[Beta],\[Beta]d]-\[ScriptG][\[Nu],\[Rho]]\[Sigma][\[Mu]][\[Beta],\[Beta]d]),
            \[Sigma][\[Mu]_,\[Nu]_][\[Beta]_,\[Delta]_]\[Sigma]b[\[Rho]_][\[Delta]d_,\[Gamma]_]\[CurlyEpsilon][x:OrderlessPatternSequence[\[Delta]_,\[Gamma]_]]:> \[Sigma][\[Mu],\[Nu]][\[Gamma],\[Delta]]\[Sigma]b[\[Rho]][\[Delta]d,\[Gamma]]\[CurlyEpsilon][\[Delta],\[Beta]]If[{x}[[1]]===\[Delta],1,-1],
            \[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]d_,\[Beta]d_]\[Sigma][\[Rho]_][\[Beta]_,\[Gamma]d_]\[CurlyEpsilon][x:OrderlessPatternSequence[\[Beta]d_,\[Gamma]d_]]:> \[Sigma]b[\[Mu],\[Nu]][\[Gamma]d,\[Beta]d]\[Sigma][\[Rho]][\[Beta],\[Gamma]d]\[CurlyEpsilon][\[Beta]d,\[Alpha]d]If[{x}[[1]]===\[Beta]d,1,-1],
            \[Sigma][\[Mu]_,\[Nu]_][\[Beta]_,\[Delta]_]\[Sigma][\[Rho]_][\[Gamma]_,\[Alpha]d_]\[CurlyEpsilon][x:OrderlessPatternSequence[\[Beta]_,\[Gamma]_]]:> \[Sigma][\[Mu],\[Nu]][\[Beta],\[Gamma]]\[Sigma][\[Rho]][\[Gamma],\[Alpha]d]\[CurlyEpsilon][\[Beta],\[Delta]]If[{x}[[1]]===\[Beta],1,-1],
            \[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]d_,\[Beta]d_]\[Sigma]b[\[Rho]_][\[Gamma]d_,\[Alpha]_]\[CurlyEpsilon][x:OrderlessPatternSequence[\[Alpha]d_,\[Gamma]d_]]:> \[Sigma]b[\[Mu],\[Nu]][\[Alpha]d,\[Gamma]d]\[Sigma]b[\[Rho]][\[Gamma]d,\[Alpha]]\[CurlyEpsilon][\[Alpha]d,\[Beta]d]If[{x}[[1]]===\[Alpha]d,1,-1],
            (*REDUNDANT\[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[Sigma]b[\[Rho]_,\[Nu]_][\[Alpha]d_,\[Beta]d_]:>-(1/8)(\[Sigma][\[Mu]][\[Alpha],\[Beta]d]\[Sigma]b[\[Rho]][\[Alpha]d,\[Beta]]-\[Sigma][\[Mu]][\[Alpha],#2]\[CurlyEpsilon][#2,\[Alpha]d]\[Sigma][\[Rho]][#1,\[Beta]d]\[CurlyEpsilon][\[Beta],#1]-\[Sigma]b[\[Mu]][#2,\[Beta]]\[CurlyEpsilon][\[Beta]d,#2]\[Sigma]b[\[Rho]][\[Alpha]d,#1]\[CurlyEpsilon][#1,\[Alpha]]+\[Sigma]b[\[Mu]][\[Alpha]d,\[Beta]]\[Sigma][\[Rho]][\[Alpha],\[Beta]d])&@@{Unique[\[Zeta]],Unique[\[Zeta]d]},*)
            \[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[Sigma]b[\[Nu]_][\[Alpha]d_,\[Beta]_]:>2\[Sigma][\[Mu],\[Nu]][\[Alpha],\[Beta]]-\[ScriptG][\[Mu],\[Nu]]\[Delta][\[Alpha],\[Beta]],
            \[Sigma]b[\[Mu]_][\[Beta]d_,\[Alpha]_]\[Sigma][\[Nu]_][\[Alpha]_,\[Alpha]d_]:>2\[Sigma]b[\[Mu],\[Nu]][\[Beta]d,\[Alpha]d]-\[ScriptG][\[Mu],\[Nu]]\[Delta][\[Alpha]d,\[Beta]d],
            \[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[CurlyEpsilon][\[Alpha]_,\[Gamma]_]\[Sigma][\[Rho]_][\[Gamma]_,\[Gamma]d_]:>\[Sigma][\[Mu],\[Nu]][\[Alpha],\[Gamma]]\[Sigma][\[Rho]][\[Gamma],\[Gamma]d] \[CurlyEpsilon][\[Alpha],\[Beta]],
            \[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[CurlyEpsilon][\[Gamma]_,\[Alpha]_]\[Sigma][\[Rho]_][\[Gamma]_,\[Gamma]d_]:>\[Sigma][\[Mu],\[Nu]][\[Alpha],\[Gamma]]\[Sigma][\[Rho]][\[Gamma],\[Gamma]d] \[CurlyEpsilon][\[Beta],\[Alpha]],
            \[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[ScriptG][\[Mu]_,\[Nu]_]:>0,\[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[ScriptG][\[Mu]_,\[Nu]_]:>0,
            \[Sigma][\[Mu]_,\[Mu]_][\[Alpha]_,\[Beta]_]:>0,\[Sigma]b[\[Mu]_,\[Mu]_][\[Alpha]_,\[Beta]_]:>0,
            \[Sigma]b[\[Mu]_,\[Nu]_][\[Gamma]d_,\[Alpha]d_]\[CurlyEpsilon][\[Alpha]d_,\[Delta]d_]\[Sigma]b[\[Rho]_,\[Lambda]_][ed_,\[Delta]d_]:>(1/4 (\[ScriptG][\[Mu],\[Lambda]]\[ScriptG][\[Rho],\[Nu]]-\[ScriptG][\[Rho],\[Mu]]\[ScriptG][\[Lambda],\[Nu]]-I \[Epsilon]4[\[Lambda],\[Rho],\[Mu],\[Nu]])\[CurlyEpsilon][ed,\[Gamma]d]+1/2 (\[CurlyEpsilon][#,\[Gamma]d](\[Sigma]b[\[Mu],\[Lambda]][ed,#]\[ScriptG][\[Nu],\[Rho]]+\[Sigma]b[\[Nu],\[Rho]][ed,#]\[ScriptG][\[Mu],\[Lambda]]-\[Sigma]b[\[Nu],\[Lambda]][ed,#]\[ScriptG][\[Mu],\[Rho]]-\[Sigma]b[\[Mu],\[Rho]][ed,#]\[ScriptG][\[Nu],\[Lambda]]))&@Unique[\[Zeta]]),
            \[Sigma]b[\[Mu]_,\[Nu]_][\[Gamma]d_,\[Alpha]d_]\[CurlyEpsilon][\[Delta]d_,\[Alpha]d_]\[Sigma]b[\[Rho]_,\[Lambda]_][ed_,\[Delta]d_]:>(-1/4 (\[ScriptG][\[Mu],\[Lambda]]\[ScriptG][\[Rho],\[Nu]]-\[ScriptG][\[Rho],\[Mu]]\[ScriptG][\[Lambda],\[Nu]]-I \[Epsilon]4[\[Lambda],\[Rho],\[Mu],\[Nu]])\[CurlyEpsilon][ed,\[Gamma]d]-1/2 (\[CurlyEpsilon][#,\[Gamma]d](\[Sigma]b[\[Mu],\[Lambda]][ed,#]\[ScriptG][\[Nu],\[Rho]]+\[Sigma]b[\[Nu],\[Rho]][ed,#]\[ScriptG][\[Mu],\[Lambda]]-\[Sigma]b[\[Nu],\[Lambda]][ed,#]\[ScriptG][\[Mu],\[Rho]]-\[Sigma]b[\[Mu],\[Rho]][ed,#]\[ScriptG][\[Nu],\[Lambda]]))&@Unique[\[Zeta]]),
            \[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[Sigma]b[\[Rho]_,\[Lambda]_][\[Delta]_,\[Alpha]_]:>((\[Sigma]b[\[Mu],\[Nu]][\[Alpha],#1]\[CurlyEpsilon][#1,#2]\[Sigma]b[\[Rho],\[Lambda]][\[Delta],#2]\[CurlyEpsilon][\[Alpha],\[Beta]])&@@{Unique[\[Zeta]],Unique[\[Gamma]]}),
            \[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[Sigma]b[\[Rho]_,\[Lambda]_][\[Beta]_,\[Delta]_]:>((\[Sigma]b[\[Mu],\[Nu]][\[Alpha],\[Beta]]\[CurlyEpsilon][\[Beta],#1]\[Sigma]b[\[Rho],\[Lambda]][#2,#1]\[CurlyEpsilon][\[Delta],#2])&@@{Unique[\[Zeta]],Unique[\[Gamma]]}),
            \[CurlyEpsilon][\[Alpha]_,\[Gamma]_]\[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[Sigma]b[\[Rho]_,\[Lambda]_][\[Gamma]_,\[Delta]_]:>\[CurlyEpsilon][\[Alpha],\[Beta]]\[Sigma]b[\[Mu],\[Nu]][\[Alpha],\[Gamma]]\[Sigma]b[\[Rho],\[Lambda]][\[Gamma],\[Delta]],
            \[CurlyEpsilon][\[Gamma]_,\[Alpha]_]\[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_]\[Sigma]b[\[Rho]_,\[Lambda]_][\[Gamma]_,\[Delta]_]:>\[CurlyEpsilon][\[Beta],\[Alpha]]\[Sigma]b[\[Mu],\[Nu]][\[Alpha],\[Gamma]]\[Sigma]b[\[Rho],\[Lambda]][\[Gamma],\[Delta]],
            \[Sigma][x:OrderlessPatternSequence[\[Mu]_,\[Nu]_]][\[Alpha]_,\[Beta]_] \[Sigma][y:OrderlessPatternSequence[\[Mu]_,\[Rho]_]][\[Gamma]_,\[Delta]_]:>(\[Sigma][x][\[Alpha],\[Beta]]/.Open\[Sigma]\[Mu]\[Nu])(\[Sigma][y][\[Gamma],\[Delta]]/.Open\[Sigma]\[Mu]\[Nu]),
            \[Sigma]b[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_]][\[Alpha]_,\[Beta]_] \[Sigma][y:OrderlessPatternSequence[\[Mu]_,\[Rho]_]][\[Gamma]_,\[Delta]_]:>(\[Sigma]b[x][\[Alpha],\[Beta]]/.Open\[Sigma]\[Mu]\[Nu])(\[Sigma][y][\[Gamma],\[Delta]]/.Open\[Sigma]\[Mu]\[Nu]),
            \[Sigma]b[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_]][\[Alpha]_,\[Beta]_] \[Sigma]b[y:OrderlessPatternSequence[\[Mu]_,\[Rho]_]][\[Gamma]_,\[Delta]_]:>(\[Sigma]b[x][\[Alpha],\[Beta]]/.Open\[Sigma]\[Mu]\[Nu])(\[Sigma]b[y][\[Gamma],\[Delta]]/.Open\[Sigma]\[Mu]\[Nu])
};
(*\[Epsilon]4*)       
PauliRules\[Epsilon]4:={\[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Epsilon]4[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_] :> -24*Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]],
            \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Epsilon]4[y:OrderlessPatternSequence[\[Kappa]_,\[Nu]_,\[Rho]_,\[Lambda]_]] :> -6*\[ScriptG][\[Mu],\[Kappa]]*Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]]*Signature[PermutationList[FindPermutation[{y},{\[Kappa],\[Nu],\[Rho],\[Lambda]}]]],
            \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Epsilon]4[y:OrderlessPatternSequence[\[Kappa]_,\[Tau]_,\[Rho]_,\[Lambda]_]] :> -2*Det[Table[\[ScriptG][ii,jj],{ii,{\[Mu],\[Nu]}},{jj,{\[Kappa],\[Tau]}}]]*Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]]*Signature[PermutationList[FindPermutation[{y},{\[Kappa],\[Tau],\[Rho],\[Lambda]}]]],
            \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Epsilon]4[y:OrderlessPatternSequence[\[Kappa]_,\[Tau]_,\[Omega]_,\[Lambda]_]] :> -Det[Table[\[ScriptG][ii,jj],{ii,{\[Mu],\[Nu],\[Rho]}},{jj,{\[Kappa],\[Tau],\[Omega]}}]]*Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]]*Signature[PermutationList[FindPermutation[{y},{\[Kappa],\[Tau],\[Omega],\[Lambda]}]]],
            \[Epsilon]4[x:PatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Epsilon]4[y:PatternSequence[\[Kappa]_,\[Tau]_,\[Omega]_,\[Upsilon]_]] :> -Det[Table[\[ScriptG][ii,jj],{ii,{x}},{jj,{y}}]],
            \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Sigma][y:OrderlessPatternSequence[\[Rho]_,\[Lambda]_]][\[Alpha]_,\[Beta]_] :> -2 I \[Sigma][\[Mu],\[Nu]][\[Alpha],\[Beta]] If[{y}[[1]]===\[Rho],1,-1]*Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]],
            \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Sigma]b[y:OrderlessPatternSequence[\[Rho]_,\[Lambda]_]][\[Alpha]d_,\[Beta]d_] :> 2 I \[Sigma]b[\[Mu],\[Nu]][\[Alpha]d,\[Beta]d] If[{y}[[1]]===\[Rho],1,-1]*Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]],
            \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Sigma][y:OrderlessPatternSequence[\[Lambda]_,\[Kappa]_]][\[Alpha]_,\[Beta]_] :> (I/2 \[Epsilon]4[x]If[{y}[[1]]===\[Lambda],1,-1]\[Sigma][#1,#2][\[Alpha],\[Beta]]\[Epsilon]4[\[Lambda],\[Kappa],#1,#2]&@@{Unique[\[Tau]],Unique[\[Omega]]}),
            \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Sigma]b[y:OrderlessPatternSequence[\[Lambda]_,\[Kappa]_]][\[Alpha]d_,\[Beta]d_] :> (-I/2 \[Epsilon]4[x]If[{y}[[1]]===\[Lambda],1,-1]\[Sigma]b[#1,#2][\[Alpha]d,\[Beta]d]\[Epsilon]4[\[Lambda],\[Kappa],#1,#2]&@@{Unique[\[Tau]],Unique[\[Omega]]}),
(*These last 3 rules must be checked*)
            \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[Sigma][\[Nu]_][\[Beta]_,\[Beta]d_]:>(Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]]\[Epsilon]4[\[Mu],\[Nu],\[Rho],\[Lambda]](\[Sigma][\[Mu],\[Nu]][\[Alpha],#1]\[CurlyEpsilon][#1,\[Beta]]\[CurlyEpsilon][\[Alpha]d,\[Beta]d]+\[Sigma]b[\[Mu],\[Nu]][#2,\[Beta]d]\[CurlyEpsilon][\[Alpha]d,#2]\[CurlyEpsilon][\[Alpha],\[Beta]])&@@{Unique[\[Zeta]],Unique[\[Zeta]d]}),
            \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Sigma]b[\[Mu]_][\[Alpha]d_,\[Alpha]_]\[Sigma]b[\[Nu]_][\[Beta]d_,\[Beta]_]:>(Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]]\[Epsilon]4[\[Mu],\[Nu],\[Rho],\[Lambda]](\[Sigma][\[Mu],\[Nu]][#1,\[Beta]]\[CurlyEpsilon][\[Alpha],#1]\[CurlyEpsilon][\[Alpha]d,\[Beta]d]+\[Sigma]b[\[Mu],\[Nu]][\[Alpha]d,#2]\[CurlyEpsilon][#2,\[Beta]d]\[CurlyEpsilon][\[Alpha],\[Beta]])&@@{Unique[\[Zeta]],Unique[\[Zeta]d]}),
            \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]\[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]\[Sigma]b[\[Nu]_][\[Beta]d_,\[Beta]_]:>Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]]\[Epsilon]4[\[Mu],\[Nu],\[Rho],\[Lambda]](\[Sigma][\[Mu],\[Nu]][\[Alpha],\[Beta]]\[Delta][\[Alpha]d,\[Beta]d]-\[Sigma]b[\[Mu],\[Nu]][\[Beta]d,\[Alpha]d]\[Delta][\[Alpha],\[Beta]])
};
(*Split open \[Sigma]\[Mu]\[Nu] and \[Sigma]b\[Mu]\[Nu]*)
Open\[Sigma]\[Mu]\[Nu] :=  {\[Sigma][\[Mu]_,\[Nu]_][\[Alpha]_,\[Beta]_] :> (1/4 (\[Sigma][\[Mu]][\[Alpha],#]\[Sigma]b[\[Nu]][#,\[Beta]]-\[Sigma][\[Nu]][\[Alpha],#]\[Sigma]b[\[Mu]][#,\[Beta]])&@Unique[\[Alpha]d]),
            \[Sigma]b[\[Mu]_,\[Nu]_][\[Alpha]d_,\[Beta]d_] :> (1/4 (\[Sigma]b[\[Mu]][\[Alpha]d,#]\[Sigma][\[Nu]][#,\[Beta]d]-\[Sigma]b[\[Nu]][\[Alpha]d,#]\[Sigma][\[Mu]][#,\[Beta]d])&@Unique[\[Alpha]])};



(*Rules to contract objects with \[CurlyEpsilon] or \[ScriptG]*)
(*The function hold applied on d makes it disregard any signs due to anticommutations of \[Theta] or \[Theta]b*)
ContrRule = {v[x_][\[Mu]_]^2 :> sq[x],
			 v[x_][\[Mu]_]v[y_][\[Mu]_] :> glue[x,y],
			 up[h_?ubQ][\[Alpha]_]low[k_?ubQ][\[Alpha]_] :> hold[d][h,k],
			 low[h_?bQ][\[Alpha]_]up[k_?bQ][\[Alpha]_] :> hold[d][h,k],		 
			 up[X_?ubQ][\[Alpha]_]\[Sigma][\[Mu]_][\[Alpha]_,\[Alpha]d_]up[Y_?bQ][\[Alpha]d_]v[V_][\[Mu]_]:>hold[d][X,V,Y],
			 low[X_?bQ][\[Alpha]d_]\[Sigma]b[\[Mu]_][\[Alpha]d_,\[Alpha]_]low[Y_?ubQ][\[Alpha]_]v[V_][\[Mu]_]:>hold[d][Y,V,X],
			 v[V_][m_]up[X_][\[Alpha]_]\[Sigma][m_,n_][\[Alpha]_,\[Beta]_]low[Y_][\[Beta]_]v[W_][n_]:>If[V===W,0,1]hold[d][glue[V,"v[\[Mu]]"],glue[W,"v[\[Nu]]"],X,\[Sigma][\[Mu],\[Nu]],Y],
			 v[V_][m_]low[X_][\[Alpha]d_]\[Sigma]b[m_,n_][\[Alpha]d_,\[Beta]d_]up[Y_][\[Beta]d_]v[W_][n_]:>If[V===W,0,1]hold[d][glue[V,"v[\[Mu]]"],glue[W,"v[\[Nu]]"],X,\[Sigma]b[\[Mu],\[Nu]],Y],
			 v[W_][\[Mu]_]^2:>glue[W,"sq"],
			 \[Epsilon]4[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]v[X_][\[Mu]_]v[Y_][\[Nu]_]v[Z_][\[Rho]_]v[T_][\[Lambda]_]:>d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],glue[X,"v[\[Kappa]]"],glue[Y,"v[\[Lambda]]"],glue[Z,"v[\[Mu]]"],glue[T,"v[\[Nu]]"]],
			 \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]v[X_][\[Mu]_]v[Y_][\[Nu]_]v[Z_][\[Rho]_]up[H_][\[Alpha]_]up[Hb_][\[Alpha]d_]\[Sigma][\[Lambda]_][\[Alpha]_,\[Alpha]d_]:>Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]]d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],glue[X,"v[\[Kappa]]"],glue[Y,"v[\[Lambda]]"],glue[Z,"v[\[Mu]]"],H,\[Sigma][Symbol["\[Nu]"]],Hb],
			 \[Epsilon]4[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_,\[Lambda]_]]v[X_][\[Mu]_]v[Y_][\[Nu]_]v[Z_][\[Rho]_]low[H_][\[Alpha]_]low[Hb_][\[Alpha]d_]\[Sigma]b[\[Lambda]_][\[Alpha]d_,\[Alpha]_]:>Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho],\[Lambda]}]]]d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],glue[X,"v[\[Kappa]]"],glue[Y,"v[\[Lambda]]"],glue[Z,"v[\[Mu]]"],H,\[Sigma][Symbol["\[Nu]"]],Hb]
};

(*Rules for contracting equal \[Theta]s*)
(*Same\[Theta]ruleOLD = {(up[h_?\[Theta]Q][\[Alpha]_]*a___)**(up[h_?\[Theta]Q][\[Beta]_]*b___) :> -1/2 \[CurlyEpsilon][\[Alpha],\[Beta]] sq[h] a**b,
             (low[h_?\[Theta]Q][\[Alpha]_]*a___)**(low[h_?\[Theta]Q][\[Beta]_]*b___) :> 1/2 \[CurlyEpsilon][\[Alpha],\[Beta]] sq[h] a**b,
             (up[h_?\[Theta]bQ][\[Alpha]_]*a___)**(up[h_?\[Theta]bQ][\[Beta]_]*b___) :> 1/2 \[CurlyEpsilon][\[Alpha],\[Beta]] sq[h] a**b,
             (low[h_?\[Theta]bQ][\[Alpha]_]*a___)**(low[h_?\[Theta]bQ][\[Beta]_]*b___) :> -1/2 \[CurlyEpsilon][\[Alpha],\[Beta]] sq[h] a**b,
             (up[h_?\[Theta]Q][\[Alpha]_]*a___)**(low[h_?\[Theta]Q][\[Beta]_]*b___) :> 1/2 \[Delta][\[Alpha],\[Beta]] sq[h] a**b,
             (up[h_?\[Theta]bQ][\[Alpha]_]*a___)**(low[h_?\[Theta]bQ][\[Beta]_]*b___) :> -1/2 \[Delta][\[Alpha],\[Beta]] sq[h] a**b,
             (low[h_?\[Theta]Q][\[Alpha]_]*a___)**(up[h_?\[Theta]Q][\[Beta]_]*b___) :> -1/2 \[Delta][\[Alpha],\[Beta]] sq[h] a**b,
             (low[h_?\[Theta]bQ][\[Alpha]_]*a___)**(up[h_?\[Theta]bQ][\[Beta]_]*b___) :> 1/2 \[Delta][\[Alpha],\[Beta]] sq[h] a**b};*)
Same\[Theta]rule = {a___**up[h_?\[Theta]Q][\[Alpha]_]**b___**up[h_?\[Theta]Q][\[Beta]_]**c___ :> -1/2 \[CurlyEpsilon][\[Alpha],\[Beta]] sq[h] a**b**c (-1)^FN[b],
             a___**low[h_?\[Theta]Q][\[Alpha]_]**b___**low[h_?\[Theta]Q][\[Beta]_]**c___ :> 1/2 \[CurlyEpsilon][\[Alpha],\[Beta]] sq[h] a**b**c (-1)^FN[b],
             a___**up[h_?\[Theta]bQ][\[Alpha]_]**b___**up[h_?\[Theta]bQ][\[Beta]_]**c___ :> 1/2 \[CurlyEpsilon][\[Alpha],\[Beta]] sq[h] a**b**c (-1)^FN[b],
             a___**low[h_?\[Theta]bQ][\[Alpha]_]**b___**low[h_?\[Theta]bQ][\[Beta]_]**c___ :> -1/2 \[CurlyEpsilon][\[Alpha],\[Beta]] sq[h] a**b**c (-1)^FN[b],
             a___**up[h_?\[Theta]Q][\[Alpha]_]**b___**low[h_?\[Theta]Q][\[Beta]_]**c___ :> 1/2 \[Delta][\[Alpha],\[Beta]] sq[h] a**b**c (-1)^FN[b],
             a___**up[h_?\[Theta]bQ][\[Alpha]_]**b___**low[h_?\[Theta]bQ][\[Beta]_]**c___ :> -1/2 \[Delta][\[Alpha],\[Beta]] sq[h] a**b**c (-1)^FN[b],
             a___**low[h_?\[Theta]Q][\[Alpha]_]**b___**up[h_?\[Theta]Q][\[Beta]_]**c___ :> -1/2 \[Delta][\[Alpha],\[Beta]] sq[h] a**b**c (-1)^FN[b],
             a___**low[h_?\[Theta]bQ][\[Alpha]_]**b___**up[h_?\[Theta]bQ][\[Beta]_]**c___ :> 1/2 \[Delta][\[Alpha],\[Beta]] sq[h] a**b**c (-1)^FN[b]};


(*Apply a replacement rule repeatedly expanding the expression every time*)
MAXITERATIONS=100;
Rex[expr_,rule_]:=FixedPoint[Expand[#]/.rule&,expr,MAXITERATIONS];
Rex[rule_]:=Rex[#,rule]&;


(*Writes any d-expression containing \[Chi] ,s and/or \[Theta](b) by putting explicitly the indices.*)
PutIndices[X__Plus]:=Plus@@(PutIndices/@(List@@X));
PutIndices[X__Times]:=NonCommutativeMultiply@@(PutIndices/@(List@@X));
PutIndices[X__NonCommutativeMultiply]:=NonCommutativeMultiply@@(PutIndices/@(List@@X));
PutIndices[X_^p_]/;IntegerQ[p]&&p>0:=Module[{tempNCM},Flatten[tempNCM@@(Table[PutIndices[X],{\[ScriptT],1,p}])/.{Times->tempNCM,NonCommutativeMultiply->tempNCM},Infinity,tempNCM]/.tempNCM->NonCommutativeMultiply];
PutIndices[X_^p_]:=X^p;
PutIndices[CC_?ConstQ]:=CC;


PutIndices[x_?xsqQ]:=x;
PutIndices[xdx_?xxQ]:=xdx;(*StringCases[toString[xdx],(a:{"x"~~DigitCharacter..})~~(b:{"x"~~DigitCharacter..}):>v[ToExpression[a]][#] v[ToExpression[b]][#]][[1]]&@Unique[\[Mu]];*)
PutIndices[d[something__]]/;Select[{something},StringMatchQ[toString[#],("\[Chi]"|"s"|"\[Theta]")~~__]&]==={}:=d[something];
PutIndices[d[a_?\[Eta]\[Theta]\[Chi]Q,b_?\[Eta]\[Theta]\[Chi]Q]]:=(up[a][#]**low[b][#])&@Unique[\[Alpha]];
PutIndices[d[a_?\[Eta]b\[Theta]b\[Chi]bQ,b_?\[Eta]b\[Theta]b\[Chi]bQ]]:=(low[a][#]**up[b][#])&@Unique[\[Alpha]d];
PutIndices[d[a_?\[Eta]\[Theta]\[Chi]Q,x_?xQ|x_?sQ,b_?\[Eta]b\[Theta]b\[Chi]bQ]]:=(v[x][#3]\[Sigma][#3][#1,#2]up[a][#1]**up[b][#2])&@@{Unique[\[Alpha]],Unique[\[Alpha]d],Unique[\[Mu]]};
PutIndices[d[(x_?xvQ|x_?svQ),(y_?xvQ|y_?svQ),a_?\[Eta]\[Theta]\[Chi]Q,\[Sigma][\[Mu]_,\[Nu]_],b_?\[Eta]\[Theta]\[Chi]Q]]:=((v[ToExpression[StringDrop[toString[Head[x]],-1]]]@@x)(v[ToExpression[StringDrop[toString[Head[y]],-1]]]@@y)\[Sigma][\[Mu],\[Nu]][#1,#2]up[a][#1]**low[b][#2]/.{\[Mu]->#3,\[Nu]->#4})&@@{Unique[\[Alpha]],Unique[\[Beta]],Unique[\[Mu]],Unique[\[Nu]]};
PutIndices[d[(x_?xvQ|x_?svQ),(y_?xvQ|y_?svQ),a_?\[Eta]b\[Theta]b\[Chi]bQ,\[Sigma]b[\[Mu]_,\[Nu]_],b_?\[Eta]b\[Theta]b\[Chi]bQ]]:=((v[ToExpression[StringDrop[toString[Head[x]],-1]]]@@x)(v[ToExpression[StringDrop[toString[Head[y]],-1]]]@@y)\[Sigma]b[\[Mu],\[Nu]][#1,#2]low[a][#1]**up[b][#2]/.{\[Mu]->#3,\[Nu]->#4})&@@{Unique[\[Alpha]d],Unique[\[Beta]d],Unique[\[Mu]],Unique[\[Nu]]};
PutIndices[th_?\[Theta]sqQ]:=up[ToExpression[StringDrop[toString[th],-2]]][#]**low[ToExpression[StringDrop[toString[th],-2]]][#]&@Unique[\[Alpha]];
PutIndices[th_?\[Theta]bsqQ]:=low[ToExpression[StringDrop[toString[th],-2]]][#]**up[ToExpression[StringDrop[toString[th],-2]]][#]&@Unique[\[Alpha]d];
PutIndices[xdx_?xsQ]:=StringCases[toString[xdx],(a:{"x"~~DigitCharacter..})~~(b:{"s"~~DigitCharacter..}):>v[ToExpression[a]][#] v[ToExpression[b]][#]][[1]]&@Unique[\[Mu]];
PutIndices[dxdx_?ssQ]:=StringCases[toString[dxdx],(a:{"s"~~DigitCharacter..})~~(b:{"s"~~DigitCharacter..}):>v[ToExpression[a]][#] v[ToExpression[b]][#]][[1]]&@Unique[\[Mu]];
PutIndices[d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],(x_?xvQ|x_?svQ),(y_?xvQ|y_?svQ),(z_?xvQ|z_?svQ),(t_?xvQ|t_?svQ)]]:=(\[Epsilon]4[#1,#2,#3,#4]((v[ToExpression[StringDrop[toString[Head[x]],-1]]]@@x)(v[ToExpression[StringDrop[toString[Head[y]],-1]]]@@y)(v[ToExpression[StringDrop[toString[Head[z]],-1]]]@@z)(v[ToExpression[StringDrop[toString[Head[t]],-1]]]@@t)/.{\[Mu]->#1,\[Nu]->#2,\[Rho]->#3,\[Lambda]->#4}))&@@{Unique[\[Mu]],Unique[\[Nu]],Unique[\[Rho]],Unique[\[Lambda]]};
PutIndices[d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],(x_?xvQ|x_?svQ),(y_?xvQ|y_?svQ),(z_?xvQ|z_?svQ),(t_?xvQ|t_?svQ)]]:=(\[Epsilon]4[#1,#2,#3,#4]((v[ToExpression[StringDrop[toString[Head[x]],-1]]]@@x)(v[ToExpression[StringDrop[toString[Head[y]],-1]]]@@y)(v[ToExpression[StringDrop[toString[Head[z]],-1]]]@@z)(v[ToExpression[StringDrop[toString[Head[t]],-1]]]@@t)/.{\[Kappa]->#1,\[Lambda]->#2,\[Mu]->#3,\[Nu]->#4}))&@@{Unique[\[Kappa]],Unique[\[Lambda]],Unique[\[Mu]],Unique[\[Nu]]};
PutIndices[d[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],(x_?xvQ|x_?svQ),(y_?xvQ|y_?svQ),(z_?xvQ|z_?svQ),a_?\[Eta]\[Theta]\[Chi]Q,si_?\[Sigma]Q,b_?\[Eta]b\[Theta]b\[Chi]bQ]]:=(\[Epsilon]4[#1,#2,#3,#4]((v[ToExpression[StringDrop[toString[Head[x]],-1]]]@@x)(v[ToExpression[StringDrop[toString[Head[y]],-1]]]@@y)(v[ToExpression[StringDrop[toString[Head[z]],-1]]]@@z)(si[#5,#6])/.{\[Mu]->#1,\[Nu]->#2,\[Rho]->#3,\[Lambda]->#4})up[a][#5]**up[b][#6])&@@{Unique[\[Mu]],Unique[\[Nu]],Unique[\[Rho]],Unique[\[Lambda]],Unique[\[Alpha]],Unique[\[Alpha]d]};
PutIndices[d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],(x_?xvQ|x_?svQ),(y_?xvQ|y_?svQ),(z_?xvQ|z_?svQ),a_?\[Eta]\[Theta]\[Chi]Q,si_?\[Sigma]Q,b_?\[Eta]b\[Theta]b\[Chi]bQ]]:=(\[Epsilon]4[#1,#2,#3,#4]((v[ToExpression[StringDrop[toString[Head[x]],-1]]]@@x)(v[ToExpression[StringDrop[toString[Head[y]],-1]]]@@y)(v[ToExpression[StringDrop[toString[Head[z]],-1]]]@@z)(si[#5,#6])/.{\[Kappa]->#1,\[Lambda]->#2,\[Mu]->#3,\[Nu]->#4})up[a][#5]**up[b][#6])&@@{Unique[\[Kappa]],Unique[\[Lambda]],Unique[\[Mu]],Unique[\[Nu]],Unique[\[Alpha]],Unique[\[Alpha]d]};


(* ::Subsection::Closed:: *)
(*Removal of placeholders for derivatives \[Chi] and s*)


(*Takes the derivative of the auxiliary variables \[Chi] and s*)
D\[Chi]up[\[Chi]_,\[ScriptA]_] := {low[\[Chi]][\[Beta]_]:>\[CurlyEpsilon][\[Beta],\[ScriptA]], up[\[Chi]][\[Beta]_]:>\[Delta][\[ScriptA],\[Beta]]};
D\[Chi]low[\[Chi]_,\[ScriptA]_] := {up[\[Chi]][\[Beta]_]:>\[CurlyEpsilon][\[Beta],\[ScriptA]], low[\[Chi]][\[Beta]_]:>\[Delta][\[ScriptA],\[Beta]]};
\[Sigma]Dx[s_,\[ScriptA]_,\[ScriptA]d_] := {v[s][\[Mu]_]:>\[Sigma][\[Mu]][\[ScriptA],\[ScriptA]d]};
\[Sigma]bDx[s_,\[ScriptA]d_,\[ScriptA]_] := {v[s][\[Mu]_]:>\[Sigma]b[\[Mu]][\[ScriptA]d,\[ScriptA]]};
Dx\[Mu][s_,\[ScriptM]_] := {v[s][\[Nu]_]:>\[ScriptG][\[ScriptM],\[Nu]]};


\[Theta]OrderedList[mon_Times|mon_NonCommutativeMultiply|mon_d]:=Module[{tempNCM},
    (*FN[x_tempNCM]:=Mod[Plus@@(FN/@List@@x),2]; This might be needed?*)
    Join[Flatten[tempNCM[mon]/.b_^p_/;IntegerQ[p]&&p>0:>tempNCM@@Table[b,{\[ScriptT],1,p}]/.{Times->tempNCM,NonCommutativeMultiply->tempNCM,d->tempNCM},Infinity,tempNCM]/.{low[a_][b_]:>a,up[a_][b_]:>a}/.{tempNCM[seq__] :> Cases[{seq},_?\[Theta]\[Theta]bQ]},
         Cases[mon,t_?\[Theta]\[Theta]bsqQ:>Sequence[ToExpression[StringDrop[toString[t],-2]],ToExpression[StringDrop[toString[t],-2]]],Infinity]]
];
\[Theta]OrderedList[b_^p_?IntegerQ]/;p>0:=Flatten[Table[\[Theta]OrderedList[b],{\[ScriptT],1,p}]];
\[Theta]OrderedList[b_^p_]:=\[Theta]OrderedList[b] (*This is ok as long as \[Theta]s don't appear inside real powers. They have to be expanded first*)
\[Theta]OrderedList[mon_]/;(Head[Head[mon]]===up||Head[Head[mon]]===low):=Cases[{mon[[0,1]]},t_?\[Theta]\[Theta]bQ];
\[Theta]OrderedList[mon_]/;(Head[Head[mon]]===v):={};
\[Theta]OrderedList[mon_?\[Theta]\[Theta]bsqQ]:={ToExpression[StringDrop[toString[mon],-2]],ToExpression[StringDrop[toString[mon],-2]]};
\[Theta]OrderedList[mon_?xxQ|mon_?xsqQ]:={};
\[Theta]OrderedList[sym_?ConstQ]:={};
\[Theta]OrderedList[sym_]/;Head[sym]===\[Delta]:={};
\[Theta]OrderedList[sym_]/;Head[sym]===\[CurlyEpsilon]:={};


(*This function takes an expression with \[Chi]'s and s's that need to be contracted*)
(*The parameters are:
  derivative : the replacement used to contract \[Chi] and s (it's a pure function, not a replacement table)
  redRules : (Optional) specify the reduction rules
  contRules : (Optional) specify the reduction rules
*)
(*Note: the order PauliRed,PauliRulesgen matters: if you do PauliRulesgen before it'll contract stuff in a stupid way and require more identities*)
Rulesremove\[Chi]s=<|redRules:>Join[PauliRulesgen,PauliRed,PauliRules\[Epsilon]4],contRules:>Join[PauliRulesgen,ContrRule]|>;
remove\[Chi]s::contractionFail="I couldn't contract the indices in the following expression:\n `1`";
(*This prevents Auxremeve\[Chi]s to evaluate the rules red and cont, so that they evaluate everytime they are called*)
SetAttributes[Auxremove\[Chi]s,HoldRest];
remove\[Chi]s[expr_,derivative_]:=Module[{expanded},Plus@@(Auxremove\[Chi]s[#,derivative,Rulesremove\[Chi]s[redRules],Rulesremove\[Chi]s[contRules]]&/@(If[(Head[expanded=Expand[expr]])===Plus,List@@expanded,{expanded}]))];
Auxremove\[Chi]s[a_?ConstQ*b_,derivative_,red_,cont_]:=a Auxremove\[Chi]s[b,derivative,red,cont];
Auxremove\[Chi]s[a_?ConstQ,derivative_,red_,cont_]:=a;
Auxremove\[Chi]s[expr_Times|expr_d|expr_NonCommutativeMultiply|expr_Symbol,derivative_,red_,cont_]:=Module[{uptosign,uptosignl,iniorder},
   (*applies the derivative and the rule for same \[Theta]s*)
     uptosign=(derivative[PutIndices[expr]]//.Same\[Theta]rule);
   (*memorizes the initial order of \[Theta]s*)
     iniorder = \[Theta]OrderedList[uptosign];
   (*removes the ** product and applies all reduction rules*)
     uptosign=(uptosign/.NonCommutativeMultiply->Times//Rex[red]//Rex[cont])/.Times-> NonCommutativeMultiply;
   (*lists the terms*)
     uptosignl=If[Head[uptosign]===Plus,List@@(Expand[uptosign]),{uptosign}];
   (*Puts to zero terms with too many \[Theta] or \[Theta]b*)
     uptosignl = DeleteCases[(If[tooMany\[Theta]Q[#]||tooMany\[Theta]bQ[#],Nothing,#]&)/@uptosignl,0];(*Print[iniorder,">>>",uptosignl];*)
   (*Checking for this error message could waste some time. For now let's have it commented out*)
     (*Do[If[Cases[el,up[x_][y_]|low[z_][t_]|v[o_][p_]|\[Epsilon]4[w__]|\[Delta][v__]|\[CurlyEpsilon][u__]|\[Sigma][m__][n__],Infinity]=!={},Message[remove\[Chi]s::contractionFail,Cases[el,up[x_][y_]|low[z_][t_]|v[o_][p_]|\[Epsilon]4[w__]|\[Delta][v__]|\[CurlyEpsilon][u__]|\[Sigma][m__][n__],Infinity]]],{el,uptosignl}];*)
   (*Returns the sum where terms not agreeing with the initial ordering are multiplied by the signature of the permutation*)
     Plus@@(Signature[PermutationList[Check[FindPermutation[\[Theta]OrderedList[#],iniorder],Message[remove\[Chi]s::contractionFail,Times@@(Cases[#,up[x_][y_]|low[z_][t_]|v[o_][p_]|\[Epsilon]4[w__]|\[Delta][v__]|\[CurlyEpsilon][u__]|\[Sigma][m__][n__],Infinity])];$Failed,FindPermutation::norel]]]*#&/@uptosignl)
];


(* ::Subsection::Closed:: *)
(*Reduction*)


RulesReduction=<|redRules:>Join[PauliRulesgen,PauliRed,PauliRules\[Epsilon]4],contRules:>Join[PauliRulesgen,ContrRule]|>;
Reduction::contractionFail="I couldn't contract the indices in the following expression:\n `1`";
(*This prevents AuxReduction to evaluate the rules red and cont, so that they evaluate everytime they are called*)
SetAttributes[AuxReduction,HoldRest];
Reduction[expr_]:=Module[{expanded},Plus@@(AuxReduction[#,RulesReduction[redRules],RulesReduction[contRules]]&/@(If[(Head[expanded=Expand[expr]])===Plus,List@@expanded,{expanded}]))];
AuxReduction[a_?ConstQ*b_,red_,cont_]:=a AuxReduction[b,red,cont];
AuxReduction[a_?ConstQ,red_,cont_]:=a;
AuxReduction[expr_,red_,cont_]:=Module[{exprNCM,tempNCM,uptosign,uptosignl,iniorder},
   (*applies the rule for same \[Theta]s*)
     uptosign=(Flatten[tempNCM[PutIndices[expr]/.{Times->tempNCM,NonCommutativeMultiply->tempNCM}],Infinity,tempNCM]/.tempNCM->NonCommutativeMultiply)//Rex[Same\[Theta]rule];    (*Here you need to make sure that you apply all possible Same\[Theta]rule, because afterwards you take away the ** and you may miss some signs*)
   (*memorizes the initial order of \[Theta]s*)
     iniorder = \[Theta]OrderedList[uptosign];
   (*removes the ** product and applies all reduction rules*)
     uptosign=(uptosign/.NonCommutativeMultiply->Times//Rex[red]//Rex[cont])/.Times-> NonCommutativeMultiply;
   (*lists the terms*)
     uptosignl=If[Head[uptosign]===Plus,List@@(Expand[uptosign]),{uptosign}];
   (*Puts to zero terms with too many \[Theta] or \[Theta]b*)
     uptosignl = DeleteCases[(If[tooMany\[Theta]Q[#]||tooMany\[Theta]bQ[#],Nothing,#]&)/@uptosignl,0];
   (*Returns the sum where terms not agreeing with the initial ordering are multiplied by the signature of the permutation*)
     Plus@@(Signature[PermutationList[Check[FindPermutation[\[Theta]OrderedList[#],iniorder],Message[Reduction::contractionFail,Times@@(Cases[#,up[x_][y_]|low[z_][t_]|v[o_][p_]|\[Epsilon]4[w__]|\[Delta][v__]|\[CurlyEpsilon][u__]|\[Sigma][m__][n__],Infinity])];$Failed,FindPermutation::norel]]]*#&/@uptosignl)
]


(* ::Subsection::Closed:: *)
(*Memoize and apply functions*)


memoizeAndEvaluate[f_][expr_]:=Module[{set},
    set[Activate[Inactivate[f[expr]],Function],
        f[expr]
        ]/.set[a__]:>Hold[set[a]]/.Inactive[x_]:>x/.set->Set//ReleaseHold]


fastApply[f_][expr_]:=Module[{all,\[Theta]Lis,xsqLis,\[ScriptI]=1,ruledelayed,expression},
    If[working\[Theta]s === {} || workingxs === {},
    all = Join[Cases[expr,x_d:>Sequence@@x,Infinity(*,-1*)],Cases[expr,x_?sqQ,{-1}(*,-1*)],Cases[expr,x_?xxQ,{-1}(*,-1*)]];
    all = DeleteDuplicates[all//.{
          x_?\[Sigma]Q->Nothing,x_?\[Sigma]bQ -> Nothing,x_Power->Nothing,
          x_?\[Theta]\[Theta]bsqQ :> ToExpression[StringDrop[toString[x],-2]]}];
    \[Theta]Lis = Join[Select[all,\[Theta]Q],Select[all,\[Theta]bQ]];
    xsqLis = Select[all,xsqQ[#]||xxQ[#]&];,
    \[Theta]Lis = working\[Theta]s;
    xsqLis = workingxs;];
    THETA/:ConstQ[THETA[i_]]:=True;
    expression=(Collect[Rex[
        Join[
            Table[ruledelayed[th \[ScriptD][yPattern___], \[ScriptD][yPattern,th]]/.ruledelayed->RuleDelayed,{th,sq/@\[Theta]Lis}],
            Table[ruledelayed[xsqr^pPattern_. \[ScriptD][yPattern___], \[ScriptD][yPattern,xsqr^pPattern]]/.ruledelayed->RuleDelayed,{xsqr,xsqLis}],
           {HoldPattern[NonCommutativeMultiply[x__]\[ScriptD][y___]]:>\[ScriptD][y,NonCommutativeMultiply[x]],
           d[x__]^j_. \[ScriptD][y___]:>\[ScriptD][y,d[x]^j], A_^pow_ \[ScriptD][y___] :> \[ScriptD][y,A^pow]}
        ]][
       Expand[(expr/.Flatten[Table[{th -> THETA[\[ScriptI]] th, sq[th] -> THETA[\[ScriptI]++]^2 sq[th]},{th,\[Theta]Lis}]]/.{
               THETA[i_]^p_/;p>2 -> 0}/.{THETA[i_]->1}) \[ScriptD][]]],
    \[ScriptD][___],THETA[Factor[#]]&]/.\[ScriptD][x___]:>If[Head[f]===Function,memoizeAndEvaluate[f][Times[x]],(f[Times[x]]=f[Times[x]])]);
    Collect[expression, THETA[___]]/.THETA[x_]:>x
];


fastReduction[expr_]:=Module[{all,\[Theta]Lis,\[ScriptI]=1,ruledelayed,expression},
    If[working\[Theta]s === {},
    all = Join[Cases[expr,x_d:>Sequence@@x,Infinity(*,-1*)],Cases[expr,x_?sqQ,{-1}(*,-1*)],Cases[expr,x_?xxQ,{-1}(*,-1*)]];
    all = DeleteDuplicates[all//.{
          x_?\[Sigma]Q->Nothing,x_?\[Sigma]bQ -> Nothing,x_Power->Nothing,
          x_?sqQ :> ToExpression[StringDrop[toString[x],-2]]}];
    \[Theta]Lis = Join[Select[all,\[Theta]Q],Select[all,\[Theta]bQ]],
    \[Theta]Lis = working\[Theta]s];
    THETA/:ConstQ[THETA[i_]]:=True;
    expression=(Collect[Rex[
        Join[
            Table[ruledelayed[th \[ScriptD][yPattern___], \[ScriptD][yPattern,th]]/.ruledelayed->RuleDelayed,{th,sq/@\[Theta]Lis}],
           {HoldPattern[NonCommutativeMultiply[x__]\[ScriptD][y___]]:>\[ScriptD][y,NonCommutativeMultiply[x]],d[x__]^j_. \[ScriptD][y___]:>\[ScriptD][y,d[x]^j]}
        ]][
       (Expand[expr/.Flatten[Table[{th -> THETA[\[ScriptI]] th, sq[th] -> THETA[\[ScriptI]++]^2 sq[th]},{th,\[Theta]Lis}]]
               ]/.{THETA[i_]^p_/;p>2 -> 0}/.{THETA[i_]->1}) \[ScriptD][]],
    \[ScriptD][___],THETA[Factor[#]]&]/.\[ScriptD]->Times);
    Collect[expression, THETA[___], (Reduction[#]=Reduction[#])&]/.THETA[x_]:>x
];


(*Memoized version of some functions*)
fdxsq[a_,b_]:=fastApply[dxsq[#,b]&][a];
f\[Theta]d\[Eta][a_,b_,c_]:=fastApply[\[Theta]d\[Eta][#,b,c]&][a];
f\[Theta]bd\[Eta]b[a_,b_,c_]:=fastApply[\[Theta]bd\[Eta]b[#,b,c]&][a];
fd\[Theta]d\[Eta][a_,b_,c_]:=fastApply[d\[Theta]d\[Eta][#,b,c]&][a];
fd\[Theta]bd\[Eta]b[a_,b_,c_]:=fastApply[d\[Theta]bd\[Eta]b[#,b,c]&][a];
fd\[Eta]d\[Eta][a_,b_,c_]:=fastApply[d\[Eta]d\[Eta][#,b,c]&][a];
fd\[Eta]bd\[Eta]b[a_,b_,c_]:=fastApply[d\[Eta]bd\[Eta]b[#,b,c]&][a];
(**)
fd\[Eta]dxd\[Eta]b[a_,b_,c_,d_]:=fastApply[d\[Eta]dxd\[Eta]b[#,b,c,d]&][a];
f\[Eta]dxd\[Eta]b[a_,b_,c_,d_]:=fastApply[\[Eta]dxd\[Eta]b[#,b,c,d]&][a];
fd\[Eta]dx\[Eta]b[a_,b_,c_,d_]:=fastApply[d\[Eta]dx\[Eta]b[#,b,c,d]&][a];
f\[Eta]dx\[Eta]b[a_,b_,c_,d_]:=fastApply[\[Eta]dx\[Eta]b[#,b,c,d]&][a];
f\[Theta]dxd\[Eta]b[a_,b_,c_,d_]:=fastApply[\[Theta]dxd\[Eta]b[#,b,c,d]&][a];
fd\[Eta]dx\[Theta]b[a_,b_,c_,d_]:=fastApply[d\[Eta]dx\[Theta]b[#,b,c,d]&][a];
fd\[Eta]xd\[Eta]b[a_,b_,c_,d_]:=fastApply[d\[Eta]xd\[Eta]b[#,b,c,d]&][a];
fd\[Theta]dx\[Theta]b[a_,b_,c_,d_]:=fastApply[d\[Theta]dx\[Theta]b[#,b,c,d]&][a];
f\[Theta]dxd\[Theta]b[a_,b_,c_,d_]:=fastApply[\[Theta]dxd\[Theta]b[#,b,c,d]&][a];
(**)
fChiralDm[ expr_,\[Eta]_,\[Theta]_,x_]:= -fd\[Theta]d\[Eta][expr,\[Theta],\[Eta]] + I fd\[Eta]dx\[Theta]b[expr/.sq[bar[\[Theta]]]->0,\[Eta],x,bar[\[Theta]]];
fChiralDbm[expr_,\[Eta]b_,\[Theta]_,x_]:= -fd\[Theta]bd\[Eta]b[expr,bar[\[Theta]],\[Eta]b] - I f\[Theta]dxd\[Eta]b[expr/.sq[\[Theta]]->0,\[Theta],x,\[Eta]b];
fChiralQm[ expr_,\[Eta]_,\[Theta]_,x_]:= -fd\[Theta]d\[Eta][expr,\[Theta],\[Eta]] - I fd\[Eta]dx\[Theta]b[expr/.sq[bar[\[Theta]]]->0,\[Eta],x,bar[\[Theta]]];
fChiralQbm[expr_,\[Eta]b_,\[Theta]_,x_]:= -fd\[Theta]bd\[Eta]b[expr,bar[\[Theta]],\[Eta]b] + I f\[Theta]dxd\[Eta]b[expr/.sq[\[Theta]]->0,\[Theta],x,\[Eta]b];


(* ::Subsection::Closed:: *)
(*From an input with explicit indices*)


RulesContraction=<|redRules:>Join[PauliRulesgen,PauliRed,PauliRules\[Epsilon]4],contRules:>Join[PauliRulesgen,ContrRule]|>;
(*This prevents Contraction to evaluate the rules red and cont, so that they evaluate everytime they are called*)
SetAttributes[AuxContraction,HoldRest];
Contraction[expr_]:=Module[{expanded},Plus@@(AuxContraction[#,RulesContraction[redRules],RulesContraction[contRules]]&/@(If[(Head[expanded=Expand[expr]])===Plus,List@@expanded,{expanded}]))];
AuxContraction[a_?NumericQ*b_,red_,cont_]:=a AuxContraction[b,red,cont];
AuxContraction[a_?NumericQ,red_,cont_]:=a;
AuxContraction[expr_,red_,cont_]:=Module[{exprNCM,tempNCM,uptosign,uptosignl,iniorder},
   (*applies the derivative and the rule for same \[Theta]s*)
     uptosign=(Flatten[tempNCM[expr/.d[X__]^p_:>PutIndices[d[X]^p]/.{d[A__]:>PutIndices[d[A]],X_?\[Theta]\[Theta]bsqQ:>PutIndices[X]
                                     }/.{Times->tempNCM,NonCommutativeMultiply->tempNCM}],Infinity,tempNCM]/.tempNCM->NonCommutativeMultiply)//Rex[Same\[Theta]rule];    (*Here you need to make sure that you apply all possible Same\[Theta]rule, because afterwards you take away the ** and you may miss some signs*)
   (*memorizes the initial order of \[Theta]s*)
     iniorder = \[Theta]OrderedList[uptosign];
   (*removes the ** product and applies all reduction rules*)
     uptosign=(uptosign/.NonCommutativeMultiply->Times//Rex[red]//Rex[cont])/.Times-> NonCommutativeMultiply;
   (*lists the terms*)
     uptosignl=If[Head[uptosign]===Plus,List@@(Expand[uptosign]),{uptosign}];
   (*Puts to zero terms with too many \[Theta] or \[Theta]b*)
     uptosignl = DeleteCases[(If[tooMany\[Theta]Q[#]||tooMany\[Theta]bQ[#],Nothing,#]&)/@uptosignl,0];
   (*Returns the sum where terms not agreeing with the initial ordering are multiplied by the signature of the permutation*)
     Plus@@(Signature[PermutationList[Check[FindPermutation[\[Theta]OrderedList[#],iniorder],Message[Reduction::contractionFail,Times@@(Cases[#,up[x_][y_]|low[z_][t_]|v[o_][p_]|\[Epsilon]4[w__]|\[Delta][v__]|\[CurlyEpsilon][u__]|\[Sigma][m__][n__],Infinity])];$Failed,FindPermutation::norel]]]*#&/@uptosignl)
]


(* ::Subsection::Closed:: *)
(*Taylor expansion*)


(*Raises an expression to a positive integer power*)
FPower[expr_,p_?IntegerQ]/;p>0:=Reduction[NonCommutativeMultiply@@Table[expr,{i,1,p}]];
FPower[expr_,0]:=1;
(*Raises an expression to a real power*)
FPower[expr_,around_,p_,n_]/;\[Theta]FreeQ[around]:=around^p Sum[Binomial[p,k]FPower[(expr-around)/around,k],{k,0,n}];
(*Computes the Taylor expansion of f[expr] around expr = 0.*)
FTaylor[expr_,f_,n_]:=Sum[ SeriesCoefficient[f[x],{x,0,k}] FPower[expr,k],{k,0,n}]


(* ::Section::Closed:: *)
(*Derivatives*)


(* ::Subsection::Closed:: *)
(*Definitions*)


(*Leibniz and graded Leibniz rules. These are supposed to be used only on expressions with head Times or NonCommutativeMultiply*)
Leibniz[f_]:=Module[{factors=List@@#},
    Plus@@Table[(MapAt[Function[{x},f[x,##2]],factors,iter]/.List->Head[#]),{iter,1,Length@factors}]
]&;
(*GradedLeibnizOld[f_]:=Module[{factors,tempNCM,grading},
    factors = Flatten[tempNCM[#]/.{Times->tempNCM,NonCommutativeMultiply->tempNCM},Infinity,tempNCM];
    factors = If[Head[factors]===tempNCM,List@@factors,{factors}]/.tempNCM->Times;
    grading[list_]:=(-1)^(Plus@@(FN/@(list/.tempNCM->Times)));
    Plus@@Table[grading[factors[[1;;iter-1]]]*(MapAt[Function[{x},f[x,##2]],factors,iter]/.List-> NonCommutativeMultiply/.tempNCM->NonCommutativeMultiply),{iter,1,Length@factors}]   
]&;*)
GradedLeibniz[f_]:=Module[{expr = #, expanded, sumexpr, factors, tempNCM, grading},
    expanded = Expand[expr];
    sumexpr = If[Head[expanded]===Plus,List@@expanded, {expanded}];
    grading[list_]:=(-1)^(Plus@@(FN/@(list/.tempNCM->Times)));
    Sum[
        factors = Flatten[tempNCM[ex]/.{Times->tempNCM,NonCommutativeMultiply->tempNCM},Infinity,tempNCM];
        factors = If[Head[factors]===tempNCM,List@@factors,{factors}]/.tempNCM->Times;
        Plus@@Table[grading[factors[[1;;iter-1]]]*(MapAt[Function[{x},f[x,##2]],factors,iter]/.List-> NonCommutativeMultiply/.tempNCM->NonCommutativeMultiply),{iter,1,Length@factors}]
    ,{ex, sumexpr}]
]&;
dPower[f_,\[ScriptL]_]:=\[ScriptL] #^(\[ScriptL]-1) f[#,##2]&;


applyRuleSequence[rules__][expr_]:=Fold[#1/.#2&,expr,{rules}];


\[Eta]to\[Chi][sym_Symbol]:=ToExpression[StringReplace[ToString[sym],"\[Eta]"->"\[Chi]"]];


(* ::Subsection::Closed:: *)
(*\[Chi]\[PartialD]\[Eta], \[Chi]\[PartialD]\[Theta] and s\[PartialD]x*)


(*These functions' only purpose is to replace \[Eta],\[Theta] with \[Chi] and x with s and apply the Leibniz rule in doing so*)


(*\[Chi]\[PartialD]\[Eta] = \[Chi]^\[Alpha]\[PartialD]/\[PartialD]\[Eta]^\[Alpha] or = Overscript[\[Chi], _]^Overscript[\[Alpha], .]\[PartialD]/\[PartialD]Overscript[\[Eta], _]^Overscript[\[Alpha], .]*)
(*Define the fermion numbers of this operator*)
\[Chi]d\[Eta]/:FN[\[Chi]d\[Eta][expr_,\[Eta]_,\[Chi]_]]:=FN[expr];
(*Define the behaviour on +, * and ** *)
\[Chi]d\[Eta][expr_Plus,\[Eta]_,\[Chi]_]:=Plus@@(\[Chi]d\[Eta][#,\[Eta],\[Chi]]&/@List@@(expr));
\[Chi]d\[Eta][expr_Times|expr_NonCommutativeMultiply,\[Eta]_,\[Chi]_]:=Leibniz[\[Chi]d\[Eta]][expr,\[Eta],\[Chi]];
\[Chi]d\[Eta][expr_^\[ScriptL]_,\[Eta]_,\[Chi]_]:=dPower[\[Chi]d\[Eta],\[ScriptL]][expr,\[Eta],\[Chi]];
\[Chi]d\[Eta][expr_?ConstQ,\[Eta]_,\[Chi]_]:=0;
(*Actual definition*)
\[Chi]d\[Eta][expr_d,\[Eta]_,\[Chi]_]/;Length[Cases[expr,\[Eta]]]==1:=expr/.\[Eta]->\[Chi];
\[Chi]d\[Eta][expr_d,\[Eta]_,\[Chi]_]/;FreeQ[expr,\[Eta]]:=0;
\[Chi]d\[Eta][d[x_?xvQ,y_?xvQ,\[Eta]_,si_?\[Sigma]Q|si_?\[Sigma]bQ,\[Eta]_],\[Eta]_,\[Chi]_]:=2d[x,y,\[Chi],si,\[Eta]];
\[Chi]d\[Eta][a_?xxQ,\[Eta]_,\[Chi]_]:=0;
\[Chi]d\[Eta][a_?xsQ,\[Eta]_,\[Chi]_]:=0;
\[Chi]d\[Eta][a_?ssQ,\[Eta]_,\[Chi]_]:=0;
\[Chi]d\[Eta][a_?sqQ,\[Eta]_,\[Chi]_]:=0;


(*\[Chi]\[PartialD]\[Theta] = \[Chi]^\[Alpha]\[PartialD]/\[PartialD]\[Theta]^\[Alpha] or = Overscript[\[Chi], _]^Overscript[\[Alpha], .]\[PartialD]/\[PartialD]Overscript[\[Theta], _]^Overscript[\[Alpha], .]*)
(*Define the fermion numbers of this operator*)
\[Chi]d\[Theta]/:FN[\[Chi]d\[Theta][expr_,\[Theta]_,\[Chi]_]]:=Mod[FN[expr]+1,2];
(*Define the behaviour on +, * and ** *)
\[Chi]d\[Theta][expr_Plus,\[Theta]_,\[Chi]_]:=Plus@@(\[Chi]d\[Theta][#,\[Theta],\[Chi]]&/@List@@(expr));
\[Chi]d\[Theta][expr_Times|expr_NonCommutativeMultiply,\[Theta]_,\[Chi]_]:=GradedLeibniz[\[Chi]d\[Theta]][expr,\[Theta],\[Chi]];
\[Chi]d\[Theta][expr_^\[ScriptL]_,\[Eta]_,\[Chi]_]:=dPower[\[Chi]d\[Theta],\[ScriptL]][expr,\[Eta],\[Chi]];
\[Chi]d\[Theta][expr_?ConstQ,\[Theta]_,\[Chi]_]:=0;
(*Actual definition*)
\[Chi]d\[Theta][d[\[Theta]_,\[Theta]2_],\[Theta]_,\[Chi]_]:=d[\[Chi],\[Theta]2];
\[Chi]d\[Theta][d[\[Theta]2_,\[Theta]_],\[Theta]_,\[Chi]_]:=((-1)^FN[\[Theta]2])d[\[Theta]2,\[Chi]];
\[Chi]d\[Theta][d[\[Theta]_,x_,\[Theta]b_],\[Theta]_,\[Chi]_]:=d[\[Chi],x,\[Theta]b];
\[Chi]d\[Theta][d[\[Theta]_,x_,\[Theta]b_],\[Theta]b_,\[Chi]b_]:=((-1)^FN[\[Theta]])d[\[Theta],x,\[Chi]b];
\[Chi]d\[Theta][d[x_?xvQ,y_?xvQ,\[Theta]_,si_?\[Sigma]Q|si_?\[Sigma]bQ,z_],\[Theta]_,\[Chi]_]:=d[x,y,\[Chi],si,z];
\[Chi]d\[Theta][d[x_?xvQ,y_?xvQ,z_,si_?\[Sigma]Q|si_?\[Sigma]bQ,\[Theta]_],\[Theta]_,\[Chi]_]:=((-1)^FN[z])d[x,y,z,si,\[Chi]];
\[Chi]d\[Theta][d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x1_,x2_,x3_,\[Theta]_,si_?\[Sigma]Q,\[Theta]b_],\[Theta]_,\[Chi]_]:=d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x1,x2,x3,\[Chi],si,\[Theta]b];
\[Chi]d\[Theta][d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x1_,x2_,x3_,\[Theta]_,si_?\[Sigma]Q,\[Theta]b_],\[Theta]b_,\[Chi]b_]:=((-1)^FN[\[Theta]])d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x1,x2,x3,\[Theta],si,\[Chi]b];
\[Chi]d\[Theta][expr_d,\[Theta]_,\[Chi]_]/;FreeQ[expr,\[Theta]]:=0;
\[Chi]d\[Theta][expr_?\[Theta]\[Theta]bsqQ,\[Theta]_,\[Chi]_]:=If[StringMatchQ[toString[expr],toString[\[Theta]]~~"sq"],2d[\[Chi],\[Theta]],0];
\[Chi]d\[Theta][a_?xxQ,\[Theta]_,\[Chi]_]:=0;
\[Chi]d\[Theta][a_?xsQ,\[Eta]_,\[Chi]_]:=0;
\[Chi]d\[Theta][a_?ssQ,\[Eta]_,\[Chi]_]:=0;
\[Chi]d\[Theta][a_?sqQ,\[Theta]_,\[Chi]_]:=0;


(*s\[PartialD]x = s^\[Mu]\[PartialD]/\[PartialD]x^\[Mu]*)
(*Define the fermion numbers of this operator*)
sdx/:FN[sdx[expr_,x_,s_]]:=FN[expr];
(*Define the behaviour on +, * and ** *)
sdx[expr_Plus,x_,s_]:=Plus@@(sdx[#,x,s]&/@List@@(expr));
sdx[expr_Times|expr_NonCommutativeMultiply,x_,s_]:=Leibniz[sdx][expr,x,s];
sdx[expr_^\[ScriptL]_,\[Eta]_,\[Chi]_]:=dPower[sdx,\[ScriptL]][expr,\[Eta],\[Chi]];
sdx[expr_?ConstQ,x_,s_]:=0;
(*These two functions return 1,-1 or 0: e.g xMatchQ[x12,x1]=1,xMatchQ[x12,x2]=-1,xMatchQ[x12,x3]=0*)
xMatchQ[xx_,x_]:=If[StringMatchQ[ToString[xx],ToString[x]~~DigitCharacter..],1,If[StringMatchQ[ToString[xx],"x"~~DigitCharacter~~StringTake[ToString[x],-1]],-1,If[StringMatchQ[ToString[xx],ToString[x]],1,0]]];
xvMatchQ[xx_,x_]:=If[StringMatchQ[ToString[Head[xx]],ToString[x]~~DigitCharacter..~~"v"],1,If[StringMatchQ[ToString[Head[xx]],"x"~~DigitCharacter~~StringTake[ToString[x],-1]~~"v"],-1,If[StringMatchQ[ToString[Head[xx]],ToString[x]~~"v"],1,0]]];
(*Actual definition*)
sdx[d[H_,xx_,Hb_],x_,s_]:=xMatchQ[xx,x]d[H,s,Hb];
sdx[d[x1_?xvQ,x2_?xvQ,\[Eta]1_,si_?\[Sigma]Q|si_?\[Sigma]bQ,\[Eta]2_],x_,s_]:=xvMatchQ[x1,x]d[Symbol[ToString[s]<>"v"]@@x1,x2,\[Eta]1,si,\[Eta]2]+xvMatchQ[x2,x]d[x1,Symbol[ToString[s]<>"v"]@@x2,\[Eta]1,si,\[Eta]2];
sdx[d[s1_?svQ,x2_?xvQ,\[Eta]1_,si_?\[Sigma]Q|si_?\[Sigma]bQ,\[Eta]2_],x_,s_]:=xvMatchQ[x2,x]d[s1,Symbol[ToString[s]<>"v"]@@x2,\[Eta]1,si,\[Eta]2];
sdx[d[x1_?xvQ,s2_?svQ,\[Eta]1_,si_?\[Sigma]Q|si_?\[Sigma]bQ,\[Eta]2_],x_,s_]:=xvMatchQ[x1,x]d[Symbol[ToString[s]<>"v"]@@x1,s2,\[Eta]1,si,\[Eta]2];
sdx[d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x1_?xvQ|x1_?svQ,x2_?xvQ|x2_?svQ,x3_?xvQ|x3_?svQ,\[Eta]1_,si_?\[Sigma]Q|si_?\[Sigma]bQ,\[Eta]b2_],x_,s_]:=xvMatchQ[x1,x]d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],Symbol[ToString[s]<>"v"]@@x1,x2,x3,\[Eta]1,si,\[Eta]b2]+xvMatchQ[x2,x]d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x1,Symbol[ToString[s]<>"v"]@@x2,x3,\[Eta]1,si,\[Eta]b2]+xvMatchQ[x3,x]d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x1,x2,Symbol[ToString[s]<>"v"]@@x3,\[Eta]1,si,\[Eta]b2];
sdx[d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x1_?xvQ|x1_?svQ,x2_?xvQ|x2_?svQ,x3_?xvQ|x3_?svQ,x4_?xvQ|x4_?svQ],x_,s_]:=xvMatchQ[x1,x]d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],Symbol[ToString[s]<>"v"]@@x1,x2,x3,x4]+xvMatchQ[x2,x]d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x1,Symbol[ToString[s]<>"v"]@@x2,x3,x4]+xvMatchQ[x3,x]d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x1,x2,Symbol[ToString[s]<>"v"]@@x3,x4]+xvMatchQ[x4,x]d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],x1,x2,x3,Symbol[ToString[s]<>"v"]@@x4];
sdx[a_?sqQ,x_,s_]:=xMatchQ[StringDrop[ToString[a],-2],x]*2*Symbol[StringDrop[ToString[a],-2]<>ToString[s]];
sdx[a_?xxQ,x_,s_]:=Module[{aa},aa=StringCases[ToString[a],{(e:("x"~~DigitCharacter..))~~(f:("x"~~DigitCharacter..)):>{e,f}}][[1]];xMatchQ[aa[[1]],x]Symbol[aa[[2]]<>ToString[s]]+xMatchQ[aa[[2]],x]Symbol[aa[[1]]<>ToString[s]]];
sdx[a_?xsQ,x_,s_]:=Module[{aa},aa=StringCases[ToString[a],{(e:("x"~~DigitCharacter..))~~(f:("s"~~DigitCharacter..)):>{e,f},(e:("s"~~DigitCharacter..))~~(f:("x"~~DigitCharacter..)):>{f,e}}][[1]];xMatchQ[aa[[1]],x]Symbol[aa[[2]]<>ToString[s]]];
sdx[d[a_,b_],x_,s_]:=0;


(*This is an alias, but careful: the arguments are swapped!*)
\[Eta]d\[Eta][expr_,\[Chi]_,\[Eta]_]:=\[Chi]d\[Eta][expr,\[Eta],\[Chi]];
\[Eta]d\[Theta][expr_,\[Chi]_,\[Theta]_]:=\[Chi]d\[Theta][expr,\[Theta],\[Chi]];
\[Eta]bd\[Eta]b[expr_,\[Chi]b_,\[Eta]b_]:=\[Chi]d\[Eta][expr,\[Eta]b,\[Chi]b];
\[Eta]bd\[Theta]b[expr_,\[Chi]b_,\[Theta]b_]:=\[Chi]d\[Theta][expr,\[Theta]b,\[Chi]b];
xdx[expr_,s_,x_]:=sdx[expr,x,s];  (*Check xdx[x1x2,x1,x2] = x1x1 !!*)


(* ::Subsection::Closed:: *)
(*Composite operators*)


defineD::argumentsNotPresent="The arguments `2` are not all present in `1`.";
SetAttributes[defineD,HoldFirst];


(*This is a function that assigns to symbol a differential operator that is given by the expression operator. 
   The elements in derArguments are the derivatives and the ones in multArguments are the ones that appear multiplied.*)
defineD[symbol_Symbol,operator_,derArguments_List,multArguments_List]:=Module[{
    arguments = Join[derArguments,multArguments],
    multArgumentsUnique, multArgumentsUniqueRule,
    derArguments\[Chi]Rule,
    derArgumentsOrdered, derArgumentsBut\[Chi],
    expandedOp,
    replacementRules={},elementaryDerivatives={},
    dsd\[Eta]d\[Theta],opRule,tempNCM
    },
    
    ClearAll[symbol];
    symbol/:FN[symbol[expr_,args_]]:=Evaluate[FN[operator]];
    
    If[Or@@(Length[Cases[operator,#,\[Infinity]]]==0&/@arguments),
        Message[defineD::argumentsNotPresent,operator,arguments]
    ];
    
    multArgumentsUniqueRule = Table[marg -> Unique[ToString[marg]],{marg,multArguments}];
    multArgumentsUnique = multArguments /. multArgumentsUniqueRule;
    
    derArguments\[Chi]Rule = Table[darg -> \[Eta]to\[Chi][darg],{darg,derArguments}];
    derArgumentsBut\[Chi] = derArguments /. derArguments\[Chi]Rule;

    expandedOp = PutIndices[operator/.derArguments\[Chi]Rule] /. multArgumentsUniqueRule;
    
    derArgumentsOrdered = Cases[
        Flatten[
            expandedOp /. {Times->tempNCM,NonCommutativeMultiply->tempNCM},
            \[Infinity],tempNCM]/.{low[x_][a_]:>x,up[x_][a_]:>x,v[x_][m_]:>x,\[Sigma][__][__]->Nothing,\[Sigma]b[__][__]->Nothing,nn_?NumericQ->Nothing}/.tempNCM->List,
        Alternatives@@(derArgumentsBut\[Chi])];
    
    expandedOp = expandedOp/.Join@@Table[With[{chiOrs=Unique[StringReplace[ToString[ar],{
                "x"~~DigitCharacter.. -> "s", "\[Theta]"~~DigitCharacter.. -> "\[Chi]", "\[Theta]b"~~DigitCharacter.. -> "\[Chi]b"}]],
                \[Theta]or\[Eta]Q = If[StringMatchQ[ToString[ar],("\[Chi]"|"\[Chi]b")~~DigitCharacter..],\[Chi]d\[Eta],\[Chi]d\[Theta]],
                arReplace = ar},{
            up[ar][alph_] :> (
                AppendTo[replacementRules,D\[Chi]low[chiOrs,alph]];
                AppendTo[elementaryDerivatives,arReplace->(\[Theta]or\[Eta]Q[#,arReplace,chiOrs]&)];
                1),
            low[ar][alph_] :> (
                AppendTo[replacementRules,D\[Chi]up[chiOrs,alph]];
                AppendTo[elementaryDerivatives,arReplace->(\[Theta]or\[Eta]Q[#,arReplace,chiOrs]&)];
                1),
            v[ar][mu_] :> (
                AppendTo[replacementRules,Dx\[Mu][chiOrs,mu]];
                AppendTo[elementaryDerivatives,arReplace->(sdx[#,arReplace,chiOrs]&)];
                1)
        }]
    ,{ar,derArgumentsOrdered}];
    
    opRule = With[{ll=Length[derArguments]+1, multArgumentsUniqueReplaced=multArgumentsUnique, 
        replacementRulesReplaced=replacementRules, expandedOpReplaced=expandedOp /. {Times->tempNCM,NonCommutativeMultiply->tempNCM}},
        Function[{derivedExpr,argsList},
            With[{expanded=Expand[derivedExpr]},
                Plus@@((Flatten[tempNCM[
                        expandedOpReplaced,
                        # /. {Times->tempNCM,NonCommutativeMultiply->tempNCM}
                    ],\[Infinity],tempNCM]//applyRuleSequence[Sequence@@replacementRulesReplaced,tempNCM->NonCommutativeMultiply]
                )& /@ If[Head[expanded]===Plus,List@@expanded,{expanded}]
            )/.Thread[multArgumentsUniqueReplaced -> argsList[[ll;;-1]]]]
        ]
    ];
    
    dsd\[Eta]d\[Theta] = With[{LL=Length[arguments], ll=Length[derArguments],elementaryDerivativesReplaced=elementaryDerivatives,derArgumentsReplaced=derArgumentsOrdered},
        function[{initialExpr,Sequence@@Table[glue["arg",nnn],{nnn,1,LL}]},
            (composition@@(derArgumentsReplaced/.elementaryDerivativesReplaced))[initialExpr]/.Thread[derArgumentsBut\[Chi] -> Table[glue["arg",nnn],{nnn,1,ll}]]
    ]]/.function->Function/.composition->Composition;
    
    symbol[expr_Plus,args__]:=Plus@@(symbol[#,args]&/@List@@(expr));
    symbol[expr_?ConstQ,args__]:=0;
    With[{dsd\[Eta]d\[Theta]Replaced=dsd\[Eta]d\[Theta],opRuleReplaced=opRule},
        symbol[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,args__]:=remove\[Chi]s[dsd\[Eta]d\[Theta]Replaced[expr,args],opRuleReplaced[#,{args}]&]
    ];
];


(* ::Subsection::Closed:: *)
(*\[PartialD]\[Eta]\[PartialD]x\[Theta]b and \[PartialD]\[Theta]\[PartialD]x\[Eta]b*)


(*\[PartialD]\[Eta]\[PartialD]x\[Theta]b*)
(*Define the fermion numbers of this operator*)
d\[Eta]dx\[Theta]b/:FN[d\[Eta]dx\[Theta]b[expr_,\[Eta]_,x_,\[Theta]b_]]:=Mod[FN[expr]+1,2];

(*Define the differentiation rules*)
d\[Eta]dx\[Theta]bRule[expr_,\[Chi]_,s_,\[Theta]b_]:=Module[{expanded,al=Unique[\[Alpha]],ad=Unique[\[Alpha]d],tempNCM},Plus@@((Flatten[tempNCM[up[\[Theta]b][ad],#/.{Times->tempNCM,NonCommutativeMultiply->tempNCM}],Infinity,tempNCM]/.D\[Chi]low[\[Chi],al]/.\[Sigma]Dx[s,al,ad]/.tempNCM->NonCommutativeMultiply)&/@If[Head[expanded=Expand[expr]]===Plus,List@@expanded,{expanded}])];

(*Definition*)
d\[Eta]dx\[Theta]b[expr_Plus,\[Eta]_,x_,\[Theta]b_]:=Plus@@(d\[Eta]dx\[Theta]b[#,\[Eta],x,\[Theta]b]&/@List@@(expr));
d\[Eta]dx\[Theta]b[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Eta]_,x_,\[Theta]b_]:=Block[{\[Chi]1,s1},\[Chi]1=Unique["\[Chi]"];s1=Unique["s"];remove\[Chi]s[sdx[\[Chi]d\[Eta][expr,\[Eta],\[Chi]1],x,s1],d\[Eta]dx\[Theta]bRule[#,\[Chi]1,s1,\[Theta]b]&]];
d\[Eta]dx\[Theta]b[expr_?ConstQ,\[Eta]_,x_,\[Theta]b_]:=0;


(*\[PartialD]\[Theta]\[PartialD]x\[Eta]b*)
(*Define the fermion numbers of this operator*)
d\[Theta]dx\[Eta]b/:FN[d\[Theta]dx\[Eta]b[expr_,\[Theta]_,x_,\[Eta]b_]]:=Mod[FN[expr]+1,2];

(*Define the differentiation rules*)
d\[Theta]dx\[Eta]bRule[expr_,\[Theta]_,s_,\[Chi]b_]:=Module[{expanded,al=Unique[\[Alpha]],ad=Unique[\[Alpha]d],tempNCM},Plus@@((Flatten[tempNCM[up[\[Chi]b][ad],#/.{Times->tempNCM,NonCommutativeMultiply->tempNCM}],Infinity,tempNCM]/.D\[Chi]low[\[Theta],al]/.\[Sigma]Dx[s,al,ad]/.tempNCM->NonCommutativeMultiply)&/@If[Head[expanded=Expand[expr]]===Plus,List@@expanded,{expanded}])];

(*Definition*)
d\[Theta]dx\[Eta]b[expr_Plus,\[Theta]_,x_,\[Eta]b_]:=Plus@@(d\[Eta]dx\[Theta]b[#,\[Theta],x,\[Eta]b]&/@List@@(expr));
d\[Theta]dx\[Eta]b[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Theta]_,x_,\[Eta]b_]:=Block[{\[Chi]1,s1},\[Chi]1=Unique["\[Chi]"];s1=Unique["s"];remove\[Chi]s[sdx[\[Chi]d\[Theta][expr,\[Theta],\[Chi]1],x,s1],d\[Theta]dx\[Eta]bRule[#,\[Chi]1,s1,\[Eta]b]&]];
d\[Theta]dx\[Eta]b[expr_?ConstQ,\[Theta]_,x_,\[Eta]b_]:=0;


(* ::Subsection::Closed:: *)
(*\[Eta]\[PartialD]x\[PartialD]\[Theta]b and \[Theta]\[PartialD]x\[PartialD]\[Eta]b*)


(*\[Eta]\[PartialD]x\[PartialD]\[Theta]b*)
(*Define the fermion numbers of this operator*)
\[Eta]dxd\[Theta]b/:FN[\[Eta]dxd\[Theta]b[expr_,\[Eta]_,x_,\[Theta]b_]]:=Mod[FN[expr]+1,2];

(*Define the differentiation rules*)
\[Eta]dxd\[Theta]bRule[expr_,\[Eta]_,s_,\[Theta]b_]:=Module[{expanded,al=Unique[\[Alpha]],ad=Unique[\[Alpha]d],tempNCM},Plus@@((Flatten[tempNCM[up[\[Eta]][al],#/.{Times->tempNCM,NonCommutativeMultiply->tempNCM}],Infinity,tempNCM]/.D\[Chi]low[\[Theta]b,ad]/.\[Sigma]Dx[s,al,ad]/.tempNCM->NonCommutativeMultiply)&/@If[Head[expanded=Expand[expr]]===Plus,List@@expanded,{expanded}])];

(*Definition*)
\[Eta]dxd\[Theta]b[expr_Plus,\[Eta]_,x_,\[Theta]b_]:=Plus@@(\[Eta]dxd\[Theta]b[#,\[Eta],x,\[Theta]b]&/@List@@(expr));
\[Eta]dxd\[Theta]b[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Eta]_,x_,\[Theta]b_]:=Block[{\[Chi]b1,s1},\[Chi]b1=Unique["\[Chi]b"];s1=Unique["s"];remove\[Chi]s[sdx[\[Chi]d\[Eta][expr,\[Theta]b,\[Chi]b1],x,s1],\[Eta]dxd\[Theta]bRule[#,\[Eta],s1,\[Chi]b1]&]];
\[Eta]dxd\[Theta]b[expr_?ConstQ,\[Eta]_,x_,\[Theta]b_]:=0;


(*\[Theta]\[PartialD]x\[PartialD]\[Eta]b*)
(*Define the fermion numbers of this operator*)
\[Theta]dxd\[Eta]b/:FN[\[Theta]dxd\[Eta]b[expr_,\[Theta]_,x_,\[Eta]b_]]:=Mod[FN[expr]+1,2];

(*Define the differentiation rules*)
\[Theta]dxd\[Eta]bRule[expr_,\[Theta]_,s_,\[Chi]b_]:=Module[{expanded,al=Unique[\[Alpha]],ad=Unique[\[Alpha]d],tempNCM},Plus@@((Flatten[tempNCM[up[\[Theta]][al],#/.{Times->tempNCM,NonCommutativeMultiply->tempNCM}],Infinity,tempNCM]/.D\[Chi]low[\[Chi]b,ad]/.\[Sigma]Dx[s,al,ad]/.tempNCM->NonCommutativeMultiply)&/@If[Head[expanded=Expand[expr]]===Plus,List@@expanded,{expanded}])];

(*Definition*)
\[Theta]dxd\[Eta]b[expr_Plus,\[Theta]_,x_,\[Eta]b_]:=Plus@@(\[Theta]dxd\[Eta]b[#,\[Theta],x,\[Eta]b]&/@List@@(expr));
\[Theta]dxd\[Eta]b[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Theta]_,x_,\[Eta]b_]:=Block[{\[Chi]b1,s1},\[Chi]b1=Unique["\[Chi]b"];s1=Unique["s"];remove\[Chi]s[sdx[\[Chi]d\[Eta][expr,\[Eta]b,\[Chi]b1],x,s1],\[Theta]dxd\[Eta]bRule[#,\[Theta],s1,\[Chi]b1]&]];
\[Theta]dxd\[Eta]b[expr_?ConstQ,\[Theta]_,x_,\[Eta]b_]:=0;


(* ::Subsection::Closed:: *)
(*\[Theta]\[PartialD]\[Eta] and \[Theta]b\[PartialD]\[Eta]b*)


(*\[Theta]\[PartialD]\[Eta]*)
(*Define the fermion numbers of this operator*)
\[Theta]d\[Eta]/:FN[\[Theta]d\[Eta][expr_,\[Theta]_,\[Eta]_]]:=Mod[FN[expr]+1,2];

(*Define the differentiation rules*)
\[Theta]d\[Eta]Rule[expr_,\[Theta]1_,\[Chi]1_]:=Module[{expanded,al=Unique[\[Alpha]],tempNCM},Plus@@((Flatten[tempNCM[up[\[Theta]1][al],#/.{Times->tempNCM,NonCommutativeMultiply->tempNCM}],Infinity,tempNCM]/.D\[Chi]up[\[Chi]1,al]/.tempNCM->NonCommutativeMultiply)&/@If[Head[expanded=Expand[expr]]===Plus,List@@expanded,{expanded}])];
(*Definition*)
\[Theta]d\[Eta][expr_Plus,\[Theta]_,\[Eta]_]:=Plus@@(\[Theta]d\[Eta][#,\[Theta],\[Eta]]&/@List@@(expr));
(***The auxiliary spinor for \[PartialD]\[Theta] must be recognized as bosonic.*)
\[Theta]d\[Eta][expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Theta]_,\[Eta]_]:=Block[{\[Chi]1},\[Chi]1=Unique["\[Chi]"];remove\[Chi]s[\[Chi]d\[Eta][expr,\[Eta],\[Chi]1],\[Theta]d\[Eta]Rule[#,\[Theta],\[Chi]1]&]];
\[Theta]d\[Eta][expr_?ConstQ,\[Theta]_,\[Eta]_]:=0;


(*\[Theta]b\[PartialD]\[Eta]b*)
(*Define the fermion numbers of this operator*)
\[Theta]bd\[Eta]b/:FN[\[Theta]bd\[Eta]b[expr_,\[Theta]_,\[Eta]_]]:=Mod[FN[expr]+1,2];

\[Theta]bd\[Eta]bRule[expr_,\[Theta]1_,\[Chi]1_]:=Module[{expanded,al=Unique[\[Alpha]],tempNCM},Plus@@((Flatten[tempNCM[low[\[Theta]1][al],#/.{Times->tempNCM,NonCommutativeMultiply->tempNCM}],Infinity,tempNCM]/.D\[Chi]low[\[Chi]1,al]/.tempNCM->NonCommutativeMultiply)&/@If[Head[expanded=Expand[expr]]===Plus,List@@expanded,{expanded}])];

(*Definition*)
\[Theta]bd\[Eta]b[expr_Plus,\[Theta]_,\[Eta]_]:=Plus@@(\[Theta]bd\[Eta]b[#,\[Theta],\[Eta]]&/@List@@(expr));
(***The auxiliary spinor for \[PartialD]\[Theta] must be recognized as bosonic.*)
\[Theta]bd\[Eta]b[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Theta]_,\[Eta]_]:=Block[{\[Chi]1},\[Chi]1=Unique["\[Chi]b"];remove\[Chi]s[\[Chi]d\[Eta][expr,\[Eta],\[Chi]1],\[Theta]d\[Eta]Rule[#,\[Theta],\[Chi]1]&]];
\[Theta]bd\[Eta]b[expr_?ConstQ,\[Theta]_,\[Eta]_]:=0;


(* ::Subsection::Closed:: *)
(*\[PartialD]\[Theta]\[PartialD]\[Eta]*)


(*\[PartialD]\[Theta]\[PartialD]\[Eta]*)
(*Define the fermion numbers of this operator*)
d\[Theta]d\[Eta]/:FN[d\[Theta]d\[Eta][expr_,\[Theta]_,\[Eta]_]]:=Mod[FN[expr]+1,2];

(*Define the differentiation rules*)
d\[Theta]d\[Eta]Rule[expr_,\[Chi]1_,\[Chi]2_]:=(expr/.D\[Chi]up[\[Chi]1,#1]/.D\[Chi]low[\[Chi]2,#1])&@Unique[\[Alpha]]; (*\[Chi]1\[Rule]\[Eta], \[Chi]2\[Rule]\[Theta]*)

(*Definition*)
d\[Theta]d\[Eta][expr_Plus,\[Theta]_,\[Eta]_]:=Plus@@(d\[Theta]d\[Eta][#,\[Theta],\[Eta]]&/@List@@(expr));
(***The auxiliary spinor for \[PartialD]\[Theta] must be recognized as bosonic.*)
d\[Theta]d\[Eta][expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Theta]_,\[Eta]_]:=Block[{\[Chi]1,\[Chi]2},\[Chi]1=Unique["\[Chi]"];\[Chi]2=Unique["\[Chi]"];remove\[Chi]s[\[Chi]d\[Theta][\[Chi]d\[Eta][expr,\[Eta],\[Chi]1],\[Theta],\[Chi]2],d\[Theta]d\[Eta]Rule[#,\[Chi]1,\[Chi]2]&]];
d\[Theta]d\[Eta][expr_?ConstQ,\[Theta]_,\[Eta]_]:=0;


(* ::Subsection::Closed:: *)
(*\[PartialD]\[Eta]\[PartialD]\[Eta]*)


(*\[PartialD]\[Eta]\[PartialD]\[Eta]*)
(*Define the fermion numbers of this operator*)
d\[Eta]d\[Eta]/:FN[d\[Eta]d\[Eta][expr_,\[Eta]1_,\[Eta]2_]]:=FN[expr];

(*Define the differentiation rules*)
d\[Eta]d\[Eta]Rule[expr_,\[Chi]1_,\[Chi]2_]:=(expr/.D\[Chi]up[\[Chi]1,#1]/.D\[Chi]low[\[Chi]2,#1])&@Unique[\[Alpha]]; 

(*Definition*)
d\[Eta]d\[Eta][expr_Plus,\[Eta]1_,\[Eta]2_]:=Plus@@(d\[Eta]d\[Eta][#,\[Eta]1,\[Eta]2]&/@List@@(expr));
(***The auxiliary spinor for \[PartialD]\[Theta] must be recognized as bosonic.*)
d\[Eta]d\[Eta][expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Eta]1_,\[Eta]2_]:=Block[{\[Chi]1,\[Chi]2},\[Chi]1=Unique["\[Chi]"];\[Chi]2=Unique["\[Chi]"];remove\[Chi]s[\[Chi]d\[Eta][\[Chi]d\[Eta][expr,\[Eta]2,\[Chi]1],\[Eta]1,\[Chi]2],d\[Eta]d\[Eta]Rule[#,\[Chi]1,\[Chi]2]&]];
d\[Eta]d\[Eta][expr_?ConstQ,\[Eta]1_,\[Eta]2_]:=0;


(* ::Subsection::Closed:: *)
(*\[PartialD]\[Theta]b\[PartialD]\[Eta]b*)


(*\[PartialD]\[Theta]b\[PartialD]\[Eta]b*)
(*Define the fermion numbers of this operator*)
d\[Theta]bd\[Eta]b/:FN[d\[Theta]bd\[Eta]b[expr_,\[Theta]b_,\[Eta]b_]]:=Mod[FN[expr]+1,2];

(*Define the differentiation rules*)
d\[Theta]bd\[Eta]bRule[expr_,\[Chi]b1_,\[Chi]b2_]:=(expr/.D\[Chi]low[\[Chi]b1,#1]/.D\[Chi]up[\[Chi]b2,#])&@Unique[\[Alpha]d]; (*\[Chi]b1\[Rule]\[Eta]b, \[Chi]b2\[Rule]\[Theta]b*)

(*Definition*)
d\[Theta]bd\[Eta]b[expr_Plus,\[Theta]b_,\[Eta]b_]:=Plus@@(d\[Theta]bd\[Eta]b[#,\[Theta]b,\[Eta]b]&/@List@@(expr));
(***The auxiliary spinor for \[PartialD]\[Theta] must be recognized as bosonic.*)
d\[Theta]bd\[Eta]b[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Theta]b_,\[Eta]b_]:=Block[{\[Chi]b1,\[Chi]b2},\[Chi]b1=Unique["\[Chi]b"];\[Chi]b2=Unique["\[Chi]b"];remove\[Chi]s[\[Chi]d\[Theta][\[Chi]d\[Eta][expr,\[Eta]b,\[Chi]b1],\[Theta]b,\[Chi]b2],d\[Theta]bd\[Eta]bRule[#,\[Chi]b1,\[Chi]b2]&]];
d\[Theta]bd\[Eta]b[expr_?ConstQ,\[Theta]b_,\[Eta]b_]:=0;


(* ::Subsection::Closed:: *)
(*\[PartialD]\[Eta]b\[PartialD]\[Eta]b*)


(*\[PartialD]\[Eta]b\[PartialD]\[Eta]b*)
(*Define the fermion numbers of this operator*)
d\[Eta]bd\[Eta]b/:FN[d\[Eta]bd\[Eta]b[expr_,\[Theta]b_,\[Eta]b_]]:=FN[expr];

(*Define the differentiation rules*)
d\[Eta]bd\[Eta]bRule[expr_,\[Chi]b1_,\[Chi]b2_]:=(expr/.D\[Chi]low[\[Chi]b1,#1]/.D\[Chi]up[\[Chi]b2,#])&@Unique[\[Alpha]d]; 

(*Definition*)
d\[Eta]bd\[Eta]b[expr_Plus,\[Eta]b1_,\[Eta]b2_]:=Plus@@(d\[Eta]bd\[Eta]b[#,\[Eta]b1,\[Eta]b2]&/@List@@(expr));
(***The auxiliary spinor for \[PartialD]\[Theta] must be recognized as bosonic.*)
d\[Eta]bd\[Eta]b[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Eta]b1_,\[Eta]b2_]:=Block[{\[Chi]b1,\[Chi]b2},\[Chi]b1=Unique["\[Chi]b"];\[Chi]b2=Unique["\[Chi]b"];remove\[Chi]s[\[Chi]d\[Eta][\[Chi]d\[Eta][expr,\[Eta]b2,\[Chi]b1],\[Eta]b1,\[Chi]b2],d\[Eta]bd\[Eta]bRule[#,\[Chi]b1,\[Chi]b2]&]];
d\[Eta]bd\[Eta]b[expr_?ConstQ,\[Eta]b1_,\[Eta]b2_]:=0;


(* ::Subsection::Closed:: *)
(*\[Eta]\[PartialD]x\[Eta]b*)


(*\[Eta]\[PartialD]x\[Eta]b*)
(*Define the fermion numbers of this operator*)
\[Eta]dx\[Eta]b/:FN[\[Eta]dx\[Eta]b[expr_,\[Eta]_,x_,\[Eta]b_]]:=FN[expr];

(*Define the differentiation rules*)
\[Eta]dx\[Eta]bRule[expr_,\[Chi]_,s_,\[Chi]b_]:=(expr*up[\[Chi]][#1]*up[\[Chi]b][#2]/.\[Sigma]Dx[s,#1,#2])&@@{Unique[\[Alpha]],Unique[\[Alpha]d]};

(*Definition*)
\[Eta]dx\[Eta]b[expr_Plus,\[Eta]_,x_,\[Eta]b_]:=Plus@@(\[Eta]dx\[Eta]b[#,\[Eta],x,\[Eta]b]&/@List@@(expr));
\[Eta]dx\[Eta]b[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Symbol|expr_Power|expr_Symbol,\[Eta]_,x_,\[Eta]b_]:=Block[{s1},s1=Unique["s"];remove\[Chi]s[sdx[expr,x,s1],\[Eta]dx\[Eta]bRule[#,\[Eta],s1,\[Eta]b]&]];
\[Eta]dx\[Eta]b[expr_?ConstQ,\[Eta]_,x_,\[Eta]b_]:=0;


(* ::Subsection::Closed:: *)
(*\[PartialD]\[Eta]\[PartialD]x\[PartialD]\[Eta]b*)


(*\[PartialD]\[Eta]\[PartialD]x\[PartialD]\[Eta]b*)
(*Define the fermion numbers of this operator*)
d\[Eta]dxd\[Eta]b/:FN[d\[Eta]dxd\[Eta]b[expr_,\[Eta]_,x_,\[Eta]b_]]:=FN[expr];

(*Define the differentiation rules*)
d\[Eta]dxd\[Eta]bRule[expr_,\[Chi]_,s_,\[Chi]b_]:=(expr/.D\[Chi]low[\[Chi],#1]/.D\[Chi]low[\[Chi]b,#2]/.\[Sigma]Dx[s,#1,#2])&@@{Unique[\[Alpha]],Unique[\[Alpha]d]};

(*Definition*)
d\[Eta]dxd\[Eta]b[expr_Plus,\[Eta]_,x_,\[Eta]b_]:=Plus@@(d\[Eta]dxd\[Eta]b[#,\[Eta],x,\[Eta]b]&/@List@@(expr));
d\[Eta]dxd\[Eta]b[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Eta]_,x_,\[Eta]b_]:=Block[{s1,\[Chi]1,\[Chi]b1},\[Chi]1=Unique["\[Chi]"];\[Chi]b1=Unique["\[Chi]b"];s1=Unique["s"];remove\[Chi]s[\[Chi]d\[Eta][\[Chi]d\[Eta][sdx[expr,x,s1],\[Eta],\[Chi]1],\[Eta]b,\[Chi]b1],d\[Eta]dxd\[Eta]bRule[#,\[Chi]1,s1,\[Chi]b1]&]];
d\[Eta]dxd\[Eta]b[expr_?ConstQ,\[Eta]_,x_,\[Eta]b_]:=0;


(* ::Subsection::Closed:: *)
(*\[PartialD]\[Theta]\[PartialD]x\[PartialD]\[Eta]b and \[PartialD]\[Eta]\[PartialD]x\[PartialD]\[Theta]b *)


(*\[PartialD]\[Theta]\[PartialD]x\[PartialD]\[Eta]b*)
(*Define the fermion numbers of this operator*)
d\[Theta]dxd\[Eta]b/:FN[d\[Theta]dxd\[Eta]b[expr_,\[Theta]_,x_,\[Eta]b_]]:=Mod[FN[expr]+1,2];

(*Define the differentiation rules*)
d\[Theta]dxd\[Eta]bRule[expr_,\[Theta]_,s_,\[Chi]b_]:=(expr/.D\[Chi]low[\[Theta],#1]/.D\[Chi]low[\[Chi]b,#2]/.\[Sigma]Dx[s,#1,#2])&@@{Unique[\[Alpha]],Unique[\[Alpha]d]};

(*Definition*)
d\[Theta]dxd\[Eta]b[expr_Plus,\[Theta]_,x_,\[Eta]b_]:=Plus@@(d\[Eta]dxd\[Eta]b[#,\[Theta],x,\[Eta]b]&/@List@@(expr));
d\[Theta]dxd\[Eta]b[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Theta]_,x_,\[Eta]b_]:=Block[{s1,\[Chi]1,\[Chi]b1},\[Chi]1=Unique["\[Chi]"];\[Chi]b1=Unique["\[Chi]b"];s1=Unique["s"];remove\[Chi]s[\[Chi]d\[Eta][\[Chi]d\[Theta][sdx[expr,x,s1],\[Theta],\[Chi]1],\[Eta]b,\[Chi]b1],d\[Theta]dxd\[Eta]bRule[#,\[Chi]1,s1,\[Chi]b1]&]];
d\[Theta]dxd\[Eta]b[expr_?ConstQ,\[Theta]_,x_,\[Eta]b_]:=0;


(*\[PartialD]\[Eta]\[PartialD]x\[PartialD]\[Theta]b*)
(*Define the fermion numbers of this operator*)
d\[Eta]dxd\[Theta]b/:FN[d\[Eta]dxd\[Theta]b[expr_,\[Eta]_,x_,\[Theta]b_]]:=Mod[FN[expr]+1,2];

(*Define the differentiation rules*)
d\[Eta]dxd\[Theta]bRule[expr_,\[Chi]_,s_,\[Theta]b_]:=(expr/.D\[Chi]low[\[Chi],#1]/.D\[Chi]low[\[Theta]b,#2]/.\[Sigma]Dx[s,#1,#2])&@@{Unique[\[Alpha]],Unique[\[Alpha]d]};

(*Definition*)
d\[Eta]dxd\[Theta]b[expr_Plus,\[Eta]_,x_,\[Theta]b_]:=Plus@@(d\[Eta]dxd\[Eta]b[#,\[Eta],x,\[Theta]b]&/@List@@(expr));
d\[Eta]dxd\[Theta]b[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Eta]_,x_,\[Theta]b_]:=Block[{s1,\[Chi]1,\[Chi]b1},\[Chi]1=Unique["\[Chi]"];\[Chi]b1=Unique["\[Chi]b"];s1=Unique["s"];remove\[Chi]s[\[Chi]d\[Theta][\[Chi]d\[Eta][sdx[expr,x,s1],\[Eta],\[Chi]1],\[Theta]b,\[Chi]b1],d\[Eta]dxd\[Theta]bRule[#,\[Chi]1,s1,\[Chi]b1]&]];
d\[Eta]dxd\[Theta]b[expr_?ConstQ,\[Eta]_,x_,\[Theta]b_]:=0;


(* ::Subsection::Closed:: *)
(*\[PartialD]\[Theta]\[PartialD]x\[PartialD]\[Theta]b*)


(*\[PartialD]\[Theta]\[PartialD]x\[PartialD]\[Theta]b*)
(*Define the fermion numbers of this operator*)
d\[Theta]dxd\[Theta]b/:FN[d\[Theta]dxd\[Theta]b[expr_,\[Theta]_,x_,\[Theta]b_]]:=FN[expr];

(*Define the differentiation rules*)
d\[Theta]dxd\[Theta]bRule[expr_,\[Theta]_,s_,\[Theta]b_]:=(expr/.D\[Chi]low[\[Theta],#1]/.D\[Chi]low[\[Theta]b,#2]/.\[Sigma]Dx[s,#1,#2])&@@{Unique[\[Alpha]],Unique[\[Alpha]d]};

(*Definition*)
d\[Theta]dxd\[Theta]b[expr_Plus,\[Theta]_,x_,\[Theta]b_]:=Plus@@(d\[Eta]dxd\[Eta]b[#,\[Theta],x,\[Theta]b]&/@List@@(expr));
d\[Theta]dxd\[Theta]b[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Theta]_,x_,\[Theta]b_]:=Block[{s1,\[Chi]1,\[Chi]b1},\[Chi]1=Unique["\[Chi]"];\[Chi]b1=Unique["\[Chi]b"];s1=Unique["s"];remove\[Chi]s[\[Chi]d\[Theta][\[Chi]d\[Theta][sdx[expr,x,s1],\[Theta]b,\[Chi]b1],\[Theta],\[Chi]1],d\[Theta]dxd\[Theta]bRule[#,\[Chi]1,s1,\[Chi]b1]&]];
d\[Theta]dxd\[Theta]b[expr_?ConstQ,\[Theta]_,x_,\[Theta]b_]:=0;


(* ::Subsection::Closed:: *)
(*\[PartialD]\[Eta]x\[PartialD]\[Eta]b*)


(*\[PartialD]\[Eta]\[PartialD]x\[PartialD]\[Eta]b*)
(*Define the fermion numbers of this operator*)
d\[Eta]xd\[Eta]b/:FN[d\[Eta]xd\[Eta]b[expr_,\[Eta]_,x_,\[Eta]b_]]:=FN[expr];

(*Define the differentiation rules*)
d\[Eta]xd\[Eta]bRule[expr_,\[Chi]_,x_,\[Chi]b_]:=(v[x][#3] \[Sigma][#3][#1,#2] expr/.D\[Chi]low[\[Chi],#1]/.D\[Chi]low[\[Chi]b,#2])&@@{Unique[\[Alpha]],Unique[\[Alpha]d],Unique[\[Mu]]};

(*Definition*)
d\[Eta]xd\[Eta]b[expr_Plus,\[Eta]_,x_,\[Eta]b_]:=Plus@@(d\[Eta]xd\[Eta]b[#,\[Eta],x,\[Eta]b]&/@List@@(expr));
d\[Eta]xd\[Eta]b[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Eta]_,x_,\[Eta]b_]:=Block[{\[Chi]1,\[Chi]b1},\[Chi]1=Unique["\[Chi]"];\[Chi]b1=Unique["\[Chi]b"];remove\[Chi]s[\[Chi]d\[Eta][\[Chi]d\[Eta][expr,\[Eta],\[Chi]1],\[Eta]b,\[Chi]b1],d\[Eta]xd\[Eta]bRule[#,\[Chi]1,x,\[Chi]b1]&]];
d\[Eta]xd\[Eta]b[expr_?ConstQ,\[Eta]_,x_,\[Eta]b_]:=0;


(* ::Subsection::Closed:: *)
(*\[PartialD]xsq*)


(*\[PartialD]xsq*)
(*Define the fermion numbers of this operator*)
dxsq/:FN[dxsq[expr_,x_]]:=FN[expr];

(*Define the differentiation rules*)
dxsqRule[expr_,s1_,s2_]:=(expr*\[ScriptG][#1,#2]/.v[s1][m_]:>\[ScriptG][m,#1]/.v[s2][n_]:>\[ScriptG][n,#2])&@@{Unique[\[Mu]],Unique[\[Nu]]};

(*Definition*)
dxsq[expr_Plus,x_]:=Plus@@(dxsq[#,x]&/@List@@(expr));
dxsq[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Symbol|expr_Power,x_]:=Block[{s1,s2},s1=Unique["s"];s2=Unique["s"];remove\[Chi]s[sdx[sdx[expr,x,s1],x,s2],dxsqRule[#,s1,s2]&]];
dxsq[expr_?ConstQ,x_]:=0;


(* ::Subsection::Closed:: *)
(*\[PartialD]\[Theta]\[PartialD]x\[Theta]b and \[Theta]\[PartialD]x\[PartialD]\[Theta]b*)


(*\[PartialD]\[Theta]\[PartialD]x\[Theta]b*)
(*Define the fermion numbers of this operator*)
d\[Theta]dx\[Theta]b/:FN[d\[Theta]dx\[Theta]b[expr_,\[Theta]_,x_,\[Theta]b_]]:=FN[expr];

(*Define the differentiation rules*)
d\[Theta]dx\[Theta]bRule[expr_,\[Chi]_,s_,\[Theta]b_]:=Module[{expanded,al=Unique[\[Alpha]],ad=Unique[\[Alpha]d],tempNCM},Plus@@((Flatten[tempNCM[-1,up[\[Theta]b][ad],#/.{Times->tempNCM,NonCommutativeMultiply->tempNCM}],Infinity,tempNCM]/.D\[Chi]low[\[Chi],al]/.\[Sigma]Dx[s,al,ad]/.tempNCM->NonCommutativeMultiply)&/@If[Head[expanded=Expand[expr]]===Plus,List@@expanded,{expanded}])];

(*Definition*)
d\[Theta]dx\[Theta]b[expr_Plus,\[Theta]_,x_,\[Theta]b_]:=Plus@@(d\[Theta]dx\[Theta]b[#,\[Theta],x,\[Theta]b]&/@List@@(expr));
d\[Theta]dx\[Theta]b[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Theta]_,x_,\[Theta]b_]:=Block[{\[Chi]1,s1},\[Chi]1=Unique["\[Chi]"];s1=Unique["s"];remove\[Chi]s[sdx[\[Chi]d\[Theta][expr,\[Theta],\[Chi]1],x,s1],d\[Theta]dx\[Theta]bRule[#,\[Chi]1,s1,\[Theta]b]&]];
d\[Theta]dx\[Theta]b[expr_?ConstQ,\[Theta]_,x_,\[Theta]b_]:=0;


(*\[Theta]\[PartialD]x\[PartialD]\[Theta]b*)
(*Define the fermion numbers of this operator*)
\[Theta]dxd\[Theta]b/:FN[\[Theta]dxd\[Theta]b[expr_,\[Theta]_,x_,\[Theta]b_]]:=FN[expr];

(*Define the differentiation rules*)
\[Theta]dxd\[Theta]bRule[expr_,\[Theta]_,s_,\[Chi]b_]:=Module[{expanded,al=Unique[\[Alpha]],ad=Unique[\[Alpha]d],tempNCM},Plus@@((Flatten[tempNCM[up[\[Theta]][al],#/.{Times->tempNCM,NonCommutativeMultiply->tempNCM}],Infinity,tempNCM]/.D\[Chi]low[\[Chi]b,ad]/.\[Sigma]Dx[s,al,ad]/.tempNCM->NonCommutativeMultiply)&/@If[Head[expanded=Expand[expr]]===Plus,List@@expanded,{expanded}])];

(*Definition*)
\[Theta]dxd\[Theta]b[expr_Plus,\[Theta]_,x_,\[Theta]b_]:=Plus@@(\[Theta]dxd\[Theta]b[#,\[Theta],x,\[Theta]b]&/@List@@(expr));
\[Theta]dxd\[Theta]b[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Theta]_,x_,\[Theta]b_]:=Block[{\[Chi]b1,s1},\[Chi]b1=Unique["\[Chi]b"];s1=Unique["s"];remove\[Chi]s[sdx[\[Chi]d\[Theta][expr,\[Theta]b,\[Chi]b1],x,s1],\[Theta]dxd\[Theta]bRule[#,\[Theta],s1,\[Chi]b1]&]];
\[Theta]dxd\[Theta]b[expr_?ConstQ,\[Theta]_,x_,\[Theta]b_]:=0;


(* ::Subsection::Closed:: *)
(*\[PartialD]\[Eta]\[PartialD]x\[Eta]b and \[Eta]\[PartialD]x\[PartialD]\[Eta]b*)


(*\[PartialD]\[Eta]\[PartialD]x\[Eta]b*)
(*Define the fermion numbers of this operator*)
d\[Eta]dx\[Eta]b/:FN[d\[Eta]dx\[Eta]b[expr_,\[Eta]_,x_,\[Eta]b_]]:=FN[expr];

(*Define the differentiation rules*)
d\[Eta]dx\[Eta]bRule[expr_,\[Chi]_,s_,\[Chi]b_]:=(up[\[Chi]b][#2]*expr/.D\[Chi]low[\[Chi],#1]/.\[Sigma]Dx[s,#1,#2])&@@{Unique[\[Alpha]],Unique[\[Alpha]d]};

(*Definition*)
d\[Eta]dx\[Eta]b[expr_Plus,\[Eta]_,x_,\[Eta]b_]:=Plus@@(d\[Eta]dx\[Eta]b[#,\[Eta],x,\[Eta]b]&/@List@@(expr));
d\[Eta]dx\[Eta]b[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Eta]_,x_,\[Eta]b_]:=Block[{s1,\[Chi]1},\[Chi]1=Unique["\[Chi]"];s1=Unique["s"];remove\[Chi]s[\[Chi]d\[Eta][sdx[expr,x,s1],\[Eta],\[Chi]1],d\[Eta]dx\[Eta]bRule[#,\[Chi]1,s1,\[Eta]b]&]];
d\[Eta]dx\[Eta]b[expr_?ConstQ,\[Eta]_,x_,\[Eta]b_]:=0;


(*\[Eta]\[PartialD]x\[PartialD]\[Eta]b*)
(*Define the fermion numbers of this operator*)
\[Eta]dxd\[Eta]b/:FN[\[Eta]dxd\[Eta]b[expr_,\[Eta]_,x_,\[Eta]b_]]:=FN[expr];

(*Define the differentiation rules*)
\[Eta]dxd\[Eta]bRule[expr_,\[Chi]_,s_,\[Chi]b_]:=(up[\[Chi]][#1]*expr/.D\[Chi]low[\[Chi]b,#2]/.\[Sigma]Dx[s,#1,#2])&@@{Unique[\[Alpha]],Unique[\[Alpha]d]};

(*Definition*)
\[Eta]dxd\[Eta]b[expr_Plus,\[Eta]_,x_,\[Eta]b_]:=Plus@@(\[Eta]dxd\[Eta]b[#,\[Eta],x,\[Eta]b]&/@List@@(expr));
\[Eta]dxd\[Eta]b[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Eta]_,x_,\[Eta]b_]:=Block[{s1,\[Chi]b1},\[Chi]b1=Unique["\[Chi]b"];s1=Unique["s"];remove\[Chi]s[\[Chi]d\[Eta][sdx[expr,x,s1],\[Eta]b,\[Chi]b1],\[Eta]dxd\[Eta]bRule[#,\[Eta],s1,\[Chi]b1]&]];
\[Eta]dxd\[Eta]b[expr_?ConstQ,\[Eta]_,x_,\[Eta]b_]:=0;


(* ::Subsection::Closed:: *)
(*Chiral D and Db*)


(*This is just a wrapper of some already defined derivatives*)
(*If z_- = x + 2 \[ImaginaryI] \[Theta] \[Theta]b and z_+ = x - 2 \[ImaginaryI] \[Theta] \[Theta]b then D z_- = 0 and Db z_+ = 0*)
ChiralDm[  expr_,\[Eta]_,\[Theta]_,x_]:= -d\[Theta]d\[Eta][expr,\[Theta],\[Eta]] + I d\[Eta]dx\[Theta]b[expr/.sq[bar[\[Theta]]]->0,\[Eta],x,bar[\[Theta]]];
ChiralDbm[expr_,\[Eta]b_,\[Theta]_,x_]:= -d\[Theta]bd\[Eta]b[expr,bar[\[Theta]],\[Eta]b] - I \[Theta]dxd\[Eta]b[expr/.sq[\[Theta]]->0,\[Theta],x,\[Eta]b];
ChiralDp[  expr_,\[Eta]_,\[Theta]_,x_]:= With[{new\[Eta] = Unique["\[Eta]"]}, \[Eta]d\[Theta][expr,\[Eta],\[Theta]] + I d\[Eta]dx\[Theta]b[d[\[Eta],new\[Eta]]expr/.sq[bar[\[Theta]]]->0,new\[Eta],x,bar[\[Theta]]]];
ChiralDbp[expr_,\[Eta]b_,\[Theta]_,x_]:= With[{new\[Eta]b = Unique["\[Eta]b"]}, -\[Eta]bd\[Theta]b[expr,\[Eta]b,bar[\[Theta]]] - I \[Theta]dxd\[Eta]b[d[new\[Eta]b,\[Eta]b]expr/.sq[\[Theta]]->0,\[Theta],x,new\[Eta]b]];

ChiralDsq[ expr_,\[Theta]_,x_]:= - 4 D[expr,sq[\[Theta]]] - 2 I d\[Theta]dx\[Theta]b[expr/.sq[bar[\[Theta]]]->0,\[Theta],x,bar[\[Theta]]] - sq[bar[\[Theta]]] (dxsq[expr/.sq[bar[\[Theta]]]->0,x]);
ChiralDbsq[expr_,\[Theta]_,x_]:= - 4 D[expr,sq[bar[\[Theta]]]] - 2 I \[Theta]dxd\[Theta]b[expr/.sq[\[Theta]]->0,\[Theta],x,bar[\[Theta]]] - sq[\[Theta]] (dxsq[expr/.sq[\[Theta]]->0,x]);


(*This is the dual derivative*)
ChiralQm[  expr_,\[Eta]_,\[Theta]_,x_]:= -d\[Theta]d\[Eta][expr,\[Theta],\[Eta]] - I d\[Eta]dx\[Theta]b[expr/.sq[bar[\[Theta]]]->0,\[Eta],x,bar[\[Theta]]];
ChiralQbm[expr_,\[Eta]b_,\[Theta]_,x_]:= -d\[Theta]bd\[Eta]b[expr,bar[\[Theta]],\[Eta]b] + I \[Theta]dxd\[Eta]b[expr/.sq[\[Theta]]->0,\[Theta],x,\[Eta]b];
ChiralQp[  expr_,\[Eta]_,\[Theta]_,x_]:= With[{new\[Eta] = Unique["\[Eta]"]}, \[Eta]d\[Theta][expr,\[Eta],\[Theta]] - I d\[Eta]dx\[Theta]b[d[\[Eta],new\[Eta]]expr/.sq[bar[\[Theta]]]->0,new\[Eta],x,bar[\[Theta]]]];
ChiralQbp[expr_,\[Eta]b_,\[Theta]_,x_]:= With[{new\[Eta]b = Unique["\[Eta]b"]}, -\[Eta]bd\[Theta]b[expr,\[Eta]b,bar[\[Theta]]] + I \[Theta]dxd\[Eta]b[d[new\[Eta]b,\[Eta]b]expr/.sq[\[Theta]]->0,\[Theta],x,new\[Eta]b]];

ChiralQsq[ expr_,\[Theta]_,x_]:= - 4 D[expr,sq[\[Theta]]] + 2 I d\[Theta]dx\[Theta]b[expr/.sq[bar[\[Theta]]]->0,\[Theta],x,bar[\[Theta]]] - sq[bar[\[Theta]]] (dxsq[expr/.sq[bar[\[Theta]]]->0,x]);
ChiralQbsq[expr_,\[Theta]_,x_]:= - 4 D[expr,sq[bar[\[Theta]]]] + 2 I \[Theta]dxd\[Theta]b[expr/.sq[\[Theta]]->0,\[Theta],x,bar[\[Theta]]] - sq[\[Theta]] (dxsq[expr/.sq[\[Theta]]->0,x]);


(*These are aliases for when acting on the t*)
Chiral\[ScriptCapitalQ]m[  expr_,\[Eta]_,\[Theta]_,x_]:= ChiralDm[expr,\[Eta],\[Theta],x];
Chiral\[ScriptCapitalQ]bm[expr_,\[Eta]b_,\[Theta]_,x_]:= ChiralDbm[expr,\[Eta]b,\[Theta],x];
Chiral\[ScriptCapitalQ]p[  expr_,\[Eta]_,\[Theta]_,x_]:= ChiralDp[expr,\[Eta],\[Theta],x];
Chiral\[ScriptCapitalQ]bp[expr_,\[Eta]b_,\[Theta]_,x_]:= ChiralDbp[expr,\[Eta]b,\[Theta],x];


(*These are aliases for when acting on the t*)
Chiral\[ScriptCapitalD]m[  expr_,\[Eta]_,\[Theta]_,x_]:= ChiralQm[expr,\[Eta],\[Theta],x];
Chiral\[ScriptCapitalD]bm[expr_,\[Eta]b_,\[Theta]_,x_]:= ChiralQbm[expr,\[Eta]b,\[Theta],x];
Chiral\[ScriptCapitalD]p[  expr_,\[Eta]_,\[Theta]_,x_]:= ChiralQp[expr,\[Eta],\[Theta],x];
Chiral\[ScriptCapitalD]bp[expr_,\[Eta]b_,\[Theta]_,x_]:= ChiralQbp[expr,\[Eta]b,\[Theta],x];


(* ::Section::Closed:: *)
(*Differential operator coefficients and norms*)


normsAndci = Uncompress["1:eJztWNFOgzAURQSnJib+gs+Er/DVhDH9AUlo0oUFJ079XD/BDzDO3hJKWlsGbWEZsIempKU93HPP6e3uknyFbh3HKTzSPODiDbnV02qXpY9npBNhaIUB6MRJFCLoFD5pnvAmLcpZC9Lc55uXLP3EpOtgly0AM5f5R/pKe+vk/TnDe/Krl19mu4LuWM9A59ygyw+6HAL8S1ajj9t6VIQdlCtqwG7ESffmZ6hxskcZToAXm0QXXlw3BbeaIIstG5OFFga3KsR2A8s+QhJXFUY2S4LRp+wX20hAib9hfRtJZkA5Tc0O2DqSpB20C8AFUg/RFf/WD2NFT9U2U7YtU9qB9Bv2bF5CILyRqVoO/H6e9f2EKPNUB+jyINVtnVCHYD09mYlEFXoWUymEfbWQUlZKOjjRlTHx2n23rU0V9AdhC/oNlH7Y6g0EbiOE1mTbjNwWBgkBRk4fBGhhX/5dD/jOp2jX766RKXLGnq61Dg99IQN7pJSg9fk195qiHvsy1PcxDuTJCn3klYi2fdVZHwiVavus7+16N3wBP9l6ZuS+Tu6vsYmvD3LNGlgkHNHS7BrQIWGwfwyTOAuErLfr69auHEerW/8bu2bemRl7/5uO3djhuk1LF/j/74Z/zyHJOlvxbMUnVpbPLnX6LvUHs9QKfg=="];


diffOps = Uncompress["1:eJztms1SwjAQx1HAcRQfQq+dHvx+CQ+M+gItNFoGZBDR1/bogQEbGEq3uyktk/SL9cBQSLLZ3d9/s61cuuNncdVoNKat4OXJn36J483V82zovTSDN45lrT9tBy+v/sibbgd1h7Pp6s3g2xnGRvnhqAn4sjv+8T5FCywhPx+4wbDkxeTlxN1lyl8Gf4QjbuBIE86DI+FW0u6gMH8cGyWmGd3FdhVqA4tgUWV22mid+HS5pxRuLjbDSDfBHpRpw25qyViZfHQs24DGoIeL0HRmD7WID3lIwKpMpSrG5XPUsdM4qkGVLRAwswq0ycJpKoH5eCavepY4VRAQ8SLc2JHCxbUTcCxcKLKBpAqkjt9y447eSJT4AE+Kjg4rBA59S5zAGRDFTDhwdpOyi4PvoeCHjhvTYsW6vlXJsuMly2iYKnh2JfUFe0owc94UYJgyStUyOy6neQZOOO26qhpKQ/mrWlniJyHsuahFW2CTMH54O8rwbYYai578Mtf7yvp2aBKHvou6hHk2HDi9+4vRw9EnjgYjYqxCF7uuV6g/MxeiSt6AJ50zeT22AWCYN0oWMtQYhJvayQnnXWNJQ3kodUkrS+xkIIS4gLOy3GFkbW3LWUFr000UzFV9u9ZVTq9ZKawUVkqyUiSfb+IcTvhNL5R8eqeacnwYrV+O4iFOgRuGm+GuIdySwXfRgbP+dLOdtWFhNKt9q5kjzmdRA8rVqG480npTSAEs41NDXKmZHehD7Oc3rei6alfnOJjbwaTZcOEJYfUPQEhwo7YaxntrFJ6Nt1w/uH7o0jPXj0OqH1Lmfry1LuABS+qmee8fVwCQzZcfLoFF3zHeMdaMda2wlowOi3gOkg5FRoObvv3L9T2DzWDXDGzJ36iW/76sonCY4XT2cJr0WqGq/wOrhFXCKlGrRLL5UaU7WqaaqU5X+x8Za8a62lj/AwEfmCk="];


operatorNorm::operatorMissing = "Choose an operator among O, QbO\[PlusMinus], QO\[PlusMinus], QbsqO, QsqO, QQbO\[PlusMinus]\[PlusMinus], QsqQbO\[PlusMinus], QbsqQO\[PlusMinus], QsqQbsqO as a string.";
operatorNorm[string_,{q_,qb_},{j_,jb_}]:=Lookup[Association[normsAndci],string,Message[operatorNorm::operatorMissing]]/.{qval->q,qbval->qb,jval->j,jbval->jb};


coefficientD::coefficientMissing = "Choose a coefficient as a\[PlusMinus]\[PlusMinus], b\[PlusMinus]\[PlusMinus], c\[PlusMinus], d\[PlusMinus], e\[PlusMinus], cb\[PlusMinus], db\[PlusMinus], eb\[PlusMinus], f,...,n or f1,...,f7 in the form of a string.";
coefficientD[string_,{q_,qb_},{j_,jb_}]:=Lookup[Association[diffOps],string,Message[coefficientD::coefficientMissing]]/.{qval->q,qbval->qb,jval->j,jbval->jb};


(* ::Section::Closed:: *)
(*Separating orders in \[Theta]*)


separateOrders::noworking\[Theta]s = "The global variable working\[Theta]s is set to {}. Set it to a list of \[Theta]'s so separateOrder can separate the orders in \[Theta] based on that list.";
separateOrders[expr_]:=Module[{list,listsq,listW,listsqW,allorders,f,safeCoefficient,constList,table},
    If[working\[Theta]s === {}, Message[separateOrders::noworking\[Theta]s]; list = {}, list = working\[Theta]s];
    constList=Unique["temp"<>ToString[#]]&/@ConstantsList;
    Compare[0,variables->constList];
    listsq = sq/@list;
    listW = constList[[1;;Length[list]]] list;
    listsqW = (#^2&/@constList[[1;;Length[list]]]) listsq;
    allorders = Flatten[table[f@@list,Sequence@@Table[{l,0,2},{l,list}]]/.table -> Table]/.f->List;
    safeCoefficient[Expr_,1]:=Expr/.Thread[constList->0];
    safeCoefficient[Expr_,monomial_]:=Coefficient[Expr,monomial]/.Thread[constList->0];
    
    Association@@Table[order -> (safeCoefficient[Collect[expr /. Thread[list -> listW] /. Thread[listsq->listsqW],constList],
                                                Times@@(listW/.a_ b_/;MemberQ[constList,b]:>b^a/.Thread[list->order])]),
                       {order,allorders}]
];


(* ::Section::Closed:: *)
(*Replacements*)


(* ::Subsection::Closed:: *)
(*Replace \[Eta] with any expression with one \[Alpha] index*)


Replace\[Eta]ToAny[expr_Plus,\[Eta]_,any_]:=Plus@@(Replace\[Eta]ToAny[#,\[Eta],any]&/@List@@(Expand[expr]));
Replace\[Eta]ToAny[expr_?ConstQ,\[Eta]_,any_]:=expr;
Replace\[Eta]ToAny[expr_Symbol,\[Eta]_,any_]:=expr;
Replace\[Eta]ToAny[expr_^y_,\[Eta]_,any_]:=Power[Replace\[Eta]ToAny[expr,\[Eta],any],y];
Replace\[Eta]ToAny[expr1_ expr2_^y_,\[Eta]_,any_]:=Replace\[Eta]ToAny[expr1,\[Eta],any] Power[Replace\[Eta]ToAny[expr2,\[Eta],any],y];

Replace\[Eta]ToAny[expr_Times|expr_NonCommutativeMultiply|expr_d,\[Eta]_,any_]/;FreeQ[expr,Power]:=Module[
{n\[Eta],\[Chi]s,i=1,exprw\[Chi],anyRule,Sumremove\[Chi]s,tempNCM,expanded,uplow},

n\[Eta] = Count[List@@expr/.{NonCommutativeMultiply->Sequence,Times->Sequence,d->Sequence},\[Eta]];
\[Chi]s=If[\[Eta]Q[\[Eta]],Table[Unique["\[Chi]1"],{n,1,n\[Eta]}],If[\[Eta]bQ[\[Eta]],Table[Unique["\[Chi]b1"],{n,1,n\[Eta]}]]];
exprw\[Chi] = expr/.\[Eta]:>(\[Chi]s[[i++]]);
anyRule[\[Xi]1_]:=
  Table[uplow={low[\[Xi]1][a_]->Sequence@@(case[[1]][a]),up[\[Xi]1][a_]->Sequence@@(case[[2]][a])};
        Function[exp,Flatten[tempNCM[exp/.{Times->tempNCM,NonCommutativeMultiply->tempNCM}],Infinity,tempNCM]/.Ruplow/.tempNCM->NonCommutativeMultiply]/.Ruplow->uplow
   ,{case,any}];
Sumremove\[Chi]s[exp_,{rules__}]:=Sum[remove\[Chi]s[exp,r],{r,{rules}}];

Fold[Sumremove\[Chi]s,exprw\[Chi],anyRule/@\[Chi]s]
];



(* ::Section::Closed:: *)
(*Comparing expressions*)


(* ::Subsection::Closed:: *)
(*Definition of the numerical values*)


(*If I insert the ordering rule for ** the numerical values comparison fails. Only for one coefficient though... Find out what goes wrong.*)


\[Sigma]1={{0,1},{1,0}};\[Sigma]2={{0,-I},{I,0}};\[Sigma]3={{1,0},{0,-1}};
\[Epsilon]={{0,1},{-1,0}};
unit={{-1,0},{0,-1}};
\[CapitalSigma]\[Mu]\[Nu]={{{{0,0},{0,0}},{{0,1/2},{1/2,0}},{{0,-(I/2)},{I/2,0}},{{1/2,0},{0,-(1/2)}}},{{{0,-(1/2)},{-(1/2),0}},{{0,0},{0,0}},{{-(I/2),0},{0,I/2}},{{0,1/2},{-(1/2),0}}},{{{0,I/2},{-(I/2),0}},{{I/2,0},{0,-(I/2)}},{{0,0},{0,0}},{{0,-(I/2)},{-(I/2),0}}},{{{-(1/2),0},{0,1/2}},{{0,-(1/2)},{1/2,0}},{{0,I/2},{I/2,0}},{{0,0},{0,0}}}};
\[CapitalSigma]b\[Mu]\[Nu]={{{{0,0},{0,0}},{{0,-(1/2)},{-(1/2),0}},{{0,I/2},{-(I/2),0}},{{-(1/2),0},{0,1/2}}},{{{0,1/2},{1/2,0}},{{0,0},{0,0}},{{-(I/2),0},{0,I/2}},{{0,1/2},{-(1/2),0}}},{{{0,-(I/2)},{I/2,0}},{{I/2,0},{0,-(I/2)}},{{0,0},{0,0}},{{0,-(I/2)},{-(I/2),0}}},{{{1/2,0},{0,-(1/2)}},{{0,-(1/2)},{1/2,0}},{{0,I/2},{I/2,0}},{{0,0},{0,0}}}};
g[m_,n_]:=If[m==n,Switch[m,1,-1,2,1,3,1,4,1],0];


(*The numerical contraction d \[Rule] nd*)
nd[x_n,y_n]:=x . \[Epsilon] . y/.n->List;
nd[x_b,y_b]:=y . \[Epsilon] . x/.b->List;

nd[a_b,x__,c_n]:=c . Dot[x] . a/.{n->List,b->List};
nd[a_n,x__,c_b]:=a . Dot[x] . c/.{n->List,b->List};

nd[v[x__,\[Alpha]_],v[y__,\[Beta]_],m1__,\[Sigma][\[Alpha]_,\[Beta]_],m2__]:=Sum[({x}[[\[Mu]]])*(m1 . \[CapitalSigma]\[Mu]\[Nu][[\[Mu],\[Nu]]] . \[Epsilon] . m2)*({y}[[\[Nu]]]),{\[Mu],1,4},{\[Nu],1,4}]/.{n->List,b->List};
nd[v[x__,\[Alpha]_],v[y__,\[Beta]_],m1__,\[Sigma]b[\[Alpha]_,\[Beta]_],m2__]:= Sum[({x}[[\[Mu]]])*(-m1 . \[Epsilon] . \[CapitalSigma]b\[Mu]\[Nu][[\[Mu],\[Nu]]] . m2)*({y}[[\[Nu]]]),{\[Mu],1,4},{\[Nu],1,4}]/.{n->List,b->List};
nd[v[x__,\[Beta]_],v[y__,\[Alpha]_],m1__,\[Sigma][\[Alpha]_,\[Beta]_],m2__]:=-Sum[({x}[[\[Mu]]])*(m1 . \[CapitalSigma]\[Mu]\[Nu][[\[Mu],\[Nu]]] . \[Epsilon] . m2)*({y}[[\[Nu]]]),{\[Mu],1,4},{\[Nu],1,4}]/.{n->List,b->List};
nd[v[x__,\[Beta]_],v[y__,\[Alpha]_],m1__,\[Sigma]b[\[Alpha]_,\[Beta]_],m2__]:= -Sum[({x}[[\[Mu]]])*(-m1 . \[Epsilon] . \[CapitalSigma]b\[Mu]\[Nu][[\[Mu],\[Nu]]] . m2)*({y}[[\[Nu]]]),{\[Mu],1,4},{\[Nu],1,4}]/.{n->List,b->List};

nd[v[x__],v[y__]]:=Sum[{x}[[m]]*{y}[[m]]*g[m,m],{m,1,4}];

(*Sometimes when expressions with \[Epsilon] have the x's ordered but the indices not are left as they are. This may result in a non matching of the rules below. Maybe fixed!*)
nd[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],v[v1__,\[Kappa]],v[v2__,\[Lambda]],v[v3__,\[Mu]],v[v4__,\[Nu]]]:=Sum[LeviCivitaTensor[4][[k,l,m,n]]{v1}[[k]]{v2}[[l]]{v3}[[m]]{v4}[[n]],{k,1,4},{l,1,4},{m,1,4},{n,1,4}];
nd[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],v[v1__,\[Mu]],v[v2__,\[Nu]],v[v3__,\[Rho]],v[v4__,\[Lambda]]]:=Sum[LeviCivitaTensor[4][[k,l,m,n]]{v1}[[k]]{v2}[[l]]{v3}[[m]]{v4}[[n]],{k,1,4},{l,1,4},{m,1,4},{n,1,4}];
nd[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],v[v1__,\[Kappa]],v[v2__,\[Lambda]],v[v3__,\[Mu]],e_n,\[Sigma][\[Nu]],f_b]:=Sum[LeviCivitaTensor[4][[k,l,m,nn]]{v1}[[k]]{v2}[[l]]{v3}[[m]](e . {unit,\[Sigma]1,\[Sigma]2,\[Sigma]3}[[nn]] . f/.{n->List,b->List})g[nn,nn],{k,1,4},{l,1,4},{m,1,4},{nn,1,4}];
nd[\[Epsilon]\[Mu]\[Nu]\[Rho]\[Lambda],v[v1__,\[Mu]],v[v2__,\[Nu]],v[v3__,\[Rho]],e_n,\[Sigma][\[Lambda]],f_b]:=Sum[LeviCivitaTensor[4][[k,l,m,nn]]{v1}[[k]]{v2}[[l]]{v3}[[m]](e . {unit,\[Sigma]1,\[Sigma]2,\[Sigma]3}[[nn]] . f/.{n->List,b->List})g[nn,nn],{k,1,4},{l,1,4},{m,1,4},{nn,1,4}];


nd[x_?NumericQ]:=x;
nd[x_?ConstQ]:=x;


(*Replacement rule for rational values*)
rule[\[Eta]s_,\[Theta]s_,xs_][\[Eta]1_List,\[Eta]2_List,\[Eta]1i_List,\[Eta]2i_List,\[Theta]1_List,\[Theta]2_List,\[Theta]1i_List,\[Theta]2i_List,xa_List,xb_List,xc_List,xd_List]:=Module[{\[Eta]List,\[Theta]List,xList,xxList},
\[Eta]List = Join[Table[\[Eta]s[[e]] -> n[\[Eta]1[[e]]+I \[Eta]1i[[e]],\[Eta]2[[e]]+I \[Eta]2i[[e]]],{e,1,Length[\[Eta]s]}],
    Table[bar[\[Eta]s[[e]]] -> b[\[Eta]1[[e]]-I \[Eta]1i[[e]],\[Eta]2[[e]]-I \[Eta]2i[[e]]],{e,1,Length[\[Eta]s]}]];

\[Theta]List = Join[Table[\[Theta]s[[e]] -> n[\[Theta]1[[e]]+I \[Theta]1i[[e]],\[Theta]2[[e]]+I \[Theta]2i[[e]]],{e,1,Length[\[Theta]s]}],
    Table[bar[\[Theta]s[[e]]] -> b[\[Theta]1[[e]]-I \[Theta]1i[[e]],\[Theta]2[[e]]-I \[Theta]2i[[e]]],{e,1,Length[\[Theta]s]}]];

xList = Join[Table[xs[[e]] -> xa[[e]] \[Sigma]1 + xb[[e]] \[Sigma]2 + xc[[e]] \[Sigma]3 + xd[[e]] unit,{e,1,Length[xs]}],
    Table[sq[xs[[e]]] -> -Det[xa[[e]] \[Sigma]1 + xb[[e]] \[Sigma]2 + xc[[e]] \[Sigma]3 + xd[[e]] unit],{e,1,Length[xs]}],
    Table[glue[xs[[e]],"v"][m_] -> v[xd[[e]] ,xa[[e]], xb[[e]], xc[[e]],m],{e,1,Length[xs]}]];

xxList = Flatten[Table[glue[xs[[e]],xs[[f]]] -> nd[v[xd[[e]] ,xa[[e]], xb[[e]], xc[[e]]],v[xd[[f]] ,xa[[f]], xb[[f]], xc[[f]]]],{e,1,Length[xs]},{f,1,Length[xs]}]];

Return[Join[\[Eta]List,\[Theta]List,xList,xxList]]
];


(*Replacement rule for random rational values*)
denom = 19;

ran[\[Eta]s_,\[Theta]s_,xs_]:=rule[\[Eta]s,\[Theta]s,xs][
    RandomInteger[{-denom,denom},Length@\[Eta]s]/denom,
    RandomInteger[{-denom,denom},Length@\[Eta]s]/denom,
    RandomInteger[{-denom,denom},Length@\[Eta]s]/denom,
    RandomInteger[{-denom,denom},Length@\[Eta]s]/denom,
    RandomInteger[{-denom,denom},Length@\[Theta]s]/denom,
    RandomInteger[{-denom,denom},Length@\[Theta]s]/denom,
    RandomInteger[{-denom,denom},Length@\[Theta]s]/denom,
    RandomInteger[{-denom,denom},Length@\[Theta]s]/denom,
    RandomInteger[{-denom,denom},Length@xs]/denom,
    RandomInteger[{-denom,denom},Length@xs]/denom,
    RandomInteger[{-denom,denom},Length@xs]/denom,
    RandomInteger[{-denom,denom},Length@xs]/denom
]


(* ::Subsection::Closed:: *)
(*Compare an expression to zero*)


Options[Compare]={expReplacement -> {(*q->2,qb\[Rule]2,\[ScriptL]->2,l->2,k->1,\[CapitalDelta]\[Rule]4,j->0,\[CapitalDelta]\[Phi]->1*)},(*Replacements for the exponents of e.g. x12sq^\[CapitalDelta]*)
                  variables -> ConstantsList,(*Usual set of symbols treated as constants, which are the unknowns of this system*)
                  verbose -> 0,  (*Displays more info on the system that is being solved*)
                  minEqns -> -1,  (*Writes at least mineqns equations, if -1 the number of equations equals the number of variables matched*)
                  wrapfunction -> (#&)}; (*Wraps the equations of the randomly generated system with this function*)

(*Puts an overall minus to terms that contain \[Theta]s or \[Theta]bs in a non-canonical order, so that the replacement of numerical values is consistent*)
FixSigns[a_?ConstQ*x_]:=a FixSigns[x];
FixSigns[a_?ConstQ]:=a;
FixSigns[x_Plus]:=Plus@@(FixSigns/@(Apply[List,x]));
FixSigns[mon_Times|mon_NonCommutativeMultiply|mon_d]:=Module[{tempNCM},
    (Flatten[mon/.{Times->tempNCM,NonCommutativeMultiply->tempNCM,d->tempNCM},Infinity,tempNCM]/.{tempNCM[seq__] :> Signature[Cases[{seq},_?\[Theta]\[Theta]bQ]]})
    *mon
];


prepareCompare[expr_, OptionsPattern[Compare]]:=Module[{vars,powerRule,zeroExpr,all,\[Eta]Lis,\[Theta]Lis,xLis,\[Theta]sqs,vPrint,vvPrint,ruledelayed},
    (*Prints if verbose \[Rule] >0*)
    vPrint[str__]:=If[(OptionValue[verbose]/.{True->1,False->0})>0,Print[str],Null];
    vvPrint[{str__},{str2__}]:=If[(OptionValue[verbose]/.{True->1,False->0})>0,If[OptionValue[verbose]>1,Print[str2],Print[str]],Null];

    (*Sets a rule to treat the list usualVariables as constants for the theta Algebra*)
    ToExpression[
        StringJoin@@Table[
            toString[con]<>"/: ConstQ["<>toString[con]<>"] := True; ",
        {con,Cases[OptionValue[variables],_Symbol]}]];

    vars = Cases[Variables[expr],Alternatives@@OptionValue[variables]];
    vPrint["Solving for ",vars,"."];
    
    powerRule = {Power[b_,e_]:>Power[b,e/.OptionValue[expReplacement]]};
    all = Join[Cases[expr,x_d:>Sequence@@x,Infinity(*,-1*)],Cases[expr,x_?sqQ,{-1}(*,-1*)],Cases[expr,x_?xxQ,{-1}(*,-1*)]];
    all = DeleteDuplicates[all//.{x_?bQ :> unbar[x],
          x_?\[Sigma]Q->Nothing,x_?\[Sigma]bQ -> Nothing,x_Power->Nothing,
          x_?xvQ :> ToExpression[StringDrop[toString[Head[x]],-1]],
          x_?sqQ :> ToExpression[StringDrop[toString[x],-2]],
          x_?xxQ :> Sequence@@(ToExpression/@StringCases[toString[x],"x"~~(DigitCharacter..)])}];
    (*List of variables that will get assigned numerical values*)
    \[Eta]Lis = Select[all,\[Eta]Q];
    \[Theta]Lis = Select[all,\[Theta]Q];
    xLis = Select[all,xQ];
    vPrint["Replacing numerical values to ",\[Eta]Lis," ",\[Theta]Lis," ",xLis,"."];

    \[Theta]sqs=Join[sq/@#,sq[bar[#]]&/@#]&@\[Theta]Lis; If[\[Theta]sqs === {}, \[Theta]sqs = {\[Theta]0sq}];
    
    (*Now it collects similarly as fastApply before applying the FixSigns*)
    zeroExpr = Collect[
        Rex[
            Join[
                Table[ruledelayed[th \[ScriptD][yPattern___], \[ScriptD][yPattern,th]]/.ruledelayed->RuleDelayed,{th,\[Theta]sqs}],
                Table[ruledelayed[xsqr^pPattern_. \[ScriptD][yPattern___], \[ScriptD][yPattern,xsqr^pPattern]]/.ruledelayed->RuleDelayed,{xsqr,sq/@xLis}],
               {HoldPattern[NonCommutativeMultiply[x__]\[ScriptD][y___]]:>\[ScriptD][y,NonCommutativeMultiply[x]],d[x__]^j_. \[ScriptD][y___]:>\[ScriptD][y,d[x]^j],
                A_^pow_ \[ScriptD][y___] :> \[ScriptD][y,A^pow]}
            ]][
           (expr) \[ScriptD][]]
    ,\[ScriptD][___],coefficientwrapper];
    zeroExpr = zeroExpr /. {
               \[ScriptD][a___] :> \[ScriptD][FixSigns[Times[a]]/.powerRule/.d->nd]
               } //. Flatten[Table[{ruledelayed[\[ScriptD][yPattern_ th],th \[ScriptD][yPattern]],ruledelayed[\[ScriptD][th],th \[ScriptD][]]}/.ruledelayed->RuleDelayed,{th,\[Theta]sqs}]] /. {\[ScriptD][x_] :> x, \[ScriptD][] -> 1};
               
    Return[<|"zeroExpr" -> zeroExpr, "\[Eta]Lis" -> \[Eta]Lis, "\[Theta]Lis" -> \[Theta]Lis, "xLis" -> xLis, "\[Theta]sqs" -> \[Theta]sqs, "vars" -> vars|>]
];



(*Solves expr \[Congruent] \[ScriptCapitalA] t1 + \[ScriptCapitalB] t2 + ... = 0 for \[ScriptCapitalA],\[ScriptCapitalB],..*)
Compare[expr_, options: OptionsPattern[]]:=Module[{assoc, vars,zeroExpr,\[Eta]Lis,\[Theta]Lis,xLis,\[Theta]sqs,eqnlist={},vPrint,vvPrint, wrap},
    (*Prints if verbose \[Rule] >0*)
    vPrint[str__]:=If[(OptionValue[verbose]/.{True->1,False->0})>0,Print[str],Null];
    vvPrint[{str__},{str2__}]:=If[(OptionValue[verbose]/.{True->1,False->0})>0,If[OptionValue[verbose]>1,Print[str2],Print[str]],Null];
    
    wrap = OptionValue[wrapfunction];

    assoc = prepareCompare[expr, options];
    zeroExpr = assoc["zeroExpr"];
    \[Eta]Lis = assoc["\[Eta]Lis"];
    \[Theta]Lis = assoc["\[Theta]Lis"];
    xLis = assoc["xLis"];
    \[Theta]sqs = assoc["\[Theta]sqs"];
    vars = assoc["vars"];
    
    (*Solves the linear system for vars - ForceCoeffs*)

    Do[
    AppendTo[eqnlist,wrap/@(Flatten[Normal[CoefficientArrays[zeroExpr/.ran[\[Eta]Lis,\[Theta]Lis,xLis],\[Theta]sqs]]]/.coefficientwrapper[x_]:>x)];
    vvPrint[{Variables[Last[eqnlist]]},{Last[eqnlist]}];
    ,{i,1,Max[Length@vars,OptionValue[minEqns]]}];
        
    Solve[
        #==0&/@DeleteCases[Collect[#,vars,Collect[#,OptionValue[expReplacement][[All,1]]]&]&/@Flatten[eqnlist],0]
        ,vars
    ]/.Rule[symb_,something_]:> Rule[symb,Collect[something,vars,Factor]]
];


(*Solves expr \[Congruent] \[ScriptCapitalA] t1 + \[ScriptCapitalB] t2 + ... = 0 for \[ScriptCapitalA],\[ScriptCapitalB],..*)
fastCompare[expr_, options: OptionsPattern[Compare]]:=Module[{assoc, vars,zeroExpr,\[Eta]Lis,\[Theta]Lis,xLis,\[Theta]sqs,eqnlist={},vPrint,vvPrint, im, wrap, remember={}, appendRemember, sol},
    (*Prints if verbose \[Rule] >0*)
    vPrint[str__]:=If[(OptionValue[verbose]/.{True->1,False->0})>0,Print[str],Null];
    vvPrint[{str__},{str2__}]:=If[(OptionValue[verbose]/.{True->1,False->0})>0,If[OptionValue[verbose]>1,Print[str2],Print[str]],Null];
    
    wrap = OptionValue[wrapfunction];
    
    appendRemember[x_] := (AppendTo[remember,glue[temp\[ScriptCapitalZ], Length[remember]+1] -> x];
                           ToExpression["temp\[ScriptCapitalZ]"<>ToString[Length[remember]]<>"/: ConstQ[temp\[ScriptCapitalZ]"<>ToString[Length[remember]]<>"]:=True"];
                           glue[temp\[ScriptCapitalZ], Length[remember]]);

    assoc = prepareCompare[expr, options];
    zeroExpr = assoc["zeroExpr"]/.coefficientwrapper[x_]:>appendRemember[x];
    \[Eta]Lis = assoc["\[Eta]Lis"];
    \[Theta]Lis = assoc["\[Theta]Lis"];
    xLis = assoc["xLis"];
    \[Theta]sqs = assoc["\[Theta]sqs"];
    vars = assoc["vars"];
    
    (*Solves the linear system for vars - ForceCoeffs*)

    Do[
    AppendTo[eqnlist,wrap/@(Flatten[Normal[CoefficientArrays[zeroExpr/.ran[\[Eta]Lis,\[Theta]Lis,xLis],\[Theta]sqs]]]/.coefficientwrapper[x_]:>x)];
    vvPrint[{Variables[Last[eqnlist]]},{Last[eqnlist]}];
    ,{i,1,Max[Length@vars,OptionValue[minEqns]]}];
    
    sol = Quiet[Solve[
        #==0&/@DeleteCases[Collect[#,vars,Collect[#,OptionValue[expReplacement][[All,1]]]&]&/@Flatten[eqnlist],0]
        ,remember[[All,1]]
    ],Solve::svars];
    vPrint["The intermediate solution is: ",sol];
    
    If[Length[sol] == 0, sol,
    Solve[sol[[1]]/.remember/.Rule->Equal,vars]/.Rule[symb_,something_]:> Rule[symb,Collect[something,vars,Factor]]
    ]
];


(* ::Subsection::Closed:: *)
(*Schouten identities*)


SchoutenContract[t1_,t2_,t3_,t4_]:=Module[{T1,T2,T3,T4,\[Chi]1=Symbol["\[Chi]1"],\[Chi]2=Symbol["\[Chi]2"],\[Chi]3=Symbol["\[Chi]3"],\[Chi]4=Symbol["\[Chi]4"]},

{T1,T2,T3,T4}=({t1,t2,t3,t4})/.{Global`\[Eta]01->\[Chi]1,Global`\[Eta]02->\[Chi]2,Global`\[Eta]03->\[Chi]3,Global`\[Eta]04->\[Chi]4};
remove\[Chi]s[NonCommutativeMultiply@@{T1,T2,T3,T4},d\[Theta]d\[Eta]Rule[d\[Theta]d\[Eta]Rule[#,\[Chi]1,\[Chi]2],\[Chi]3,\[Chi]4]&]+
remove\[Chi]s[NonCommutativeMultiply@@{T1,T2,T3,T4},d\[Theta]d\[Eta]Rule[d\[Theta]d\[Eta]Rule[#,\[Chi]3,\[Chi]1],\[Chi]2,\[Chi]4]&]+
remove\[Chi]s[NonCommutativeMultiply@@{T1,T2,T3,T4},d\[Theta]d\[Eta]Rule[d\[Theta]d\[Eta]Rule[#,\[Chi]1,\[Chi]4],\[Chi]2,\[Chi]3]&]
];

SchoutenContractb[t1_,t2_,t3_,t4_]:=Module[{T1,T2,T3,T4,\[Chi]b1=Symbol["\[Chi]b1"],\[Chi]b2=Symbol["\[Chi]b2"],\[Chi]b3=Symbol["\[Chi]b3"],\[Chi]b4=Symbol["\[Chi]b4"]},

{T1,T2,T3,T4}=({t1,t2,t3,t4})/.{Global`\[Eta]b01->\[Chi]b1,Global`\[Eta]b02->\[Chi]b2,Global`\[Eta]b03->\[Chi]b3,Global`\[Eta]b04->\[Chi]b4};
remove\[Chi]s[NonCommutativeMultiply@@{T1,T2,T3,T4},d\[Theta]bd\[Eta]bRule[d\[Theta]bd\[Eta]bRule[#,\[Chi]b1,\[Chi]b2],\[Chi]b3,\[Chi]b4]&]+
remove\[Chi]s[NonCommutativeMultiply@@{T1,T2,T3,T4},d\[Theta]bd\[Eta]bRule[d\[Theta]bd\[Eta]bRule[#,\[Chi]b3,\[Chi]b1],\[Chi]b2,\[Chi]b4]&]+
remove\[Chi]s[NonCommutativeMultiply@@{T1,T2,T3,T4},d\[Theta]bd\[Eta]bRule[d\[Theta]bd\[Eta]bRule[#,\[Chi]b1,\[Chi]b4],\[Chi]b2,\[Chi]b3]&]
];


GenerateSchouten[{terms__},{termsb__}]:=Module[{list,schout,listb,schoutb,ctimes,schoutrule},

list  = {#1/.Global`\[Eta]00  -> Global`\[Eta]01, #2/.Global`\[Eta]00  -> Global`\[Eta]02, #3/.Global`\[Eta]00  -> Global`\[Eta]03, #4/.Global`\[Eta]00  -> Global`\[Eta]04 }&@@@Subsets[{terms},{4}];
listb = {#1/.Global`\[Eta]b00 -> Global`\[Eta]b01,#2/.Global`\[Eta]b00 -> Global`\[Eta]b02,#3/.Global`\[Eta]b00 -> Global`\[Eta]b03,#4/.Global`\[Eta]b00 -> Global`\[Eta]b04}&@@@Subsets[{termsb},{4}];
schout  = SchoutenContract@@@list;
schoutb = SchoutenContractb@@@listb;
schout  = If[Head[#]===Times,#/.x_Symbol?xsqQ->1,#]&/@Factor/@schout;
schoutb = If[Head[#]===Times,#/.x_Symbol?xsqQ->1,#]&/@Factor/@schoutb;

SetAttributes[ctimes,Orderless];
ctimes[-1,a__]:=-ctimes[a];

Join[Solve[0==#&/@((ReorderNCM/@schout) /.NonCommutativeMultiply->Times/.Times->ctimes)][[1]]/.ctimes->NonCommutativeMultiply,
     Solve[0==#&/@((ReorderNCM/@schoutb)/.NonCommutativeMultiply->Times/.Times->ctimes)][[1]]/.ctimes->NonCommutativeMultiply]
];


(* ::Section::Closed:: *)
(*Two and three-point functions*)


(* ::Subsection::Closed:: *)
(*From CFTs4D package*)


oglue["x",i_,j_]:=If[j>i,glue["x",i,j], - glue["x",j,i]];
oglue["x",i_,j_,"sq"]:=If[j>i,glue["x",i,j,"sq"], glue["x",j,i,"sq"]];
oglue["x",i_,j_,"v",\[Mu]_]:=If[j>i,glue["x",i,j,"v"][\[Mu]], - glue["x",j,i,"v"][\[Mu]]];


removex13[x12_,x23_,{x13_,x13v_,x13sq_}]:={x13 -> x12 + x23, x13sq -> sq[x12]+2 glue[x12,x23]+sq[x23], x13v[m_] :> glue[x12,"v"][m] + glue[x23,"v"][m]};
removex12[x13_,x23_,{x12_,x12v_,x12sq_}]:={x12 -> x13 - x23, x12sq -> sq[x13]-2 glue[x13,x23]+sq[x23], x12v[m_] :> glue[x13,"v"][m] - glue[x23,"v"][m]};
removex23[x12_,x13_,{x23_,x23v_,x23sq_}]:={x23 -> x13 - x12, x23sq -> sq[x13]-2 glue[x12,x13]+sq[x12], x23v[m_] :> glue[x13,"v"][m] - glue[x12,"v"][m]};


(*Xij*)
myspM[i_,j_]:=oglue["x",i,j,"sq"];
(*I^ij*)
myinvSbSnorm[{i_,j_}, {}]:=d[glue["\[Eta]",j],oglue["x",i,j],glue["\[Eta]b",i]];
(*J^k_ij*)
myinvSbSnorm[{k_,k_}, {i_, j_}]:=(oglue["x",i,k,"sq"] oglue["x",j,k,"sq"])/oglue["x",i,j,"sq"] (d[glue["\[Eta]",k],oglue["x",i,k],glue["\[Eta]b",k]]/oglue["x",i,k,"sq"]-d[glue["\[Eta]",k],oglue["x",j,k],glue["\[Eta]b",k]]/oglue["x",j,k,"sq"]);
(*I^ij_kl*)
myinvSbSnorm[{i_,j_}, {k_, l_}]/;i=!=j:=1/(2 oglue["x",k,l,"sq"]) (
                                        (oglue["x",i,k,"sq"]d[glue["\[Eta]",j],oglue["x",j,l],glue["\[Eta]b",i]]-oglue["x",i,l,"sq"]d[glue["\[Eta]",j],oglue["x",j,k],glue["\[Eta]b",i]])+
                                        (oglue["x",j,k,"sq"]d[glue["\[Eta]",j],oglue["x",i,l],glue["\[Eta]b",i]]-oglue["x",j,l,"sq"]d[glue["\[Eta]",j],oglue["x",i,k],glue["\[Eta]b",i]])-
                                         oglue["x",i,j,"sq"]d[glue["\[Eta]",j],oglue["x",k,l],glue["\[Eta]b",i]]-oglue["x",k,l,"sq"]d[glue["\[Eta]",j],oglue["x",i,j],glue["\[Eta]b",i]]+
                                         2I d[\[Epsilon]\[Kappa]\[Lambda]\[Mu]\[Nu],oglue["x",i,k,"v",\[Kappa]],oglue["x",l,j,"v",SpinorAlgebra`\[Lambda]],oglue["x",l,k,"v",\[Mu]],glue["\[Eta]",j],\[Sigma][\[Nu]],glue["\[Eta]b",i]]);
(*K^ij_k*)
myinvSSnorm[{i_,j_},{k_}]:=1/2 Sqrt[oglue["x",i,j,"sq"]/(oglue["x",i,k,"sq"] oglue["x",j,k,"sq"])] (-4 d[oglue["x",i,k,"v",\[Mu]],oglue["x",j,k,"v",\[Nu]],glue["\[Eta]",i],\[Sigma][\[Mu],\[Nu]],glue["\[Eta]",j]]+d[glue["\[Eta]",i],glue["\[Eta]",j]] (-oglue["x",i,j,"sq"]+oglue["x",i,k,"sq"]+oglue["x",j,k,"sq"]));
(*Kb^ij_k*)
myinvSbSbnorm[{i_,j_},{k_}]:=1/2 Sqrt[oglue["x",i,j,"sq"]/(oglue["x",i,k,"sq"] oglue["x",j,k,"sq"])] (-4 d[oglue["x",i,k,"v",\[Mu]],oglue["x",j,k,"v",\[Nu]],glue["\[Eta]b",i],\[Sigma]b[\[Mu],\[Nu]],glue["\[Eta]b",j]]+d[glue["\[Eta]b",i],glue["\[Eta]b",j]] (-oglue["x",i,j,"sq"]+oglue["x",i,k,"sq"]+oglue["x",j,k,"sq"]));
(*L^i_jkl*)
myinvSSnorm[{i_,i_},{j_,k_,l_}]:=2/Sqrt[oglue["x",j,k,"sq"]oglue["x",k,l,"sq"]oglue["x",l,j,"sq"]] (
                                 oglue["x",i,j,"sq"]d[oglue["x",k,l,"v",\[Mu]],oglue["x",i,l,"v",\[Nu]],glue["\[Eta]",i],\[Sigma][\[Mu],\[Nu]],glue["\[Eta]",i]]+
                                 oglue["x",i,k,"sq"]d[oglue["x",l,j,"v",\[Mu]],oglue["x",i,j,"v",\[Nu]],glue["\[Eta]",i],\[Sigma][\[Mu],\[Nu]],glue["\[Eta]",i]]+
                                 oglue["x",i,l,"sq"]d[oglue["x",j,k,"v",\[Mu]],oglue["x",i,k,"v",\[Nu]],glue["\[Eta]",i],\[Sigma][\[Mu],\[Nu]],glue["\[Eta]",i]]);
(*Lb^i_jkl*)
myinvSbSbnorm[{i_,i_},{j_,k_,l_}]:=2/Sqrt[oglue["x",j,k,"sq"]oglue["x",k,l,"sq"]oglue["x",l,j,"sq"]] (
                                 oglue["x",i,j,"sq"]d[oglue["x",k,l,"v",\[Mu]],oglue["x",i,l,"v",\[Nu]],glue["\[Eta]b",i],\[Sigma]b[\[Mu],\[Nu]],glue["\[Eta]b",i]]+
                                 oglue["x",i,k,"sq"]d[oglue["x",l,j,"v",\[Mu]],oglue["x",i,j,"v",\[Nu]],glue["\[Eta]b",i],\[Sigma]b[\[Mu],\[Nu]],glue["\[Eta]b",i]]+
                                 oglue["x",i,l,"sq"]d[oglue["x",j,k,"v",\[Mu]],oglue["x",i,k,"v",\[Nu]],glue["\[Eta]b",i],\[Sigma]b[\[Mu],\[Nu]],glue["\[Eta]b",i]]);


FromCFTs4D[expr_]:=expr/.{CFTs4D`spM        -> myspM,
                          CFTs4D`invSbSnorm -> myinvSbSnorm,
                          CFTs4D`invSSnorm  -> myinvSSnorm,
                          CFTs4D`invSbSbnorm-> myinvSbSbnorm};


(* ::Subsection::Closed:: *)
(*Non supersymmetric two and three-point functions*)


Options[NonSUSY3pf]={points -> Global`x12};
NonSUSY3pf::point="The allowed values for the option points are x12, x23 or x13. Defaulting to x12.";
NonSUSY3pf[{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_},{{l1_,lb1_},{l2_,lb2_},{l3_,lb3_}},OptionsPattern[]]:=
    FromCFTs4D[CFTs4D`n3CorrelationFunction[{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3},{{l1,lb1},{l2,lb2},{l3,lb3}}]]/.Switch[
    OptionValue[points]
    ,Global`x12, removex12[Global`x13,Global`x23,{Global`x12,Global`x12v,Global`x12sq}]
    ,Global`x13, removex13[Global`x12,Global`x23,{Global`x13,Global`x13v,Global`x13sq}]
    ,Global`x23, removex23[Global`x12,Global`x13,{Global`x23,Global`x23v,Global`x23sq}]
    ,_,   Message[NonSUSY3pf::point];
          removex12[Global`x13,Global`x23,{Global`x12,Global`x12v,Global`x12sq}]];


NonSUSY2pf[\[CapitalDelta]_,{j_,jb_}]:=Module[{m\[Eta]1,m\[Eta]2,m\[Eta]b1,m\[Eta]b2,mx12},
    m\[Eta]1  = Global`\[Eta]1;
    m\[Eta]2  = Global`\[Eta]2;
    m\[Eta]b1 = Global`\[Eta]b1;
    m\[Eta]b2 = Global`\[Eta]b2;
    mx12 = Global`x12;
    
    (I)^(j+jb) (d[m\[Eta]1,mx12,m\[Eta]b2]^j d[m\[Eta]2,mx12,m\[Eta]b1]^jb)/sq[mx12]^(\[CapitalDelta]+(j+jb)/2)
];


(* ::Subsection::Closed:: *)
(*Supersymmetric two-point function*)


twopfjjbqqbText = "1:eJztXb2SG8cRBglQBM8mxXLgctmJHDoz4OBYymxXuZTYYskOHehYPrrAOvIOPJKWQr+BHscPo8CPwFABRQrASQvMomene6Zndn4+Bld3vMPuTk9/X/9O72+fXH7x9L+TyeR6tvny+OL19dMH25/ubL78Y/X8/Prp7cNfrj5s/l3/ZvPT369WLy5f/vHi3+dPXp59+fjl6s3Zq/Mvnz15c3bh+j11yV/bP7K94vCvby64feTHl/85f7n77qvF8nr9dGas7Daxsvnmyxdnr1aXL84udo+y2v03awm8S3GffXcll4DXxN1dElxvJXh38+t/fvqX09Pl59f3dt///g9/erS4Xh/8tJHY9Un305OF+eNWnqagb368tfnyrx+vv/nD08XB98udCAY+Nu/+dHO7gx/oD07p+01/3O/Dz/eU7L1byVg3W1I3W1A3c2nQx+Ye7perijXWmn+6o1DrXUjp6Wofm3eOFguA6gB0EJEs9OkBj8HuesDjIKYPPA5YXQuYyuHvDSWAFWBtFqx8bVJ3LzvcyTTQobRroA6oyx11LEc0D0cTKAVKG0VpJEc2EpAAVAC1FaBGz6i6ljuUBtWz7mwqIjUWyAayi0O2h6OsH54C3UA30B0B3abdXn2vACUDq8q4lGV4gUvgsgpcEpqxvTVLyVzrOjl8OKEd7GnxDXgcf96reh/cR9YBMfzX5F3ecVAxNT8z44hYiLTZ5Kju77qEfT0OZqOW4+2LrZmLGb4C1fcAygXljky5M0IzKP1EwxWhmgGKEU8LnBK/Z66Zb+3QoZCX+g1yYZm6OTe3A/030O6KtJtiXrSoFKyY7k7qMhTTTbuoFkOzS9Tsn4uEmNTZHV/330P31dIMZQPDI3UAGAFGgBFlX9AxC1y0jov7prRYBSXgBrhpHTdut0zzyBNABBBVCKL7Igk/23dAVFNqAP6Av2zwx9kUwA/wA/xiwC9826PixQS+hy4CloBlHbDsupvrsYZAN9DdIrrztK51d3IAfUBfVmU4gA/gaxR8tXqwADFA3AKIg/eq6BwrEbUCk8BkfpjcZYz2Z5GRygG2ge2asH17DGwX6uQC2UB2EcjOqfwaG5SNzbAD7oF7t0WvP4IG7AH7RmBPdewf71WiID02rk21VzvV47NKABlAztVvryKeNpGxz0TsrxIB/3PgH/jPHv+tHGY4pgDpbRijVzVcgDkoABQAX34iMYl3DbX/zlB7n6HW/AnVOU+bLn10NOgL9OXuF3BeqpYEnkly70ByIDmQXN0k1/lorocEx4HjwHHguAI5ztL4qTO488EQL8p0zf/lSOw8G5tfZZTt/b4lBwnGe99S75qO9yQ9S8nFsqQg9Qonx1usXHsb6a1QvSt+x2BCGBkYmUKMDHvbXYuAQYFBgUGBQYFBadugDEYt3sCBiYGJgYmBiYGJqd/EnLBMzMOtNv1uyCJoJcldn9lBoYe4zzjEKbMi33ouR6jQlNQ8ygTfihrivG/zC4/98i2EyD5CacXbsMwtaU7nHhKYDmsneZ9v3KrkvOTMvORVuF8kXcX//LRbujIO/GM8Rg/Lb0WUYa+r/UoJZT73vpI01vvf5jMdUvC59SceyuK6z3Ry7N0+9HheisW+YTyv0EGlYBlo26jbvA2KQdeRKEx2R0pRFdwAoSQpq+OAy1S+X5/ccnOeUHz/fx9ugn1uGWbGWMd42HLTMKzCndzKwIcrhaLeLVzfWAhjY8rscmJjyvKpORHW2/gSisKtKb9C1RO33tlBA2tf2VL6qNYs5b3a7WOE5cf8b+02PcK7cF0bx2WoBBayUchG9fdqbkprpwpGU6lyA6lPnCeyUcOq/gGq3qn6zcd7erTR+v33y8M/6dRqq/wHPyzLVHzTpxys9J0cqlBf6LKjsf6FOdEJfO8qmqPeE6+KFqnk5ajGRapPpS0mgdP6nNZh1fRXe7bdtOZlsph1gctK/BOKpos+sgyfPBTUH/GX0km3QugHiqEWgjDFYH1zRyQgj08UsP5WougZR0rbGdFPaYAY8nkqjueYPBDfZYh2B3BBFC5YtMgFy5a4YO9LI3JojATM+QheLnMjVBEqm5pdqvCIsxmy9YlDKxaOOZATh1AV6og4IYQTQjgh1JIXF9CTwbcSiwADY7r+/Fd3e9kc6mau3edfftnvCHnIYUDOYhF7tYXaXgKGd+ytS1hQVRtxM9Yxeg+0n/Q6Tw+9ztMgrzN7UrjHUq4dHYCAExLwXWJftv/358vnVxfnX60mE/qtmihBZcN6pYWp1LUeEU+5jcMpIj9Sz1vH6gmz3wQABq1D/ej4GQcdnJgZ6AF6gB4KPRWW9gFCgLAoENridyIsKRE0Jrt4aCmQCWRmiMxqrKYJEQ+9x1v9QATlEgErRZiy7Q1JwkaThMS1NhjkJQk/JFRQRFg5AcB+HKvYc0gSdLDSHGwHBeABeAAeVxDUbWgC0BDpA2AT2AQ2t9hMVRtu6RAwIAgIapnHKnBznB8k83+6s83mHFoCCYAEopNA7wDs8fuK4I+2gQNzT8i8saUJu7E5mDNCMqWtQVJBkLBJ70TILm2Lo4jdk+MoIm1rOHuLo4iUQHAUMW97Wq9hWAgMw5IczkhX+cw5HLJ98wnFHDSPMdOjuaBTYinV46nG9Vo4gd+aUk0zGuLe3ElHMKOmvgmpErbyElQtE3XVaW7vZaPSBZIDyYHkMhWUheQoQZEk57wr0oXdkyNdiHQh0oWw0Nal9MpvsNBMQcFCD2RbXOuBgWZwIQw0DDQMNAy0aXdgoGGggwz0kPiqqZzeNTjoncFBPh4D31bHsLtaRlLfPMGWZGJL7hgXa9xeWIXRok1g1Y72LSRF165rMV9MPvwefOjde75fSifdxlkzUGQtcut9QmT0aAUW9CQjxYVppj2FDHFks+zBmPCU03h/UddFkpHzu4/8cvPlb5cvNtr//PWrza6/Of/r64tXq6uLr50ifLSwiNDHm2EkJoGPVJ3ocmqLye6p9JQ1m8BxhDent924ndF9CAG4xBoYCCwJuh/VvCigC+hqHF0ch7OuExaAJCA5FiTNd6XdFP1hqMpDRTWKz8+JLfKzW7G9wtFOpwBqE8oAqQ6JyQuHwdiyJSp9JityFKGlw2KAI+AYB46s+fYcRTBy28q1cJkTCfQBfYWgT7fkVHR/DROL6NPbY9HcE+ZMxO6Wjr4UJFd0KsiCHQChpiFUx0kXdM3kx21z1lKY51nQbjM+WfaHsTK31NYJiC0dn32ZYxRFfAOulHPllFgKXAg2L0F8QRzA8sAYx8NQ8wej/cRo4w8EqwzLTCocf75YZXLX5ND9yWPNKLWxY24gXX/S3SWzYo6QqAz8WqRLyR2kG0y6rAlUnFoMKqGgS9Al6LJQuSvSpW03OIA+MdAl8xrV5vjJU7k+Q/cc4+HiDd27fXxNvtTsE/IcRjLSODvGYhTnBcESTmyW0KwCwNppWjurbGHR0mRd2AhEM1bTJOhsBdovhdGMBaqUU2XgDoBQlUMEeRc50s9NEai591QviXd7kBdoR0So1+JrGSw3IxZfAetKhr+lspEtidoymjCVMQyaNOOf6CGamdHkkofBi3X2ooLe0yWDyhYe+GLljdF4BkwCkxqYZB0uZgEMNXXAD/ATwk/tMFwmCW9gKfrp4OGUQkFJUB9c8i8fMUJnYNhn2/j7tKh5i6NRr8/0flsHd2C0j8N1kVwS2/n8nA6fJoUcPzYOgRxDhOyj/5y2KQAk1STKymwDU1GDpkvqZoIx0C4DAMWaAAN0JQx6gSQgqUUksXSKqKhgVCvwCDyK8EiN7oe7VxwoqtF7fpeNZ7p0/CQAmvhbxGaWE1Xzwn4wnlXLH9YdgNeRhlG3n0Aloyxym7OWUuBI1QpLICy2rHikaoVbyqJf5khVn3EIIEsJWU6JpcCJYBMTxBdEAiwfzDFFBJlY0FlGA1UrAzKTB8cfqFqZ3FMTKM6vg2JtFPuRsffOWIzRuIvwWrkQwQiRqW1BiKy2LZYD7zzDiOmZ40x4x/TMFNMz0+p7XlyRlwNe5lSZKhxwj0XIDg/DKW/KKRfmPSROOeCvR7sSrxtyZ9Mu650GrHYoDGAAX4IvwZeFyl2RL4O4AW81MNnKLAzjrQZ2G2ZfDN5qkMIUmo04MHea5s4qW5i0NJkXvuXBiYimWdDZj79fSiddcKUmVwbuABhVOUjwmF+JFHRTDGru/R3WUsoM7Z376LX4QowGjwN6i89sDT60K2nzSGUkWxK15nsNYsUXu64rzNupzrQJgwNG1rqgJi4tBEcbMRtwwtdn2/j7hBGzPsTLeL/dEfG+VQgtELuA4PVmiIPg9QheK2XErCaD4BMS/FGeSW1mJUWBOAPuztzwmjJ2/20/LNLkSRBdorOBym9/ltif/PbHMEPO259ic1wugIfBYW6Q5IwiNkhzg3zmGCJtloNfYUZVzqLIsN2qIK+t4FFzU+OnR+1GUkPUlrztjMV9Rl/iQtoJBBlCkI0CdjSCbNQg6RBksmQ+3MHc2U4rW4ukRhTOpMSh9aJOxNLRiJc2dUeeKWuIosmh0RrhwaLwGcfkPwuQLE8InzGcuiwWh/QZ+4eEXecT4f1VzFsFNVWkGMElcQBGSAYWK297M5rEU/bgLeQCwY/gR7ViPd+vAz9q8KMkk3Dk1zGGSCAirZa5GkXSaMzVqKXQYa5kZ8HgD7bLqubeO9Puu6UMp91RLYnGzZJ6p7TIhc7dxEUuvaOCLm1CkcuvyGXxhCKQJLZNz52yteGw3KmHk3B3CuFjjo6OGT5qYBgWM7Gjwx3XJermAfUqUq/lGU841MuR0wMDxDLqVBv4yyG4qfkRzlSz+DOEHY3i8WYI967JqFGNNZbYkR6mxhIHvugw0qTj3hUdx53XnIdkiFxxeDLGhk4656WKAT/BZkkSWofGc3SeNQ/Hf/yOI2Bzj01pBnWf0+mkW8V4l8TH4u3NM4E7gNcxDvWCsUKD8V/HWJbcgw0bqxs/MFrGPniGv0te+Lu9lnMFiH8niH8R/yL+bcvHZtTRvMxTWWlZSUeAemGMMy8PAXCL4Mw4AC4L32T4FeOdNgkD4Mp2IODFl2kD4LLkLikBSyzb+AFwrfsgq//OjX048JwlmQFM49Xvh9FI6sjff6ua1Jk5dFqWVg7IezKnTvawoNZwBs1Prvn5dfbEAoPYB4o3ghVgyCN4U+mva7hz0kvUAROuGha1mQuueaXZ8D/TY8LO+NXeg/JDPm6qLWTzr4yR5UhY61iuazI08F/grJYKjeCnmmT2A3uf4x8=";


SUSY2pf[{q_,qb_},{j_,jb_}]:= Uncompress[twopfjjbqqbText] /. {qval ->  q, qbval -> qb, jval -> j, jbval -> jb};


(* ::Subsection::Closed:: *)
(*Supersymmetric three-point functions*)


deltaRCharge[{{l1_,lb1_},{l2_,lb2_},{l3_,lb3_}},delta_,scaling_,listQ_]:=Module[{
     X3=glue["x3"],\[CapitalTheta]=glue["\[Theta]",3],\[CapitalTheta]b=glue["\[Theta]b",3],\[CapitalEta]=glue["\[Eta]",3],\[CapitalEta]b=glue["\[Eta]b",3],U,
     ts,ts\[Theta]\[Theta]b,ts\[Theta]\[Theta]b1,ts\[Theta]\[Theta]b2,ts\[Theta]\[Theta]b3,ts\[Theta]\[Theta]b4,ts\[Theta]1,ts\[Theta]2,ts\[Theta]b1,ts\[Theta]b2,ts\[Theta],ts\[Theta]b,replacements,list,\[ScriptI]},
    replacements={
        CFTs4D`invSSnorm[{a_,b_},{_}]    :> d[glue["\[Eta]",a],glue["\[Eta]",b]],
        CFTs4D`invSbSbnorm[{a_,b_},{_}]  :> d[glue["\[Eta]b",a],glue["\[Eta]b",b]],
        CFTs4D`invSbSnorm[{a_,b_},{___}] :> d[glue["\[Eta]",b],X3,glue["\[Eta]b",a]] /Sqrt[sq[X3]]
    };
    (*Preparing the structures from n3ListStructures*)
    If[delta == 0 || delta == 2 || delta == -2,
        ts = CFTs4D`n3ListStructures[{{lb1,l1},{lb2,l2},{l3,lb3}}];
    ];
    If[delta == 0,
        ts\[Theta]\[Theta]b1 = CFTs4D`n3ListStructures[{{lb1,l1},{lb2,l2},{l3+1,lb3+1}}];
        ts\[Theta]\[Theta]b2 = CFTs4D`n3ListStructures[{{lb1,l1},{lb2,l2},{l3-1,lb3+1}}];
        ts\[Theta]\[Theta]b3 = CFTs4D`n3ListStructures[{{lb1,l1},{lb2,l2},{l3+1,lb3-1}}];
        ts\[Theta]\[Theta]b4 = CFTs4D`n3ListStructures[{{lb1,l1},{lb2,l2},{l3-1,lb3-1}}];
    ];
    If[delta == 1 || delta == -1,
        ts\[Theta]1  = CFTs4D`n3ListStructures[{{lb1,l1},{lb2,l2},{l3+1,lb3}}];
        ts\[Theta]2  = CFTs4D`n3ListStructures[{{lb1,l1},{lb2,l2},{l3-1,lb3}}];
        ts\[Theta]b1 = CFTs4D`n3ListStructures[{{lb1,l1},{lb2,l2},{l3,lb3+1}}];
        ts\[Theta]b2 = CFTs4D`n3ListStructures[{{lb1,l1},{lb2,l2},{l3,lb3-1}}];
    ];
    
    (*Mapping them to t(X,\[CapitalTheta],\[CapitalTheta]b) structures*)
    If[delta == 0 || delta == 2 || delta == -2,
        ts=ts/.replacements;
    ]; 
    If[delta == 0,
        ts\[Theta]\[Theta]b=If[Head[#]===Times,Times@@Replace[List@@#,x_Integer->Nothing,{1}],#]&/@(If[Head[#]===Plus,#[[1]],#]&/@Join[
            Expand[1/Sqrt[sq[X3]] \[Theta]bd\[Eta]b[\[Theta]d\[Eta][#,\[CapitalTheta],\[CapitalEta]],\[CapitalTheta]b,\[CapitalEta]b]&/@(ts\[Theta]\[Theta]b1/.replacements)],
            Expand[1/Sqrt[sq[X3]] d[\[CapitalTheta],\[CapitalEta]]**\[Theta]bd\[Eta]b[#,\[CapitalTheta]b,\[CapitalEta]b] &/@(ts\[Theta]\[Theta]b2/.replacements)],
            Expand[1/Sqrt[sq[X3]] d[\[CapitalTheta]b,\[CapitalEta]b]**\[Theta]d\[Eta][#,\[CapitalTheta],\[CapitalEta]]   &/@(ts\[Theta]\[Theta]b3/.replacements)],
            Expand[1/Sqrt[sq[X3]] d[\[CapitalTheta],\[CapitalEta]]**d[\[CapitalTheta]b,\[CapitalEta]b]#      &/@(ts\[Theta]\[Theta]b4/.replacements)]
    ])];
    If[delta == 1 || delta == -1,
        ts\[Theta]=If[Head[#]===Times,Times@@Replace[List@@#,x_Integer->Nothing,{1}],#]&/@(If[Head[#]===Plus,#[[1]],#]&/@Join[
            Expand[\[Theta]d\[Eta][#,\[CapitalTheta],\[CapitalEta]]&/@(ts\[Theta]1/.replacements)],
            Expand[d[\[CapitalTheta],\[CapitalEta]]**# &/@(ts\[Theta]2/.replacements)]
        ]);
        ts\[Theta]b=If[Head[#]===Times,Times@@Replace[List@@#,x_Integer->Nothing,{1}],#]&/@(If[Head[#]===Plus,#[[1]],#]&/@Join[
            Expand[\[Theta]bd\[Eta]b[#,\[CapitalTheta]b,\[CapitalEta]b]&/@(ts\[Theta]b1/.replacements)],
            Expand[d[\[CapitalTheta]b,\[CapitalEta]b]**#   &/@(ts\[Theta]b2/.replacements)]
    ])];

    U = sq[X3]^Simplify[scaling/2];
    Switch[delta
        ,0,  list = {U ts, U ts\[Theta]\[Theta]b, U (sq[\[CapitalTheta]] sq[\[CapitalTheta]b])/sq[X3] ts}
        ,1,  list = {U ts\[Theta], U sq[\[CapitalTheta]]/Sqrt[sq[X3]] ts\[Theta]b}
        ,-1, list = {U ts\[Theta]b, U sq[\[CapitalTheta]b]/Sqrt[sq[X3]] ts\[Theta]}
        ,2,  list = {sq[\[CapitalTheta]]  U ts}
        ,-2, list = {sq[\[CapitalTheta]b] U ts}
        
    ];
    
    If[listQ,
        Return[list], 
        ConstQ[\[ScriptCapitalC][n_]]:=True;
        FN[\[ScriptCapitalC][n_]]:=0;
        \[ScriptI]=1;
        Return[Plus@@Table[\[ScriptCapitalC][\[ScriptI]++] tensStruct, {tensStruct,Flatten[list]}]]
    ];
];


Options[SUSY3pf]={list -> False};
SUSY3pf::Rcharges="The sum of the R-charges must be \[PlusMinus]2,\[PlusMinus]1 or 0. It is `1` instead.";
SUSY3pf[{{q1_,qb1_},{q2_,qb2_},{q3_,qb3_}},{{l1_,lb1_},{l2_,lb2_},{l3_,lb3_}},OptionsPattern[]] := Module[{r1,r2,r3,delta, aPab},
    r1 = 2/3(q1-qb1);
    r2 = 2/3(q2-qb2);
    r3 = 2/3(q3-qb3);
    delta = Simplify[-r3 - r2 - r1];
    aPab = Simplify[-q1-q2+q3-qb1-qb2+qb3];
    
    If[!MemberQ[{2,-2,1,-1,0},delta], Message[SUSY3pf::Rcharges,ToString[-delta,InputForm]]; Return[0]];
    
    deltaRCharge[{{l1,lb1},{l2,lb2},{l3,lb3}},delta,aPab - Abs[delta]/2,OptionValue[list]]
];


(* ::Subsection::Closed:: *)
(*Permutation of points*)


permute12[threepf_,{{q1_,qb1_},{q2_,qb2_},{q3_,qb3_}},{{j1_,jb1_},{j2_,jb2_},{j3_,jb3_}}, opts : OptionsPattern[Compare]]:=
    permute12Basis[threepf,SUSY3pf[{{q2, qb2}, {q1, qb1}, {q3, qb3}}, {{j2, jb2}, {j1, jb1}, {j3, jb3}}], opts];


permute12Basis[threepf_,Basis_, opts : OptionsPattern[Compare]]:=Module[{tpf12, basis, vars={}},
    tpf12 = threepf /. {Global`\[Eta]1 -> Global`\[Eta]2, Global`\[Eta]2 -> Global`\[Eta]1, 
                        Global`\[Eta]b1 -> Global`\[Eta]b2, Global`\[Eta]b2 -> Global`\[Eta]b1,
                        Global`x3 -> -Global`x3, Global`\[Theta]3 -> -Global`\[Theta]3, Global`\[Theta]b3 -> -Global`\[Theta]b3};
    basis = Basis /. \[ScriptCapitalC][i_] :> (AppendTo[vars, glue[temp\[ScriptCapitalC],i] -> \[ScriptCapitalC][i]];glue[temp\[ScriptCapitalC],i]);
    Compare[tpf12 - basis, variables -> vars[[All,1]], opts][[1]]/.vars
];


permutet[permutation_][expr_]:=expr/.{
    Global`\[Eta]1 :> glue["\[Eta]",permutation[1]], Global`\[Eta]2 :> glue["\[Eta]",permutation[2]], Global`\[Eta]3 :> glue["\[Eta]",permutation[3]],
    Global`\[Eta]b1 :> glue["\[Eta]b",permutation[1]], Global`\[Eta]b2 :> glue["\[Eta]b",permutation[2]], Global`\[Eta]b3 :> glue["\[Eta]b",permutation[3]]};


Xup[a_,ad_]:=Module[{m=Unique["\[Mu]"]},v[Global`x3][m]\[Sigma][m][a,ad]+2I low[Global`\[Theta]3][a]**low[Global`\[Theta]b3][ad]];
Xbup[a_,ad_]:=Module[{m=Unique["\[Mu]"]},v[Global`x3][m]\[Sigma][m][a,ad]-2I low[Global`\[Theta]3][a]**low[Global`\[Theta]b3][ad]];
Xuptilde[ad_,a_]:=Module[{m=Unique["\[Mu]"]},v[Global`x3][m]\[Sigma]b[m][ad,a]-2I up[Global`\[Theta]b3][ad]**up[Global`\[Theta]3][a]];
Xbuptilde[ad_,a_]:=Module[{m=Unique["\[Mu]"]},v[Global`x3][m]\[Sigma]b[m][ad,a]+2I up[Global`\[Theta]b3][ad]**up[Global`\[Theta]3][a]];
Xsq :=Contraction[-1/2Xup[a,ad]Xuptilde[ad,a]];
Xbsq :=Contraction[-1/2Xbup[a,ad]Xbuptilde[ad,a]];
XsqToTheQSaved:=FPower[Xsq,Global`x3sq,Q,4];
XbsqToTheQSaved:=FPower[Xbsq,Global`x3sq,Q,4];
XsqToTheQ[QQ_]:=XsqToTheQSaved/.Q->QQ;
XbsqToTheQ[QQ_]:=XbsqToTheQSaved/.Q->QQ;


IXXb[{{q1_,qb1_},{q2_,qb2_},{q3_,qb3_}},{{j1_,jb1_},{j2_,jb2_},{j3_,jb3_}}] := Module[{a,ab},
    a = 1/3(q3-qb1-qb2)+2/3(qb3-q1-q2);
    ab = 2/3(q3-qb1-qb2)+1/3(qb3-q1-q2);
    (-1)^(j2+jb2+jb3+j1) XsqToTheQ[-(a+qb2)-jb1/2+j3/2]XbsqToTheQ[-(ab+q2)-j1/2+jb3/2]//fastReduction
]


\[Eta]bXReplacement[i_]:=Block[{ad=Unique["\[Alpha]d"],expand},
    Map[Flatten[#,\[Infinity],tempNCM]/.tempNCM[X__]:>Function[{X}]&,
        Thread[((If[Head[expand=Expand[#]]===Plus,List@@expand,{expand}]/.{Times->tempNCM,NonCommutativeMultiply->tempNCM})&/@{
           -Xup[#,ad] up[glue["\[Eta]b",i]][ad], low[glue["\[Eta]b",i]][ad]Xuptilde[ad,#]})],{2}]];
Xb\[Eta]Replacement[i_]:=Block[{a=Unique["\[Alpha]"],expand},
    Map[Flatten[#,\[Infinity],tempNCM]/.tempNCM[X__]:>Function[{X}]&,
        Thread[((If[Head[expand=Expand[#]]===Plus,List@@expand,{expand}]/.{Times->tempNCM,NonCommutativeMultiply->tempNCM})&/@{
            -up[glue["\[Eta]",i]][a]Xbup[a,#],Xbuptilde[#,a] low[glue["\[Eta]",i]][a]})],{2}]];

\[Eta]bXbReplacement[i_]:=Block[{ad=Unique["\[Alpha]d"],expand},
    Map[Flatten[#,\[Infinity],tempNCM]/.tempNCM[X__]:>Function[{X}]&,
        Thread[((If[Head[expand=Expand[#]]===Plus,List@@expand,{expand}]/.{Times->tempNCM,NonCommutativeMultiply->tempNCM})&/@{
           -Xbup[#,ad] up[glue["\[Eta]b",i]][ad], low[glue["\[Eta]b",i]][ad]Xbuptilde[ad,#]})],{2}]];
X\[Eta]Replacement[i_]:=Block[{a=Unique["\[Alpha]"],expand},
    Map[Flatten[#,\[Infinity],tempNCM]/.tempNCM[X__]:>Function[{X}]&,
        Thread[((If[Head[expand=Expand[#]]===Plus,List@@expand,{expand}]/.{Times->tempNCM,NonCommutativeMultiply->tempNCM})&/@{
            -up[glue["\[Eta]",i]][a]Xup[a,#],Xuptilde[#,a] low[glue["\[Eta]",i]][a]})],{2}]];


p231to123 = #/.{1->2,2->3,3->1}&;


Options[permuteCyclic] = {finalOutput -> True};


permuteCyclic[threepf_,{{q1_,qb1_},{q2_,qb2_},{q3_,qb3_}},{{j1_,jb1_},{j2_,jb2_},{j3_,jb3_}}, opts : OptionsPattern[{Compare,permuteCyclic}]]:=
    permuteCyclicBasis[threepf,SUSY3pf[{{q2, qb2}, {q3, qb3}, {q1, qb1}}, {{j2, jb2}, {j3, jb3}, {j1, jb1}}],{{q1, qb1}, {q2, qb2}, {q3, qb3}}, {{j1, jb1}, {j2, jb2}, {j3, jb3}}, opts]


permuteCyclicBasis[threepf_, Basis_,{{q1_,qb1_},{q2_,qb2_},{q3_,qb3_}},{{j1_,jb1_},{j2_,jb2_},{j3_,jb3_}}, opts : OptionsPattern[{Compare,permuteCyclic}]]:=Module[{LHS, RHS, basis, vars={}},
    basis = Basis /. \[ScriptCapitalC][i_] :> (AppendTo[vars, glue[temp\[ScriptCapitalC],i] -> \[ScriptCapitalC][i]];glue[temp\[ScriptCapitalC],i]);
    LHS = Replace\[Eta]ToAny[Replace\[Eta]ToAny[permutet[p231to123][basis]/.{Global`\[Eta]3->Global`\[Eta]33,Global`\[Eta]b3->Global`\[Eta]b33}/.{
        d[asd__]^P_:>Module[{replaced},
            replaced=fastReduction[Replace\[Eta]ToAny[
            Replace\[Eta]ToAny[
                d[asd],Global`\[Eta]33,\[Eta]bXbReplacement[3]]//fastReduction,
            Global`\[Eta]b33,X\[Eta]Replacement[3]]//fastReduction
            ];
        If[IntegerQ[P]&&P>1,
            FPower[replaced,P],
            FPower[replaced,SetToZero[replaced,{Global`\[Theta]3,Global`\[Theta]b3}],P,4]
        ]]
    }//fastReduction,Global`\[Eta]33,\[Eta]bXbReplacement[3]],Global`\[Eta]b33,X\[Eta]Replacement[3]]/.\[ScriptCapitalC][i_]:>\[ScriptCapitalD][i] // fastReduction;
    RHS = IXXb[{{q1,qb1},{q2,qb2},{q3,qb3}},{{j1,jb1},{j2,jb2},{j3,jb3}}] Replace\[Eta]ToAny[Replace\[Eta]ToAny[threepf/.{Global`\[Eta]1->Global`\[Eta]11,Global`\[Eta]b1->Global`\[Eta]b11}/.{
        d[asd__]^P_:>Module[{replaced},
            replaced=fastReduction[Replace\[Eta]ToAny[
            Replace\[Eta]ToAny[
                d[asd],Global`\[Eta]11,\[Eta]bXReplacement[1]]//fastReduction,
            Global`\[Eta]b11,Xb\[Eta]Replacement[1]]//fastReduction
            ];
        If[IntegerQ[P]&&P>1,
            FPower[replaced,P],
            FPower[replaced,SetToZero[replaced,{Global`\[Theta]3,Global`\[Theta]b3}],P,4]
        ]]
    }//fastReduction,Global`\[Eta]11,\[Eta]bXReplacement[1]],Global`\[Eta]b11,Xb\[Eta]Replacement[1]]// fastReduction;
    If[OptionValue[finalOutput] == True,
        Compare[LHS - RHS, variables -> vars[[All,1]], opts][[1]]/.vars,
        {LHS,RHS,vars}]
];


(* ::Section::Closed:: *)
(*Expansion of three-point functions*)


(* ::Subsection::Closed:: *)
(*General*)


orderexp = 4;
tokeep = {Global`\[Theta]1,Global`\[Theta]2,Global`\[Theta]3,
		  Global`\[Theta]b1,Global`\[Theta]b2,Global`\[Theta]b3,
          Global`\[Theta]1sq,Global`\[Theta]2sq,Global`\[Theta]3sq,
          Global`\[Theta]b1sq,Global`\[Theta]b2sq,Global`\[Theta]b3sq};
          
removeDots = {
	Global`x12x13|Global`x13x12 -> -1/2(Global`x23sq - Global`x12sq - Global`x13sq),
	Global`x12x23|Global`x23x12 -> 1/2(Global`x13sq - Global`x12sq - Global`x23sq),
	Global`x13x23|Global`x23x13 -> -1/2(Global`x12sq - Global`x23sq - Global`x13sq)
};

tokeepSet[tk_]:=Module[{},
	ClearAll[xibjsqToTheQSaved,pre\[Eta]U\[Eta]b,pre\[Eta]U\[CapitalTheta]b,pre\[CapitalTheta]U\[Eta]b,\[CapitalTheta]U\[CapitalTheta]b,pre\[CapitalTheta]\[Eta],pre\[CapitalTheta]b\[Eta]b,Usq,\[CapitalTheta]sq,\[CapitalTheta]bsq,UsqToTheQSaved,\[Eta]U\[Eta]bToTheQSaved];
    \[Theta]tozero=Flatten[{low[#][_]->0,up[#][_]->0}&/@Complement[{Global`\[Theta]1,Global`\[Theta]2,Global`\[Theta]3,Global`\[Theta]b1,Global`\[Theta]b2,Global`\[Theta]b3},tk]];
    \[Theta]sqtozero=#->0&/@Complement[{Global`\[Theta]1sq,Global`\[Theta]2sq,Global`\[Theta]3sq,Global`\[Theta]b1sq,Global`\[Theta]b2sq,Global`\[Theta]b3sq},tk];
    tokeep = tk;
    defineStuff[];
]


tokeepSet[tokeep];


beforeContract[expr_]:=Expand[expr]/.\[Theta]sqtozero/.{(Global`\[Theta]1sq|Global`\[Theta]b1sq|Global`\[Theta]2sq|Global`\[Theta]b2sq|Global`\[Theta]3sq|Global`\[Theta]b3sq)^p_ :> 0};


(* ::Subsection::Closed:: *)
(*Helper functions*)


removeDotProd[expr_] := expr /. removeDots;


removex[i_,j_][expr_] := Switch[{i,j},
	{1,2}, expr /. removex12[Global`x13,Global`x23,{Global`x12,Global`x12v,Global`x12sq}]~Join~{
		Global`x12x23|Global`x23x12 -> Global`x13x23 - Global`x23sq,
		Global`x12x13|Global`x13x12 -> Global`x13sq - Global`x13x23
	},
	{1,3}, expr /. removex13[Global`x12,Global`x23,{Global`x13,Global`x13v,Global`x13sq}]~Join~{
		Global`x13x23|Global`x23x13 -> Global`x12x23 + Global`x23sq,
		Global`x12x13|Global`x13x12 -> Global`x12sq + Global`x12x23
	},
	{2,3}, expr /. removex23[Global`x12,Global`x13,{Global`x23,Global`x23v,Global`x23sq}]~Join~{
		Global`x12x23|Global`x23x12 -> Global`x12x13 - Global`x12sq,
		Global`x13x23|Global`x23x13 -> Global`x13sq - Global`x12x13
	}
]


(* ::Subsection::Closed:: *)
(*Functions of two and three points*)


defineStuff[] := Module[{},


xijb[i_,j_][a_,ad_]:=Module[{m=Unique["\[Mu]"]},
	If[i<j,
		v[glue["x",i,j]][m],
		-v[glue["x",j,i]][m]
	]\[Sigma][m][a,ad]+4I low[glue["\[Theta]",i]][a]**low[glue["\[Theta]b",j]][ad]
	- 2I low[glue["\[Theta]",i]][a]**low [glue["\[Theta]b",i]][ad]
	- 2I low[glue["\[Theta]",j]][a]**low[glue["\[Theta]b",j]][ad]/.\[Theta]tozero
];


xibj[i_,j_][ad_,a_]:=Module[{m=Unique["\[Mu]"]},
	If[i<j,
		v[glue["x",i,j]][m],
		-v[glue["x",j,i]][m]
	]\[Sigma]b[m][ad,a]+4I up[glue["\[Theta]b",i]][ad]**up[glue["\[Theta]",j]][a]
	- 2I up[glue["\[Theta]b",i]][ad]**up[glue["\[Theta]",i]][a]
	- 2I up[glue["\[Theta]b",j]][ad]**up[glue["\[Theta]",j]][a]/.\[Theta]tozero
];


\[Theta]ij[i_,j_][\[Alpha]_]:=up[glue["\[Theta]",i]][\[Alpha]]-up[glue["\[Theta]",j]][\[Alpha]]/.\[Theta]tozero;
\[Theta]bij[i_,j_][\[Alpha]d_]:=up[glue["\[Theta]b",i]][\[Alpha]d]-up[glue["\[Theta]b",j]][\[Alpha]d]/.\[Theta]tozero;


xibjsq[i_,j_]:=Module[{\[Alpha]=Unique["\[Alpha]"],\[Alpha]d=Unique["\[Alpha]d"]},
	Contraction[1/2xibj[i,j][\[Alpha],\[Alpha]d]xijb[j,i][\[Alpha]d,\[Alpha]]]/.\[Theta]sqtozero
];


xibjsqToTheQSaved[i_,j_]:=xibjsqToTheQSaved[i,j]=fastReduction[
	FPower[xibjsq[i,j],
		   If[i<j,glue["x",i,j,sq],glue["x",j,i,sq]],
		   $Q,
		   orderexp
	]/.\[Theta]sqtozero];
xibjsqToTheQ[i_,j_][QQ_]:=xibjsqToTheQSaved[i,j]/.{$Q->QQ};




\[CapitalTheta][\[Alpha]_]:=Module[{\[Alpha]d=Unique["\[Alpha]d"]},
	I(FPower[xibjsq[1,3],Global`x13sq,-1,orderexp]xijb[3,1][\[Alpha],\[Alpha]d]\[Theta]bij[3,1][\[Alpha]d]
	- FPower[xibjsq[2,3],Global`x23sq,-1,orderexp]xijb[3,2][\[Alpha],\[Alpha]d]\[Theta]bij[3,2][\[Alpha]d])/.\[Theta]sqtozero
];
\[CapitalTheta]b[\[Alpha]d_]:=Module[{\[Alpha]=Unique["\[Alpha]"]},
	I(FPower[xibjsq[3,1],Global`x13sq,-1,orderexp]\[Theta]ij[3,1][\[Alpha]]xijb[1,3][\[Alpha],\[Alpha]d]
	- FPower[xibjsq[3,2],Global`x23sq,-1,orderexp]\[Theta]ij[3,2][\[Alpha]]xijb[2,3][\[Alpha],\[Alpha]d])/.\[Theta]sqtozero
];


U[\[Alpha]_,\[Alpha]d_]:=Module[{\[Beta]=Unique["\[Beta]"],\[Beta]d=Unique["\[Beta]d"]},
	(1/2FPower[xibjsq[1,3]xibjsq[3,2],Global`x13sq Global`x23sq,-1,orderexp](
		xijb[3,1][\[Alpha],\[Beta]d]xibj[1,2][\[Beta]d,\[Beta]]xijb[2,3][\[Beta],\[Alpha]d]
	)-1/2FPower[xibjsq[3,1]xibjsq[2,3],Global`x13sq Global`x23sq,-1,orderexp](
		xijb[3,2][\[Alpha],\[Beta]d]xibj[2,1][\[Beta]d,\[Beta]]xijb[1,3][\[Beta],\[Alpha]d]
	))/.\[Theta]sqtozero
];


pre\[Eta]U\[Eta]b[\[Eta]_,\[Eta]b_]:=pre\[Eta]U\[Eta]b[\[Eta],\[Eta]b]=Module[{\[Alpha]=Unique["\[Alpha]"],\[Alpha]d=Unique["\[Alpha]d"]},
	Contraction[up[\[Eta]][\[Alpha]]U[\[Alpha],\[Alpha]d]up[\[Eta]b][\[Alpha]d]/.\[Theta]sqtozero]
];


\[Eta]U\[Eta]b[\[Eta]_,\[Eta]b_]:=pre\[Eta]U\[Eta]b[Global`\[Eta]999, Global`\[Eta]b999] /. {Global`\[Eta]999 -> \[Eta], Global`\[Eta]b999 -> \[Eta]b};


pre\[Eta]U\[CapitalTheta]b[\[Eta]_]:=pre\[Eta]U\[CapitalTheta]b[\[Eta]]=Module[{\[Alpha]=Unique["\[Alpha]"],\[Alpha]d=Unique["\[Alpha]d"],\[Beta]d=Unique["\[Beta]d"]},
	Contraction[up[\[Eta]][\[Alpha]]U[\[Alpha],\[Alpha]d]  \[CurlyEpsilon][\[Alpha]d,\[Beta]d] \[CapitalTheta]b[\[Beta]d]]/.\[Theta]sqtozero
];


\[Eta]U\[CapitalTheta]b[\[Eta]_]:=pre\[Eta]U\[CapitalTheta]b[Global`\[Eta]999] /. {Global`\[Eta]999 -> \[Eta]};


pre\[CapitalTheta]U\[Eta]b[\[Eta]b_]:=pre\[CapitalTheta]U\[Eta]b[\[Eta]b]=Module[{\[Alpha]=Unique["\[Alpha]"],\[Alpha]d=Unique["\[Alpha]d"],\[Beta]=Unique["\[Beta]"]},
	Contraction[\[CurlyEpsilon][\[Alpha],\[Beta]] \[CapitalTheta][\[Beta]]U[\[Alpha],\[Alpha]d]up[\[Eta]b][\[Alpha]d]]/.\[Theta]sqtozero
];


\[CapitalTheta]U\[Eta]b[\[Eta]b_]:=pre\[CapitalTheta]U\[Eta]b[Global`\[Eta]b999] /. {Global`\[Eta]b999 -> \[Eta]b};


\[CapitalTheta]U\[CapitalTheta]b[]:=\[CapitalTheta]U\[CapitalTheta]b[]=Module[{\[Alpha]=Unique["\[Alpha]"],\[Alpha]d=Unique["\[Alpha]d"],\[Beta]=Unique["\[Beta]"],\[Beta]d=Unique["\[Beta]d"]},
	Contraction[\[CurlyEpsilon][\[Alpha],\[Beta]] U[\[Alpha],\[Alpha]d]\[CurlyEpsilon][\[Alpha]d,\[Beta]d]\[CapitalTheta][\[Beta]]** \[CapitalTheta]b[\[Beta]d]]/.\[Theta]sqtozero
];


pre\[CapitalTheta]\[Eta][\[Eta]_]:=pre\[CapitalTheta]\[Eta][\[Eta]]=Module[{\[Alpha] = Unique["\[Alpha]"]},
	-Contraction[up[\[Eta]][\[Alpha]]\[CapitalTheta][\[Alpha]]]/.\[Theta]sqtozero
];
pre\[CapitalTheta]b\[Eta]b[\[Eta]b_]:=pre\[CapitalTheta]b\[Eta]b[\[Eta]b]=Module[{\[Alpha]d = Unique["\[Alpha]d"]},
	Contraction[\[CapitalTheta]b[\[Alpha]d]up[\[Eta]b][\[Alpha]d]]/.\[Theta]sqtozero
];


\[CapitalTheta]\[Eta][\[Eta]_]:=pre\[CapitalTheta]\[Eta][Global`\[Eta]999] /. {Global`\[Eta]999 -> \[Eta]};
\[CapitalTheta]b\[Eta]b[\[Eta]b_]:=pre\[CapitalTheta]b\[Eta]b[Global`\[Eta]b999] /. {Global`\[Eta]b999 -> \[Eta]b};


Usq[]:=Usq[]=-1/2Contraction[U[\[Alpha],\[Alpha]d]U[\[Beta],\[Beta]d]\[CurlyEpsilon][\[Alpha]d,\[Beta]d]\[CurlyEpsilon][\[Alpha],\[Beta]]]/.\[Theta]sqtozero//removex[1,2]//Simplify;
(*Usq[]:=Usq[]=Module[{U\[Mu]},
	U\[Mu] := Contraction[U[\[Alpha],\[Alpha]d]\[Sigma]b[\[Mu]999][\[Alpha]d,\[Alpha]]] /. \[Epsilon]4[__] -> 0;
	Contraction[U\[Mu]\[CenterDot]U\[Mu] /. CenterDot \[Rule] Times]/.\[Theta]sqtozero//.removeDots//Simplify
];*)


\[CapitalTheta]sq[]:=\[CapitalTheta]sq[]=Contraction[\[CapitalTheta][\[Alpha]]**\[CapitalTheta][\[Beta]]\[CurlyEpsilon][\[Beta],\[Alpha]]]/.\[Theta]sqtozero;
\[CapitalTheta]bsq[]:=\[CapitalTheta]bsq[]=Contraction[\[CapitalTheta]b[\[Alpha]d]**\[CapitalTheta]b[\[Beta]d]\[CurlyEpsilon][\[Alpha]d,\[Beta]d]]/.\[Theta]sqtozero;


UsqToTheQSaved[]:=UsqToTheQSaved[]=Module[{UsqTemp},
	UsqTemp = Usq[];
	fastReduction[
		FPower[UsqTemp,
			   UsqTemp/.Map[(up|low)[#][_] -> 0&,{Global`\[Theta]1,Global`\[Theta]2,Global`\[Theta]3,
								 Global`\[Theta]b1,Global`\[Theta]b2,Global`\[Theta]b3}]/.Map[
						    d[___,#,___] -> 0&,{Global`\[Theta]1,Global`\[Theta]2,Global`\[Theta]3,
								 Global`\[Theta]b1,Global`\[Theta]b2,Global`\[Theta]b3}]/. {
								 Global`\[Theta]1sq->0,Global`\[Theta]2sq->0,Global`\[Theta]3sq->0,
                                  Global`\[Theta]b1sq->0,Global`\[Theta]b2sq->0,Global`\[Theta]b3sq->0},
			   $Q,
			   orderexp
		]/.\[Theta]sqtozero]
];
UsqToTheQ[QQ_]:=UsqToTheQSaved[]/.{$Q->QQ};


\[Eta]U\[Eta]bToTheQSaved[]:=\[Eta]U\[Eta]bToTheQSaved[]=Module[{UTemp},
	UTemp = pre\[Eta]U\[Eta]b[Global`\[Eta]999, Global`\[Eta]b999];
	fastReduction[
		FPower[UTemp,
			   UTemp/.Map[(up|low)[#][_] -> 0&,{Global`\[Theta]1,Global`\[Theta]2,Global`\[Theta]3,
								 Global`\[Theta]b1,Global`\[Theta]b2,Global`\[Theta]b3}]/.Map[
						  d[___,#,___] -> 0&,{Global`\[Theta]1,Global`\[Theta]2,Global`\[Theta]3,
								 Global`\[Theta]b1,Global`\[Theta]b2,Global`\[Theta]b3}]/. {
								 Global`\[Theta]1sq->0,Global`\[Theta]2sq->0,Global`\[Theta]3sq->0,
                                  Global`\[Theta]b1sq->0,Global`\[Theta]b2sq->0,Global`\[Theta]b3sq->0},
			   $Q,
			   orderexp
		]/.\[Theta]sqtozero]
];
\[Eta]U\[Eta]bToTheQ[\[Eta]_,\[Eta]b_][QQ_]:=\[Eta]U\[Eta]bToTheQSaved[]/.{Global`\[Eta]999 -> \[Eta], Global`\[Eta]b999 -> \[Eta]b}/.{$Q->QQ};


];


saveBuildingBlocks[path_]:=If[StringMatchQ[path,__~~".mx"],
	DumpSave[path,{xibjsqToTheQSaved,pre\[Eta]U\[Eta]b,pre\[Eta]U\[CapitalTheta]b,pre\[CapitalTheta]U\[Eta]b,\[CapitalTheta]U\[CapitalTheta]b,pre\[CapitalTheta]\[Eta],pre\[CapitalTheta]b\[Eta]b,Usq,\[CapitalTheta]sq,\[CapitalTheta]bsq,UsqToTheQSaved,\[Eta]U\[Eta]bToTheQSaved}],
	Print["Provide a path .mx"]
];


loadBuildingBlocks[path_]:=If[StringMatchQ[path,__~~".mx"],
	Get[path],
	Print["Provide a path .mx"]
];


(* ::Subsection::Closed:: *)
(*Prefactor*)


general\[Eta]Replacement[i_]:=Block[{ad=Unique["\[Alpha]d"],expand},
	Map[
		Flatten[#,\[Infinity],tempNCM]/.tempNCM[X__]:>Function[{X}]&,
		Thread[((If[Head[expand=Expand[#]]===Plus, List@@expand,{expand}]/.{
			Times->tempNCM,NonCommutativeMultiply->tempNCM})&/@{
				xijb[3,i][#,ad]up[glue["\[Eta]b",i]][ad],
				low[glue["\[Eta]b",i]][ad]xibj[i,3][ad,#]
			})],
		{2}]];
general\[Eta]bReplacement[i_]:=Block[{a=Unique["\[Alpha]"],expand},
	Map[
		Flatten[#,\[Infinity],tempNCM]/.tempNCM[X__]:>Function[{X}]&,
		Thread[((If[Head[expand=Expand[#]]===Plus, List@@expand,{expand}]/.{
			Times->tempNCM,NonCommutativeMultiply->tempNCM})&/@{
				up[glue["\[Eta]",i]][a]xijb[i,3][a,#],
				xibj[3,i][#,a]low[glue["\[Eta]",i]][a]
			})],
		{2}]];


generalKin[{{q1_,qb1_},{q2_,qb2_}},{{j1_,jb1_},{j2_,jb2_}}]:=
	generalKin[{{q1,qb1},{q2,qb2}},{{j1,jb1},{j2,jb2}}]=
	fastReduction[
		xibjsqToTheQ[1,3][-qb1-jb1/2]xibjsqToTheQ[3,1][-q1-j1/2]
		xibjsqToTheQ[2,3][-qb2-jb2/2]xibjsqToTheQ[3,2][-q2-j2/2]
		/.\[Theta]sqtozero
	]/.\[Theta]sqtozero;


(* ::Subsection::Closed:: *)
(*Final function*)


(*Expands the three-point function by replacing the previous definitions and by using the Replace\[Eta]ToAny method. This is called internally by expand expandThreePointFunction, don't use it.*)
expandThreePointFunction[{{q1_,qb1_},{q2_,qb2_}},{{j1_,jb1_},{j2_,jb2_}},t_]:=Module[{uu,vv,ww,xx,yy,zz,\[Theta]scale},
	ConstQ[uu]=True;
	ConstQ[vv]=True;
	ConstQ[ww]=True;
	ConstQ[xx]=True;
	ConstQ[yy]=True;
	ConstQ[zz]=True;
	\[Theta]scale = MapIndexed[#->{uu,vv,ww,xx,yy,zz}[[#2[[1]]]]#&,{Global`\[Theta]1,Global`\[Theta]2,Global`\[Theta]3,Global`\[Theta]b1,Global`\[Theta]b2,Global`\[Theta]b3}
             ]~Join~MapIndexed[#->{uu,vv,ww,xx,yy,zz}[[#2[[1]]]]^2 #&,sq/@{Global`\[Theta]1,Global`\[Theta]2,Global`\[Theta]3,Global`\[Theta]b1,Global`\[Theta]b2,Global`\[Theta]b3}];
	fastReduction[
		generalKin[{{q1,qb1},{q2,qb2}},{{j1,jb1},{j2,jb2}}]*Replace\[Eta]ToAny[
		Replace\[Eta]ToAny[
			Replace\[Eta]ToAny[
				Replace\[Eta]ToAny[
					(Expand[t]/.{Global`\[Eta]1->Global`\[Eta]11, Global`\[Eta]b1->Global`\[Eta]b11, Global`\[Eta]2->Global`\[Eta]22,Global`\[Eta]b2->Global`\[Eta]b22,
								Global`\[Theta]3->Global`\[Theta]4,Global`\[Theta]b3->Global`\[Theta]b4,Global`\[Theta]b3sq->Global`\[Theta]b4sq,Global`\[Theta]3sq->Global`\[Theta]4sq}/.Global`x3sq^QQ_:>UsqToTheQ[QQ]/.{
						Global`x3sq -> Usq[],
						d[Global`\[Theta]4,Global`x3,Global`\[Theta]b4] -> \[CapitalTheta]U\[CapitalTheta]b[],
						d[\[Eta]_,Global`x3,Global`\[Theta]b4]/;\[Eta]Q[\[Eta]] :> \[Eta]U\[CapitalTheta]b[\[Eta]],
						d[Global`\[Theta]4,Global`x3,\[Eta]b_]/;\[Eta]bQ[\[Eta]b] :> \[CapitalTheta]U\[Eta]b[\[Eta]b],
						d[\[Eta]_,Global`x3,\[Eta]b_]^QQ_/;\[Eta]Q[\[Eta]]&&\[Eta]bQ[\[Eta]b] :> \[Eta]U\[Eta]bToTheQ[\[Eta],\[Eta]b][QQ],
						d[\[Eta]_,Global`x3,\[Eta]b_]/;\[Eta]Q[\[Eta]]&&\[Eta]bQ[\[Eta]b] :> \[Eta]U\[Eta]b[\[Eta],\[Eta]b],
						d[\[Eta]_,Global`\[Theta]4]:> - \[CapitalTheta]\[Eta][\[Eta]],
						d[\[Eta]b_,Global`\[Theta]b4]:> - \[CapitalTheta]b\[Eta]b[\[Eta]b],
						Global`\[Theta]4sq-> \[CapitalTheta]sq[],
						Global`\[Theta]b4sq-> \[CapitalTheta]bsq[]
						}/.\[Theta]scale//Expand
					)/.{
						uu^a_./;a>2->0,vv^a_./;a>2->0,ww^a_./;a>2->0,xx^a_./;a>2->0,yy^a_./;a>2->0,xx^a_./;a>2->0
					}/.{uu->1,vv->1,ww->1,xx->1,yy->1,zz->1}/.{
						d[a__]^P_:> Module[{replaced},
							replaced=fastReduction[Replace\[Eta]ToAny[
								Replace\[Eta]ToAny[
									Replace\[Eta]ToAny[
										Replace\[Eta]ToAny[
											d[a], Global`\[Eta]11, general\[Eta]Replacement[1]]//fastReduction,
										Global`\[Eta]b11, general\[Eta]bReplacement[1]]//fastReduction,
									Global`\[Eta]22, general\[Eta]Replacement[2]]//fastReduction,
								Global`\[Eta]b22, general\[Eta]bReplacement[2]]];
							If[IntegerQ[P]&&P>1,
								FPower[replaced,P],
								FPower[replaced,SetToZero[replaced,{Global`\[Theta]1,Global`\[Theta]b1,Global`\[Theta]2,Global`\[Theta]b2,Global`\[Theta]3,Global`\[Theta]b3}],P,orderexp]
							]
						]
					}
					,Global`\[Eta]11, general\[Eta]Replacement[1]]//fastReduction,
				Global`\[Eta]b11, general\[Eta]bReplacement[1]]//fastReduction,
			Global`\[Eta]22, general\[Eta]Replacement[2]]//fastReduction,
		Global`\[Eta]b22, general\[Eta]bReplacement[2]
	]//fastReduction]
];


(* ::Section::Closed:: *)
(*End of package*)


End[];

EndPackage[];
